import React from 'react'

const Container = ({ story }) => (
  <div
    role="main"
    style={{
      padding: '3em',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    }}>
    { story() }
  </div>
)

export default Container
