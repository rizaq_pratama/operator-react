//  @flow
import React from 'react'
import moment from 'moment'
import { DatePicker } from 'antd'
import { DATE_FORMAT } from '@app/constants/DateTime'
import FilterContainer from '../FilterContainer'
import type { FilterComponent } from '../Types'

const DateRangePicker = DatePicker.RangePicker

type FilterDateBoxProps = {
  id: string,
  label: string,
}

class FilterDateBox extends React.Component<FilterDateBoxProps> implements FilterComponent {
  constructor (props) {
    super(props)
    this.state = {
      startDate: moment(),
      endDate: moment(),
    }
  }
  dateChangeHandler (dates) {
    this.setState({
      startDate: dates[0],
      endDate: dates[1],
    })
  }
  clear() {
    this.setState({
      startDate: moment(),
      endDate: moment(),
    })
  }
  serialize(): string[] {
    const {startDate, endDate} = this.state
    return [startDate.startOf('day'), endDate.endOf('day')]
  }
  deserialize(values: string[]) {
    this.setState({
      startDate: values[0],
      endDate: values[1],
    })
  }
  render () {
    return (
      <FilterContainer label={this.props.label} id={this.props.id}>
        <DateRangePicker
          value={[this.state.startDate, this.state.endDate]}
          format={DATE_FORMAT}
          onChange={this.dateChangeHandler.bind(this)} />
      </FilterContainer>
    )
  }
}

export default FilterDateBox
