import claimApi from '@app/services/api/claimApi'
import { takeEvery, put } from 'redux-saga/effects'
import { Types, Creators } from './redux'
import _ from 'lodash'


export function* getClaims(action) {
  try {
    const claims = yield claimApi.getClaims(action.filter)
    const claimsData = _.map(claims.data, (c) => {
      return {
        ...c,
        claimId: c.uuid.substring(0, 8),
      }
    })
    yield put(Creators.getClaimsSuccess(claimsData))
  } catch (err) {
    console.log(err)
    yield put(Creators.getClaimsError(err))
  }
}

export function* updateClaim(action) {
  try {
    const created = yield claimApi.updateClaim(action.id, action.data)
    const data = {
      ...created.data,
      claimId: (created.data.uuid.substring(0, 8)),
    }
    yield put(Creators.putClaimSuccess(data))
    if (action.callback) {
      action.callback()
    }
  } catch (err) {
    console.log(err)
    yield put(Creators.putClaimError(err))
  }
}


export default function* defaultSaga() {
  yield takeEvery(Types.GET_CLAIMS_REQUEST, getClaims)
  yield takeEvery(Types.PUT_CLAIM_REQUEST, updateClaim)
}
