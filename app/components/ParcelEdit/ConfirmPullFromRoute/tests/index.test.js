import React from 'react'
import { Table } from 'antd'
import { mount, shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { ConfirmPullFromRoute } from '../index'
import { TestIntlProviderWrapper } from '@internals/testing/test-helpers'
import Route from '@app/services/api/routeApi'

jest.mock('@app/services/api/routeApi', () => {
  return {
    pullJobFromRoute: jest.fn(() => ({ok: true})),
  }
})

jest.mock('@app/services/api/routeApi')

describe('ConfirmPullFromRoute', () => {
  let jobs
  beforeEach(() => {
    jobs = [
      {routeId: 111, jobType: 'parcel-delivery', id: 1},
      {routeId: 222, jobType: 'parcel-pickup', id: 2},
    ]
  })

  it('renders a ConfirmPullFromRoute component', () => {
    const wrapper = shallow(
      <ConfirmPullFromRoute visible jobs={jobs}/>
    )
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  describe('full render behaviors', () => {
    let wrapper, submitButton
    beforeEach(() => {
      wrapper = mount(
        <TestIntlProviderWrapper>
          <ConfirmPullFromRoute visible jobs={jobs} onClose={jest.fn()}/>
        </TestIntlProviderWrapper>
      )
      submitButton = wrapper.find('.ant-modal-footer [type="primary"]')
    })

    afterEach(() => wrapper.unmount())

    it('renders jobs', () => {
      expect(wrapper).toBeDefined()
      expect(wrapper.find(Table).prop('dataSource')).toEqual(jobs)
    })

    it('do not submit with empty selection', ()=> {
      submitButton.simulate('click')
      expect(Route.pullJobFromRoute).not.toHaveBeenCalled()
    })

    it('submit with selected job', async () => {
      const cbx = wrapper.find('input[type="checkbox"]')

      expect(cbx).toHaveLength(3)
      cbx.first().simulate('change', {target: {checked: true}})
      submitButton.simulate('click')
      expect(Route.pullJobFromRoute.mock.calls).toHaveLength(2)
      await new Promise(setTimeout)
    })
  })
})
