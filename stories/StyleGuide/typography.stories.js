// @flow
import React from 'react'
import { StyleGuideLayout } from './layout'
import { storiesOf } from '@storybook/react'
import styled from 'styled-components'
import { Button } from 'antd'
import _ from 'lodash'

const styleReg = /^\.(.*){(.*)}/
const StyledTable = styled.table`
& td {
padding: 20px;

& pre {
  font-size: 80%;
}
`

const headersTable = [
  [
    '',
    'Dark',
    'Light',
    'Link',
  ],
  [
    'Header1',
    `.header1-dark {
  font-family: Roboto;
  font-size: 20px;
  font-weight: 500;
  font-style: normal;
  line-height: 28px;
  letter-spacing: 0.2px;
  color: #212125;
}
`,
    `.header1-light {
  font-family: Roboto;
  font-size: 20px;
  font-weight: 500;
  font-style: normal;
  line-height: 28px;
  letter-spacing: 0.2px;
  color: #8A9096;
  }`,
    `.header1-link {
  font-family: Roboto;
  font-size: 20px;
  font-weight: 500;
  font-style: normal;
  line-height: 28px;
  letter-spacing: 0.2px;
  color: #4C6EF5;
  }`,
  ],
  [
    'Header2',
    `.header2-dark {
  font-family: Roboto;
  font-size: 18px;
  font-weight: 500;
  font-style: normal;
  line-height: 28px;
  letter-spacing: 0.2px;
  color: #212125;
  }`,
    `.header2-light {
    font-family: Roboto;
  font-size: 18px;
  font-weight: 500;
  font-style: normal;
  line-height: 28px;
  letter-spacing: 0.2px;
  color: #8A9096;
  }`,
    `.header2-link {
  font-family: Roboto;
  font-size: 18px;
  font-weight: 500;
  font-style: normal;
  line-height: 28px;
  letter-spacing: 0.2px;
  color: #4C6EF5;
  }`,
  ],
  [
    'Header3',
    `.header3-dark {
  font-family: Roboto;
  font-size: 16px;
  font-weight: 500;
  font-style: normal;
  line-height: 21px;
  letter-spacing: 0.3px;
  color: #212125;
  }`,
    `.header3-light {
  font-family: Roboto;
  font-size: 16px;
  font-weight: 500;
  font-style: normal;
  line-height: 21px;
  letter-spacing: 0.3px;
  color: #8A9096;
  }`,
    `.header3-link {
  font-family: Roboto;
  font-size: 16px;
  font-weight: 500;
  font-style: normal;
  line-height: 21px;
  letter-spacing: 0.3px;
  color: #4C6EF5;
  }`,
  ],
  [
    'Header4',
    `.header4-dark {
  font-family: Roboto;
  font-size: 14px;
  font-weight: 500;
  font-style: normal;
  line-height: 21px;
  letter-spacing: 0.3px;
  color: #212125;
  }`,
    `.header4-light {
  font-family: Roboto;
  font-size: 14px;
  font-weight: 500;
  font-style: normal;
  line-height: 21px;
  letter-spacing: 0.3px;
  color: #8A9096;
  }`,
    `.header4-link {
  font-family: Roboto;
  font-size: 14px;
  font-weight: 500;
  font-style: normal;
  line-height: 21px;
  letter-spacing: 0.3px;
  color: #4C6EF5;
  }`,
  ],
  [
    'Header5',
    `.header5-dark {
  font-family: Roboto;
  font-size: 12px;
  font-weight: 500;
  font-style: normal;
  line-height: 14px;
  letter-spacing: 0.3px;
  color: #212125;
  }`,
    `.header5-light {
  font-family: Roboto;
  font-size: 12px;
  font-weight: 500;
  font-style: normal;
  line-height: 14px;
  letter-spacing: 0.3px;
  color: #8A9096;
  }`,
    `.header5-link {
  font-family: Roboto;
  font-size: 12px;
  font-weight: 500;
  font-style: normal;
  line-height: 14px;
  letter-spacing: 0.3px;
  color: #4C6EF5;
  }`,
  ],
]

const bodiesTable = [
  [
    '', 'Dark', 'Light', 'Link', 'Disabled',
  ],
  [
    'Body1',
    `.body1-dark {
  font-family: Roboto;
  font-weight: 400;
  font-style: normal;
  font-size: 16px;
  line-height: 21px;
  color: #212125;
  }`,
    `.body1-light {
  font-family: Roboto;
  font-weight: 400;
  font-style: normal;
  font-size: 16px;
  line-height: 21px;  
  color: #8A9096;
  }`,
    `.body1-link {
  font-family: Roboto;
  font-weight: 400;
  font-style: normal;
  font-size: 16px;
  line-height: 21px;
  color: #4C6EF5;
  }`,
    `.body1-disabled {
  font-family: Roboto;
  font-weight: 400;
  font-style: normal;
  font-size: 16px;
  line-height: 21px;
  color: #B5B9BD;
  }`,
  ],
  [
    'Body2',
    `.body2-dark {
  font-family: Roboto;
  font-weight: 400;
  font-style: normal;
  font-size: 14px;
  line-height: 21px;
  color: #212125;
  }`,
    `.body2-light {
  font-family: Roboto;
  font-weight: 400;
  font-style: normal;
  font-size: 14px;
  line-height: 21px;
  color: #8A9096;
  }`,
    `.body2-link {
  font-family: Roboto;
  font-weight: 400;
  font-style: normal;
  font-size: 14px;
  line-height: 21px;
  color: #4C6EF5;
  }`,
    `.body2-disabled {
  font-family: Roboto;
  font-weight: 400;
  font-style: normal;
  font-size: 14px;
  line-height: 21px;
  color: #B5B9BD;
  }`,
  ],
  [
    'Body3',
    `.body2-dark {
  font-family: Roboto;
  font-weight: 400;
  font-style: normal;
  font-size: 12px;
  line-height: 18px;
  color: #212125;
  }`,
    `.body3-light {
  font-family: Roboto;
  font-weight: 400;
  font-style: normal;
  font-size: 12px;
  line-height: 18px;
  color: #8A9096;
  }`,
    `.body3-link {
  font-family: Roboto;
  font-weight: 400;
  font-style: normal;
  font-size: 12px;
  line-height: 18px;
  color: #4C6EF5;
  }`,
    `.body3-disabled {
  font-family: Roboto;
  font-weight: 400;
  font-style: normal;
  font-size: 12px;
  line-height: 18px;
  color: #B5B9BD;
  }`,
  ],
]

const buttonsTable = [
  [
    '', 'Button', 'Button', 'Button', 'Button',
  ],
  [
    <Button type='primary' size='large' key='big'>Button</Button>,
    `.buttonLarge-primary {
  font-family: Roboto;
  font-weight: 400;
  font-style: normal;
  font-size: 16px;
  line-height: 40px;
  letter-spacing: 0.35px;
  color: #FFFFFF;
  }`,
    `.buttonLarge-secondary {
  font-family: Roboto;
  font-weight: 400;
  font-style: normal;
  font-size: 16px;
  line-height: 40px;
  letter-spacing: 0.35px;
  color: #212125;
  }`,
    `.buttonLarge-disabled {
  font-family: Roboto;
  font-weight: 400;
  font-style: normal;
  font-size: 16px;
  line-height: 40px;
  letter-spacing: 0.35px;
  color: #B5B9BD;
  }`,
    `.buttonLarge-warn {
  font-family: Roboto;
  font-weight: 400;
  font-style: normal;
  font-size: 16px;
  line-height: 40px;
  letter-spacing: 0.35px;
  color: #F03E3E;
  }`,
  ],
  [
    <Button type='primary' size='default' key='medium'>Button</Button>,
    `.buttonMedium-primary {
  font-family: Roboto;
  font-weight: 400;
  font-style: normal;
  font-size: 14px;
  line-height: 32px;
  letter-spacing: 0.3px;
  color: #FFFFFF;
  }`,
    `.buttonMedium-secondary {
  font-family: Roboto;
  font-weight: 400;
  font-style: normal;
  font-size: 14px;
  line-height: 32px;
  letter-spacing: 0.3px;
  color: #212125;
  }`,
    `.buttonMedium-disabled {
  font-family: Roboto;
  font-weight: 400;
  font-style: normal;
  font-size: 14px;
  line-height: 32px;
  letter-spacing: 0.3px;
  color: #B5B9BD;
  }`,
    `.buttonMedium-warn {
  font-family: Roboto;
  font-weight: 400;
  font-style: normal;
  font-size: 14px;
  line-height: 32px;
  letter-spacing: 0.3px;
  color: #F03E3E;
  }`,
  ],
  [
    <Button type='primary' size='small' key='small'>Button</Button>,
    `.buttonSmall-primary {
  font-family: Roboto;
  font-weight: 400;
  font-style: normal;
  font-size: 14px;
  line-height: 24px;
  letter-spacing: 0.3px;
  color: #FFFFFF;
  }`,
    `.buttonSmall-secondary {
  font-family: Roboto;
  font-weight: 400;
  font-style: normal;
  font-size: 14px;
  line-height: 24px;
  letter-spacing: 0.3px;
  color: #212125;
  }`,
    `.buttonSmall-disabled {
  font-family: Roboto;
  font-weight: 400;
  font-style: normal;
  font-size: 14px;
  line-height: 24px;
  letter-spacing: 0.3px;
  color: #B5B9BD;
  }`,
    `.buttonSmall-warn {
  font-family: Roboto;
  font-weight: 400;
  font-style: normal;
  font-size: 14px;
  line-height: 24px;
  letter-spacing: 0.3px;
  color: #F03E3E;
  }`,
  ],
]


storiesOf('Style Guide', module)
  .add('Typography', () => {
  return <StyleGuideLayout title='NINJITSU DESIGN SYSTEM - TYPOGRAPHY (LIGHT)'>
    <StyledTable>
      <tbody>
      {buildTable(headersTable)}
      </tbody>
    </StyledTable>

    <StyledTable>
      <tbody>
      {buildTable(bodiesTable)}
      </tbody>
    </StyledTable>

    <StyledTable>
      <tbody>
      {buildTable(buttonsTable, Button)}
      </tbody>
    </StyledTable>
  </StyleGuideLayout>
})

function buildTable(table, TdWrapper) {
  let currentRow = null
  let currentRowIdx = -1

  return table.map(generateRow)

  function generateRow(row, j) {
    currentRow  = row
    currentRowIdx = j

    if (j === 0) {
      let headerStyle = null
      let buttonType = null
      const cols = []
      for (let i=0; i< row.length; i++) {
        if (i === 0) {
          cols.push(<td key={i}></td>)
          continue
        }

        const matched = table[1][i].replace(/\n/g, '').match(styleReg)
        if (matched) {
          headerStyle = matched[2]
          buttonType = _.last(matched[1].split('-')).trim()
        }
        const Wrapper = styled.td`${headerStyle}`
        let cell = row[i]
        if (TdWrapper) {
          if (buttonType === 'disabled') {
            cell = <TdWrapper disabled size='large'>{row[i]}</TdWrapper>
          } else if (buttonType === 'warn') {
            cell = <TdWrapper type='danger' size='large'>{row[i]}</TdWrapper>
          } else {
            cell = <TdWrapper type={buttonType} size='large'>{row[i]}</TdWrapper>
          }
        }
        cols.push(
          <Wrapper key={i}>
            {cell}
          </Wrapper>
        )
      }
      return <tr key={j}>
        {cols}
      </tr>
    } else {
      return <tr key={j}>
        {row.map(generateCell)}
      </tr>
    }
  }

  function generateCell(cell, i) {
    let headerStyle = null
    if (i===0) {
      if (currentRowIdx > 0) {
        const matched = currentRow[1].replace(/\n/g, '').match(styleReg)
        if (matched) {
          headerStyle = matched[2]
        }
      }
    }
    return <td key={i}>
      { typeof cell === 'string' && styleReg.test(cell.replace(/\n/g, '')) ? <pre>{cell}</pre> : <Center style={headerStyle}>{cell}</Center>}
    </td>
  }

  function Center(props: {style: Object, children: React.Children}) {
    const style = {}
    if (props.style) {
      Object.assign(style, props.style)
    }
    return <div style={style}>
      {props.children}
    </div>
  }
}
