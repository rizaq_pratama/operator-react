/* eslint-disable no-undef */

import React from 'react'
import { storiesOf } from '@storybook/react'
import RecoveryInformation from '@app/components/ParcelEdit/RecoveryInformation'

const tickets = [
  { 'id': 244604, 'description': 'Shipper Issue', 'status': 'Pending' },
  { 'id': 212543, 'description': 'Delivery Issue', 'status': 'Pending' },
]

storiesOf('ParcelEdit', module)
  .add('RecoveryInformation', () =>
    <RecoveryInformation
      tickets={tickets}
      daysSinceOrderCreated={0}
      daysSinceInbounded={0}
      onTicketLinkClicked={(ticketId) => alert('Handle Ticket Link Click for Ticket Id ' + ticketId)} />
  )
