import React from 'react'
import { storiesOf } from '@storybook/react'
import {Icon, Row, Col} from 'antd'
import styled from 'styled-components'
import { StyleGuideLayout } from './layout'

const SmallHeader = styled.div`
  font-family: Roboto;
  font-size: 16px;
  font-weight: 500;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #212125;
`
const SmallContent = styled.div`
  font-weight: 300;
`

const Span = styled.span`
font-family: Roboto;
font-size: 14px;
font-weight: 500;
font-style: normal;
font-stretch: normal;
line-height: normal;
letter-spacing: 0.8px;
color: #25282a;
`

const iconMapping = [
  ['ADD / CREATE', 'plus'],
  ['minus', 'minus'],
  ['close', 'close'],
  ['drop down', 'down'],
  ['reveal', 'eye', 'usually used for password fields'],
  ['edit', 'edit'],
  ['calendar', 'calendar'],
  ['download', 'download'],
  ['check', 'check', 'usually used to indicate success'],
  ['location', 'environment'],
  ['email / mail', 'mail'],
  ['phone', 'phone'],
  ['delete', 'delete'],
  ['copy', 'copy'],
  ['info', 'info-circle'],
  ['attention', 'exclamation-circle'],
  ['progress / loading', 'loading'],
  ['check circle', 'check-circle'],
  ['close circle', 'close-circle'],
]


storiesOf('Style Guide', module).add('Icons', () => {

  return <StyleGuideLayout title='NINJITSU DESIGN SYSTEM - ICONGRAPHY'>
    <div style={{marginBottom: '10px'}}>
      < SmallHeader>Icon Set</ SmallHeader>
      <SmallContent>Icons can be used to provide additional visual indication to specific functions / fields.</SmallContent>
    </div>

    <div style={{marginBottom: '10px'}}>
      <SmallHeader>Styling Icons</SmallHeader>
      <SmallContent>Icon color can be changed via symbol overrides, but should only be colors within the set color palette</SmallContent>
    </div>
    <div style={{marginBottom: '10px'}}>
      <SmallHeader>Importing New Icons</SmallHeader>
      <SmallContent>When new icons are imported, they should be in SVG format, matching the standardized 20px by 20px dimensions and have the export function turned on in Sketch for easy handover to enginners. They should also be masked similarly to existing icons to allow for easy color overriding. Addtional icons can be found in Ant Design (https://ant.design/components/icon/) in the Outlined style.</SmallContent>
    </div>


    <Row type='flex'>
      <Col span={24}>
        {iconMapping.map(([label, icon, desc], i) => (
          <Row type='flex' key={i} style={{marginTop: '10px'}}>
            <Col span={6}><Span>{label.toUpperCase()}</Span></Col>
            <Col span={4}><Icon type={icon}></Icon></Col>
            <Col span={10}>{desc ? desc : null}</Col>
          </Row>
        ))
        }
      </Col>
    </Row>
  </StyleGuideLayout>
})
