export const LIABLE_DEPARTMENT = [
  'FIRST_MILE',
  'LAST_MILE',
  'MIDDLE_MILE',
  'WAREHOUSE',
  'DRIVER',
]


export const CLAIM_STATUS = {
  PENDING: {
    name: 'PENDING',
    displayName: 'Pending',
  },
  IN_PROGRESS: {
    name: 'IN_PROGRESS',
    displayName: 'In Progress',
  },
  PAID: {
    name: 'PAID',
    displayName: 'Paid',
  },
}
