// @flow
import { CountryUtils } from '@nv/react-commons/src/Utils'
import { injectIntl, FormattedNumber } from 'react-intl'
import React from 'react'
import { Card, Form, Row, Col } from 'antd'
import { PARCEL_SIZE } from '@nv/react-commons/src/Constants'
import FormControl from '@app/components/Generic/FormControl'
import DataField from '@app/components/Generic/DataField'

type Props = {
  deliveryFee: number,
  codFee: number,
  insuranceFee: number,
  handlingFee: number,
  gst: number,
  total: string,
  insuredValue: number,
  parcelSize: string,
  weight: number,
  dimensions: {
    width: number,
    height: number,
    length: number,
  },
  form: Object,
  currency: string,
  onSaveChangesButtonClicked: Function,
  visible: boolean,
  country: string,
  onCancel: Function
}

function PricingForm (props: Props) {
  const total = [
    props.deliveryFee,
    props.codFee,
    props.insuranceFee,
    props.handlingFee,
    props.gst,
  ].reduce((sum, n) => sum + (n ? n : 0), 0)

  const {form} = props
  return (
    <>
      <Card className='parcel-dimensions'
            title={props.intl.formatMessage({id: 'commons.parcel-dimensions'})}>
        <Row type='flex'
             justify='space-between'
             align='left'>
          <FormControl
            span={5}
            fieldName={'commons.parcel-sizing'}
            type='dropdown'
            stateKey='parcelSize'
            form={form}
            menu={PARCEL_SIZE}
            options={{initialValue: props.parcelSize}}
          />

          <FormControl
            fieldName={'commons.length'}
            type='number'
            stateKey='length'
            postfix='cm'
            span={5}
            form={form}
            options={{initialValue: props.dimension && props.dimension.length}}
          />

          <FormControl
            fieldName='commons.width'
            type='number'
            form={form}
            postfix='cm'
            span={5}
            stateKey='width'
            options={{initialValue: props.dimension && props.dimension.width}}
          />

          <FormControl
            fieldName='commons.height'
            type='number'
            form={form}
            span={5}
            postfix='cm'
            stateKey='height'
            options={{initialValue: props.dimension && props.dimension.height}}
          />

          <FormControl
            fieldName='commons.weight'
            form={form}
            postfix='kg'
            span={4}
            type='number'
            stateKey='weight'
            options={{ initialValue: props.weight }}
          />
        </Row>
      </Card>

      <Card
        style={{marginTop: '10px'}}
        className='pricing-information'
        title={props.intl.formatMessage({id: 'commons.pricing-information'})}
      >
        <Row type='flex'
             justify='space-between'>
          <Col span={5}>
            <DataField label='commons.delivery-fee'>
              {props.deliveryFee || '-'}
            </DataField>
          </Col>

          <Col span={5}>
            <DataField label='commons.cod-fee'>
              {props.codFee || '-'}
            </DataField>
          </Col>

          <Col span={5}>
            <DataField label='commons.insurance'>
              {props.insuranceFee || '-'}
            </DataField>
          </Col>

          <Col span={5}>
            <DataField label='commons.handling'>
              {props.handlingFee || '-'}
            </DataField>
          </Col>

          <Col span={4}>
            <DataField label='commons.tax'>
              {props.gst || '-'}
            </DataField>
          </Col>

          <FormControl
            span={5}
            fieldName={'commons.total'}
            stateKey='total'
            form={form}
            value={printTotal() || '-'}
            type='custom'
          />

          <Col span={19}>
            <Row>
              <FormControl
                span={6}
                fieldName={'commons.insured-value'}
                stateKey='insuredValue'
                postfix='SGD'
                form={form}
                type='currency'
                options={{initialValue: props.insuredValue}}
              />
            </Row>
          </Col>
        </Row>
      </Card>
    </>
  )

  function printTotal () {
    const currency = CountryUtils.getCurrencyCode('SG')
    return (
      <FormattedNumber
        value={total}
        style='currency'
        currency={currency}
      />
    )
  }
}

const PricingDimensionForm = Form.create()(injectIntl(PricingForm))
export default PricingDimensionForm
