// needed for regenerator-runtime
// (ES7 generator support is required by redux-saga)
require('@babel/polyfill')
require('jest-styled-components')

if (!global.jasmine.testPath.includes('.e2e')) {
  const Enzyme = require('enzyme')
  const Adapter = require('enzyme-adapter-react-16')

  Enzyme.configure({ adapter: new Adapter() })

  if (window.URL) {
    window.URL.createObjectURL = jest.fn()
  }
}
