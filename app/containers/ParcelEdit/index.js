// @flow

import React from 'react'
import { Row, Col } from 'antd'
import Layout from 'antd/lib/layout'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { createStructuredSelector } from 'reselect'

import GeneralInfo from '@app/components/ParcelEdit/GeneralInfo'
import ItemSummary from '@app/components/ParcelEdit/ItemSummary'
import PricingAndDimensionCard from '@app/components/ParcelEdit/PricingAndDimensionCard'
import RecoveryInformation from '@app/components/ParcelEdit/RecoveryInformation'
import JobHistory from '@app/components/ParcelEdit/JobHistory'
import EventHistory from '@app/components/ParcelEdit/EventHistory'
import EditParcelDetailsModal from '@app/components/ParcelEdit/EditParcelDetails'
import JobDetailsModal from '@app/components/ParcelEdit/JobDetailsModal'
import PricingAndDimensionEdit from '@app/components/ParcelEdit/PricingAndDimensionEdit'
import TopBarActions from '@app/components/ParcelEdit/TopBarActions'
import PickupDeliveryJob from '@app/components/ParcelEdit/PickupDeliveryJob'
import EditDeliveryDetails from '@app/components/ParcelEdit/EditDeliveryDetails'
import EditPickupDetails from '@app/components/ParcelEdit/EditPickupDetails'
import EditCashCollectionDetails from '@app/components/ParcelEdit/EditCashCollectionDetails'
import InformationBar from '@app/components/ParcelEdit/InformationBar'
import ConfirmManuallyCompleteOrder from '@app/components/ParcelEdit/ConfirmManuallyCompleteParcel'
import ConfirmCancelOrder from '@app/components/ParcelEdit/ConfirmCancelParcel'
import ConfirmPullFromRoute from '@app/components/ParcelEdit/ConfirmPullFromRoute'
import { ConfirmAddDeliveryJobToRoute, ConfirmAddPickupJobToRoute } from '@app/components/ParcelEdit/ConfirmAddToRoute'
import ConfirmDPDropOffSettings from '@app/components/ParcelEdit/ConfirmDPDropOffSettings'
import RTSModal from '@app/components/ParcelEdit/ReturnToSenderModal'
import ResumeParcelModal from '@app/components/ParcelEdit/ConfirmResumeParcel'
import ViewAllProofOfDeliveries from '@app/components/ParcelEdit/ViewAllProofOfDeliveries'
import { Creators as GlobalCreators } from '@app/containers/Base/redux'
import globalSelectors from '@app/containers/Base/selectors'
import injectReducer from '@app/utils/injectReducer'
import injectSaga from '@app/utils/injectSaga'

import reducer, { Creators } from './redux'
import saga from './saga'
import type { Props, State } from './Types'
import selectors from './selectors'
import './style.less'
import { MODAL_TYPES } from './constants'

const { Header, Content } = Layout

export class ParcelEdit extends React.Component<Props, State> {
  static defaultProps = {
    parcelsByOrder: [],
    parcelDetails: {},
  }

  componentDidMount = () => {
    const { params: { parcelId } } = this.props.match
    this.props.loadParcelEditView(parcelId)
  }

  onCurrentJobClicked = (currentJob: any) => {
    // TODO: Create Action when the current job is clicked
  }

  onOrderTrackingIdClicked = (orderTrackingId: string) => {
    // TODO: Create Action when the Order Tracking ID is clicked
  }

  onTicketLinkClicked = (ticketId: number) => {
    // TODO: Create Action for Clicking Ticket Link
  }

  onJobIdClicked = (jobId: number) => {
    this.props.openModal(MODAL_TYPES.JOB_DETAIL)
  }

  onRouteIdClicked = (routeId: number) => {
    // TODO: Create Action for Clicking Route ID
  }

  onRouteIdLinkClicked = (routeId: number) => {
    // TODO: Create Action for Clicking Event History RouteID
  }

  onModalClicked = (menu) => {
    const { key } = menu
    if (key) {
      this.props.openModal(key)
    }
  }

  render () {
    const { modalId, closeModal } = this.props
    const modals = [
      <EditParcelDetailsModal key={MODAL_TYPES.PARCEL_EDIT}/>,
      <JobDetailsModal key={MODAL_TYPES.JOB_DETAIL}/>,
      <PricingAndDimensionEdit key={MODAL_TYPES.PRICING_DIMENSIONS}/>,
      <EditDeliveryDetails key={MODAL_TYPES.DELIVERY_DETAILS}/>,
      <EditPickupDetails key={MODAL_TYPES.PICKUP_DETAILS}/>,
      <EditCashCollectionDetails key={MODAL_TYPES.CASH_COLLECTION_DETAILS}/>,
      <ConfirmManuallyCompleteOrder key={MODAL_TYPES.MANUALLY_COMPLETE_PARCEL}/>,
      <ConfirmCancelOrder key={MODAL_TYPES.CANCEL_PARCEL}/>,
      <ConfirmPullFromRoute key={MODAL_TYPES.PULL_FROM_ROUTE}/>,
      <ConfirmAddDeliveryJobToRoute key={MODAL_TYPES.ADD_DELIVERY_JOB_TO_ROUTE}/>,
      <ConfirmAddPickupJobToRoute key={MODAL_TYPES.ADD_PICKUP_JOB_TO_ROUTE}/>,
      <ConfirmDPDropOffSettings key={MODAL_TYPES.DP_DROP_OFF_SETTINGS}/>,
      <ViewAllProofOfDeliveries key={MODAL_TYPES.ALL_PROOF_OF_DELIVERIES}/>,
      <ResumeParcelModal key={MODAL_TYPES.RESUME_PARCEL}/>,
      <RTSModal key={MODAL_TYPES.RETURN_TO_SENDER}/>,
    ].map(node => {
      return React.cloneElement(node, Object.assign({}, node.props, {
        visible: node.key === modalId,
        onClose: () => closeModal(),
      }))
    })
    return (
      <Layout className='parcel-edit-main'>
        <Header style={{ position: 'fixed', zIndex: 1, width: '100%', height: 'auto', background: 'white', padding: 0, margin: 0 }}>
          <div className="inner">
            <Row className='header' justify='center' type='flex'>
              <Col className='column' xs={24} sm={20} md={20}>
                <InformationBar {...this.props.informationBar}/>
              </Col>
            </Row>
          </div>
          <hr/>
          <div className="inner">
            <Row className='header' justify='center' type='flex'>
              <Col className='column' xs={24} sm={24} md={24}>
                <TopBarActions onClick={this.onModalClicked}/>
              </Col>
            </Row>
          </div>
        </Header>
        <Content>
          <div className="inner">
            <Row gutter={16} justify='center' type='flex'>
              <Col className='column' xs={24} sm={24} md={20}>
                <GeneralInfo
                  {...this.props.generalInfoView}
                  onCurrentJobClicked={this.onCurrentJobClicked}
                  onOrderTrackingIdClicked={this.onOrderTrackingIdClicked} />
              </Col>
            </Row>
            <Row gutter={16} justify='center' type='flex'>
              <Col className='column' xs={24} sm={24} md={10}>
                <ItemSummary
                  {...this.props.itemSummaryView}
                  onEditButtonClicked={() => this.props.openModal(MODAL_TYPES.PARCEL_EDIT)}
                />
              </Col>
              <Col className='column' xs={24} sm={24} md={10}>
                <PricingAndDimensionCard
                  {...this.props.pricingAndDimensionView}
                  onEditPricingAndDimensions={() => this.props.openModal(MODAL_TYPES.PRICING_DIMENSIONS)} />
              </Col>
            </Row>
            <Row gutter={16} justify='center' type='flex'>
              <Col className='column' xs={24} sm={24} md={20}>
                <RecoveryInformation
                  {...this.props.recoveryInfoView}
                  onTicketLinkClicked={this.onTicketLinkClicked} />
              </Col>
            </Row>
            <Row gutter={16} justify='center' type='flex'>
              <Col className='column' xs={24} sm={24} md={20}>
                <PickupDeliveryJob
                  {...this.props.pickupDeliveryJobView}
                  onPickupDetailsClicked={() => this.props.openModal(MODAL_TYPES.PICKUP_DETAILS)}
                  onDeliveryDetailsClicked={() => this.props.openModal(MODAL_TYPES.DELIVERY_DETAILS)} />
              </Col>
            </Row>
            <Row gutter={16} justify='center' type='flex'>
              <Col className='column' xs={24} sm={24} md={20}>
                <JobHistory
                  {...this.props.jobHistoryView}
                  onJobIdClicked={this.onJobIdClicked}
                  onRouteIdClicked={this.onRouteIdClicked}
                />
              </Col>
            </Row>
            <Row gutter={16} justify='center' type='flex'>
              <Col className='column' xs={24} sm={24} md={20}>
                <EventHistory
                  {...this.props.eventHistoryView}
                  onRouteIdLinkClicked={this.onRouteIdLinkClicked}
                />
              </Col>
            </Row>
          </div>
        </Content>
        { modals }
      </Layout>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  informationBar: selectors.InformationBarSelector,
  generalInfoView: selectors.GeneralInfoViewSelector,
  itemSummaryView: selectors.ItemSummaryViewSelector,
  pricingAndDimensionView: selectors.PricingAndDimensionViewSelector,
  recoveryInfoView: selectors.RecoveryInfoViewSelector,
  pickupDeliveryJobView: selectors.PickupDeliveryJobViewSelector,
  jobHistoryView: selectors.JobHistoryViewSelector,
  eventHistoryView: selectors.EventHistoryViewSelector,
  modalId: globalSelectors.ModalIdSelector,
})

const mapDispatchToProps = dispatch => {
  return {
    loadParcelEditView: (parcelId) => dispatch(Creators.loadParcelEditView(parcelId)),
    openModal: (id) => dispatch(GlobalCreators.openModal(id)),
    closeModal: () => dispatch(GlobalCreators.closeModal()),
  }
}

const withConnect = connect(mapStateToProps, mapDispatchToProps)
const withReducer = injectReducer({ key: 'parcelEdit', reducer })
const withSaga = injectSaga({ key: 'parcelEdit', saga })

export default compose(
  withConnect,
  withReducer,
  withSaga
)(ParcelEdit)
