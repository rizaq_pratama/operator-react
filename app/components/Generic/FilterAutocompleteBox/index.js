//  @flow
import React from 'react'
import { Select, Tag, Icon } from 'antd'
import FilterContainer from '../FilterContainer'
import { FILTER_TAG_COLORS } from '@app/constants/Colors'
import _ from 'lodash'
import type { FilterComponent } from '../Types'
import { injectIntl, intlShape } from 'react-intl'

type FilterAutocompleteBoxProps = {
  id: string,
  label: string,
  options: () => Promise<string[]> | string[],
  intl: intlShape
}

class FilterAutocompleteBox extends React.Component<FilterAutocompleteBoxProps> implements FilterComponent {
  constructor (props) {
    super(props)
    this.state = {
      options: [],
      selected: new Set(),
    }
    if (typeof props.options !== 'function') {
      this.state.options = props.options
    }
    this.searchRef = React.createRef()
  }
  searchOptions(value) {
    const { options } = this.props
    if (typeof options === 'function') {
      return options(value).then(response => {
        this.setState({ options: response.filter(v => !this.state.selected.has(v)) })
      })
    } else {
      this.setState({ options: options.filter(v => !this.state.selected.has(v)) })
    }
  }
  clear() {
    this.setState({
      selected: new Set(),
    })
    if (typeof options !== 'function') {
      this.setState({ options: this.props.options })
    }
  }
  deserialize(values): Set<String>{
    if (typeof values !== 'string') {
      return new Set()
    }
    return new Set(values.split(','))
  }
  serialize(): string {
    const values = this.state.selected
    return [...values].map(v => v.trim()).join(',')
  }
  getSelectedOptions() {
    return _.map([...this.state.selected], (option, i) =>
      <Tag closable color={FILTER_TAG_COLORS[i]} key={i}
        onClose={this.updateValues.bind(this, option, false)}>
        {option}
      </Tag>
    )
  }
  updateValues(value, toAdd=true) {
    let newOptions
    if (toAdd) {
      this.state.selected.add(value)
      newOptions = this.state.options.filter(v => v !== value)
    } else {
      this.state.selected.delete(value)
      this.state.options.push(value)
      newOptions = this.state.options
    }
    this.setState({
      selected: this.state.selected,
      options: newOptions,
    })
  }
  render () {
    const { id } = this.props
    const options = _.map(this.state.options, (option, i) => {
        if (this.state.selected.has(option)) {
          return null
        }
        return <Select.Option key={i} value={option}>{option}</Select.Option>
      }
    )
    return (
      <FilterContainer
        label={this.props.label}
        items={this.getSelectedOptions()}
        id={id}>
        <div style={{display: 'flex', alignItems: 'center'}}>
          <Icon style={{flex: 'none'}} type='search'></Icon>
          <Select
            style={{flex: '1 1', marginLeft: '5px'}}
            showSearch
            value={undefined}
            ref={this.searchRef}
            placeholder={this.props.intl.formatMessage({id: 'container.filter-box.search-select'})}
            onSearch={this.searchOptions.bind(this)}
            onChange={(value) => this.updateValues(value)}>
            {options}
          </Select>
        </div>
      </FilterContainer>
    )
  }
}

export default injectIntl(FilterAutocompleteBox, {withRef: true})
