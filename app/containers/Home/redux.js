import { createActions, createReducer } from 'reduxsauce'
import { fromJS } from 'immutable'

export const { Types, Creators } = createActions(
  {
    setDriver: ['driver'],
  },
  { prefix: 'app/Home/' }
)

export const INITIAL_STATE = fromJS({ driver: null })

export default createReducer(INITIAL_STATE, {
  [Types.SET_DRIVER]: (state, action) => state.set('driver', action.data),
})
