//  @flow
import React from 'react'
import { Switch } from 'antd'
import FilterContainer from '../FilterContainer'
import type { FilterComponent } from '../Types'

type FilterBooleanBoxProps = {
  label: string,
  id: string,
  intl: intlShape, property: string,
  onDelete: Function
}

class FilterBooleanBox extends React.Component<FilterBooleanBoxProps> implements FilterComponent {
  constructor(props) {
    super(props)
    this.state = {
      checked: false,
    }
  }
  switchChangeHandler(checked) {
    this.setState({checked})
  }
  deserialize(checked) {
    this.setState({checked})
  }
  clear() {
    this.setState({checked: false})
  }
  serialize() {
    return this.state.checked
  }
  render () {
    return (
      <FilterContainer label={this.props.label} id={this.props.id}>
        <div>
          <Switch
            checked={this.state.checked}
            onChange={this.switchChangeHandler.bind(this)}
            checkedChildren={'Yes'}
            unCheckedChildren={'No'}
          />
        </div>
      </FilterContainer>
    )
  }
}

export default FilterBooleanBox
