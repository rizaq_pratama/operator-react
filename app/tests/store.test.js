/**
 * Test store addons
 */

// FIXME
import { browserHistory } from 'react-router'
import configureStore from '../configureStore'
// import RehydrationService from '../services/rehydrationService'

jest.mock('../services/rehydrationService', () => ({
  updateReducers: jest.fn(),
}))

describe('configureStore', () => {
  let store

  beforeAll(() => {
    store = configureStore({}, browserHistory)
  })

  describe('injectedReducers', () => {
    it('should contain an object for reducers', () => {
      expect(typeof store.injectedReducers).toBe('object')
    })
  })

  describe('injectedSagas', () => {
    it('should contain an object for sagas', () => {
      expect(typeof store.injectedSagas).toBe('object')
    })
  })

  describe('runSaga', () => {
    it('should contain a hook for `sagaMiddleware.run`', () => {
      expect(typeof store.runSaga).toBe('function')
    })
  })
})
//
// describe('configureStore params', () => {
//   it('should call rehydrationService.updateReducers', () => {
//     configureStore(undefined, browserHistory)
//     expect(RehydrationService.updateReducers).toHaveBeenCalled()
//   })
// })
