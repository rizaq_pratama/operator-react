import React from 'react'
import { StyleGuideLayout } from './layout'
import { storiesOf } from '@storybook/react'
import styled from 'styled-components'
import { Row, Col } from 'antd'
import functionImgPath from './assets/function.png'
import ambiguityImgPath from './assets/ambiguity.png'
import consistencyImgPath from './assets/consistency.png'
import responsiveImgPath from './assets/responsive.png'

const StyledTitle = styled.span`
  width: 201px;
  height: 19px;
  font-family: Roboto;
  font-size: 16px;
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: 0.5px;
  color: #25282a;
`

storiesOf('Style Guide', module).add('Guideline', () => {
  return <StyleGuideLayout>
    <Row type='flex' align='middle' style={{margin: '20px 0'}}>
      <Col span={4}>
        <img src={functionImgPath} alt="" width='50px'/>
      </Col>
      <Col span={20} >
        <div>
          <StyledTitle>
          1. Function over Form
          </StyledTitle>
        </div>
        <p style={{marginTop: '20px'}}>
          Aesthetic elements should be used only to support functions and not as a distraction from the task. Keep things purposeful — all elements on any given page should contribute to the functionality of that page.
        </p>
      </Col>
    </Row>

    <Row type='flex' align='middle' style={{margin: '20px 0'}}>
      <Col span={4}>
        <img src={ambiguityImgPath} alt="" width='50px'/>
      </Col>
      <Col span={20} >
        <div>
          <StyledTitle>
            2. Eliminate Ambiguity
          </StyledTitle>
        </div>
        <p style={{marginTop: '20px'}}>
          Language and terminology used should be clear and concise to ensure comprehension regardless of users’ language abilities. Conventional and comprehensible icons can be used in conjunction with labels to indicate functionality of elements. When feasible, reduce number of actions possible so as to prevent distraction or confusion.
        </p>
      </Col>
    </Row>

    <Row type='flex' align='middle' style={{margin: '20px 0'}}>
      <Col span={4}>
        <img src={responsiveImgPath} alt="" width='50px'/>
      </Col>
      <Col span={20} >
        <div>
          <StyledTitle>
            3. Responsiveness is Key
          </StyledTitle>
        </div>
        <p style={{marginTop: '20px'}}>
          Speed of loading and navigation is key in Operator to allow users to maximize their operational efficiency. Unnecessary animation that slows down actions should be avoided as much as possible. In addition, feedback should always be provided to indicate response to user actions.
        </p>
      </Col>
    </Row>

    <Row type='flex' align='middle' style={{margin: '20px 0'}}>
      <Col span={4}>
        <img src={consistencyImgPath} alt="" width='50px'/>
      </Col>
      <Col span={20} >
        <div>
          <StyledTitle>
            4. Be Consistent
          </StyledTitle>
        </div>
        <p style={{marginTop: '20px'}}>
          Interactions and layouts should be consistent across pages to create a sense of intuitiveness. When designing new pages, always take into account existing patterns. If an improvement needs to be made, document it in the DS library and update existing pages to reflect the new patterns when possible.
        </p>
      </Col>
    </Row>


  </StyleGuideLayout>
})

