// @flow
export interface FilterComponent {
  serialize(): string;
  deserialize(): any;
  clear(): void;
}
