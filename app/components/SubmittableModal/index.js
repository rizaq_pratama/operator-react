// @flow
import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { Creators } from '@app/containers/Base/redux'
import { Modal, Button, Icon, Form } from 'antd'
import { Colors } from '@app/themes'
import { T } from '@app/components/Generic/T'
import omit from 'lodash/omit'
import './style.less'
import { FormContext } from '@app/components/Generic/Contexts/Form'

type SubmittableModalProps = {
  visible: boolean,
  disabled: boolean,
  onSubmit: Function,
  onClose?: Function,
  children: React.Node,
  form?: Form,
  title?: string,
  okText?: string,
  okType?: string,
  cancelText?: string,
  isConfirmButton?: boolean,
}

type SubmittableModalStates = {
  isFormDisabled: boolean
}

const buttonStyling = {
  fontWeight: 300,
  width: '30%',
  display: 'block',
  margin: '0 auto',
  backgroundColor: Colors.nvPriBlue,
  borderColor: Colors.nvPriBlue,
}

export const FormDisableContext = React.createContext(false)

/**
 * SubmittableModal
 */
export class SubmittableModal extends React.Component<SubmittableModalProps, SubmittableModalStates> {
  constructor (props) {
    super(props)
    this.state = {
      visible: props.visible,
      isFormDisabled: false,
    }
  }

  componentDidUpdate (prevProps, prevState) {
    if (prevProps.visible !== this.props.visible) {
      this.setState({ visible: this.props.visible })
    }
    if (prevProps.isFormDisabled !== this.props.isFormDisabled) {
      this.setState({ isFormDisabled: this.props.isFormDisabled })
    }
  }

  getForm () {
    return this.props.form
  }

  onSubmit = () => {
    const form = this.getForm()
    form.validateFields((err, values) => {
      if (!err) {
        this.props.onSubmit(values)
        this.props.formSubmitted()
      }
    })
  }

  hasError() {
    const form = this.getForm()
    const fieldsError = form.getFieldsError()
    return Object.keys(fieldsError).some(field => fieldsError[field]);
  }
  render () {
    let formChild = React.Children.only(this.props.children)

    const submitDisabled = this.props.disabled || this.hasError()
    const okButtonStyle = {
      width: '150px',
    }
    const modalProps = this.props.isConfirmButton
      ? {
        width: this.props.width || 520,
        className: 'modal-style-confirm',
        cancelButtonProps: { disabled: this.disabled },
        cancelText: <T id={this.props.cancelText} />,
        onOk: this.onSubmit,
        okType: this.props.okType || 'primary',
        okButtonProps: {
          disabled: submitDisabled,
          style: okButtonStyle,
        },
        okText: submitDisabled ? <Icon type='loading' /> : <T id={this.props.okText} />,
        visible: this.props.visible,
        ...omit(this.props, ['visible', 'onClose', 'onSubmit', 'title', 'okText', 'cancelText', 'width']),
      }: {
        className: 'modal-style-submit',
        visible: this.props.visible,
        footer: [
          <Button disabled={this.props.disabled} style={buttonStyling} key='submit' type='primary' size='large' onClick={this.onSubmit}>
            {this.props.disabled
              ? <Icon type='loading' />
              : <span style={{ width: '100%' }}><Icon type='check' style={{ float: 'left', marginTop: '3px' }} />Save Changes</span>
            }
          </Button>,
        ],
      }

    return (
      <Modal
        hoverable={false}
        onCancel={this.props.onClose}
        closable={!this.props.disabled}
        maskClosable={!this.props.disabled}
        title={<T id={this.props.title} />}
        {...modalProps} >
        <FormDisableContext.Provider value={this.props.disabled}>
          <FormContext.Provider value={this.props.form}>
            {formChild}
          </FormContext.Provider>
        </FormDisableContext.Provider>
      </Modal>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    formSubmitted: () => dispatch(Creators.modalFormSubmitted()),
  }
}

export default compose(
  connect(null, mapDispatchToProps),
  Form.create()
)(SubmittableModal)
