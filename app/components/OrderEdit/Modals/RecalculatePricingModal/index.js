//  @flow
import React, { useState } from 'react'
import { T } from '@app/components/Generic/T'
import { notification, Form } from 'antd'
import { injectIntl, intlShape } from 'react-intl'
import SubmitModal from '@app/components/SubmittableModal'
import NC from '@app/services/api/ninjaControlApi'

type ModalProps = {
  visible: boolean,
  orderId: number,
  onClose: Function,
  intl: intlShape
}

export function RecalculatePricing (props: ModalProps) {

  const [isLoading, setLoading] = useState(false)

  return (
    <SubmitModal
      visible={props.visible}
      title='container.order.edit.recalculate-order-pricing'
      cancelText='commons.go-back'
      okText='container.order.edit.recalculate-pricing'
      onSubmit={onSubmit}
      onClose={props.onClose}
      isConfirmButton={true}
      disabled={isLoading}>
      <Form>
        <T id={'container.order.edit.recalculate-order-text'} />
      </Form>
    </SubmitModal>
  )

  async function onSubmit () {
    setLoading(true)
    try {
      await NC.recalculateOrderPriceById(props.orderId)
      props.onClose()
      notification.success({
        message: props.intl.formatMessage({ id: 'commons.update-success' }),
      })
    } catch (err) {
      //  handle error
    }
    setLoading(false)
  }
}

export default injectIntl(RecalculatePricing)
