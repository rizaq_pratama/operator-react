import { select, spawn, call, put, takeEvery } from 'redux-saga/effects'
import { stringifyEqual } from '@internals/testing/test-helpers'
import NC from '@app/services/api/ninjaControlApi'
import Address from '@app/services/api/addressApi'
import Route from '@app/services/api/routeApi'
import Events from '@app/services/api/eventsApi'
import Ticketing from '@app/services/api/ticketingApi'
import selectors from '../selectors'

import saga, {
  loadParcelEdit,
  loadEvents,
  loadTickets,
  loadJobsAndRelated,
  loadParcelAndRelated,
  loadDestinationZone,
  loadJobRoute,
  loadProofs,
} from '../saga'
import { Types, Creators } from '../redux'

describe('Parcel edit sagas', () => {

  it('load events', () => {
    const g = loadEvents(123)
    stringifyEqual(g.next().value, call(Events.getEventsByParcelId, 123))
    stringifyEqual(g.next().value, put(Creators.setEvents([])))
  })

  it('load tickets', () => {
    const g = loadTickets(123)
    stringifyEqual(g.next().value, call(Ticketing.getTicketsForParcel, 123))
    stringifyEqual(g.next().value, put(Creators.setTickets([])))
  })

  it('load job route', () => {
    const g = loadJobRoute({id: 111, jobType: 'parcel-delivery', __uid__: 28737181})
    stringifyEqual(g.next().value, call(Route.searchJobs, 'parcel-delivery', [111]))
    g.next([{route: {id: 999}}, {route: {id: 1212}}])
    g.next({})
    stringifyEqual(g.next().value, Creators.setJobToRouteMapping({28737181: {route: {id: 999}}}))
  })

  describe('load destination zone', () => {
    it('with valid transit job', () => {
      const g = loadDestinationZone()
      stringifyEqual(g.next().value, select(selectors.TransitJobSelector))
      stringifyEqual(g.next({destinationSortingZoneId: 222}).value, call(Address.getZoneInfo, 222))
      stringifyEqual(g.next({zoneId: 8989}).value, put(Creators.setDestinationZone({zoneId: 8989})))
    })

    it('with invalid transit job', () => {
      const g = loadDestinationZone()
      stringifyEqual(g.next().value, select(selectors.TransitJobSelector))
      stringifyEqual(g.next().value, put(Creators.setDestinationZone(null)))
    })
  })

  describe('load jobs and related', () => {
    let g
    beforeEach(() => {
      g = loadJobsAndRelated(123)
    })

    it('load jobs', () => {
      stringifyEqual(g.next().value, call(NC.getJobsByParcelId, 123))
      stringifyEqual(g.next([]).value, put(Creators.setJobs([])))
      expect(g.next().done).toBe(true)
    })

    it('load job related', () => {
      const jobs = [
        {id: 456, jobType: 'parcel-delivery'},
        {id: 789, jobType: 'parcel-pickup'},
        {id: 789, jobType: 'parcel-transit'},
      ]

      g.next()
      g.next(jobs)

      stringifyEqual(g.next().value, spawn(loadDestinationZone))
      stringifyEqual(g.next().value, spawn(loadJobRoute, jobs[0]))
      stringifyEqual(g.next().value, spawn(loadProofs, jobs[0]))

      stringifyEqual(g.next().value, spawn(loadJobRoute, jobs[1]))
      stringifyEqual(g.next().value, spawn(loadProofs, jobs[1]))

      stringifyEqual(g.next().value, spawn(loadJobRoute, jobs[2]))

      expect(g.next().done).toBe(true)
    })
  })

  describe('default saga', () => {
    it('should listen to loadParcelEdit event', () => {
      const g = saga()
      stringifyEqual(g.next().value, takeEvery(Types.LOAD_PARCEL_EDIT_VIEW, loadParcelEdit))
    })
    it('loadParcelEdit should spawn loading of multiple service', () => {
      const g = loadParcelEdit({parcelId: 123})
      stringifyEqual(g.next().value, spawn(loadParcelAndRelated, 123))
      stringifyEqual(g.next().value, spawn(loadJobsAndRelated, 123))
      stringifyEqual(g.next().value, spawn(loadTickets, 123))
      stringifyEqual(g.next().value, spawn(loadEvents, 123))
    })
  })
})
