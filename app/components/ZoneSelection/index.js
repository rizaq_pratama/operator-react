//  @flow
import React from 'react'
import styled from 'styled-components'
import _ from 'lodash'
import { Select } from 'antd'
import { T } from '@app/components/Generic/T'

const Option = Select.Option
const StyledZoneSelection = styled(Select)``

type ZoneSelectionProps = {
  children: Array<Object>,
  zones: Array<Number>,
  onSelect: Function,
  wpId: number,
}

class ZoneSelection extends React.Component<ZoneSelectionProps> {

  static defaultProps = {
    children: 'ZoneSelection',
    zones: [],
    wpId: -1,
  }
  
  renderZoneOptions (zones) {
    const sortedZones = _.orderBy(zones, 'name', 'asc')
    if (this.props.wpId) {
      return (
        _.map(sortedZones, (z) => {
          const ojsVal = `${this.props.wpId},${z.id}`
          return (<Option key={z.id} value={ojsVal}>{z.name}</Option>)
        })
      )
    }
    return (
      _.map(sortedZones, (z) => (
        <Option key={z.id} value={z.id}>{z.name}</Option>
      ))
    )
  }

  render () {
    const { zones, onSelect } = this.props
    return (
      <StyledZoneSelection
        {...this.props}
        showSearch
        placeholder={<T id='container.address-verification.search-or-select-zone' />}
        onSelect={onSelect}
        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
        {this.renderZoneOptions(zones)}
      </StyledZoneSelection>
    )
  }
}

export { ZoneSelection }
