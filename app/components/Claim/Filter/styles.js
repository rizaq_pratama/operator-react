import styled from 'styled-components'
import { Button } from 'antd'

export const SubmitButton = styled(Button)`
&& {
  width: 150px;
  margin: 10px auto;
}
`

export const ClaimFilterContainer = styled.div`
  display: flex;
  flex-direction: column;

`
