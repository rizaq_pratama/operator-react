
import React from 'react'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'
import ParcelTable from '../index'
import 'jest-styled-components'
import { TestIntlProviderWrapper } from '../../../../../internals/testing/test-helpers'

const testParcelTableData = [{
  trackingId: 'NINJ1',
  parcelMetadata: {
    currentJobType: 'Pickup',
    currentJobStatus: 'In-Progress',
  },
  currentGranularStatus: 'En-route to Sorting Hub',
  latestEvent: 'Driver pickup scan',
},{
  trackingId: 'NINJ2',
  parcelMetadata: {
    currentJobType: 'Pickup',
    currentJobStatus: 'In-Progress',
  },
  currentGranularStatus: 'En-route to Sorting Hub',
  latestEvent: 'Driver pickup scan',
},{
  trackingId: 'NINJ3',
  parcelMetadata: {
    currentJobType: 'Pickup',
    currentJobStatus: 'In-Progress',
  },
  currentGranularStatus: 'En-route to Sorting Hub',
  latestEvent: 'Driver pickup scan',
},{
  trackingId: 'NINJ4',
  parcelMetadata: {
    currentJobType: 'Pickup',
    currentJobStatus: 'In-Progress',
  },
  currentGranularStatus: 'En-route to Sorting Hub',
  latestEvent: 'Driver pickup scan',
}]

describe('ParcelTable', () => {
  let wrapper, instance
  beforeEach(() => {
    wrapper = shallow(
      <ParcelTable
        data={testParcelTableData} />
    )
    instance = wrapper.instance()
  })
  it('matches snapshot', () => {
    const component = renderer.create(
      <TestIntlProviderWrapper>
        <ParcelTable
          data={testParcelTableData} />
      </TestIntlProviderWrapper>
    )
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('renders a ParcelTable', () => {
    expect(instance).toBeDefined()
  })
  it('correctly searches and sets state of parcel table', () => {
    instance.searchParcelHandler({
      target: {
        value: 'NINJ1',
      },
    })
    expect(instance.state['searchText']).toBe('NINJ1')
  })
})
