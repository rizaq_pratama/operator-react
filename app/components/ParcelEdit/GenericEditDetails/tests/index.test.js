import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import {PickupForm, DeliveryForm} from '../index'
import { PickupSlotsForm, DeliverySlotsForm } from '../Forms/Slots'
import AddressForm from '../Forms/AddressForm'
import DetailsForm from '../Forms/DetailsForm'

describe('Forms', () => {

  describe('Pickup form', () => {
    it('should contains correct components', () => {
      const wrapper = shallow(<PickupForm/>)
      expect(toJson(wrapper)).toMatchSnapshot()
      expect(wrapper.find(AddressForm)).toHaveLength(1)
      expect(wrapper.find(PickupSlotsForm)).toHaveLength(1)
      expect(wrapper.find(DetailsForm)).toHaveLength(1)
    })
  })

  describe('Delivery form', () => {
    it('should contains correct components', () => {
      const wrapper = shallow(<DeliveryForm/>)
      expect(toJson(wrapper)).toMatchSnapshot()
      expect(wrapper.find(AddressForm)).toHaveLength(1)
      expect(wrapper.find(DeliverySlotsForm)).toHaveLength(1)
      expect(wrapper.find(DetailsForm)).toHaveLength(1)
    })
  })
})
