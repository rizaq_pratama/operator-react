import api from '@nv/react-commons/src/Migration_corev2/Services'
import { makeAPIProxy } from '../apiProxy'
import Config from 'configs'

const Ticketing = api.Ticketing

Ticketing.BASE_URL = Config.BASE_URL + '/ticketing'

export default makeAPIProxy(Ticketing)

