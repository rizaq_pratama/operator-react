// @flow
/**
*
* AvCoordinateDialog
*
*/

import React from 'react'
import styled from 'styled-components'
import ReactMapboxGL, { Marker } from 'react-mapbox-gl'
import { injectIntl, intlShape } from 'react-intl'
import CopyToClipboard from 'react-copy-to-clipboard'
import { Button, Form, Modal, Col, Row, Input, message } from 'antd'
import { T } from '@app/components/Generic/T'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMapMarkerAlt } from '@fortawesome/pro-solid-svg-icons'
import { getDefaultLatLngForDomain } from '@app/utils/helpers'
import _ from 'lodash'

const FormItem = Form.Item
let Map
const StyledAvCoordinateDialog = styled(Modal)`
  && {
    width: 660px !important;
    height: 520px;
    font-style: roboto;
  }
  .ant-form-item-label {
    line-height: 14px;
  }
  .ant-form-item-control {
    line-height: 14px;
  }
  .ant-form-item {
    margin-bottom: 5px;
  }
`

const CoordInput = styled(Input)`
  && {
    width: 120px;
  }
`

const CopyButton = styled(Button)`
  && {
    margin-left: 20px;
  }
`

const SaveButton = styled(Button)`
  && {
    margin-top: 21px;
  }
`
const ResetLink = styled.a`
  && {
    margin-left: 15px;
    margin-bottom: 10px;
  }
`

const AddressFormItem = styled(FormItem)`
  .ant-form-item-children{
    font-weight: bold;
    text-transform: uppercase;
  }
`
const AddressText = styled.span`
  && {
    max-width: 300px;
  }

`


type AvCoordinateDialogProps = {
  country: string,
  address: any,
  onOk: Function,
  onCancel: Function,
  visible: boolean,
  mapBoxToken: string,
  intl: intlShape,
  country: string
}

class AvCoordinateDialog extends React.Component<AvCoordinateDialogProps> {
  static defaultProps = {
    visible: false,
  }
  constructor (props) {
    super(props)
    this.state = {
      lat: this.props.address.lat || getDefaultLatLngForDomain(this.props.country).lat,
      lng: this.props.address.lng || getDefaultLatLngForDomain(this.props.country).lng,
      visible: this.props.visible,
      latValid: true,
      lngValid: true,
      latLng: '',
      canSave: true,
    }
    this.handleLatLongChange = this.handleLatLongChange.bind(this)
    this.handleCancel = this.handleCancel.bind(this)
    this.handleLatitudeChange = this.handleLatitudeChange.bind(this)
    this.handleLongitudeChange = this.handleLongitudeChange.bind(this)
    this.handleOnCopy = this.handleOnCopy.bind(this)
    this.handleResetCoordinate = this.handleResetCoordinate.bind(this)
    this.handleOk = this.handleOk.bind(this)
    Map = ReactMapboxGL(
      {
        accessToken: props.mapBoxToken,
        interactive: true,
      }
    )
  }

  static getDerivedStateFromProps (nextProps, prevState) {
    if (nextProps.visible !== prevState.visible) {
      return {
        visible: nextProps.visible,
        lat: nextProps.address.lat || getDefaultLatLngForDomain(nextProps.country).lat,
        lng: nextProps.address.lng || getDefaultLatLngForDomain(nextProps.country).lng,
        latLng: '',
      }
    }
    return null
  }

  getDefaultCoordinate () {
    return [getDefaultLatLngForDomain(this.props.country).lng, getDefaultLatLngForDomain(this.props.country).lat]
  }

  handleResetCoordinate () {
    const lat = this.props.address.lat || getDefaultLatLngForDomain(this.props.country).lat
    const lng = this.props.address.lng || getDefaultLatLngForDomain(this.props.country).lng
    this.setState({
      lat: lat,
      lng: lng,
      latValid: true,
      lngValid: true,
      latLng: lat + ',' + lng,
      canSave: true,
    })
  }

  handleOnCopy () {
    message.info(this.props.intl.formatMessage({ id: 'commons.copied-to-clipboard' }))
  }

  handleLatitudeChange (e) {
    e.preventDefault()
    let value = e.target.value

    if(this.isBlank(value)) {
      this.setState({ lat: value, latValid: false, canSave: false })
      return
    }

    if ((value >= -90 && value <= 90)) {
      this.setState({ lat: value, latValid: true, canSave: this.state.lngValid })
    } else if (value === '-') {
      this.setState({ lat: value, latValid: false, canSave: false })
    } else {
      message.error(this.props.intl.formatMessage({ id: 'container.address-verification.latitude-must-between-90-to-90-degrees' }))
    }
  }

  handleLongitudeChange (e) {
    e.preventDefault()
    let value = e.target.value

    if(this.isBlank(value)) {
      this.setState({ lng: value, lngValid: false, canSave: false })
      return
    }

    if (value >= -180 && value <= 180) {
      this.setState({ lng: value, lngValid: true, canSave: this.state.latValid })
    } else if (value === '-') {
      this.setState({ lng: value, lngValid: false, canSave: false })
    } else {
      message.error(this.props.intl.formatMessage({ id: 'container.address-verification.longitude-must-between-180-to-180-degrees' }))
    }
  }

  handleLatLongChange (e) {
    e.preventDefault()
    this.setState({ latLng: e.target.value })
    const latLng = _.split(e.target.value, ',')
    if (latLng && latLng.length === 2 && !_.isNaN(Number(latLng[0])) && !_.isNaN(Number(latLng[1]))) {
      if (Number(latLng[0]) < -90 || Number(latLng[0]) > 90) {
        this.setState({ canSave: false })
        message.error(this.props.intl.formatMessage({ id: 'container.address-verification.latitude-must-between-90-to-90-degrees' }))
        return
      }

      if (Number(latLng[1]) < -180 || Number(latLng[1]) > 180) {
        this.setState({ canSave: false })
        message.error(this.props.intl.formatMessage({ id: 'container.address-verification.longitude-must-between-180-to-180-degrees' }))
        return
      }
      this.setState({ lat: +latLng[0], lng: +latLng[1], canSave: true })
    } else {
      this.setState({ canSave: false })
      message.error(this.props.intl.formatMessage({ id: 'container.address-verification.invalid-lat-long-entered' }))
    }
  }

  handleCancel () {
    this.setState({ visible: false })
    this.props.onCancel()
  }

  isBlank(param) {
    return param.length === 0 || ( typeof param === 'string' && !param.trim() )
  }

  handleOk () {
    if(this.isBlank(this.state.lat) || this.isBlank(this.state.lng)){
      message.error(this.props.intl.formatMessage({ id: 'container.address-verification.invalid-lat-long-entered' }))
      return
    }

    const coord = {
      lat: Number(this.state.lat),
      lng: Number(this.state.lng),
    }

    if(this.state.canSave){
      this.props.onOk(coord, this.props.address)
    }
    else{
      message.error(this.props.intl.formatMessage({ id: 'container.address-verification.invalid-lat-long-entered' }))
    }
  }

  render () {
    const { address } = this.props
    const { canSave } = this.state

    return (
      <StyledAvCoordinateDialog visible={this.state.visible} onOk={this.handleOk} footer={null} onCancel={this.handleCancel}>
        <Form layout='horizontal'>
          <Row>
            <Col span='18'>
              <AddressFormItem label={<T id='commons.address' />}>
                <AddressText>{address.address}</AddressText>
                <CopyToClipboard text={address.address} onCopy={this.handleOnCopy}>
                  <CopyButton size='default' icon='copy' />
                </CopyToClipboard>
              </AddressFormItem>
            </Col>
          </Row>
          <Row>
            <Col span='10'>
              <FormItem label={<T id='commons.latitude-longitude' />}>
                <Input value={this.state.latLng} style={{ width: '232px' }} placeholder='eg. 1.3308569,103.908599' onChange={this.handleLatLongChange} />
              </FormItem>
            </Col>
            <Col span='10'>
              <Row>
                <Col span='12'>
                  <FormItem label={<T id='commons.latitude' />}>
                    <CoordInput value={this.state.lat} onChange={this.handleLatitudeChange} />
                  </FormItem>
                </Col>
                <Col span='12'>
                  <FormItem label={<T id='commons.longitude' />}>
                    <CoordInput value={this.state.lng} onChange={this.handleLongitudeChange} />
                  </FormItem>
                </Col>
              </Row>
            </Col>
            <Col span='4'>
              <FormItem>
                <SaveButton type='primary' onClick={this.handleOk} disabled={!canSave}>
                  <T id='commons.save' />
                </SaveButton>
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col offset='20'>
              <ResetLink href='javascript:void(0)' onClick={this.handleResetCoordinate}>
                <T id='commons.reset' />
              </ResetLink>
            </Col>
          </Row>
          <Row>
            <Col span='24'>
              <Map
                style='mapbox://styles/mapbox/streets-v9'
                center={this.state.latValid && this.state.lngValid ? [this.state.lng, this.state.lat] : this.getDefaultCoordinate()}
                containerStyle={{
                  height: '400px',
                  width: '612px',
                }}>
                <Marker
                  coordinates={this.state.latValid && this.state.lngValid ? [this.state.lng, this.state.lat] : this.getDefaultCoordinate()}
                  anchor='bottom'>
                  <FontAwesomeIcon icon={faMapMarkerAlt} size='2x' style={{ color: 'red' }} />
                </Marker>
              </Map>
            </Col>
          </Row>
        </Form>
      </StyledAvCoordinateDialog>
    )
  }
}

AvCoordinateDialog.defaultProps = {
  visible: false,
  country: 'sg',
}

export default injectIntl(AvCoordinateDialog)
