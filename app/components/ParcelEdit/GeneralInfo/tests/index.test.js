/* global alert */
import React from 'react'
import { shallow } from 'enzyme'
import GeneralInfo from '../index'
import renderer from 'react-test-renderer'
import { TestIntlProviderWrapper } from '@internals/testing/test-helpers'

describe('<GeneralInfo />', () => {
  it('should render', () => {
    const renderedComponent = shallow(
      <GeneralInfo
        stampId={'-'}
        currentJob={{id: 1234567, jobType: 'parcel-delivery'}}
        currentHub={'GW'}
        destinationHub={'Pandan'}
        orderTrackingId={'78224548'}
        latestEvent={'Hub Inbound Scan'}
        dnrGroup={'Normal'}
        zone={'L2'}
        handleCurrentJobClick={() => alert('Handle Current Job Click')}
        handleOrderTrackingIdClick={() => alert('Handle Order Tracking ID Click')}
      />)
    expect(renderedComponent).toBeDefined()
  })
  it('should match snapshot', () => {
    const tree = renderer.create(<TestIntlProviderWrapper><GeneralInfo /></TestIntlProviderWrapper>).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
