import React from 'react'
import { shallowWithIntl } from '@internals/testing/test-helpers'
import { AvstatsCard } from '../index'

describe('<AvstatsCard />', () => {

  let wrapper, instance
  beforeEach(() => {
    wrapper = shallowWithIntl(
      <AvstatsCard />
    )
    instance = wrapper.instance()
  })
  it('Should render without error', () => {
    expect(wrapper).toBeDefined()
  })
  it('should handle collapsing', () => {
    instance.handleCollapsing(true)
    expect(instance.state['collapsed']).toBe(true)
  })
  it('should render a collapsible button', () => {
    instance.setState({collapsed: true})
    const buttonComponentTrue = shallowWithIntl(instance.renderCollapsible())
    expect(buttonComponentTrue).toBeDefined()
    buttonComponentTrue.instance().props['onClick']()
    expect(instance.state['collapsed']).toBe(false)
    instance.setState({collapsed: false})
    const buttonComponentFalse = shallowWithIntl(instance.renderCollapsible())
    expect(buttonComponentFalse).toBeDefined()
    buttonComponentFalse.instance().props['onClick']()
    expect(instance.state['collapsed']).toBe(true)
  })
})
