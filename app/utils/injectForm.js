import React from 'react'
import { FormContext } from '@app/components/Generic/Contexts/Form';

function getDisplayName(Component) {
  return Component.displayName || Component.name || 'Component';
}

export function injectForm(WrappedComponent) {

  class InjectForm extends React.Component {
    static WrappedComponent = WrappedComponent
    render () {
      return <FormContext.Consumer>
        {
          form => <WrappedComponent {...this.props} {...{form}} />
        }

      </FormContext.Consumer>
    }
  }
  InjectForm.displayName = 'InjectForm(' + getDisplayName(WrappedComponent) + ')'

  return InjectForm
}

