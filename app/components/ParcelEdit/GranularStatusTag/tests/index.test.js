import React from 'react'
import renderer from 'react-test-renderer'
import { shallowWithIntl } from '@internals/testing/test-helpers'
import GranularStatusTag from '../index'

describe('GranularStatusTag', () => {

  let regularWrapper, abnormalWrapper, terminalWrapper
  beforeEach(() => {
    regularWrapper = shallowWithIntl(
      <GranularStatusTag
        status={'PENDING PICKUP'} />
    )
    abnormalWrapper = shallowWithIntl(
      <GranularStatusTag
        status={'DELIVERY EXCEPTION, MAX ATTEMPTS REACHED'} />
    )
    terminalWrapper = shallowWithIntl(
      <GranularStatusTag
        status={'CANCELLED'} />
    )
  })
  it('matches snapshot', () => {
    const component = renderer.create(
      <GranularStatusTag
        status={'PENDING PICKUP'} />
    )
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('renders a GranularStatusTag for regular statuses', () => {
    expect(regularWrapper).toBeDefined()
  })
  it('renders a GranularStatusTag for abnormal statuses', () => {
    expect(abnormalWrapper).toBeDefined()
  })
  it('renders a GranularStatusTag for terminal statuses', () => {
    expect(terminalWrapper).toBeDefined()
  })
})
