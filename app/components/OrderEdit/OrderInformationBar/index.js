//  @flow
import React from 'react'
import styled from 'styled-components'
import { Row, Col } from 'antd'
import OrderStatusTag from '../OrderStatusTag'

const StyledRow = styled(Row)`
  min-width: 100%;
`

const TrackingNumberCol = styled(Col)`
  margin-right: 10px;
  font-weight: bold;
  padding-top: 2px;
`

type OrderInformationBarProps = {
  trackingNumber: string,
  orderStatus: string,
}

class OrderInformationBar extends React.Component<OrderInformationBarProps> {
  constructor (props) {
    super(props)
  }
  render() {
    return (
      <StyledRow align='middle' type='flex'>
        <TrackingNumberCol>
          <span>{this.props.trackingNumber}</span>
        </TrackingNumberCol>
        <OrderStatusTag
          status={this.props.orderStatus} />
      </StyledRow>
    )
  }
}

export default OrderInformationBar
