import React from 'react'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'
import FilterRangeBox from '../index'
import { shallowWithIntl, TestIntlProviderWrapper } from '@internals/testing/test-helpers'
import 'jest-styled-components'

const testFilterRangeBoxParams = {
  title: 'Number of Parcels',
  property: 'Parcels',
  startValue: 1,
  endValue: 10,
}

describe('FilterRangeBox', () => {
  let wrapper, instance
  beforeEach(() => {
    wrapper = shallowWithIntl(
      <FilterRangeBox
        title={testFilterRangeBoxParams.title}
        property={testFilterRangeBoxParams.property}
        startValue={testFilterRangeBoxParams.startValue}
        endValue={testFilterRangeBoxParams.endValue}
      />
    )
    instance = wrapper.instance()
  })
  xit('matches snapshot', () => {
    const component = renderer.create(
      <TestIntlProviderWrapper>
        <FilterRangeBox
          title={testFilterRangeBoxParams.title}
          property={testFilterRangeBoxParams.property}
          startValue={testFilterRangeBoxParams.startValue}
          endValue={testFilterRangeBoxParams.endValue}
        />
      </TestIntlProviderWrapper>
    )
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('renders a FilterRangeBox component', () => {
    const renderedComponent = shallow(
      <FilterRangeBox
        title={testFilterRangeBoxParams.title}
        property={testFilterRangeBoxParams.property}
        startValue={testFilterRangeBoxParams.startValue}
        endValue={testFilterRangeBoxParams.endValue}
      />
    )
    expect(renderedComponent).toBeDefined()
  })
  it ('should change range values', () => {
    expect(instance.state['startValue']).toBe(1)
    instance.rangeChangeHandler([3,10])
    expect(instance.state['startValue']).toBe(3)
  })
})
