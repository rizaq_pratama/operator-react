import React from 'react'
import ConfirmDPDropOffSettings from '@app/components/ParcelEdit/ConfirmDPDropOffSettings'
import { storiesOf } from '@storybook/react'
import { TestIntlProviderWrapper } from '@internals/testing/test-helpers'

storiesOf('ParcelEdit', module).add('ConfirmDPDropOffSettings', () => (
  <TestIntlProviderWrapper>
    <ConfirmDPDropOffSettings
      visible
      dps={[{id: 372, name: 'Hako At AMK Hub'}, {id: 2703, name: 'PS DP_1'}]}
    />
  </TestIntlProviderWrapper>
))
