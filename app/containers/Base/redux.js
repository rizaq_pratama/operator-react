import { createActions, createReducer } from 'reduxsauce'
import { fromJS } from 'immutable'

export const { Types, Creators } = createActions(
  {
    startupSuccess: ['data'],
    openModal: ['id'],
    closeModal: [],
    modalFormSubmitted: [],
  },
  { prefix: 'global/' }
)

const INITIAL_STATE = fromJS({
  setting: {},
  modal: {},
})

export const startupReducer = createReducer(INITIAL_STATE, {
  [Types.STARTUP_SUCCESS]: (state, action) => state.merge({ settings: action.data }),
  [Types.CLOSE_MODAL]: (state, action) => state.setIn(['modal', 'id'], null),
  [Types.OPEN_MODAL]: (state, action) => state.setIn(['modal', 'id'], action.id),
})
