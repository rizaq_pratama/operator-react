//  @flow
import React from 'react'
import styled from 'styled-components'
import { Tag, Row, Col } from 'antd'
import GranularStatusTag from '../GranularStatusTag'
import PriorityLevelTag from '../PriorityLevelTag'
import { Currency } from '@nv/react-commons/src/Migration_corev2/Constants/Currency'
import { FormattedNumber } from 'react-intl'

const StyledRow = styled(Row)`
  min-width: 100%;
  margin-top: 10px;
`
const TrackingNumberCol = styled(Col)`
  font-size: 120%;
  margin-right: 10px;
  font-weight: 500;
  padding-top: 2px;
`
type InformationBarProps = {
  trackingNumber: string,
  priorityLevel: number,
  cod?: number,
  currency: $Keys<typeof Currency>,
  isRts?: boolean,
  status: string
}

class InformationBar extends React.Component<InformationBarProps> {
  render () {
    let codTag = null
    let rtsTag = null

    if (this.props.cod) {
      const { cod, currency } = this.props
      codTag = (
        <Tag color={'geekblue'}>
          COD: <FormattedNumber value={cod} style='currency' currency={currency} />
        </Tag>
      )
    }

    if (this.props.isRts) {
      rtsTag = <Tag color={'magenta'}>RTS</Tag>
    }
    return (
      <StyledRow align='middle' justify='start' type='flex'>
        <TrackingNumberCol>
          <span>{this.props.trackingNumber}</span>
        </TrackingNumberCol>
        <Col>
          <GranularStatusTag status={this.props.status} />
        </Col>
        <Col>
          <PriorityLevelTag priorityLevel={this.props.priorityLevel} />
        </Col>
        <Col>
          {codTag}
        </Col>
        <Col>
          {rtsTag}
        </Col>
      </StyledRow>
    )
  }
}

export default InformationBar
