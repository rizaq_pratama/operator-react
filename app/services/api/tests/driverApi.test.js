import IFrameHelpers from '@nv/react-commons/src/Migration_corev2/IFrameHelpers'
import DriversApi from '../driversApi'

jest.mock('@nv/react-commons/src/Migration_corev2/IFrameHelpers')

describe('driversApi', () => {
  it('searchDriverOne', () => {
    const fakeRequest = jest.fn()

    IFrameHelpers.IFrameClient = function () {
      this.request = fakeRequest
    }

    DriversApi.searchDriverOne()
    expect(fakeRequest).toBeCalled()
  })
})
