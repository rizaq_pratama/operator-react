/* eslint-disable no-console */
import React from 'react'
import { storiesOf } from '@storybook/react'
import FilterAutocompleteBox from '@app/components/Generic/FilterAutocompleteBox'
import FilterDateBox from '@app/components/Generic/FilterDateBox'
import FilterTimeBox from '@app/components/Generic/FilterTimeBox'
import FilterBooleanBox from '@app/components/Generic/FilterBooleanBox'
import FilterRangeBox from '@app/components/Generic/FilterRangeBox'
import FilterMultipleTextBox from '@app/components/Generic/FilterMultipleTextBox'
import FilterSingleTextBox from '@app/components/Generic/FilterSingleTextBox'

function changeHandler(values) {
  console.log(values)
}

function deleteHandler() {
  console.log('deleting filter')
}

const testFilterAutocompleteBoxParams = {
  label: 'Shipper',
  searchKey: 'shipperName',
  searchFunction: (text) =>
    new Promise((resolve, reject) => {
      const options = [
        {shipperName: 'Shipper ABC'},
        {shipperName: 'Shipper BCD'},
        {shipperName: 'Shipper CDE'},
      ]
      resolve(options);
    }),
}

const testFilterDateBoxParams = {
  label: 'Creation Date',
}

const testFilterTimeBoxParams = {
  label: 'Creation Time',
}

const testFilterBooleanBoxParams = {
  label: 'Archived Routes',
  property: 'Archived',
}

const testFilterRangeBoxParams = {
  label: 'Number of Parcels',
  property: 'Parcels',
  startValue: 1,
  endValue: 10,
}

const testFilterMultipleTextBoxParams = {
  label: 'Tracking IDs',
}

const testFilterSingleTextBoxParams = {
  label: 'Aged Days',
}

storiesOf('FilterBoxComponents', module)
  .add('FilterAutocompleteBox', () => {
    return (
      <FilterAutocompleteBox
        label={testFilterAutocompleteBoxParams.label}
        searchFunction={testFilterAutocompleteBoxParams.searchFunction}
        searchKey={testFilterAutocompleteBoxParams.searchKey}
        onChange={changeHandler}
        onDelete={deleteHandler} />
    )
  })
  .add('FilterDateBox', () => {
    return (
      <FilterDateBox
        label={testFilterDateBoxParams.label}
        onChange={changeHandler}
        onDelete={deleteHandler} />
    )
  })
  .add('FilterTimeBox', () => {
    return (
      <FilterTimeBox
        label={testFilterTimeBoxParams.label}
        onChange={changeHandler}
        onDelete={deleteHandler} />
    )
  })
  .add('FilterBooleanBox', () => {
    return (
      <FilterBooleanBox
        label={testFilterBooleanBoxParams.label}
        property={testFilterBooleanBoxParams.property}
        onChange={changeHandler}
        onDelete={deleteHandler} />
    )
  })
  .add('FilterRangeBox', () => {
    return (
      <FilterRangeBox
        label={testFilterRangeBoxParams.label}
        property={testFilterRangeBoxParams.property}
        startValue={testFilterRangeBoxParams.startValue}
        endValue={testFilterRangeBoxParams.endValue}
        onChange={changeHandler}
        onDelete={deleteHandler} />
    )
  })
  .add('FilterMultipleTextBox', () => {
    return (
      <FilterMultipleTextBox
        label={testFilterMultipleTextBoxParams.label}
        onChange={changeHandler}
        onDelete={deleteHandler} />
    )
  })
  .add('FilterSingleTextBox', () => {
    return (
      <FilterSingleTextBox
        label={testFilterSingleTextBoxParams.label}
        onChange={changeHandler}
        onDelete={deleteHandler} />
    )
  })
