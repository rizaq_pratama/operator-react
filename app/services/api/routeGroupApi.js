import iframeClient from '../iframeClient'

function getRouteGroups () {
  return iframeClient.request('routeGroup_read')
}

export default {
  getRouteGroups,
}
