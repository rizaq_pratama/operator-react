#!/usr/bin/env bash -e

branch=$(git rev-parse --abbrev-ref HEAD)

if [ "$branch" != 'master' ]; then
  git fetch --tags
  # create temp branch
  git checkout -b release/temp_$(git rev-parse --short HEAD)
fi
# Add changelog
# changelog
# git add CHANGELOG.md
