import React from 'react'
import { shallow } from 'enzyme'
import { shallowWithIntl } from '@internals/testing/test-helpers'
import AddressFinderForm from '../index'
import Config from 'configs'

const testLatitude = 1.3308
const testLongitude = 103.908
const testVisible = true
const dummyAccessToken = Config.MAPBOX_TOKEN
const testIsSavingEdits = false
const testFormProps = {
  setFields: jest.fn(),
}
const testSaveCoordinates = jest.fn()

describe('AddressFinderForm', () => {

  let wrapper, instance
  beforeEach(() => {
    wrapper = shallowWithIntl(
      <AddressFinderForm
        handleSaveCoordinates={testSaveCoordinates}
        form={testFormProps}
        latitude={testLatitude}
        longitude={testLongitude}
        visible={testVisible}
        accessToken={dummyAccessToken}
        isSavingEdits={testIsSavingEdits}
      />
    )
    instance = wrapper.instance()
  })
  it('renders a AddressFinderForm component', () => {
    const renderedComponent = shallow(
      <AddressFinderForm
        latitude={testLatitude}
        longitude={testLongitude}
        visible={testVisible}
        accessToken={dummyAccessToken}
        isSavingEdits={testIsSavingEdits}
      />
    )
    expect(renderedComponent).toBeDefined()
  })
  it('should resize map object', () => {
    const testMap = {
      resize: jest.fn(),
    }
    instance.handleStyleLoad(testMap)
    expect(testMap.resize).toBeCalled()
  })
  it ('should call save coordinates method', () => {
    instance.saveCoordinates()
    expect(testSaveCoordinates).toBeCalled()
  })
})
