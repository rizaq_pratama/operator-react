import React from 'react'
import { ConfirmPullFromRoute } from '@app/components/ParcelEdit/ConfirmPullFromRoute'
import { TestIntlProviderWrapper } from '@internals/testing/test-helpers'
import { storiesOf } from '@storybook/react'

const data = [];
for (let i = 0; i < 2; i++) {
    data.push({
      id: i,
      routeId: `${ (i + 1) * 100}`,
      jobType: 'Delivery' + i,
    });
  }

storiesOf('ParcelEdit', module).add('ConfirmPullFromRoute', () => (
  <TestIntlProviderWrapper>
    <ConfirmPullFromRoute
      visible
      jobs={data}
      onCancel={() => alert('Cancel is Clicked')} />
  </TestIntlProviderWrapper>
))
