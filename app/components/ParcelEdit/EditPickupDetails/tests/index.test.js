import React from 'react'
import { EditPickupDetails } from '../index'
import { mount, shallow } from 'enzyme'
import toJSON from 'enzyme-to-json'
import { TestIntlProviderWrapper } from '@internals/testing/test-helpers'
import NC from '@app/services/api/ninjaControlApi'

jest.mock('react-mapbox-gl', () => {
  function Map() {
    return function () {return null}
  }

  Map.Marker = function () {return null}

  return Map
})

jest.mock('@app/services/api/ninjaControlApi', () => {
  return {
    updatePickupDetails: jest.fn(() => ({ok: true})),
    reschedulePickupJob: jest.fn(() => ({ok: true})),
    updatePickupAddress: jest.fn(() => ({ok: true})),
  }
})

describe('EditPickupDetails', () => {
  let job
  beforeEach(() => {
    job = {
      'id': 22,
      'job_type': 'parcel-pickup',
      'priority_level': 72,
      'service_type': 'Standard',
      'service_level': 'Scheduled',
      'start_date': '2018-10-11',
      'end_date': '2018-10-11',
      'start_time': '12:00:00+08:00',
      'end_time': '15:00:00+08:00',
      'sla_date_time': '2018-10-11T15:00:00+08:00',
      'name': 'Shipper Name',
      'email': 'shipper@ninjavan.co',
      'contact': '90909090',
      'address1': '30 Jln Kilang Barat, Singapore 159363',
      'address2': '#04-19 LUCKY PLAZA',
      'neighbourhood': 'SINGAPORE',
      'locality': 'SINGAPORE',
      'region': 'SINGAPORE',
      'country': 'SINGAPORE',
      'postcode': '238863',
      'latitude': 1.3308,
      'longitude': 103.908,
      'pickup_approx_volume': 'Less than 3 Parcels',
      'status': 'Completed',
      'parcel_id': 3,
      'cop_req_id': 120,
      'cop_req_amount': 90.88,
    }
  })
  it('renders a EditPickupDetails', () => {
    const wrapper = shallow(
      <EditPickupDetails
        visible
        {...job}
      />
    )
    expect(toJSON(wrapper)).toMatchSnapshot()
  })

  describe('full render', () => {
    let wrapper
    beforeEach(() => {
      wrapper = mount(
        <TestIntlProviderWrapper>
          <EditPickupDetails visible {...job}/>
        </TestIntlProviderWrapper>
      )
    })

    it('should submit', async () => {
      wrapper.find('.ant-modal-footer [type="primary"]').simulate('click')
      expect(NC.updatePickupDetails).toHaveBeenCalled()
      expect(NC.reschedulePickupJob).toHaveBeenCalled()
      expect(NC.updatePickupAddress).toHaveBeenCalled()
      await new Promise(setTimeout)
    })
  })
})
