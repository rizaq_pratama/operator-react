import React from 'react'
import { shallow } from 'enzyme'
import { shallowWithIntl, TestIntlProviderWrapper } from '@internals/testing/test-helpers'
import FilterBoxContainer from '../index'
import 'jest-styled-components'

const testPossibleFilters = [
  {
    type: 'multipleText',
    label: 'Tracking IDs',
    key: 'tracking_ids_2',
  },
  {
    type: 'time',
    label: 'Creation Time',
    key: 'creation_time',
  },
]

const testSelectedFilters = [
  {
    type: 'options',
    options: [
      'Pending',
      'Cancelled',
      'Completed',
      'Delivery Fail',
    ],
    selectedOptions: ['Cancelled', 'Completed'],
    label: 'Granular Status',
    key: 'granular_status',
  },
  {
    type: 'time',
    label: 'Deletion Time',
    key: 'deletion_time',
  },
  {
    type: 'autocomplete',
    label: 'Shipper',
    key: 'shipper',
    searchKey: 'shipperName',
    options: (text) =>
      new Promise((resolve, reject) => {
        const options = [
          'Shipper ABC',
          'Shipper BCD',
          'Shipper CDE',
        ]
        resolve(options);
      }),
  },
  {
    type: 'date',
    label: 'Creation Date',
    key: 'creation_date',
  },
  {
    type: 'boolean',
    label: 'Archived Routes',
    property: 'Archived',
    key:'archived_routes',
  },
  {
    type: 'range',
    label: 'Number of Parcels',
    property: 'Parcels',
    startValue: 1,
    endValue: 10,
    key: 'no_of_parcels',
  },
  {
    type: 'multipleText',
    label: 'Tracking IDs',
    key: 'tracking_ids',
  },
  {
    type: 'single',
    label: 'Aged Days',
    key: 'aged_days',
  },
]

describe('FilterBoxContainer', () => {
  let wrapper, instance
  beforeEach(() => {
    wrapper = shallowWithIntl(
      <FilterBoxContainer
        allFilters={testPossibleFilters.concat(testSelectedFilters)}
        possibleFiltersKey={testPossibleFilters.map(f => f.key)}
        selectedFiltersKey={testSelectedFilters.map(f => f.key)} />)
    instance = wrapper.instance()
  })
  it('renders a FilterBoxContainer component', () => {
    const renderedComponent = shallow(
      <TestIntlProviderWrapper>
        <FilterBoxContainer
          allFilters={testPossibleFilters.concat(testSelectedFilters)}
          possibleFiltersKey={testPossibleFilters.map(f => f.key)}
          selectedFiltersKey={testSelectedFilters.map(f => f.key)} />
      </TestIntlProviderWrapper>
    )
    expect(renderedComponent).toBeDefined()
  })
  it('should add a filter to selectedFilters from possibleFilters', () => {
    expect(instance.state['filterSwitches']['creation_time']).toBe(false)
    instance.filterAddHandler('creation_time')
    expect(instance.state['filterSwitches']['creation_time']).toBe(true)
  })
  it('should clear selectedFilters', () => {
    instance.filterClearHandler()
    expect(Object.keys(instance.state['filterSwitches']).filter(k => instance.state['filterSwitches'][k]).length).toBe(0)
  })
})
