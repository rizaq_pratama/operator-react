/* eslint-disable */
const path = require('path')

const express = require('express')

const app = express()
app.use(express.static(path.resolve(__dirname, 'public')))

app.use('/node_modules', express.static(path.resolve(__dirname, '..', '..', 'node_modules')))

app.listen(8080, () => {
  console.log('starting listening on 8080');
})
