import React from 'react'
import { shallow } from 'enzyme'

import { T } from '../index'

describe('<T />', () => {
  it('renders a <FormattedMessage>', () => {
    const id = 'test'
    const renderedComponent = shallow(<T id={id} />)
    expect(
      renderedComponent.find('FormattedMessage').getElement()
    ).toBeDefined()
  })
})
