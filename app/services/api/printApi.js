import iframeClient from '../iframeClient'

function printByPost (payload) {
  return iframeClient.request('printer_printByPost')
}

export default {
  printByPost,
}
