// @flow
import React, { useState } from 'react'
import './style.less'
import { T } from '@app/components/Generic/T'
import { Row, Col, Button } from 'antd'
import { FilterBoxContainerContext } from '../Contexts/Filters'

type Props = {
  id: string,
  label: string,
  children: React.Node,
  items: {props: {children: string}}[],
}
export default function FilterContainer(props: Props) {
  const items: {props: {children: string}}[] = props.items || []
  const [showingMore, setShowingMore] = useState(true)

  React.Children.only(props.children)
  const {id}= props
  const wideChildren = React.cloneElement(props.children, {
    style: Object.assign({width: '100%'}, props.children.props.style || {}),
  })

  return (
      <FilterBoxContainerContext.Consumer>
        { containerState =>
          <div className='filter-container'>
            <Row type='flex'>
              <Col>
                {props.label}
              </Col>
              <Col style={{ flex: '1 1' }}>{wideChildren}</Col>

              { items.length > 0 ? showingMore ?
                  <Col><Button onClick={() => setShowingMore(false)}><T id='commons.hide' /> ({items.length})</Button></Col> :
                  <Col><Button onClick={() => setShowingMore(true)}><T id='commons.show' /> ({items.length})</Button></Col> : null
              }
              <Col><Button icon='close' onClick={() => containerState.switchFilter(id)} ></Button></Col>
              { items.length > 0 ?
                  <Col><Button icon='border' onClick={() => containerState.clearFilter(id)}></Button></Col> : null }
          </Row>
          { showingMore && items.length > 0?
              <Row type='flex'>
                {items.map((item, i) => {
                  return <Col key={item.props.children}>{item}</Col>
                })}
              </Row> : null }
          </div>
        }
    </FilterBoxContainerContext.Consumer>
  )
}
