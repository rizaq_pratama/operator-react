import React from 'react'
import { mount, shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { TestIntlProviderWrapper } from '@internals/testing/test-helpers'
import { PricingAndDimensionEdit } from '../index'

describe('PricingAndDimensionEdit', () => {
  let props
  beforeEach(() => {
    props = {
      deliveryFee: 1.23,
      codFee: 1.23,
      insuranceFee: 1.23,
      handlingFee: 1.23,
      gst: 1.23,
      total: 1.23,
      insuredValue: 1.23,
      parcelSize: 'XL',
      weight: 1.23,
      dimensions: {
        width: 1.23,
        height: 1.23,
        length: 1.23,
      },
      currency: 'SGD',
    }
  })

  it('should render', () => {
    const wrapper = shallow(
      <PricingAndDimensionEdit visible onClose={jest.fn()} {...props} />
    )
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  describe('full render', () => {
    it('should submit', () => {
      const close = jest.fn()
      const wrapper = mount(
        <TestIntlProviderWrapper>
          <PricingAndDimensionEdit visible onClose={close} {...props} />
        </TestIntlProviderWrapper>
      )
      wrapper.find('.ant-modal-footer [type="primary"]').simulate('click')
      expect(close).toHaveBeenCalled()
    })
  })
})
