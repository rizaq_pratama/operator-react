import { takeEvery, put } from 'redux-saga/effects'
import api from '@app/services/api/driversApi'
import { Types } from './redux'

export function * getDriver ({ id }) {
  const { driver } = yield api.searchDriverOne(id)

  yield put({ type: Types.SET_DRIVER, data: driver })
}

// Individual exports for testing
export default function * defaultSaga () {
  // See example in containers/HomePage/saga.js
  yield takeEvery('loadDriver', getDriver)
}
