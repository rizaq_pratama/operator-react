import React from 'react'
import { render } from 'enzyme'

import Loading from '../index'

describe('<Loading />', () => {
  it('should render 12 divs', () => {
    const renderedComponent = render(<Loading />)
    expect(renderedComponent.find('div').length).toBe(12)
  })
})
