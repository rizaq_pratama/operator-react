import iframeClient from '../iframeClient'
import api from '@nv/react-commons/src/Migration_corev2/Services'
import _ from 'lodash'

const Shipper = api.Shipper

let baseUrlIsSet = false

function _setUrl() {
  if (!baseUrlIsSet) {
    return iframeClient.request('url_getServiceConfig').then((response) => {
      baseUrlIsSet = true
      Shipper.BASE_URL = _.get(response, 'shipper')
    })
  }
  return Promise.resolve()
}

function getShipperById (shipperId) {
  return _setUrl().then(() =>
    Shipper.getShipperById(shipperId))
}

export default {
  getShipperById,
}
