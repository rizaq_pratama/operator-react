import React from 'react'
import { shallowWithIntl } from '@internals/testing/test-helpers'
import { AvfetchAddressForm } from '../index'

const testCallbackMap = {
  fetchShipperUnverifiedAddress: jest.fn(),
  fetchReverifiedAddress: jest.fn(),
  fetchFromPool: jest.fn(),
  fetchByRg: jest.fn(),
}

describe('<AvfetchAddressForm />', () => {

  let wrapper, instance
  beforeEach(() => {
    wrapper = shallowWithIntl(
      <AvfetchAddressForm
        callbackmap={testCallbackMap} />
    )
    instance = wrapper.instance()
  })
  it('Should render without error', () => {
    expect(wrapper).toBeDefined()
  })
  it('should change select type', () => {
    const type = 'test'
    instance.handleSelectType({
      target: {
        value: type,
      },
    })
    expect(instance.state['selectedType']).toBe(type)
  })
  it('should change fetch size', () => {
    const fetchSize = 'test'
    instance.handleFetchSize(fetchSize)
    expect(instance.state['fetchSize']).toBe(fetchSize)
  })
  it('should change route groups', () => {
    const selectedRouteGroup = 'test'
    instance.handleRouteGroups(selectedRouteGroup)
    expect(instance.state['selectedRouteGroup']).toBe(selectedRouteGroup)
  })
  it('should change zones', () => {
    const selectedZone = 'test'
    instance.handleZones(selectedZone)
    expect(instance.state['selectedZone']).toBe(selectedZone)
  })
  it('should handle different callback calls for by-type', () => {
    const testByType = 'by-type'
    instance.handleSelectType({
      target: {
        value: 1,
      },
    })
    instance.handleClicks(testByType)
    expect(testCallbackMap.fetchShipperUnverifiedAddress).toBeCalled()
    instance.handleSelectType({
      target: {
        value: 2,
      },
    })
    instance.handleClicks(testByType)
    expect(testCallbackMap.fetchReverifiedAddress).toBeCalled()
  })
  it('should not make callback for by-type', () => {
    const testByType = 'by-type'
    instance.setState({selectedType: null})
    instance.handleClicks(testByType)
    expect(testCallbackMap.fetchShipperUnverifiedAddress).not.toHaveBeenCalledTimes(2)
    expect(testCallbackMap.fetchReverifiedAddress).not.toHaveBeenCalledTimes(2)
  })
  it('should handle different callback calls for from-pool', () => {
    const testFromPool = 'from-pool'
    instance.handleZones('test')
    instance.handleFetchSize('test')
    instance.handleClicks(testFromPool)
    expect(testCallbackMap.fetchFromPool).toBeCalled()
  })
  it('should not make callback for from-pool', () => {
    const testFromPool = 'from-pool'
    instance.setState({selectedZone: null})
    expect(instance.state['selectedZone']).toBeNull()
    instance.handleClicks(testFromPool)
    expect(testCallbackMap.fetchFromPool).not.toHaveBeenCalledTimes(2)
  })
  it('should handle different callback calls for by-rg', () => {
    const testByRg = 'by-rg'
    instance.handleRouteGroups('test')
    instance.handleClicks(testByRg)
    expect(testCallbackMap.fetchByRg).toBeCalled()
  })
  it('should not make callback for by-rg', () => {
    const testByRg = 'by-rg'
    instance.setState({selectedRouteGroup: null})
    instance.handleClicks(testByRg)
    expect(testCallbackMap.fetchByRg).not.toHaveBeenCalledTimes(2)
  })
})
