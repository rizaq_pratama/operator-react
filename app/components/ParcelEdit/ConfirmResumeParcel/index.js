// @flow
import React, {useState} from 'react'
import { notification, Modal } from 'antd'
import { compose } from 'redux'
import { T } from '@app/components/Generic/T'
import { injectIntl, intlShape } from 'react-intl'
import NC from '@app/services/api/ninjaControlApi'
import selectors from '@app/containers/ParcelEdit/selectors'
import { connect } from 'react-redux'

// TODO make resume dialog
// check with product, for different scenarios
// different info may be needed to resume
// should be resume parcel, we don't resume job

type Props = {
  parcelTrackingNumber: string,
  visible: boolean,
  onClose?: Function,
  intl: intlShape,
}

export function ResumeParcelModal(props: Props) {
  const [isLoading, setLoading] = useState(false)
  const f = props.intl.formatMessage
  return (
    <Modal
      title={f({id: 'container.parcel-edit.resume-parcel'})}
      visible={props.visible}
      onOk={onSubmit}
      onCancel={props.onClose}
      okButtonProps={{
        disabled: isLoading,
      }}
      cancelButtonProps={{
        disabled: isLoading,
      }}
      closable={!isLoading}
      maskClosable={!isLoading}
    >
      <T id='container.parcel-edit.resume-parcel-description'></T>
    </Modal>
  )

  async function onSubmit() {
    setLoading(true)
    const res = await NC.resumeParcel(props.parcelTrackingNumber)
    if (res.ok !== false) {
      props.onClose()
      notification.success({
        message: 'Saved successfully',
      })
    }
    setLoading(false)
  }
}

const mapStateToProps = state => {
  const parcel = selectors.ParcelSelector(state)
  return { parcelTrackingNumber: parcel? parcel.trackingNumber: null }
}

export default compose(
  injectIntl,
  connect(mapStateToProps)
)(ResumeParcelModal)

