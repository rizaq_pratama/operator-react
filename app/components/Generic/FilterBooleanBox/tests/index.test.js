import React from 'react'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'
import FilterBooleanBox from '../index'
import { shallowWithIntl, TestIntlProviderWrapper } from '@internals/testing/test-helpers'
import 'jest-styled-components'

const testFilterBooleanBoxParams = {
  title: 'Archived Routes',
  property: 'Archived',
}

describe('FilterBooleanBox', () => {
  let wrapper, instance
  beforeEach(() => {
    wrapper = shallowWithIntl(
      <FilterBooleanBox
        title={testFilterBooleanBoxParams.title}
        property={testFilterBooleanBoxParams.property}
        onChange={jest.fn()}
      />
    )
    instance = wrapper.instance()
  })
  xit('matches snapshot', () => {
    const component = renderer.create(
      <TestIntlProviderWrapper>
        <FilterBooleanBox
          title={testFilterBooleanBoxParams.title}
          property={testFilterBooleanBoxParams.property}
          onChange={() => {}}
        />
      </TestIntlProviderWrapper>
    )
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('renders a FilterBooleanBox component', () => {
    const renderedComponent = shallow(
      <TestIntlProviderWrapper>
        <FilterBooleanBox
          title={testFilterBooleanBoxParams.title}
          property={testFilterBooleanBoxParams.property}
          onChange={() => {}}
        />
      </TestIntlProviderWrapper>
    )
    expect(renderedComponent).toBeDefined()
  })
  it('should change switch value', () => {
    expect(instance.state['checked']).toBe(false)
    instance.switchChangeHandler (true)
    expect(instance.state['checked']).toBe(true)
  })
})
