/**
*
* AvstatsCard
*
*/

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Card, Button } from 'antd'
import { T } from '@app/components/Generic/T'
import _ from 'lodash'

const StyledAvstatsCard = styled(Card)`
  width: 896px;
  border-radius: 4px;
  margin-top: 10px !important;
  &&.hidden {
    .ant-card-body{
      display:none;
    }

  }

  .ant-card-head-wrapper{
    width: 100%
  }
  .ant-card-head-title{
    font-weight: bold;
    font-size: 16px;
  }
  .ant-card-extra button{
    border: none;

    &:hover {
      border: none;
    }
  }
`

const ZoneStatItem = styled.div`
  width: 210px;
  float: left;
  margin-top: 5px;
  margin-bottom: 5px;
`

const ZoneNameText = styled.span`
  font-family: Roboto;
  font-size: 14px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #3c6062;
  display: block;
  &&.zone-size {
    font-weight: bold;
    color: #000000;
  }
`
class AvstatsCard extends React.Component {
  constructor (props) {
    super(props)
    this.state = { collapsed: false }
    this.handleCollapsing = this.handleCollapsing.bind(this)
  }

  handleCollapsing (e) {
    this.setState({ collapsed: e })
  }
  renderCollapsible () {
    if (this.state.collapsed) {
      return (<Button size='small' icon='down' onClick={() => this.handleCollapsing(false)} />)
    }
    return (<Button size='small' icon='up' onClick={() => this.handleCollapsing(true)} />)
  }

  render () {
    const { zonestats, collapsible } = this.props
    let className = this.state.collapsed ? 'hidden' : ''

    const zoneItems = _.map(zonestats, (z) => {
      return (
        <ZoneStatItem key={z.name}>
          <ZoneNameText className='zone-name'>
            {z.name.substring(0, 25)}
          </ZoneNameText>
          <ZoneNameText className='zone-size'>
            {z.size} addresses
          </ZoneNameText>
        </ZoneStatItem>
      )
    })

    return (
      <StyledAvstatsCard {..._.omit(this.props, ['collapsible'])}
        id='av-stats-card'
        className={className}
        extra={collapsible && this.renderCollapsible()}
        title={<T id='container.address-verification.no-of-addresses-in-zones' />}>
        {zoneItems}
      </StyledAvstatsCard>
    )
  }
}

AvstatsCard.propTypes = {
  zonestats: PropTypes.array,
  collapsible: PropTypes.bool,
}

AvstatsCard.defaultProps = {
  collapsible: false,
  zonestats: [],
}

export { AvstatsCard }
