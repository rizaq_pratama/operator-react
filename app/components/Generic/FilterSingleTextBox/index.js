//  @flow
import React from 'react'
import { Input } from 'antd'
import FilterContainer from '../FilterContainer'
import type { FilterComponent } from '../Types'
import { injectIntl, intlShape } from 'react-intl'

type FilterSingleTextBoxProps = {
  id: string,
  label: string,
  intl: intlShape
}

class FilterSingleTextBox extends React.Component<FilterSingleTextBoxProps> implements FilterComponent {
  constructor (props) {
    super(props)
    this.state = {
      text: '',
    }
  }
  handleSingleTextChange(e) {
    this.setState({
      text: e.target.value,
    })
  }
  serialize() {
    return this.state.text
  }
  deserialize(text) {
    this.setState({text})
  }
  clear() {
    this.setState({text: ''})
  }
  render () {
    return (
      <FilterContainer label={this.props.label} id={this.props.id}>
        <Input
          placeholder={this.props.intl.formatMessage({id: 'container.filter-box.search-term'})}
          value={this.state.text}
          onChange={this.handleSingleTextChange.bind(this)} />
      </FilterContainer>
    )
  }
}

export default injectIntl(FilterSingleTextBox, {withRef: true})
