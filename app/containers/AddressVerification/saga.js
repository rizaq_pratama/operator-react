import { takeEvery, takeLatest, put, select } from 'redux-saga/effects'
import zoneApi from '@app/services/api/zoneApi'
import addressApi from '@app/services/api/addressApi'
import routeGroupApi from '@app/services/api/routeGroupApi'
import { Types, Creators } from './redux'
import _ from 'lodash'
import { SelectorUtils } from '@nv/react-commons/src/Utils'

const { selector } = SelectorUtils

export function * getZones () {
  try {
    const zones = yield zoneApi.getZones()
    yield put(Creators.getZonesSuccess(zones))
  }catch(err){
    yield put(Creators.getZonesError(err))
  }
}

export function * getAvStats () {
  try{
    const avStats = yield addressApi.getAvStats()
    yield put(Creators.getAvStatsSuccess(avStats))
  }catch(err){
    yield put(Creators.getAvStatsSuccess(err))
  }
}

export function * initAv (action) {
  try {
    yield addressApi.initAv(action.filter)
    yield put(Creators.getAvInitSuccess())
    yield put(Creators.getAvStatsRequest())
  } catch (err) {
    yield put(Creators.getAvInitError(err))
  }
}

export function * getRouteGroups () {
  try {
    const { routeGroups } = yield routeGroupApi.getRouteGroups()
    yield put(Creators.getRouteGroupsSuccess(routeGroups))
  } catch(err){
    yield put(Creators.getRouteGroupsError(err))
  }
  
}

export function * getAddressByRouteGroup (action) {
  yield put(Creators.asShipperAddress(false))
  yield put(Creators.resetAddress())
  try {
    const addresses = yield addressApi.fetchAddressByRouteGroup(action.routeGroupId)
    let addressesForSuggestion = _.clone(addresses)
    if (addresses.length > 10) {
      addressesForSuggestion = _.slice(addresses, 0, 10)
    }
    for (let ad of addressesForSuggestion) {
      const suggested = yield addressApi.searchAddress(generateAddressString(ad))
      yield put(Creators.putSuggestedAddresses(ad.waypointId, suggested))
    }
    yield put(Creators.getAddressesRouteGroupSuccess(addresses))
    yield put(Creators.getAvStatsRequest())
    return addresses
  } catch (err) {
    yield put(Creators.getAddressesRouteGroupError(err))
  }
}

export function * getAddresses (action) {
  yield put(Creators.asShipperAddress(false))
  yield put(Creators.resetAddress())
  try {
    const addresses = yield addressApi.fetchAddress(action.filter)
    let addressesForSuggestion = _.clone(addresses)
    if (addresses.length > 10) {
      addressesForSuggestion = _.slice(addresses, 0, 10)
    }
    for (let ad of addressesForSuggestion) {
      const suggested = yield addressApi.searchAddress(generateAddressString(ad))
      yield put(Creators.putSuggestedAddresses(ad.waypointId, suggested))
    }
    yield put(Creators.getAddressesSuccess(addresses))
    yield put(Creators.getAvStatsRequest())
    return addresses
  } catch (err) {
    yield put(Creators.getAddressesError(err))
  }
}

function generateAddressString (address) {
  return `${address.address1} ${address.address2 || ''} ${address.city || ''} ${address.country || ''} ${address.postcode || ''}`
}

export function * fetchReverifiedAddress () {
  yield put(Creators.asShipperAddress(false))
  yield put(Creators.resetAddress())
  try {
    const addresses = yield addressApi.fetchReverifiedAddress()
    let addressesForSuggestion = _.clone(addresses)
    if (addresses.length > 10) {
      addressesForSuggestion = _.slice(addresses, 0, 10)
    }
    for (let ad of addressesForSuggestion) {
      const suggested = yield addressApi.searchAddress(generateAddressString(ad))
      yield put(Creators.putSuggestedAddresses(ad.waypointId, suggested))
    }
    yield put(Creators.getReverifiedAddressSuccess(addresses))
    yield put(Creators.getAvStatsRequest())
  } catch (err) {
    yield put(Creators.getReverifiedAddressError(err))
  }
}

export function * getSuggestedAddresses (action) {
  const suggestedAddresses = yield select(selector('addressVerification', 'suggestedAddresses')())
  const asShipperAddress = yield select(selector('addressVerification', 'asShipperAddress')())
  try{
    for (let address of action.addresses) {
      const id = asShipperAddress ? address.id : address.waypointId
      if (suggestedAddresses[id]) {
        continue
      }
      const suggested = yield addressApi.searchAddress(generateAddressString(address))
      yield put(Creators.putSuggestedAddresses(id, suggested))
    }
  }catch(err){
    // Do Nothing here, only catch the exception so it won't be propagated to the default saga
  }
}

export function * preAvAssign (action) {
  try {
    yield addressApi.preAvAssign(action.zoneId, action.waypointId)
    yield put(Creators.patchPreAvAssignSuccess(action.waypointId))
  } catch (err) {
    yield put(Creators.patchPreAvAssignFailure())
  }
}

export function * archiveAddress (action) {
  try {
    yield addressApi.archiveOrderJaroScore(action.ojsId)
    yield put(Creators.patchArchiveAddressSuccess(action.ojsId))
  } catch (err) {
    yield put(Creators.patchArchiveAddressError(err))
  }
}

export function * getShipperUnverifiedAddressess () {
  yield put(Creators.clearSuggestedAddress())
  yield put(Creators.asShipperAddress(true))
  yield put(Creators.resetAddress())
  try {
    const shipperAddresses = yield addressApi.getAllUnverifiedShipperAddress()
    _.each(shipperAddresses, (s) => {
      s.lat = s.latitude
      s.lng = s.longitude
    })
    let addressesForSuggestion = _.clone(shipperAddresses)
    if (shipperAddresses.length > 10) {
      addressesForSuggestion = _.slice(shipperAddresses, 0, 10)
    }
    for (let ad of addressesForSuggestion) {
      const suggested = yield addressApi.searchAddress(generateAddressString(ad))
      yield put(Creators.putSuggestedAddresses(ad.id, suggested))
    }
    yield put(Creators.getShipperUnverifiedAddressesSuccess(shipperAddresses))
  } catch (err) {
    yield put(Creators.getShipperUnverifiedAddressesError(err))
  }
}

export function * verifyAddress (action) {
  const { latLong, address, selectedAddress } = action
  const isSameday = yield select(selector('addressVerification', 'isSameday')())
  const asShipperAddress = yield select(selector('addressVerification', 'asShipperAddress')())
  try {
    if (asShipperAddress) {
      const shipperAddressVerPayload = _.assign(
        {},
        _.pick(address, ['id', 'latitude', 'longitude']),
        {
          latitude: latLong ? latLong.lat : selectedAddress.latitude,
          longitude: latLong ? latLong.lng : selectedAddress.longitude,
        }
      )
      const response = yield addressApi.verifyShipperAddress(shipperAddressVerPayload)
      if (response) {
        yield put(Creators.patchVerifyAddressSuccess(address.id))
      }
    } else {
      const addressVerPayload = _.assign({}, _.pick(address, ['addressId', 'orderJaroScoreId', 'waypointId']), {
        sameday: isSameday,
        addressId: selectedAddress ? selectedAddress.id : 'manual',
        latitude: selectedAddress ? selectedAddress.latitude : latLong.lat,
        longitude: selectedAddress ? selectedAddress.longitude : latLong.lng,
      })
      const response = yield addressApi.verifyAddress(addressVerPayload)
      if (response) {
        // create address verification event
        const addressEventPayload = {
          address1: address.address1,
          id: address.waypointId || address.id,
          latitude: selectedAddress ? selectedAddress.latitude : latLong.lat,
          longitude: selectedAddress ? selectedAddress.longitude : latLong.lng,
        }
        yield addressApi.verifyEvent(addressEventPayload)
        yield put(Creators.patchVerifyAddressSuccess(address.waypointId))
      }
    }
  } catch (err) {
    if(typeof err === 'object')
      yield put(Creators.patchVerifyAddressError(JSON.stringify(err)))
    else
      yield put(Creators.patchVerifyAddressError(err))
  }
}

// Individual exports for testing
export default function * defaultSaga () {
  // See example in containers/HomePage/saga.js
  yield takeEvery(Types.GET_ZONES_REQUEST, getZones)
  yield takeEvery(Types.GET_AV_STATS_REQUEST, getAvStats)
  yield takeLatest(Types.GET_AV_INIT_REQUEST, initAv)
  yield takeLatest(Types.GET_ROUTE_GROUPS_REQUEST, getRouteGroups)
  yield takeLatest(Types.GET_ADDRESSES_ROUTE_GROUP_REQUEST, getAddressByRouteGroup)
  yield takeLatest(Types.GET_ADDRESSES_REQUEST, getAddresses)
  yield takeEvery(Types.GET_SUGGESTED_ADDRESSES, getSuggestedAddresses)
  yield takeEvery(Types.PATCH_PRE_AV_ASSIGN_REQUEST, preAvAssign)
  yield takeEvery(Types.PATCH_ARCHIVE_ADDRESS_REQUEST, archiveAddress)
  yield takeEvery(Types.PATCH_VERIFY_ADDRESS_REQUEST, verifyAddress)
  yield takeLatest(Types.GET_SHIPPER_UNVERIFIED_ADDRESSES_REQUEST, getShipperUnverifiedAddressess)
  yield takeLatest(Types.GET_REVERIFIED_ADDRESS_REQUEST, fetchReverifiedAddress)
}
