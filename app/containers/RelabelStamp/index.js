//  @flow
import React from 'react'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { T } from '@app/components/Generic/T'
import { Input, Button, message } from 'antd'
import { compose } from 'redux'
import { injectIntl, intlShape } from 'react-intl'
import injectSaga from 'utils/injectSaga'
import { Creators } from './redux'
import styled from 'styled-components'
import saga from './saga'

const StyledButton = styled(Button)`
  && {
    margin-top: 1.5em;
  }
`

const OuterDivStyling = {
  margin: '2em',
}

type RelabelStampProps = {
  getOrderDetailsAndPrint: Function,
  intl: intlShape
}

export class RelabelStamp extends React.Component<RelabelStampProps> {
  constructor (props) {
    super(props)
    this.state = {
      scan: '',
    }
  }
  onScanChange(e) {
    this.setState({
      scan: e.target.value,
    })
  }
  getOrderDetailsAndPrint() {
    this.props.getOrderDetailsAndPrint(this.state.scan, this.showSuccessMessage.bind(this))
  }
  showSuccessMessage() {
    message.success(this.props.intl.formatMessage({ id: 'container.relabel-stamp.print-success' }))
  }
  render() {
    return(
      <div style={OuterDivStyling}>
        <h3>
          <T id={'container.relabel-stamp.relabel-stamp'} />
        </h3>
        <Input
          placeholder="Scan"
          onChange={this.onScanChange.bind(this)} />
        <StyledButton
          type='primary'
          onClick={this.getOrderDetailsAndPrint.bind(this)}>
          <T id={'container.relabel-stamp.relabel-stamp'} />
        </StyledButton>
      </div>
    )
  }
}

const mapStateToProps = createStructuredSelector({})

function mapDispatchToProps (dispatch) {
  return {
    dispatch,
    getOrderDetailsAndPrint: (scan, callback) => dispatch(Creators.getAndPrintOrderRequest(scan, callback)),
  }
}

const withConnect = connect(mapStateToProps, mapDispatchToProps)

const withSaga = injectSaga({ key: 'relabelStamp', saga })

export default compose(
  withSaga,
  withConnect,
  injectIntl
)(RelabelStamp)
