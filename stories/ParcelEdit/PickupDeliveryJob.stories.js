import React from 'react'
import PickupDeliveryJob from '@app/components/ParcelEdit/PickupDeliveryJob'
import { storiesOf } from '@storybook/react'

const samplePickupJobInfo = {
  jobStatus: 'Completed',
  shipperName: 'Amazon.com Services, Inc.',
  phoneNumber: '+6566028271',
  email: 'support@ninjavan.co',
  address: '30 Jln Kilang Barat Singapore 15936323 SG',
  routeInformation: {
    routeId: 567162,
    routeDate: '2018-10-09',
    waypointId: 44519426,
    driverId: 100083,
    failedAttempts: 0,
    instructions: null,
  },
}

const sampleDeliveryJobInfo = {
  jobStatus: 'Pending',
  customerName: 'Min Thu Ya Hlaing',
  phoneNumber: '+6585881948',
  email: 'minthuya@hotmail.com',
  address: 'Blk 62, #11-27 Woodlands Drive 16 Singapore',
  routeInformation: {
    routeId: null,
    routeDate: null,
    waypointId: null,
    driverId: null,
    failedAttempts: null,
    instructions: null,
  },
}

storiesOf('ParcelEdit', module).add('PickupDeliveryJob', () => (
  <PickupDeliveryJob
    pickupJobInfo={samplePickupJobInfo}
    deliveryJobInfo={sampleDeliveryJobInfo} />
))
