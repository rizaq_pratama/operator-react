import React from 'react'
import { shallowWithIntl } from '@internals/testing/test-helpers'
import { NvlegacyMonitor } from '../index'
import { notification } from 'antd'

const successSpy = jest.spyOn(notification, 'success')
const errorSpy = jest.spyOn(notification, 'error')

describe('<NvlegacyMonitor />', () => {

  let wrapper, instance
  beforeEach(() => {
    wrapper = shallowWithIntl(
      <NvlegacyMonitor
        successIndicator={false}
        successMessage={'test'}
        successDescription={'test descr'}
        errorIndicator={false}
        errorMessage={'test'}
       />
    )
    instance = wrapper.instance()
  })
  it('Should render without error', () => {
    expect(wrapper).toBeDefined()
  })
  it('should send a success notification', () => {
    wrapper.setProps({
      successIndicator: false,
    })
    instance.componentDidUpdate({
      successIndicator: true,
      successMessage: 'test',
      successDescription: 'test descr',
      errorIndicator: false,
    })
    expect(successSpy).toBeCalled()
  })
  it('should send a error notification', () => {
    wrapper.setProps({
      errorIndicator: false,
    })
    instance.componentDidUpdate({
      successIndicator: false,
      errorIndicator: true,
      errorMessage: 'test',
    })
    expect(errorSpy).toBeCalled()
  })
})
