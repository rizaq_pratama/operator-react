import IFrameHelpers from '@nv/react-commons/src/Migration_corev2/IFrameHelpers'
import HubApi from '../hubApi'

jest.mock('@nv/react-commons/src/Migration_corev2/IFrameHelpers')

const fakeRequest = jest.fn()

IFrameHelpers.IFrameClient = function () {
  this.request = fakeRequest
}

describe('hubApi', () => {
  it('getHubs', () => {
    HubApi.getHubs()
    expect(fakeRequest).toBeCalledWith('hub_getHubs')
  })
})
