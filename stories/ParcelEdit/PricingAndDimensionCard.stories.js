import React from 'react'
import PricingAndDimensionCard from '@app/components/ParcelEdit/PricingAndDimensionCard'
import { storiesOf } from '@storybook/react'

storiesOf('ParcelEdit', module).add('PricingAndDimensionCard', () => (
  <PricingAndDimensionCard
    deliveryFee={2.81}
    codFee={undefined}
    insuranceFee={0}
    handlingFee={0.1}
    gst={0.06}
    insuredValue={0.1}
    parcelSize='M'
    weight={0.1}
    dimensions={{ width: 1, height: 2, length: 3 }}
    country='SG'
    onEditPricingAndDimensions={() => window.alert('Edit Pricing Button Clicked')} />
))
