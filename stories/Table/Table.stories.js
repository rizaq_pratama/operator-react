import React from 'react'
import _ from 'lodash'
import { storiesOf } from '@storybook/react'
import styled from 'styled-components'
import { message, Button, Popconfirm } from 'antd'

import Table, { ActionsCellWidthByButton } from '@app/components/Table'

const StyledDiv = styled.div`
  height: 400px;
  width: 100%;
`

const StyledTable = styled(Table)`
  .virtualized-table {
    .table-row {
      &.even {
        background-color: lightyellow;
      }

      &.row-custom-style {
        background-color: lightcyan;

        .cell {
          font-weight: 500;
        }
      }

      .table-column-header {
        .age {
          text-align: center;
        }
      }

      .table-column {
        &.age {
          text-align: center;
          background-color: lightpink;
        }

        .cell-custom-style {
          background-color: lightblue;
        }
      }
    }
  }
`

const columns = [{
  key: 'id',
  label: 'id',
  width: 60,
}, {
  key: 'name',
  label: 'Name',
  width: 300,
}, {
  key: 'age',
  label: 'Age',
  width: 80,
}, {
  key: 'address1',
  label: 'Address1',
  width: 250,
}, {
  key: 'address2',
  label: 'Address2',
  width: 250,
}, {
  key: 'city',
  label: 'City',
  width: 100,
}, {
  key: 'postcode',
  label: 'Postcode',
  width: 100,
}, {
  key: 'country',
  label: 'Country',
  width: 100,
}]
const ROW_PER_PAGE = 100

function buildData(page, totalRows = ROW_PER_PAGE) {
  const dataSource = [];
  _.forEach(Array(totalRows), (value, index) => {
    let name = 'Lorem ipsum'
    switch(_.random(1, 5)) {
      case 1:
        name += ' dolor sit amet'
        break
      case 2:
        name += ' dolor sit amet, consectetur adipiscing elit'
        break
      case 3:
        name += ' dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor'
        break
      case 4:
        name += ' dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore'
        break
      case 5:
        name += ' dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
        break
      default:
    }

    dataSource.push({
      id: index + 1 + (totalRows * (page - 1)),
      name: name,
      age: _.sample([28, 29, 30, 31, 32, null]),
      address1: 'Address line 1',
      address2: _.sample(['address line 2', null]),
      city: _.sample(['SG', null, 'Jakarta']),
      postcode: _.toString(_.random(100000, 999999)),
      country: _.sample(['Singapore', 'Malaysia', 'Indonesia']),
    });
  });

  return dataSource;
}

class BasicTable extends React.Component {
  render() {
    const theColumns = _.cloneDeep(columns)
    const list = _.cloneDeep(buildData(1))

    return (
      <Table
        height={400}
        columns={theColumns}
        list={list} />
    )
  }
}

class FixedRowHeightTable extends React.Component {
  render() {
    const theColumns = _.cloneDeep(columns)
    const list = _.cloneDeep(buildData(1))

    return (
      <Table
        height={400}
        rowHeight={40}
        columns={theColumns}
        list={list} />
    )
  }
}

class FillContainerHeightTable extends React.Component {
  render() {
    const theColumns = _.cloneDeep(columns)
    const list = _.cloneDeep(buildData(1))

    return (
      <StyledDiv>
        <Table
          columns={theColumns}
          list={list} />
      </StyledDiv>
    )
  }
}

class CustomStylingTable extends React.Component {
  render() {
    const theColumns = _.cloneDeep(columns)
    const list = _.cloneDeep(buildData(1))

    // override default table styling
    // refer StyledTable `.even` class at the top

    // row custom style
    const row2 = list[1]
    row2.rowClassName = 'row-custom-style'

    // column custom style
    // refer StyledTable `.age` class at the top

    // single cell custom style
    const row1 = list[0]
    row1.cellClassName = {
      name: 'cell-custom-style',
    }

    return (
      <StyledTable
        height={400}
        columns={theColumns}
        list={list} />
    )
  }
}

class ShowCounterTable extends React.Component {
  render() {
    const theColumns = _.cloneDeep(columns)
    const list = _.cloneDeep(buildData(1))

    return (
      <Table
        height={400}
        showCounter={true}
        columns={theColumns}
        list={list} />
    )
  }
}

class FixedRightTable extends React.Component {
  render() {
    const theColumns = _.cloneDeep(columns)
    const list = _.cloneDeep(buildData(1));

    const cityColumn = _.find(theColumns, ['key', 'city'])
    cityColumn.isFixedRight = true

    const countryColumn = _.find(theColumns, ['key', 'country'])
    countryColumn.isFixedRight = true

    return (
      <StyledDiv>
        <Table
          rowHeight={80}
          columns={theColumns}
          list={list} />
      </StyledDiv>
    )
  }
}

class CheckboxTable extends React.Component {
  onSelectableChange(ids) {
    message.info(`${_.size(ids)} selected`)
  }

  render() {
    const theColumns = _.cloneDeep(columns)
    const list = _.cloneDeep(buildData(1));

    return (
      <StyledDiv>
        <Table
          rowId="id"
          selectable={true}
          onSelectableChange={this.onSelectableChange}
          rowHeight={80}
          columns={theColumns}
          list={list} />
      </StyledDiv>
    )
  }
}

class SortableTable extends React.Component {
  countrySortFn({ list, sortBy }) {
    // sort by country + postcode in asc order
    return _.orderBy(list, ['country', 'postcode'], [sortBy, 'asc'])
  }

  render() {
    const theColumns = _.cloneDeep(columns)
    const list = _.cloneDeep(buildData(1));

    // disable sort
    const cityColumn = _.find(theColumns, ['key', 'city'])
    cityColumn.sortable = false

    // custom sort function
    const countryColumn = _.find(theColumns, ['key', 'country'])
    countryColumn.sortFn = this.countrySortFn

    return (
      <StyledDiv>
        <Table
          rowHeight={80}
          columns={theColumns}
          list={list} />
      </StyledDiv>
    )
  }
}

class FilterableTable extends React.Component {
  countryFilterFn({ list, text }) {
    const processedText = _.toLower(_.trim(text))
    return _.filter(list, (row) => {
      return _.toLower(row.city).indexOf(processedText) > -1 ||
        _.toLower(row.country).indexOf(processedText) > -1
    })
  }

  render() {
    const theColumns = _.cloneDeep(columns)
    const list = _.cloneDeep(buildData(1));

    // disable filterable
    const cityColumn = _.find(theColumns, ['key', 'city'])
    cityColumn.filterable = false

    // disable sortable
    const address2Column = _.find(theColumns, ['key', 'address2'])
    address2Column.sortable = false

    // disable highlightable - useful if content contains DOM/React component
    address2Column.highlightable = false

    // custom filterable
    const countryColumn = _.find(theColumns, ['key', 'country'])
    countryColumn.filterFn = this.countryFilterFn

    return (
      <StyledDiv>
        <Table
          rowHeight={80}
          columns={theColumns}
          list={list} />
      </StyledDiv>
    )
  }
}

class LoadDataOnDemandTable extends React.Component {
  constructor(props) {
    super(props)

    this.page = 0

    this.onLoadMore = this.onLoadMore.bind(this)
  }

  onLoadMore(prevList, onLoadMoreCompleted) {
    // if promise return empty response or get rejected
    // then Table component will stop continuing to load more data
    return new Promise((resolve, reject) => {
      if(this.page == 5) {
        message.error('promise throwing error (rejected), will stop calling promise')
        reject(null)
        return
      }

      this.page += 1
      setTimeout(() => {
        const response = buildData(this.page)

        if(_.isEmpty(response)) {
          message.info('response is empty, will stop calling promise')
        } else {
          message.info(`Page ${this.page} loaded`)
        }

        const currentList = _.concat(prevList, response)
        message.info(`has ${_.size(currentList)} rows in the table now`)
        resolve(response)
      }, _.random(0, 5000))
    })
  }

  render() {
    const theColumns = _.cloneDeep(columns)

    return (
      <StyledDiv>
        <Table
          rowHeight={80}
          columns={theColumns}
          onLoadMore={this.onLoadMore} />
      </StyledDiv>
    )
  }
}

class LoadDataOnDemandSelfControlTable extends React.Component {
  constructor(props) {
    super(props)

    this.page = 0
    this.onLoadMoreModes = {
      PROMISE: 'onLoadMore',
      NO_PROMISE: 'onLoadMoreNoPromise',
    }
    this.onLoadMoreMode = _.sample([
      this.onLoadMoreModes.PROMISE,
      this.onLoadMoreModes.NO_PROMISE,
    ])

    this.state = {
      list: [],
      loading: false,
    }

    this.onLoadMore = this.onLoadMore.bind(this)
    this.onLoadMoreNoPromise = this.onLoadMoreNoPromise.bind(this)

    message.info(`Load More mode proxy to ${this.onLoadMoreMode} function`)
  }

  onLoadMoreProxy() {
    if(this.onLoadMoreMode === this.onLoadMoreModes.PROMISE) {
      return this.onLoadMore
    }

    return this.onLoadMoreNoPromise
  }

  onLoadMore(prevList, onLoadMoreCompleted) {
    return new Promise((resolve, reject) => {
      // if pass in boolean false into onLoadMoreCompleted function
      // then Table component will stop continuing to load more data
      if(this.page == 5) {
        message.error('promise throwing error (rejected), will stop calling promise')
        onLoadMoreCompleted(false)
        return
      }

      this.page += 1
      setTimeout(() => {
        const response = buildData(this.page)

        if(_.isEmpty(response)) {
          message.info('response is empty, will stop calling promise')
          onLoadMoreCompleted(false)
          return
        }

        message.info(`Page ${this.page} loaded`)
        onLoadMoreCompleted()

        const currentList = _.concat(prevList, response)
        message.info(`has ${_.size(currentList)} rows in the table now`)

        this.setState({ list: currentList })
      }, _.random(0, 500))
    })
  }

  onLoadMoreNoPromise(prevList, onLoadMoreCompleted) {
    this.setState({ loading: true })

    this.page += 1
    setTimeout(() => {
      const response = buildData(this.page)
      message.info(`Page ${this.page} loaded`)

      const currentList = _.concat(prevList, response)
      message.info(`has ${_.size(currentList)} rows in the table now`)

      this.setState({
        list: currentList,
        loading: false,
      })
    }, 1000)
  }

  render() {
    const theColumns = _.cloneDeep(columns)

    return (
      <StyledDiv>
        <Table
          rowHeight={80}
          columns={theColumns}
          onLoadMore={this.onLoadMoreProxy()}
          loading={this.state.loading}
          list={this.state.list} />
      </StyledDiv>
    )
  }
}

class OnRowsRenderedTable extends React.Component {
  constructor(props) {
    super(props)

    this.columns = _.cloneDeep(columns)
    this.list = _.cloneDeep(buildData(1))
    this.state = {
      renderedRows: [],
    }

    this.onRowsRendered = this.onRowsRendered.bind(this)
  }

  onRowsRendered(rows) {
    this.setState({
      renderedRows: rows,
    })
  }

  render() {
    return (
      <StyledDiv>
        <div>
          Rendered row by Id: {_.join(_.map(this.state.renderedRows, 'id'), ', ')}
        </div>
        <Table
          rowHeight={80}
          columns={this.columns}
          list={this.list}
          onRowsRendered={this.onRowsRendered} />
      </StyledDiv>
    )
  }
}

class ActionsTable extends React.Component {
  constructor(props) {
    super(props)

    this.page = 1
    this.state = {
      list: _.cloneDeep(buildData(1)),
    }

    this.actionsContentRenderer = this.actionsContentRenderer.bind(this)
  }

  onReloadTable() {
    this.setState({
      list: [],
    })

    message.info('Table data Reloaded')
  }

  onAddNewRows() {
    this.page += 1

    let list = _.cloneDeep(this.state.list) || []
    list = _.concat(list, _.cloneDeep(buildData(this.page)))

    this.setState({
      list: list,
    })

    message.info(`${ROW_PER_PAGE} rows added`)
  }

  onEditClick(id) {
    message.info(`Edit Button for id '${id}' triggered`)
    setTimeout(() => {
      const list = _.cloneDeep(this.state.list)
      const row = _.find(list, ['id', id])
      row.name = 'Name updated'

      this.setState({
        list: list,
      })

      message.info(`Row for id '${id}' updated`)
    }, 1000)
  }

  onDeleteClick(id) {
    message.info(`Delete Button for id '${id}' triggered`)
    setTimeout(() => {
      const list = _.cloneDeep(this.state.list)
      _.remove(list, ['id', id])

      this.setState({
        list: list,
      })

      message.info(`Row for id '${id}' deleted`)
    }, 1000)
  }

  actionsContentRenderer(id) {
    return (
      <div>
        <Button
          type="primary"
          icon="edit"
          onClick={() => this.onEditClick(id)} />

        <Popconfirm title="Are you sure？" okText="Yes" cancelText="No"
          onConfirm={() => this.onDeleteClick(id)}>
          <Button
            type="danger"
            icon="delete" />
        </Popconfirm>
      </div>
    )
  }

  render() {
    const theColumns = _.cloneDeep(columns)

    const actions = {
      width: ActionsCellWidthByButton.TWO,
      contentRenderer: this.actionsContentRenderer,
    }

    return (
      <StyledDiv>
        <Button
          style={{ marginBottom: '4px' }}
          type="primary"
          onClick={() => this.onReloadTable()}>
          Reload Table Data
        </Button>

        <Button
          style={{ marginLeft: '4px', marginBottom: '4px' }}
          type="primary"
          onClick={() => this.onAddNewRows()}>
          Add new row
        </Button>

        <Table
          rowId="id"
          actions={actions}
          rowHeight={80}
          columns={theColumns}
          list={this.state.list} />
      </StyledDiv>
    )
  }
}

class OperatorStandardTable extends React.Component {
  constructor(props) {
    super(props)

    this.reloadId = 1
    this.page = 0
    this.state = {
      list: [],
    }

    this.onLoadMore = this.onLoadMore.bind(this)
    this.actionsContentRenderer = this.actionsContentRenderer.bind(this)
  }

  onLoadMore(prevList, onLoadMoreCompleted) {
    return new Promise((resolve, reject) => {
      // if pass in boolean false into onLoadMoreCompleted function
      // then Table component will stop continuing to load more data
      if(this.page == 5) {
        message.error('promise throwing error (rejected), will stop calling promise')
        onLoadMoreCompleted(false)
        return
      }

      this.page += 1
      setTimeout(() => {
        const response = buildData(this.page)

        message.info(`Page ${this.page} loaded`)
        onLoadMoreCompleted()

        const currentList = _.concat(prevList, response)
        message.info(`has ${_.size(currentList)} rows in the table now`)

        this.setState({ list: currentList })
      }, _.random(0, 500))
    })
  }

  onSelectableChange(ids) {
    message.info(`${_.size(ids)} selected`)
  }

  onReloadTable() {
    this.setState({
      reloadId: this.reloadId,
      list: [],
    })

    this.reloadId += 1
    message.info('Table data Reloaded')
  }

  onAddNewRows() {
    this.page += 1

    let list = _.cloneDeep(this.state.list) || []
    list = _.concat(list, _.cloneDeep(buildData(this.page)))

    this.setState({
      list: list,
    })

    message.info(`${ROW_PER_PAGE} rows added`)
  }

  onEditClick(id) {
    message.info(`Edit Button for id '${id}' triggered`)
    setTimeout(() => {
      const list = _.cloneDeep(this.state.list)
      const row = _.find(list, ['id', id])
      row.name = 'Name updated'

      this.setState({
        list: list,
      })

      message.info(`Row for id '${id}' updated`)
    }, 1000)
  }

  onDeleteClick(id) {
    message.info(`Delete Button for id '${id}' triggered`)
    setTimeout(() => {
      const list = _.cloneDeep(this.state.list)
      _.remove(list, ['id', id])

      this.setState({
        list: list,
      })

      message.info(`Row for id '${id}' deleted`)
    }, 1000)
  }

  actionsContentRenderer(id) {
    return (
      <div>
        <Button
          type="primary"
          icon="edit"
          onClick={() => this.onEditClick(id)} />

        <Popconfirm title="Are you sure？" okText="Yes" cancelText="No"
          onConfirm={() => this.onDeleteClick(id)}>
          <Button
            type="danger"
            icon="delete" />
        </Popconfirm>
      </div>
    )
  }

  render() {
    const theColumns = _.cloneDeep(columns)

    const countryColumn = _.find(theColumns, ['key', 'country'])
    countryColumn.isFixedRight = true

    const actions = {
      width: ActionsCellWidthByButton.TWO,
      contentRenderer: this.actionsContentRenderer,
    }

    return (
      <StyledDiv>
        <Button
          style={{ marginBottom: '4px' }}
          type="primary"
          onClick={() => this.onReloadTable()}>
          Reload Table Data
        </Button>

        <Button
          style={{ marginLeft: '4px', marginBottom: '4px' }}
          type="primary"
          onClick={() => this.onAddNewRows()}>
          Add new row
        </Button>

        <Table
          rowId="id"
          showCounter={true}
          selectable={true}
          onSelectableChange={this.onSelectableChange}
          actions={actions}
          rowHeight={80}
          columns={theColumns}
          list={this.state.list}
          reloadId={this.state.reloadId}
          onLoadMore={this.onLoadMore} />
      </StyledDiv>
    )
  }
}

storiesOf('Table', module).add('Basic', () => <BasicTable />)
storiesOf('Table', module).add('Fixed Row Height', () => <FixedRowHeightTable />)
storiesOf('Table', module).add('Fill Container Height', () => <FillContainerHeightTable />)
storiesOf('Table', module).add('Custom Styling', () => <CustomStylingTable />)
storiesOf('Table', module).add('Show Counter', () => <ShowCounterTable />)
storiesOf('Table', module).add('Fixed Right Columns', () => <FixedRightTable />)
storiesOf('Table', module).add('Checkbox', () => <CheckboxTable />)
storiesOf('Table', module).add('Custom Sortable Column', () => <SortableTable />)
storiesOf('Table', module).add('Custom Filterable Column', () => <FilterableTable />)
storiesOf('Table', module).add('On Rows Rendered', () => <OnRowsRenderedTable />)
storiesOf('Table', module).add('Actions Column', () => <ActionsTable />)
storiesOf('Table', module).add('Load Data on Demand', () => <LoadDataOnDemandTable />)
storiesOf('Table', module).add('Load Data on Demand Self-Control', () => <LoadDataOnDemandSelfControlTable />)
storiesOf('Table', module).add('Operator Standard', () => <OperatorStandardTable />)