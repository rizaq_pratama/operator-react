import React from 'react'
import { mount, shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { EditParcelDetailsModal } from '../index'
import { TestIntlProviderWrapper } from '@internals/testing/test-helpers'
import NC from '@app/services/api/ninjaControlApi'

jest.mock('@app/services/api/ninjaControlApi', () => {
  return {
    updateParcelDetails: jest.fn(() => ({ok: true})),
    updateOrderDetails: jest.fn(() => ({ok: true})),
  }
})

describe('EditParcelDetailsModal', () => {
  let parcelInfo, formData
  beforeEach(() => {
    formData = {
      stampTrackingId: 'KDKSLSL82892929',
      deliveryType: 'Same day',
    }
    parcelInfo = {
      id: 123,
      orderId: 456,
      parcelTrackingId: 'NDKS92883JKLS',
      orderSource: 'API',
      createdOn: '2018-12-12 13:42:23+08:00',
    }
  })
  it('renders a EditParcelDetailsModal component', () => {
    const wrapper = shallow(
      <EditParcelDetailsModal
        visible
        parcelInformation={parcelInfo}
        parcelFormData={formData}
      />
    )
    expect(toJson(wrapper)).toMatchSnapshot()
  })
  describe('full render', () => {
    let wrapper

    afterEach(() => wrapper.unmount())

    it('should not call API when value is empty', async () => {
      wrapper = mount(
        <TestIntlProviderWrapper>
          <EditParcelDetailsModal
            visible
            onClose={jest.fn()}
            parcelInformation={parcelInfo}
            parcelFormData={{}}/>
        </TestIntlProviderWrapper>
      )
      wrapper.find('.ant-modal-footer [type="primary"]').simulate('click')
      expect(NC.updateParcelDetails).not.toHaveBeenCalled()
      expect(NC.updateOrderDetails).not.toHaveBeenCalled()
      await new Promise(setTimeout)
    })

    it('should submit with good values', async () => {
      wrapper = mount(
        <TestIntlProviderWrapper>
          <EditParcelDetailsModal
            visible
            onClose={jest.fn()}
            parcelInformation={parcelInfo}
            parcelFormData={formData}/>
        </TestIntlProviderWrapper>
      )
      wrapper.find('.ant-modal-footer [type="primary"]').simulate('click')
      expect(NC.updateParcelDetails).toHaveBeenCalled()
      expect(NC.updateOrderDetails).toHaveBeenCalled()
      await new Promise(setTimeout)
    })
  })
})
