/**
 * This is where UI specific data are generated from API responses
 */
import { createSelector } from 'reselect'
import moment from 'moment'
import { fromJS } from 'immutable'
import JobType  from '@nv/react-commons/src/Migration_corev2/Constants/JobType'
import { pick, formatParcelStatus } from '@app/utils/helpers'
import get from 'lodash/get'

const ParcelEditSelector = state => state.get('parcelEdit', fromJS({})).toJS()
const ParcelSelector = createSelector(ParcelEditSelector, state =>  state.parcel || null)
const OrderSelector = createSelector(ParcelEditSelector, state => state.order || null)
const JobsSelector = createSelector(ParcelEditSelector, state => state.jobs || [])
const TicketsSelector = createSelector(ParcelEditSelector, state => state.tickets|| [])
const DestinationZoneSelector = createSelector(ParcelEditSelector, state => state.destinationZone || null)
const EventsSelector = createSelector(ParcelEditSelector, state => state.events|| [])

const TransitJobSelector = createSelector(JobsSelector,
  jobs => jobs.find(job => job.jobType === JobType.PARCEL_TRANSIT))

const PickupJobSelector = createSelector(
  [ParcelSelector, JobsSelector],
  (parcel, jobs) => {
    if (parcel === null || jobs.length === 0) {
      return null
    }
    const { latestPickupJob } = parcel.parcelMetadata || {}
    return jobs.find(job => job.jobType === JobType.PARCEL_PICKUP && job.id === latestPickupJob)
  }
)
const DeliveryJobSelector = createSelector(
  [ParcelSelector, JobsSelector],
  (parcel, jobs) => {
    if (parcel === null || jobs.length === 0) {
      return null
    }
    const { latestDeliveryJob } = parcel.parcelMetadata || {}
    return jobs.find(job => job.jobType === JobType.PARCEL_DELIVERY && job.id === latestDeliveryJob)
  }
)

// mappings
const JobToRouteMappingSelector = createSelector(ParcelEditSelector, state => state.jobToRouteMapping || {})
const RouteToDriverMappingSelector = createSelector(ParcelEditSelector, state => state.routeToDriverMapping || {})
const JobToProofsMappingSelector = createSelector(ParcelEditSelector, state => state.jobToProofsMapping || {})

const PickupRouteSelector = createSelector(
  [PickupJobSelector, JobToRouteMappingSelector],
  (pickupJob, mapping) => pickupJob? mapping[pickupJob.__uid__] : null
)
const DeliveryRouteSelector = createSelector(
  [DeliveryJobSelector, JobToRouteMappingSelector],
  (deliveryJob, mapping) => deliveryJob? mapping[deliveryJob.__uid__] : null
)

const LatestEventSelector  = createSelector(EventsSelector, events => events.length? events[0] : null)
const DaysSinceOrderCreatedSelector = createSelector(OrderSelector, order => {
  if (order) {
    const m = moment.parseZone(order.createdAt.replace(/z$/i, ''), moment.ISO_8601)
    return moment().diff(m, 'days')
  }
  return 0
})
const DaysSinceInboundedSelector = createSelector([], () => {
  /*
   * HUB-340
   * days since inbounded = days from date of first inbound scan to today's date.
   * count stops increasing upon delivery success
   */
  return null
})

const CurrentJobSelector = createSelector(
  [ParcelSelector, JobsSelector],
  (parcel, jobs) => {
    if (!parcel || jobs.length === 0) {
      return null
    }
    const { parcelMetadata } = parcel
    if (parcelMetadata) {
      const currentJobId = parcelMetadata.latestJob
      const currentJobType = parcelMetadata.latestJobType
      return jobs.find(job => job.id === currentJobId && job.jobType === currentJobType)
    }
    return null
  })

// information bar view
const InformationBarSelector = createSelector(
  [ParcelSelector, JobsSelector, OrderSelector, DeliveryJobSelector],
  (parcel, jobs, order, delivery) => {
    return {
      trackingNumber: parcel? parcel.trackingNumber: null,
      priorityLevel: jobs.reduce((maxPriority, job) => {
        return Math.max(maxPriority, job.priorityLevel)
      }, 0),
      // NOTE assumption is cod is always fully collected
      cod: jobs.reduce((codSum, job) => {
        codSum += (job.codReqAmount || 0)
        return codSum
      }, 0),
      // TODO missing, order level
      // should get from delivery job
      currency: 'SGD',
      // TODO missing, parcel level?
      // will add
      isRts: delivery? delivery.isRts : false,
      status: parcel? parcel.granularStatus: null,
    }
  })

// general info view
const GeneralInfoViewSelector = createSelector(
  [ParcelSelector, JobsSelector, OrderSelector, CurrentJobSelector, DestinationZoneSelector, JobToRouteMappingSelector, LatestEventSelector],
  (parcel, jobs, order, currentJob, zone, jobRouteMapping, latestEvent) => {
    const jobRoute = currentJob? jobRouteMapping[currentJob.__uid__]: null
    return {
      // TODO missing
      // will check to put into parcel
      stampId: parcel? parcel.stampId: null,
      currentJob,
      currentHub: jobRoute? jobRoute.route.hubId: null,
      destinationHub: zone? zone.hubId: null,
      orderTrackingId: order? order.trackingNumber: null,
      latestEvent: latestEvent? latestEvent.eventName: null,
      zone: jobRoute? jobRoute.route.zoneId: null,
    }
  })

const ItemSummaryViewSelector = createSelector(
  [OrderSelector, ParcelSelector],
  (order, parcel) => {
  return {
    id: parcel? parcel.id: null,
    shipperId: order? order.shipperId: null,
    orderSource: get(order, 'internalReference.source', null),
    shipperReference: get(order,'reference.merchantOrderNumber', null),
    international: false,
    // TODO call order parcels
    // ask steve
    multiParcel: true,
    // FIXME
    createdTimestamp: parcel && parcel.createdAt? moment.parseZone(parcel.createdAt).valueOf() : Date.now(),
    // TODO find out where to get this
    chargeTo: 'chargeTo',
    deliveryType: order? order.serviceLevel: null,
    // TODO order doesn't provide this
    // check which field in db
    shipperOrderData: 'key:value',
    orderType: order? order.serviceType: null,
  }
})
const PricingAndDimensionViewSelector = createSelector(
  [ParcelSelector], (parcel) => {
    // TODO find out fees
    return {
      deliveryFee: 1,
      codFee: 2,
      insuranceFee: 0.34,
      handlingFee: 0.33,
      gst: 0.123,
      insuredValue: parcel? parcel.insuredValue: null,
      weight: parcel? parcel.weight: null,
      country: 'SG',
      dimensions: {
        width: parcel? parcel.width: null,
        height: parcel? parcel.height: null,
        length: parcel? parcel.length: null,
      },
    }
  })

const RecoveryInfoViewSelector = createSelector(
  [TicketsSelector, DaysSinceOrderCreatedSelector, DaysSinceInboundedSelector],
  (tickets, daysSinceOrderCreated, daysSinceInbounded) => {
    return {
      tickets,
      daysSinceOrderCreated,
      daysSinceInbounded,
    }
})

const PickupDeliveryJobViewSelector = createSelector(
  [PickupJobSelector, DeliveryJobSelector, PickupRouteSelector, DeliveryRouteSelector],
  (pickupJob, deliveryJob, pickupRoute, deliveryRoute) => {

    return {
      pickupJobInfo: {
        jobStatus: pickupJob? pickupJob.status: null,
        shipperName: pickupJob? pickupJob.name: null,
        phoneNumber: pickupJob? pickupJob.contact: null,
        email: pickupJob? pickupJob.email: null,
        address: pickupJob? pickupJob.address1 + ' ' + pickupJob.address2: null,
        routeInformation: {
          routeId: pickupRoute? pickupRoute.route.id: null,
          routeDate: pickupRoute? pickupRoute.route.date: null,
          waypointId: pickupRoute? pickupRoute.waypoint.id: null,
          driverId: pickupRoute? pickupRoute.route.driverId: null,
          failedAttempts: 2, // TODO find where
          instructions: pickupRoute? pickupRoute.route.comments: null,
        },
      },
      deliveryJobInfo: {
        jobStatus: deliveryJob? deliveryJob.status: null,
        shipperName: deliveryJob? deliveryJob.name: null,
        phoneNumber: deliveryJob? deliveryJob.contact: null,
        email: deliveryJob? deliveryJob.email: null,
        address: deliveryJob? deliveryJob.address1 + ' ' + deliveryJob.address2: null,
        routeInformation: {
          routeId: deliveryRoute? deliveryRoute.route.id: null,
          routeDate: deliveryRoute? deliveryRoute.route.date: null,
          waypointId: deliveryRoute? deliveryRoute.waypoint.id: null,
          driverId: deliveryRoute? deliveryRoute.route.driverId: null,
          failedAttempts: 2, // TODO find where
          instructions: deliveryRoute? deliveryRoute.route.comments: null,
        },
      },
    }
  })
const JobHistoryViewSelector = createSelector(
  [JobsSelector, JobToRouteMappingSelector, JobToProofsMappingSelector, RouteToDriverMappingSelector],
  (jobs, jobRouteMapping, jobProofsMapping, routeDriverMapping) => {
    return {
      jobs: jobs.map(job => {
        const jobRoute = jobRouteMapping[job.__uid__]
        const jobProofs = jobProofsMapping[job.__uid__]
        const jobProof = jobProofs && jobProofs.length? jobProofs[0] : null
        const endTime = moment.parseZone(job.endTime, 'HH:mm:ssZ')

        const driver = jobRoute? routeDriverMapping[jobRoute.route.id] : null
        // TODO driver or recovery official
        const assignee = driver? driver.id : null

        return {
          endTime: endTime.isValid()? endTime.format('hh:mm A'): null,
          jobType: formatParcelStatus(job.jobType),
          jobStatus: job.status,
          jobId: job.id,
          assignedTo: assignee,
          priorityLevel: job.priorityLevel,
          routeId: jobRoute? jobRoute.route.id: null,
          routeDate: jobRoute? jobRoute.route.date: null,
          // TODO missing
          // parcel level
          //   dpId: 123,
          failureReason: jobProof? jobProof.failureReason: null,
          podId: jobProof? jobProof.id: null,
        }
      }),
    }
  })

const EventHistoryViewSelector = createSelector(
  [EventsSelector], (events) => {
    // TODO pending events API from shivam ORDER-785
    return {
      data: events.map(ev => {
        return {
          eventId: 1,
          eventTime: moment.parseZone(ev.time.replace(/z$/i, ''), moment.ISO_8601).format('YYYY-MM-DD HH:mm:ss'),
          eventTags: '',
          eventName: ev.eventName,
          userType: '',
          userId: '',
          routeId: '-',
          hubName: '',
          description: '',
        }
      }),
    }
  })

const EditCashCollectionModalSelector = createSelector(
  [PickupJobSelector, DeliveryJobSelector], (pickup, delivery) => {
    return {
      ...pick(pickup, [['id', 'pickupJobId'], 'copReqId', 'copReqAmount']),
      ...pick(delivery, [['id', 'deliveryJobId'], 'codReqId', 'codReqAmount']),
    }
  })
const ConfirmPullFromRouteModalSelector = createSelector(
  [JobsSelector, JobToRouteMappingSelector], (jobs, routeMapping) => {
    const jobsWithRoute = []
    for (const job of jobs) {
      const routeRes = routeMapping[job.__uid__]
      if (routeRes) {
        jobsWithRoute.push({
          id: job.id,
          jobType: job.jobType,
          routeId: routeRes.route.id,
        })
      }
    }
    return jobsWithRoute
  })

const ConfirmAddToRouteModalSelector = createSelector(
  [], (parcelEdit) => {}
  )

const ConfirmDpDropoffSettingsModalSelector = createSelector(
  [], (parcelEdit) => {}
  )

const EditRtsDetailsModalSelector = createSelector(
  [DeliveryJobSelector, PickupJobSelector], (deliveryJob, pickupJob) => {
    return {
      instruction: deliveryJob? deliveryJob.instruction: '',
      pickup: pickupJob,
      delivery: deliveryJob,
    }
  })

const ViewAllProofOfDeliveriesSelector = createSelector(
  [JobToProofsMappingSelector, DeliveryJobSelector], (mapping, deliveryJob) => {
    return deliveryJob? mapping[deliveryJob.__uid__] : []
  })

export default {
  InformationBarSelector,
  GeneralInfoViewSelector,
  ItemSummaryViewSelector,
  PricingAndDimensionViewSelector,
  RecoveryInfoViewSelector,
  PickupDeliveryJobViewSelector,
  JobHistoryViewSelector,
  EventHistoryViewSelector,
  EditCashCollectionModalSelector,
  ConfirmPullFromRouteModalSelector,
  ConfirmAddToRouteModalSelector,
  ConfirmDpDropoffSettingsModalSelector,
  EditRtsDetailsModalSelector,
  ViewAllProofOfDeliveriesSelector,
  CurrentJobSelector,
  ParcelEditSelector,
  ParcelSelector,
  OrderSelector,
  JobsSelector,
  DestinationZoneSelector,
  TransitJobSelector,
  EventsSelector,
  PickupJobSelector ,
  DeliveryJobSelector,
  JobToRouteMappingSelector,
  RouteToDriverMappingSelector,
  JobToProofsMappingSelector,
}
