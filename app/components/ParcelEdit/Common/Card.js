import React from 'react'
import { Card } from 'antd'

export function ParcelEditCard (props) {
  const propsCopy = {...props}
  delete propsCopy['children']

  return <Card {...propsCopy}
    bodyStyle={{ padding: '16px 32px', margin: 0 }}
    headStyle={{ padding: '16px 32px', margin: 0 }}>
    {props.children}
  </Card>
}

export function RouteInfoCard (props) {
  const propsCopy = {...props}
  delete propsCopy['children']
  return <Card {...propsCopy}
    style={{ marginTop: '20px' }}
    bodyStyle={{ padding: '7px 16px', margin: 0 }}
    headStyle={{ padding: '7px 16px', margin: 0 }}>
    {props.children}
  </Card>
}
