//  @flow
import React from 'react'
import InjectedDetailsForm from './Forms/DetailsForm'
import InjectedAddressForm from './Forms/AddressForm'
import { pick } from '@app/utils/helpers'
import { PickupSlotsForm, DeliverySlotsForm } from './Forms/Slots'
import { Form } from 'antd'
import { T } from '@app/components/Generic/T'
import type { PickupFormProps, DeliveryFormProps } from './Types'


function PickupForm(props: PickupFormProps) {
  const { form } = props
  const action = 'pickup'
  const whom = 'sender'

  const detailProps = pick(props, [
    'name', 'contact', 'email', 'instruction', 'notes',
  ])
  const slotsProps = pick(props, [
    'scheduleType', 'scheduledDate', 'scheduledTimeslotFrom', 'scheduledTimeslotTo',
    'requestedByShipper',
  ])
  const addressProps = pick(props, [
    'country', 'city', 'address1', 'address2', 'postCode', 'latitude', 'longitude',
  ])

  return (
    <Form>
      <InjectedDetailsForm action={action} whom={whom} {...detailProps}/>
      <PickupSlotsForm {...slotsProps}/>
      <InjectedAddressForm
        form={form}
        title={<T id={`commons.${action}-address`} />}
        visible={false}
        {...addressProps}
      />
    </Form>
  )
}

function DeliveryForm(props: DeliveryFormProps) {
  const action = 'delivery'
  const whom = 'recipient'

  const detailProps = pick(props, [
    'name', 'contact', 'email', 'instruction',
  ])
  const slotsProps = pick(props, [
    'scheduledDate',
    'scheduledTimeslot',
    'requestedByShipper',
  ])
  const addressProps = pick(props, [
    'country', 'city', 'address1', 'address2', 'postCode', 'latitude', 'longitude',
  ])

  return (
    <Form>
      <InjectedDetailsForm action={action} whom={whom} {...detailProps}/>
      <DeliverySlotsForm {...slotsProps}/>
      <InjectedAddressForm
        title={<T id={`commons.${action}-address`} />}
        visible={false}
        {...addressProps}
      />
    </Form>
  )
}

export {
  PickupForm,
  DeliveryForm,
}
