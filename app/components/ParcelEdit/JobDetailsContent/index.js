// @flow
import React from 'react'
import styled from 'styled-components'
import { Card, Row, Col } from 'antd'

import { T } from '@app/components/Generic/T'
import { Colors, Fonts } from '@app/themes'
import { JobInformation, RouteInformation } from '@app/components/ParcelEdit/JobDetailsContent/Types'

const InnerCard = styled(Card)`
  && {
    border-radius: 0.3em;
    min-width: 50em;
    margin-bottom: 1em;
  }
`
const DataFieldCol = styled(Col)`
  && {
    ${Fonts.style.regular}
    margin-bottom: 1em;
  }
`
const DataFieldNameRow = styled(Row)``

type DataFieldProps = {
  span: number,
  hasLink: boolean,
  link: string,
  fieldValue: string,
  fieldName: string | number
}

class DataField extends React.Component<DataFieldProps> {
  displayBlankIfNull (fieldValue) {
    return fieldValue === null ? '-' : fieldValue
  }
  render () {
    const data = this.props.hasLink
      ? (<a href={this.props.link} style={{ color: Colors.nvLinkBlue }}>{this.displayBlankIfNull(this.props.fieldValue)}</a>)
      : this.displayBlankIfNull(this.props.fieldValue)
    return (
      <DataFieldCol span={this.props.span}>
        <DataFieldNameRow><T id={this.props.fieldName} /></DataFieldNameRow>
        <Row>{data}</Row>
      </DataFieldCol>
    )
  }
}

type JobDetailsContentProps = {
  jobInformation: JobInformation,
  routeInformation: RouteInformation
}
function JobDetailsContent (props: JobDetailsContentProps) {
  return (
    <div>
      <InnerCard hoverable={false} title={<T id='commons.model.job-info' />}>
        <Row>
          <DataField fieldName={'commons.model.job-id'} fieldValue={props.jobInformation.jobId} span={8} />
          <DataField fieldName={'commons.model.job-type'} fieldValue={props.jobInformation.jobType} span={8} />
          <DataField fieldName={'commons.model.priority-level'} fieldValue={props.jobInformation.priorityLevel} span={8} />
        </Row>
        <Row>
          <DataField fieldName={'commons.model.name'} fieldValue={props.jobInformation.name} span={8} />
          <DataField fieldName={'commons.contact'} fieldValue={props.jobInformation.contact} span={8} />
          <DataField fieldName={'commons.email'} fieldValue={props.jobInformation.email} span={8} />
        </Row>
        <Row>
          <DataField fieldName={'commons.address'} fieldValue={props.jobInformation.address} span={16} />
          <DataField fieldName={'commons.end-time'} fieldValue={props.jobInformation.endTime} span={8} />
        </Row>
      </InnerCard>
      <InnerCard hoverable={false} title={<T id='commons.model.route-info' />} >
        <Row>
          <DataField fieldName={'commons.model.route-id'} fieldValue={props.routeInformation.routeId} hasLink link={props.routeInformation.link} span={8} />
          <DataField fieldName={'commons.model.route-date'} fieldValue={props.routeInformation.routeDate} span={8} />
          <DataField fieldName={'commons.model.assigned-to'} fieldValue={props.routeInformation.assignedTo} span={8} />
        </Row>
        <Row>
          <DataField fieldName={'commons.model.dp-id'} fieldValue={props.routeInformation.dpId} span={8} />
          <DataField fieldName={'commons.model.failure-reason'} fieldValue={props.routeInformation.failureReason} span={8} />
          <DataField fieldName={'commons.model.dnr'} fieldValue={props.routeInformation.dnr} span={8} />
        </Row>
      </InnerCard>
    </div>
  )
}

export default JobDetailsContent
