import React from 'react'
import { EditParcelDetailsModal } from '@app/components/ParcelEdit/EditParcelDetails'
import { storiesOf } from '@storybook/react'
import { TestIntlProviderWrapper } from '@internals/testing/test-helpers'

const sampleParcelInfo = {
  parcelTrackingId: 'NINJ000238148',
  orderSource: 'API',
  createdOn: '2018-10-09 15:23:12',
}

const sampleDeliveryTypes = [
  'DELIVERY_THREE_DAYS_ANYTIME',
  'DELIVERY_FIVE_DAYS_ANYTIME',
  'DELIVERY_SIX_DAYS_ANYTIME',
]

const sampleParcelFormData = {
  stampTrackingId: 'NVP-123425632',
  currentPriorityLevel: 80,
  deliveryType: sampleDeliveryTypes[0],
}

storiesOf('ParcelEdit', module).add('EditParcelDetails', () => (
  <TestIntlProviderWrapper>
    <EditParcelDetailsModal
      visible
      parcelInformation={sampleParcelInfo}
      parcelFormData={sampleParcelFormData}
      deliveryTypes={sampleDeliveryTypes}
      />
  </TestIntlProviderWrapper>
))
