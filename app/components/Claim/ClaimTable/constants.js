import { CLAIM_STATUS } from '../constants'
import React from 'react'
import { Button } from 'antd'

export const COLUMNS = [
  {
    title: 'Claim ID',
    dataIndex: 'claimId',
    key: 'claimId',
    width: '150px',
    fixed: 'left',
  },
  {
    title: 'Tracking ID',
    dataIndex: 'trackingId',
    key: 'trackingId',
    width: '200px',
  },
  {
    title: 'Ticket ID',
    dataIndex: 'ticketId',
    key: 'ticketId',
    width: '200px',
  },
  {
    title: 'Shipper Name',
    dataIndex: 'shipperId',
    key: 'shipperId',
    width: '200px',
  },
  {
    title: 'Amount',
    dataIndex: 'amount',
    key: 'amount',
    width: '200px',
  },
  {
    title: 'Liable Department',
    dataIndex: 'liableDept',
    key: 'liableDept',
    width: '200px',
  },
  {
    title: 'Status',
    dataIndex: 'status',
    key: 'status',
    width: '200px',
  },
  {
    title: 'Paid Date',
    dataIndex: 'paidDate',
    key: 'paidDate',
    width: '200px',
  },
  {
    title: 'Action',
    dataIndex: 'action',
    key: 'action',
    width: '150px',
    fixed: 'right',
    // eslint-disable-next-line react/display-name
    render: (text, record) => (
      <div>
        <Button>Edit</Button>
      </div>
    ),
  },
]


export const DATA = [
  {
    claimId: '123',
    trackingId: 'NVSG101010131',
    ticketId: '101021',
    shipperName: 'Rizaq Pratama',
    amount: '100000',
    status: CLAIM_STATUS.PENDING,
  },
  {
    claimId: '124',
    trackingId: 'NVSG101014242',
    ticketId: '101022',
    shipperName: 'Rizaq Pratama',
    amount: '100000',
    status: CLAIM_STATUS.PENDING,
  },
  {
    claimId: '125',
    trackingId: 'NVSG1010101212',
    ticketId: '101023',
    shipperName: 'Rizaq Pratama',
    amount: '100000',
    status: CLAIM_STATUS.PENDING,
  },
  {
    claimId: '126',
    trackingId: 'NVSG101042414',
    ticketId: '101024',
    shipperName: 'Rizaq Pratama',
    amount: '100000',
    status: CLAIM_STATUS.PENDING,
  },
  {
    claimId: '127',
    trackingId: 'NVSG101053534',
    ticketId: '101025',
    shipperName: 'Rizaq Pratama',
    amount: '100000',
    status: CLAIM_STATUS.PENDING,
  },
  {
    claimId: '128',
    trackingId: 'NVSG101097896',
    ticketId: '101026',
    shipperName: 'Rizaq Pratama',
    amount: '100000',
    status: CLAIM_STATUS.PENDING,
  },
]
