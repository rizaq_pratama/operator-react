import React from 'react'
import { mount, shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { ConfirmAddToRoute } from '../index'
import { TestIntlProviderWrapper } from '@internals/testing/test-helpers'
import Route from '@app/services/api/routeApi'

jest.mock('@app/services/api/routeApi', () => {
  return {
    addJobToRoute: jest.fn(() => {
      return {ok: true}
    }),
    getAllTags: () => Promise.resolve({tags: [{name: 'abc', id: 1}]}),
  }
})


describe('ConfirmAddToRoute', () => {
  describe('click submit button ', () => {
    let jobs, wrapper, submitButton, onClose
    beforeEach(() => {
      onClose = jest.fn()
      jobs = [
        {id: 1, jobType: 'parcel-delivery'},
        {id: 2, jobType: 'parcel-pickup'},
      ]
      wrapper = mount(
        <TestIntlProviderWrapper>
          <ConfirmAddToRoute
            jobs={jobs} visible targetJob={jobs[0]} onClose={onClose}/>
        </TestIntlProviderWrapper>
      )
      submitButton = wrapper.find('.ant-modal-footer [type="primary"]')
    })

    afterEach(() => {
      wrapper.unmount()
    })

    it('should not trigger api when invalid data is given', async () => {
      submitButton.simulate('click')
      expect(Route.addJobToRoute).not.toHaveBeenCalled()
      await new Promise(setTimeout)
    })

    it('should trigger add to route api', async () => {
      const input = wrapper.find('#routeId').at(0)
      input.simulate('change', {target: {value: '123'}})
      submitButton.simulate('click')
      expect(Route.addJobToRoute).toHaveBeenCalled()

      await new Promise(setTimeout)
      expect(onClose).toHaveBeenCalled()
    })
  })

  it('renders a ConfirmAddToRoute component', () => {
    const wrapper = shallow(
      <ConfirmAddToRoute visible/>
    )
    expect(wrapper).toBeDefined()
    expect(toJson(wrapper)).toMatchSnapshot()
  })

})
