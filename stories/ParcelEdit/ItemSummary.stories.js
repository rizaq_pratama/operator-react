/* eslint-disable no-undef */
import React from 'react'
import ItemSummary from '@app/components/ParcelEdit/ItemSummary'
import { storiesOf } from '@storybook/react'

storiesOf('ParcelEdit', module)
  .add('ItemSummary', () =>
    <ItemSummary
      id={1}
      shipperId={10}
      orderSource='API'
      shipperReference='114-053199-7110637'
      createdTimestamp='2018-10-08 23:53 HRS'
      deliveryType='DELIVERY_ONE_DAY_ANYTIME'
      orderType='Normal'
      international='Yes'
      onEditButtonClicked={(props) => alert('Handle Edit Button Click for Shipper Id ' + props.shipperId)}
      onShipperOrderDataClicked={(props) => alert('Handle Shipper Order Data Clicked: for Shipper Id ' + props.shipperId)}
    />
  )
