// @flow
import React from 'react'
import styled from 'styled-components'
import { Spin } from 'antd'

import { T } from '@app/components/Generic/T'

const StyledDiv = styled.div`
  position: absolute;
  bottom: 0;
  left: 0;
  background-color: #ebebeb;
  color: #767676;
  width: 100%;
  display: table;
  text-align: center;
  height: 50px;
  font-size: 18px;
  border-top: 1px solid #e0e0e0;
  width: 100%;

  &.loading {
    background-color: white;
    border-top: 0;
  }

  .footer-content {
    display: table-cell;
    vertical-align: middle;
  }
`

class Footer extends React.Component<FooterProps> {
  static defaultProps = {
    isLoading: false,
    content: <T id="directive.table.end-of-table" />,
  }

  renderContent() {
    const { isLoading, content } = this.props
    if(isLoading) {
      return <Spin />
    }

    return content
  }

  render() {
    const { isLoading } = this.props

    return (
      <StyledDiv
        className={`table-footer${isLoading ? ' loading' : ''}`}>
        <div className="footer-content">
          {this.renderContent()}
        </div>
      </StyledDiv>
    )
  }
}

type FooterProps = {
  isLoading?: boolean,
  content?: any,
}

export default Footer
