#!/usr/bin/env bash -e

# Get the current branch
branch=$(git rev-parse --abbrev-ref HEAD)
if [ "$branch" = 'master' ]
then
  ENVIRONMENT='prod'
elif [ "$branch" = 'dev' ]
then
  ENVIRONMENT='dev'
else
  ENVIRONMENT='qa'
fi
