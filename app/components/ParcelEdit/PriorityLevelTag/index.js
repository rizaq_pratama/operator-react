// @flow
import React from 'react'
import { Tag } from 'antd'
import { T } from '@app/components/Generic/T'

type PriorityLevelTagProps = {
  priorityLevel: number
}

class PriorityLevelTag extends React.Component<PriorityLevelTagProps> {
  render () {
    let color = ''
    if (this.props.priorityLevel >= 90) {
      color = 'red'
    } else if (this.props.priorityLevel > 0) {
      color = 'volcano'
    }
    return (
      <Tag color={color}>
        <T id={'commons.model.current-priority'} />
        : {this.props.priorityLevel}
      </Tag>)
  }
}

export default PriorityLevelTag
