// @flow
import React from 'react'
import { injectIntl, intlShape } from 'react-intl'
import { T } from '@app/components/Generic/T'
import { pick } from '@app/utils/helpers'
import { Row, Col, Card, Form } from 'antd'
import DataField from '@app/components/Generic/DataField'
import FormControl from '@app/components/Generic/FormControl'
import { DeliverySlotsForm } from '@app/components/ParcelEdit/GenericEditDetails/Forms/Slots'
import AddressForm from '@app/components/ParcelEdit/GenericEditDetails/Forms/AddressForm'
import type {RtsFormProps} from '@app/components/ParcelEdit/GenericEditDetails/Types'
import RtsReasons from '@app/constants/RtsReason'

type Props = {
  intl: intlShape,
} & RtsFormProps

function RtsForm(props: Props) {
  const { instruction } = props;
  const f = props.intl.formatMessage;
  const addressProps = pick(props, [
    'country',
    'city',
    'address1',
    'address2',
    'postCode',
    'latitude',
    'longitude',
  ])
  const reasons = Object.values(RtsReasons).map(m => f({ id: m }))
  return (
    <Form>
      <Card title={f({id: 'container.parcel-edit.return-to-sender-reason'})}>
        <FormControl
          stateKey='reason'
          fieldName='container.parcel-edit.rts-reason'
          type='dropdown'
          menu={reasons}
          options={{
            rules: [{required: true}],
          }}
        />
      </Card>

      <Card title={f({ id: 'container.parcel-edit.edit-recipient-details' })}>
        <Row type='flex'>
          <Col span={8}>
            <FormControl
              type='text'
              stateKey='recipient.name'
              fieldName='container.parcel-edit.rts-reason.recipient-name'
              options={{initialValue: props.name}}
            />
          </Col>
          <Col span={8}>
            <FormControl
              type='text'
              stateKey='recipient.contact'
              fieldName='container.parcel-edit.rts-reason.recipient-contact'
              options={{initialValue: props.contact}}
            />
          </Col>
          <Col span={8}>
            <FormControl
              type='text'
              stateKey='recipient.email'
              fieldName='container.parcel-edit.rts-reason.recipient-email'
              options={{initialValue: props.email}}
            />
          </Col>
        </Row>
        <Row type='flex'>
          <Col>
            <DataField label='commons.model.rts-instructions'>
              { instruction || '' }
            </DataField>
          </Col>
        </Row>
        <Row type='flex'>
          <Col span={24}>
            <FormControl
              type='text'
              stateKey='rts.notes'
              fieldName='container.parcel-edit.rts-reason.recipient-notes'
              options={{initialValue: props.notes}}
            />
          </Col>
        </Row>
      </Card>
      <DeliverySlotsForm action='rts' />
      <AddressForm
        title={<T id={'commons.rts-address'} />}
        visible={false}
        {...addressProps}
      />
    </Form>
  );
}

export default injectIntl(RtsForm);
