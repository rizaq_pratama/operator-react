//  @flow

import React from 'react'
import { Row, Col, Divider, Spin } from 'antd'
import Layout from 'antd/lib/layout'
import { SelectorUtils } from '@nv/react-commons/src/Utils'
import { createStructuredSelector } from 'reselect'
import { connect } from 'react-redux'
import { compose } from 'redux'
import _ from 'lodash'

import { Currency } from '@nv/react-commons/Migration_corev2/Constants'
import { MODAL_TYPES } from './constants'

import OrderInfoCard from '@app/components/OrderEdit/OrderInfoCard'
import OrderInformationBar from '@app/components/OrderEdit/OrderInformationBar'
import TopBarActions from '@app/components/OrderEdit/TopBarActions'
import CancelOrderModal from '@app/components/OrderEdit/Modals/CancelOrderModal'
import ManuallyCompleteOrderModal from '@app/components/OrderEdit/Modals/ManuallyCompleteOrderModal'
import RecalculatePricingModal from '@app/components/OrderEdit/Modals/RecalculatePricingModal'
import ResumeOrderModal from '@app/components/OrderEdit/Modals/ResumeOrderModal'

import reducer, { Creators } from './redux'
import saga from './saga'

import injectSaga from 'utils/injectSaga'
import injectReducer from 'utils/injectReducer'
import globalSelectors from '@app/containers/Base/selectors'
import { Creators as GlobalCreators } from '@app/containers/Base/redux'

import './style.less'

const { selector } = SelectorUtils
const { Header, Content } = Layout

type Parcel = {
  parcelTrackingId: string,
  currentJob: string,
  currentJobStatus: string,
  currentGranularStatus: string,
}

type OrderEditProps = {
  modalId: string,
  isLoading: boolean,
  match: any,
  orderInfo: {
    id: number,
    shipperId: number,
    shipperName: string,
    orderCreated: string,
    billingStatus: string,
    orderSource: string,
    shipperReference: string,
    shipperOrderData: string,
    codAmount: number,
    codCurrency: $Keys<typeof Currency>,
    trackingNumber: string,
    status: string,
  },
  parcelInfo: Array<Parcel>,
  openModal: Function,
  closeModal: Function,
  getOrderInfo: Function,
  setLoading: Function,
}

export class OrderEdit extends React.Component<OrderEditProps> {
  constructor(props) {
    super(props)
    this.onModalClicked = this.onModalClicked.bind(this)
  }
  componentDidMount () {
    let { params: { orderId } }  = this.props.match
    this.props.setLoading(true)
    this.props.getOrderInfo(orderId)
  }
  onModalClicked (menu) {
    const { key } = menu
    if (key) {
      this.props.openModal(key)
    }
  }
  render() {
    const { modalId, closeModal } = this.props
    const modals = [
      <ManuallyCompleteOrderModal key={MODAL_TYPES.MANUALLY_COMPLETE_ORDER} />,
      <CancelOrderModal key={MODAL_TYPES.CANCEL_ORDER}/>,
      <ResumeOrderModal key={MODAL_TYPES.RESUME_ORDER}/>,
      <RecalculatePricingModal key={MODAL_TYPES.RECALCULATE_PRICING}/>,
    ].map(node => {
      return React.cloneElement(node, Object.assign({}, node.props, {
        orderId: this.props.match.params.orderId,
        visible: node.key === modalId,
        onClose: () => closeModal(),
      }))
    })
    return (
      <Layout className='order-edit-main'>
        <Spin spinning={this.props.isLoading} size='large'>
          <Header style={{ position: 'fixed', zIndex: 1, width: '100%', height: 'auto', background: 'white', padding: 0, margin: 0 }}>
            <div className='inner'>
              <Row className='header' justify='left' type='flex'>
                <Col className='column' xs={24} sm={20} md={20}>
                  <OrderInformationBar
                    trackingNumber={_.get(this.props.orderInfo, 'trackingNumber')}
                    orderStatus={_.get(this.props.orderInfo, 'status')}
                  />
                </Col>
              </Row>
            </div>
            <Divider />
            <div className='inner'>
              <Row className='header' justify='left' type='flex'>
                <Col className='column' xs={24} sm={24} md={24}>
                  <TopBarActions
                    onClick={this.onModalClicked}
                  />
                </Col>
              </Row>
            </div>
          </Header>
          <Content>
            <div className='inner'>
              <Row gutter={16} justify='center' type='flex'>
                <Col className='column' span={24}>
                  <OrderInfoCard
                    parcelInfo={this.props.parcelInfo}
                    orderInfo={this.props.orderInfo}
                  />
                </Col>
              </Row>
            </div>
          </Content>
          { modals }
        </Spin>
      </Layout>
    );
  }
}

export const mapStateToProps = createStructuredSelector({
  orderInfo: selector('orderEdit', 'orderInfo')(),
  parcelInfo: selector('orderEdit', 'parcelInfo')(),
  isLoading: selector('orderEdit', 'isLoading')(),
  modalId: globalSelectors.ModalIdSelector,
})

export function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    getOrderInfo: (orderId) => dispatch(Creators.getOrderInfoRequest(orderId)),
    setLoading: (isLoading) => dispatch(Creators.setLoading(isLoading)),
    openModal: (id) => dispatch(GlobalCreators.openModal(id)),
    closeModal: () => dispatch(GlobalCreators.closeModal()),
  }
}

const withConnect = connect(mapStateToProps, mapDispatchToProps)

const withReducer = injectReducer({ key: 'orderEdit', reducer })
const withSaga = injectSaga({ key: 'orderEdit', saga })

export default compose(
  withReducer,
  withSaga,
  withConnect
)(OrderEdit)
