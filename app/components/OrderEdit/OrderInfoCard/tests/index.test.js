
import React from 'react'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'
import OrderInfoCard from '../index'
import 'jest-styled-components'
import { TestIntlProviderWrapper } from '../../../../../internals/testing/test-helpers'

const testParcelTableData = [{
  parcelTrackingId: 'NINJ000238148',
  currentJob: 'Pickup',
  currentJobStatus: 'In-Progress',
  currentGranularStatus: 'En-route to Sorting Hub',
},{
  parcelTrackingId: 'NINJ000239148',
  currentJob: 'Delivery',
  currentJobStatus: 'In-Progress',
  currentGranularStatus: 'En-route to Sorting Hub',
},{
  parcelTrackingId: 'NINJ000240148',
  currentJob: 'Pickup',
  currentJobStatus: 'In-Progress',
  currentGranularStatus: 'En-route to Sorting Hub',
},{
  parcelTrackingId: 'NINJ000242148',
  currentJob: 'Delivery',
  currentJobStatus: 'In-Progress',
  currentGranularStatus: 'En-route to Sorting Hub',
}]

const testOrderInfo = {
  shipperId: 14904,
  shipperName: 'Amazon',
  orderSource: 'API',
  orderCreated: '2018-10-08 23:53 HRS',
  orderBillingStatus: 'Billed',
  shipperReference: '114-0503199-7110637',
  shipperOrderData: 'SHIPPA ODDA DATAAAAAAAAAAAA',
  parcelCount: 4,
  codAmount: 50,
  parcelTableData: testParcelTableData,
  codCurrency: 'SGD',
}

describe('OrderInfoCard', () => {
  it('matches snapshot', () => {
    const component = renderer.create(
      <TestIntlProviderWrapper>
        <OrderInfoCard
          parcelInfo={[]}
          orderInfo={testOrderInfo} />
      </TestIntlProviderWrapper>
    )
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('renders a OrderInfoCard', () => {
    const renderedComponent = shallow(
      <OrderInfoCard
        parcelInfo={[]}
        orderInfo={testOrderInfo} />
    )
    expect(renderedComponent).toBeDefined()
  })
  it('toggles shipper data visibility', () => {
    const componentInstance = shallow(
      <OrderInfoCard
        parcelInfo={[]}
        orderInfo={testOrderInfo} />
    ).instance()
    componentInstance.toggleOrderDataVisibility()
    expect(componentInstance.state['orderDataVisible']).toBe(true)
  })
})
