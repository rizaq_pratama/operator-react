const { setup: setupDevServer, teardown: teardownDevServer } = require('jest-dev-server')
const PuppeteerEnvironment = require('jest-environment-puppeteer')

class MyPuppeteerEnvironment extends PuppeteerEnvironment {
  async setup() {
    await setupDevServer([
      {
        command: 'ENVIRONMENT=local node server',
        port: 3000,
      },
      {
        command: 'node server/test/host.js',
        port: 8080,
      },
    ])

    await super.setup();
  }

  async teardown() {
    await super.teardown();
    await teardownDevServer()
  }
}
module.exports = MyPuppeteerEnvironment
