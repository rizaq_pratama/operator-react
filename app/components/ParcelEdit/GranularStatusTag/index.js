//  @flow
import React from 'react'
import { Colors } from '@app/themes'
import _ from 'lodash'
import GranularStatus from '@nv/react-commons/src/Migration_corev2/Constants/GranularStatus'

const {
  AbnormalGranularStatus,
  RegularGranularStatus,
  TerminalGranularStatus } = GranularStatus

const commonStatusStyling = {
  fontWeight: 'bold',
  paddingTop: '5px',
  paddingBottom: '5px',
  paddingRight: '10px',
  paddingLeft: '10px',
  textAlign: 'center',
  fontSize: '12px',
  marginRight: '30px',
}

const regularStatusStyling = _.assign({}, commonStatusStyling, { backgroundColor: Colors.nvGreen3 })
const abnormalStatusStyling = _.assign({}, commonStatusStyling, { backgroundColor: Colors.nvYellow3 })
const terminalStatusStyling = _.assign({}, commonStatusStyling, { backgroundColor: Colors.nvBlue3 })

type GranularStatusTagProps = {
  status: string
}

class GranularStatusTag extends React.Component<GranularStatusTagProps> {
  render () {
    const { status } = this.props
    if( !status ) return null
    if (RegularGranularStatus.has(status.toUpperCase())) {
      return <span style={regularStatusStyling}>{status.toUpperCase()}</span>
    } else if (AbnormalGranularStatus.has(status.toUpperCase())) {
      return <span style={abnormalStatusStyling}>{status.toUpperCase()}</span>
    } else if (TerminalGranularStatus.has(status.toUpperCase())) {
      return <span style={terminalStatusStyling}>{status.toUpperCase()}</span>
    }
    return null
  }
}

export default GranularStatusTag

export const style = {
  regularStatusStyling,
  abnormalStatusStyling,
  terminalStatusStyling,
}
