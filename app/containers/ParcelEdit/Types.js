// @flow
export type Ticket = {
  id: number,
  description: string,
  status: string
}

export type Dimension = {
  width: number,
  height: number,
  length: number
}

export type RouteInformation = {
  routeId: number,
  routeDate: string,
  waypointId: number,
  driverId: number,
  failedAttemp: number,
  instructions: string
}

export type Address = {
  country: string,
  city: string,
  address: string,
  postalCode: number,
  latitude: number,
  longitude: number
}

export type PickupSchedule = {
  pickupDate: Date,
  pickupTimeslots: Array<string>,
  selectedTimeslot: string,
  requestedByShipper: boolean
}

export type DeliverySchedule = {
  deliveryDate: Date,
  deliveryTimeslots: Array<string>,
  selectedTimeslot: string,
  requestedByShipper: boolean
}

export type PickupJob = {
  jobStatus: string,
  shipperName: string,
  phoneNumber: string,
  email: string,
  address: string,
  pickupInstruction: string,
  internalNotes: string,
  pickupSchedule: PickupSchedule,
  pickupAddress: Address,
  routeInformation: RouteInformation
}

export type DeliveryJob = {
  jobStatus: string,
  customerName: string,
  phoneNumber: string,
  email: string,
  address: string,
  pickupInstruction: string,
  internalNotes: string,
  deliverySchedule: DeliverySchedule,
  deliveryAddress: Address,
  routeInformation: RouteInformation
}

export type JobHistoryItem = {
  endTime: string,
  jobType: string,
  jobStatus: string,
  jobId: string,
  routeId: string,
  routeDate: string,
  assignedTo: string,
  dpId: string,
  failureReason: string,
  priorityLevel: string,
  dnr: string
}

export type EventHistoryItem = {
  eventId: number,
  eventTime: string,
  eventTags: string,
  eventName: string,
  userType: string,
  userId: string,
  hubName: string,
  description: string
}

export type EventType = {
  label: string,
  value: string
}

export type ParcelDetails = {
  stampId: string,
  status: string,
  rts: boolean,
  currentJob: string,
  currentHub: string,
  destinationHub: string,
  orderTrackingId: string,
  latestEvent: string,
  dnrGroup: string,
  zone: string,
  shipperId: number,
  shipperReference: string,
  orderSource: string,
  createdTimestamp: string,
  selectedDeliveryType: string,
  deliveryTypeOptions: Array<string>,
  currentPriorityLevel: number,
  orderType: string,
  international: string,
  deliveryFee: number,
  codFee: number,
  insuranceFee: number,
  handlingFee: number,
  gst: number,
  insuredValue: number,
  parcelSize: string,
  weight: number,
  dimensions: Dimension,
  country: string,
  tickets: Array<Ticket>,
  daysSinceOrderCreated: number,
  daysSinceInbounded: number,
  jobHistoryData: Array<JobHistoryItem>,
  eventHistoryData: Array<EventHistoryItem>,
  eventTypes: Array<EventType>,
  pickupJob: PickupJob,
  deliveryJob: DeliveryJob
}

export type JobDetails = {
  jobId: number,
  jobType: string,
  priorityLevel: number,
  name: string,
  contact: string,
  email: string,
  address: string,
  endTime: string,
  routeId: number,
  routeDate: string,
  assignedTo: string,
  dpId: string,
  failureReason: string,
  dnr: string,
  link: string
}

export type Props = {
  parcelDetails: ParcelDetails,
  jobDetails: JobDetails,
  isFetchingJobDetails: boolean,
  match: any,
  getParcelDetails: Function,
  saveItemSummary: Function,
  getJobDetails: Function
};

export type State = {
  isEditParcelDetailsModalOpened: boolean,
  isJobDetailModalOpened: boolean,
  isEditPricingDimensionsOpened: boolean,
  isEditDeliveryDetailsOpened: boolean,
  isEditPickupDetailsOpened: boolean
}
