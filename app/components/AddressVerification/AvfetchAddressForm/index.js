/**
*
* AvfetchAddressForm
*
*/

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Card, Radio, Select, InputNumber, Row, Col, Button, message, Form } from 'antd'
import { T } from '@app/components/Generic/T'
import { LoadingCard } from '@app/components/LoadingCard'
import _ from 'lodash'
import { Colors } from '@app/themes'
import { injectIntl } from 'react-intl'

const RadioGroup = Radio.Group
const Option = Select.Option
const FormItem = styled(Form.Item)`
  && {
    margin-bottom: 12px;
  }
  > span {
    font-weight: bold;
  }
`

const StyledAvfetchAddressForm = styled(LoadingCard)`
  && {
    .ant-card-head-title{
      font-weight: bold;
    }

    .form-section-name {
      font-weight: bold;
    }
  }
`
const verticalRadio = {
  display: 'block',
  height: '30px',
  lineHeight: '30px',
}

const GreyCard = styled(Card)`
  && {
    border-radius: 4px;
    border: solid 1px #e8e8e8;
    background-color: rgba(245, 245, 245, 0.5);
    height: 158px;
    .ant-card-body {
      padding: 14px;
    }
  }
`
const ButtonFromItem = styled(Form.Item)`
  text-align: center;

`
const StyledButton = styled(Button)`
  && {
    background-color: ${Colors.nvPriBlue};
    color: white;
    font-family: roboto;
  }
  &&:hover {
    background-color: #4c7072;
    color: white;
  }
`

const InitFormContainer = styled.div`
  padding: 14px;
`

class AvfetchAddressForm extends React.Component {
  constructor (props) {
    super(props)
    this.handleClicks = this.handleClicks.bind(this)
    this.handleSelectType = this.handleSelectType.bind(this)
    this.handleFetchSize = this.handleFetchSize.bind(this)
    this.handleRouteGroups = this.handleRouteGroups.bind(this)
    this.handleZones = this.handleZones.bind(this)
    this.state = {
      selectedType: 0,
      selectedZone: null,
      fetchSize: 10,
      selectedRouteGroup: null,
      loadingType: false,
      loadingPool: false,
      loadingRg: false,
    }
  }

  // click handler

  handleSelectType (e) {
    this.setState({ selectedType: _.get(e, 'target.value') })
  }

  handleFetchSize (e) {
    this.setState({ fetchSize: e })
  }

  handleRouteGroups (e) {
    this.setState({ selectedRouteGroup: e })
  }

  handleZones (e) {
    this.setState({ selectedZone: e })
  }

  renderZoneOptions (zones) {
    const sortedZones = _.orderBy(zones, 'name', 'asc')
    return (
      _.map(sortedZones, (z) => (
        <Option key={z.id} value={z.id}>{z.name}</Option>
      ))
    )
  }

  renderRouteGroupsOptions (routegroups) {
    const sortedRg = _.orderBy(routegroups, 'name', 'asc')
    return (
      _.map(sortedRg, (rg) => (
        <Option key={rg.id} value={rg.id}>{rg.name}</Option>
      ))
    )
  }

  handleClicks (formSection) {
    const { callbackmap } = this.props
    this.setState({
      loadingType: false,
      loadingPool: false,
      loadingRg: false,
    })
    switch (formSection) {
      case 'by-type': {
        const type = this.state.selectedType
        if (!type) {
          message.warning(this.props.intl.formatMessage({ id: 'container.address-verification.please-select-a-type' }))
        } else {
          this.setState({ loadingType: true })
          if (type === 1) {
            callbackmap.fetchShipperUnverifiedAddress()
          } else if (type === 2) {
            callbackmap.fetchReverifiedAddress()
          }
        }
        break
      }
      case 'from-pool': {
        const { fetchSize, selectedZone } = this.state
        if (_.isNull(selectedZone)) {
          message.warning(this.props.intl.formatMessage({ id: 'container.address-verification.please-select-a-zone' }))
          return
        }
        const filter = {
          size: fetchSize,
          zone_id: selectedZone,
        }
        this.setState({ loadingPool: true })
        callbackmap.fetchFromPool(filter)
        break
      }
      case 'by-rg': {
        const { selectedRouteGroup } = this.state
        if (_.isNull(selectedRouteGroup)) {
          message.warning(this.props.intl.formatMessage({ id: 'container.address-verification.please-select-a-route-group' }))
          return
        }
        this.setState({ loadingRg: true })
        callbackmap.fetchByRg(selectedRouteGroup)
        break
      }
    }
  }

  render () {
    const { zones, routegroups, isLoading, ...rest } = this.props
    const ByTypeForm = () => {
      return (
        <GreyCard type='inner'>
          <Form layout='vertical'>
            <div className='form-section-name'>
              <T className='form-section-name' id='container.address-verification.by-type' />
            </div>
            <FormItem>
              <RadioGroup onChange={this.handleSelectType} value={this.state.selectedType}>
                <Radio style={verticalRadio} value={1}>
                  <T id='container.address-verification.unverified-shipper-addresses' />
                </Radio>
                <Radio style={verticalRadio} value={2}>
                  <T id='container.address-verification.reverification-required' />
                </Radio>
              </RadioGroup>
            </FormItem>
            <ButtonFromItem>
              <StyledButton loading={this.state.loadingType && isLoading} onClick={() => this.handleClicks('by-type')}>
                <T id='container.address-verification.fetch-addresses' />
              </StyledButton>
            </ButtonFromItem>
          </Form>
        </GreyCard>
      )
    }

    const FromInitializedPoolForm = () => {
      const { isLoading } = this.props
      return (
        <InitFormContainer>
          <Form layout='vertical'>
            <div className='form-section-name'>
              <T id='container.address-verification.from-initialized-pool' />
            </div>
            <Row>
              <Col span='18'>
                <FormItem label={<T id='container.address-verification.zone' />}>
                  <Select id='zone-selector'
                    showSearch
                    placeholder={<T id='container.address-verification.search-or-select-zone' />}
                    onChange={this.handleZones}
                    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    style={{ width: 220 }}>
                    {this.renderZoneOptions(zones)}
                  </Select>
                </FormItem>
              </Col>
              <Col span='6'>
                <FormItem label={<T id='container.address-verification.number' />}>
                  <InputNumber min={0} defaultValue={this.state.fetchSize} onChange={this.handleFetchSize} />
                </FormItem>
              </Col>
            </Row>
            <ButtonFromItem>
              <StyledButton loading={this.state.loadingPool && isLoading} onClick={() => this.handleClicks('from-pool')}>
                <T id='container.address-verification.fetch-addresses' />
              </StyledButton>
            </ButtonFromItem>
          </Form>
        </InitFormContainer>
      )
    }

    const ByRouteGroupsForm = () => {
      const { isLoading } = this.props
      return (
        <GreyCard style={{ marginLeft: '10px' }}>
          <Form layout='vertical'>
            <div className='form-section-name'>
              <T className='form-section-name' id='container.address-verification.by-route-groups' />
            </div>
            <FormItem label={<T id='container.address-verification.route-group' />}>
              <Select id='route-group-selector'
                showSearch
                placeholder={<T id='container.address-verification.search-or-select-route-group' />}
                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                style={{ width: 200 }} onChange={this.handleRouteGroups}>
                {this.renderRouteGroupsOptions(routegroups)}
              </Select>
            </FormItem>
            <ButtonFromItem>
              <StyledButton loading={this.state.loadingRg && isLoading} onClick={() => this.handleClicks('by-rg')}>
                <T id='container.address-verification.fetch-addresses' />
              </StyledButton>
            </ButtonFromItem>
          </Form>
        </GreyCard>
      )
    }

    return (
      <StyledAvfetchAddressForm isLoading={isLoading} {...rest} style={{ height: '248px',
        width: '896px' }} title={<T id='container.address-verification.fetch-addresses' />}>
        <Row>
          <Col span='7'>
            {ByTypeForm()}
          </Col>
          <Col span='10'>
            {FromInitializedPoolForm()}
          </Col>
          <Col span='7'>
            {ByRouteGroupsForm()}
          </Col>
        </Row>
      </StyledAvfetchAddressForm>
    )
  }
}

AvfetchAddressForm.propTypes = {
  zones: PropTypes.array.isRequired,
  routegroups: PropTypes.array.isRequired,
  callbackmap: PropTypes.object.isRequired,
  isLoading: PropTypes.bool,
}

AvfetchAddressForm.defaultProps = {
  zones: [],
  routegroups: [],
  callbackmap: {},
  isLoading: false,
}
const IntlAvfetchAddressForm = injectIntl(AvfetchAddressForm)
export { IntlAvfetchAddressForm as AvfetchAddressForm }
