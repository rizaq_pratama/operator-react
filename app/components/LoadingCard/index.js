/**
*
* LoadingCard
*
*/

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Card } from 'antd'
import _ from 'lodash'

const StyledLoadingCard = styled.div``
const LoadingOverlay = styled.div`
    z-index: 999;
    background: rgba(255,255,255,.5);
    position: absolute;`

class LoadingCard extends React.Component {
  renderLoadingOverlay (style) {
    return (<LoadingOverlay style={style} />)
  }
  render () {
    const { children, ...rest } = this.props
    const { style } = this.props
    const overlayStyle = _.pick(style, ['width', 'height'])
    return (
      <StyledLoadingCard>
        { this.props.isLoading && this.renderLoadingOverlay(overlayStyle)}
        <Card {..._.omit(rest, ['isLoading'])} >
          {children}
        </Card>
      </StyledLoadingCard>
    )
  }
}

LoadingCard.propTypes = {
  children: PropTypes.node,
  isLoading: PropTypes.bool,
}

LoadingCard.defaultProps = {
  children: 'LoadingCard',
  isLoading: false,
}

export { LoadingCard }
