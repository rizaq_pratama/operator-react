import React from 'react'
import { mount } from 'enzyme'
import { Modal } from 'antd'
import { SubmittableModal } from '../index'
import toJson from 'enzyme-to-json'

describe('SubmittableModal', () => {
  let form, validateFields, onSubmit

  beforeEach(() => {
    validateFields = jest.fn((cb) => cb(null, {}))
    onSubmit = jest.fn()

    form = {
      getFieldsError: jest.fn(() => ({})),
      validateFields,
    }
  })
  it('snapshot and call submit', () => {
    const wrapper = mount(
      <SubmittableModal form={form} onSubmit={onSubmit} formSubmitted={jest.fn()}>
        <div>Hello World</div>
      </SubmittableModal>
    )
    expect(wrapper.find(Modal).length).toBe(1)
    wrapper.instance().onSubmit()
    expect(validateFields).toHaveBeenCalled()
    expect(onSubmit).toHaveBeenCalled()

    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
