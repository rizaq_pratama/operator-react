import React from 'react'
import { createStructuredSelector } from 'reselect'
import { connect } from 'react-redux'
import { compose } from 'redux'
import addressApi from '@app/services/api/addressApi'
import iframeClient from '@app/services/iframeClient'

import injectSaga from 'utils/injectSaga'
import injectReducer from 'utils/injectReducer'
import reducer from './redux'
import saga from './saga'

/**
 * DON'T REMOVE THIS CLASS
 * IT'S USED IN IFRAME TEST
 */
export class Home extends React.Component {

  constructor (props) {
    super(props)
    this.state = {
      address_getUnverifiedAddressStats: false,
    }
  }

  componentDidMount() {
    addressApi.getAvStats().then(res => {
      this.setState({
        address_getUnverifiedAddressStats: res,
      })
      if (res) {
        iframeClient.request('report_test_result', true)
      }
    })
  }

  render () {
    return <div></div>
  }
}

const withReducer = injectReducer({ key: 'home', reducer })
const withSaga = injectSaga({ key: 'home', saga })
const mapStateToProps = createStructuredSelector({
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

function mapDispatchToProps (dispatch) {
  return {
    dispatch,
  }
}

export default compose(
  withReducer,
  withSaga,
  withConnect
)(Home)
