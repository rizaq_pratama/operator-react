import React from 'react'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'
import FilterTimeBox from '../index'
import { shallowWithIntl, TestIntlProviderWrapper } from '@internals/testing/test-helpers'
import 'jest-styled-components'
import moment from 'moment'

const testFilterTimeBoxParams = {
  title: 'Creation Time',
}

describe('FilterTimeBox', () => {
  let wrapper, instance
  beforeEach(() => {
    wrapper = shallowWithIntl(
      <FilterTimeBox
        title={testFilterTimeBoxParams.title}
      />
    )
    instance = wrapper.instance()
  })
  xit('matches snapshot', () => {
    const component = renderer.create(
      <TestIntlProviderWrapper>
        <FilterTimeBox
          title={testFilterTimeBoxParams.title}
        />
      </TestIntlProviderWrapper>
    )
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('renders a FilterTimeBox component', () => {
    const renderedComponent = shallow(
      <FilterTimeBox
        title={testFilterTimeBoxParams.title}
      />
    )
    expect(renderedComponent).toBeDefined()
  })
  it('changes date time value', () => {
    const startClone = instance.state['start'].clone()
    const changed = moment().hour(5).minute(30)
    instance.onTimeChange('start', changed)

    startClone.hour(5)
    startClone.minute(30)
    startClone.seconds(0)
    expect(instance.state['start'].toString()).toBe(startClone.toString())
  })
})
