// @flow
import React from 'react';
import styled from 'styled-components';
import { Col, Card, Row, Form } from 'antd';
import { T } from '@app/components/Generic/T';
import { Fonts } from '@app/themes';
import FormControl from '../../../Generic/FormControl';
import { ParcelInformation, ParcelFormData } from '../Types';
import * as ServiceLevel from '@nv/react-commons/src/Migration_corev2/Constants/ServiceLevel';
import DataField from '@app/components/Generic/DataField';

const InnerCard = styled(Card)`
  && {
    border-radius: 0.3em;
    min-width: 50em;
    margin-bottom: 1em;
  }
  && .ant-card-head-title {
    ${Fonts.style.bold}
  }
`;

type EditParcelDetailsContentProps = {
  parcelInformation: ParcelInformation,
  parcelFormData: ParcelFormData,
};

class EditParcelDetailsContent extends React.Component<EditParcelDetailsContentProps> {
  static defaultProps = {
    parcelInformation: {},
    parcelFormData: {},
  }
  render() {
    const { parcelInformation, parcelFormData } = this.props;
    return (
      <Form>
        <InnerCard title={<T id={'commons.model.parcel-info'} />}>
          <Row>
            <Col span={8}>
              <DataField label='commons.model.parcel-tracking-id'>
                {parcelInformation ? parcelInformation.parcelTrackingId : '-'}
              </DataField>
            </Col>
            <Col span={8}>
              <DataField label='commons.model.order-source'>
                {parcelInformation ? parcelInformation.orderSource : '-'}
              </DataField>
            </Col>
            <Col span={8}>
              <DataField label='commons.model.created-on'>
                {parcelInformation ? parcelInformation.createdOn : '-'}
              </DataField>
            </Col>
          </Row>
          <Row>
            <FormControl
              fieldName={'commons.model.stamp-tracking-id'}
              type='text'
              stateKey={'stampId'}
              span={8}
              options={{ initialValue: parcelFormData.stampTrackingId }}
            />
            <FormControl
              fieldName={'commons.delivery-type'}
              type='dropdown'
              stateKey={'selectedDeliveryType'}
              span={16}
              menu={Object.values(ServiceLevel)}
              options={{ initialValue: this.props.parcelFormData.deliveryType }}
            />
          </Row>
        </InnerCard>
      </Form>
    )
  }
}

export default EditParcelDetailsContent;
