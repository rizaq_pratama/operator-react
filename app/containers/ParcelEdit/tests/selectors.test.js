import selectors from '../selectors'
import { fromJS } from 'immutable'

describe('Selectors', function () {
  let initialState, newState, res
  beforeEach(() => {
    initialState = fromJS({})
    newState = null
    res = null
  })

  it('ParcelEditSelector', () => {
    res = selectors.ParcelEditSelector(initialState)
    expect(res).toEqual({})

    newState = initialState.setIn(['parcelEdit', 'abc'], 123)
    res = selectors.ParcelEditSelector(newState)
    expect(res).toEqual({abc: 123})
  })

  describe('Sub selectors', () => {
    let state, json
    beforeEach(() => {
      json = {
        parcelEdit: {
          parcel: {
            id: 123,
            trackingNumber: '888888',
            stampId: '9999999',
            granularStatus: 'ON VEHICLE FOR DELIVERY',
            createdAt: 1551753770044,
            parcelMetadata: {
              latestPickupJob: 3,
              latestDeliveryJob: 2,
              latestJob: 2,
              latestJobType: 'parcel-delivery',
            },
          },
          jobs: [
            {
              id: 2,
              jobType: 'parcel-delivery',
              priorityLevel: 19,
              codReqAmount: 1.2,
              endTime: '12:12:12+0800',
              isRts: true,
              name: 'Zeng',
              contact: '32121231',
              email: 'derek@mail.com',
              address1: 'view',
              address2: 'mountain',
              status: 'Pending',
              __uid__: '2/parcel-delivery',
            },
            {
              id: 3,
              jobType: 'parcel-pickup',
              priorityLevel: 190,
              codReqAmount: 12,
              endTime: '12:10:12+0800',
              status: 'Completed',
              name: 'Derek',
              contact: '12121212',
              email: 'emailderek',
              address1: 'mountain',
              address2: 'view',
              __uid__: '3/parcel-pickup',
            },
            {
              id: 5,
              jobType: 'parcel-pickup',
              priorityLevel: 90,
              codReqAmount: 12.4,
              status: 'Cancelled',
              __uid__: '5/parcel-pickup',
            },
            {
              id: 4,
              jobType: 'parcel-transit',
              priorityLevel: 10000,
              codReqAmount: 4,
              status: 'Completed',
              __uid__: '4/parcel-transit',
            },
          ],
          order: {
            id: 2,
            trackingNumber: 123123,
            shipperId: 567,
            serviceLevel: 'SAME DAY',
            serviceType: 'PREMIUM',
            createdAt: '2018-10-16T00:00:00+00:00Z',
          },
          destinationZone: {
            id: 2828,
            hubId: 8989,
          },
          currentRoute: {
          },
          events: [],
          tickets: [
            {id: 1},
            {id: 2},
            {id: 3},
          ],
          jobToRouteMapping: {
            '2/parcel-delivery': {
              route: {
                id: 349,
                driverId: 9090,
                comments: 'none',
                date: '2018-12-12',
                hubId: 8990,
                zoneId: 111,
              },
              waypoint: {
                id: 219,
              },
            },
            '3/parcel-pickup': {
              route: {
                id: 890,
                driverId: 9090,
                comments: 'none either',
                date: '2018-12-10',
              },
              waypoint: {
                id: 876,
              },
            },
          },
          routeToDriverMapping: null,
          jobToProofsMapping: null,
        },
      }
      state = fromJS(json)
    })

    it('JobsSelector', () => {
      res = selectors.JobsSelector(state)
      expect(res).toEqual(json.parcelEdit.jobs)
    })

    it('PickupJobSelector', () => {
      res = selectors.PickupJobSelector(state)
      expect(res).toEqual(json.parcelEdit.jobs[1])
    })
    it('DeliveryJobSelector', () => {
      res = selectors.DeliveryJobSelector(state)
      expect(res).toEqual(json.parcelEdit.jobs[0])
    })
    it('TransitJobSelector', () => {
      res = selectors.TransitJobSelector(state)
      expect(res).toEqual(json.parcelEdit.jobs[3])
    })
    it('CurrentJobSelector', () => {
      res = selectors.CurrentJobSelector(state)
      expect(res).toEqual(json.parcelEdit.jobs[0])
    })

    it('InformationBarSelector', () => {
      res = selectors.InformationBarSelector(state)
      expect(res).toEqual({
        trackingNumber: '888888',
        priorityLevel: 10000,
        cod: 29.6,
        currency: 'SGD',
        isRts: true,
        status: 'ON VEHICLE FOR DELIVERY',
      })
    })

    it('GeneralInfoViewSelector', () => {
      res = selectors.GeneralInfoViewSelector(state)
      expect(res).toEqual({
        stampId: '9999999',
        currentJob: json.parcelEdit.jobs[0],
        currentHub: 8990,
        destinationHub: 8989,
        orderTrackingId: 123123,
        latestEvent: null,
        zone: 111,
      })
    })

    it('ItemSummaryViewSelector', () => {
      res = selectors.ItemSummaryViewSelector(state)
      expect(res).toEqual({
        id: 123,
        shipperId: 567,
        orderSource: null,
        shipperReference: null,
        international: false,
        multiParcel: true,
        createdTimestamp: 1551753770044,
        chargeTo: 'chargeTo',
        deliveryType: 'SAME DAY',
        shipperOrderData: 'key:value',
        orderType: 'PREMIUM',
      })
    })

    it('RecoveryInfoViewSelector', () => {
      const orderDateTs = Date.parse(json.parcelEdit.order.createdAt.split('T')[0])
      const daysNow = ~~Math.floor((Date.now() - orderDateTs) / (24 * 3600 * 1000))
      res = selectors.RecoveryInfoViewSelector(state)
      expect(res).toEqual({
        tickets: json.parcelEdit.tickets,
        daysSinceOrderCreated: daysNow ,
        daysSinceInbounded: null,
      })
    })

    xit('PricingAndDimensionViewSelector', () => {
      res = selectors.PricingAndDimensionViewSelector(state)
      expect(res).toEqual({})
    })

    it('PickupDeliveryJobViewSelector', () => {
      res = selectors.PickupDeliveryJobViewSelector(state)
      expect(res).toEqual({
        pickupJobInfo: {
          jobStatus: 'Completed',
          shipperName: 'Derek',
          phoneNumber: '12121212',
          email: 'emailderek',
          address: 'mountain view',
          routeInformation: {
            routeId: 890,
            routeDate: '2018-12-10',
            waypointId: 876,
            driverId: 9090,
            failedAttempts: 2,
            instructions: 'none either',
          },
        },
        deliveryJobInfo: {
          jobStatus: 'Pending',
          shipperName: 'Zeng',
          phoneNumber: '32121231',
          email: 'derek@mail.com',
          address: 'view mountain',
          routeInformation: {
            routeId: 349,
            routeDate: '2018-12-12',
            waypointId: 219,
            driverId: 9090,
            failedAttempts: 2,
            instructions: 'none',
          },
        },
      })
    })

    it('JobHistoryViewSelector', () => {
      res = selectors.JobHistoryViewSelector(state)
      expect(res).toEqual({
        jobs: [
          {
            endTime: '12:12 PM',
            jobType: 'Delivery',
            jobStatus: 'Pending',
            jobId: 2,
            assignedTo: null,
            priorityLevel: 19,
            routeId: 349,
            routeDate: '2018-12-12',
            failureReason: null,
            podId: null,
          },
          {
            endTime: '12:10 PM',
            jobType: 'Pickup',
            jobStatus: 'Completed',
            jobId: 3,
            assignedTo: null,
            priorityLevel: 190,
            routeId: 890,
            routeDate: '2018-12-10',
            failureReason: null,
            podId: null,
          },
          {
            endTime: null,
            jobType: 'Pickup',
            jobStatus: 'Cancelled',
            jobId: 5,
            assignedTo: null,
            priorityLevel: 90,
            routeId: null,
            routeDate: null,
            failureReason: null,
            podId: null,
          },
          {
            endTime: null,
            jobType: 'Transit',
            jobStatus: 'Completed',
            jobId: 4,
            assignedTo: null,
            priorityLevel: 10000,
            routeId: null,
            routeDate: null,
            failureReason: null,
            podId: null,
          },
        ],
      })
    })
  })
})
