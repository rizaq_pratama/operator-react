import React from 'react'
import { shallow } from 'enzyme'
import { shallowWithIntl, TestIntlProviderWrapper } from '@internals/testing/test-helpers'
import renderer from 'react-test-renderer'
import FilterAutocompleteBox from '../index'
import 'jest-styled-components'

const testFilterAutocompleteBoxParams = {
  title: 'Shipper',
  options: (text) =>
    new Promise((resolve, reject) => {
      const options = [
        'Shipper ABC',
        'Shipper BCD',
        'Shipper CDE',
      ]
      resolve(options);
    }),
}

describe('FilterAutocompleteBox', () => {
  let wrapper, instance
  beforeEach(() => {
    wrapper = shallowWithIntl(
        <FilterAutocompleteBox
          title={testFilterAutocompleteBoxParams.title}
          searchKey={testFilterAutocompleteBoxParams.searchKey}
          options={testFilterAutocompleteBoxParams.options}
        />
    )
    instance = wrapper.instance()
  })
  xit('matches snapshot', () => {
    const component = renderer.create(
      <TestIntlProviderWrapper>
        <FilterAutocompleteBox
          title={testFilterAutocompleteBoxParams.title}
          searchKey={testFilterAutocompleteBoxParams.searchKey}
          searchFunction={testFilterAutocompleteBoxParams.searchFunction}
        />
      </TestIntlProviderWrapper>
    )
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('renders a FilterAutocompleteBox component', () => {
    const renderedComponent = shallow(
      <TestIntlProviderWrapper>
        <FilterAutocompleteBox
          title={testFilterAutocompleteBoxParams.title}
          searchKey={testFilterAutocompleteBoxParams.searchKey}
          searchFunction={testFilterAutocompleteBoxParams.searchFunction}
        />
      </TestIntlProviderWrapper>
    )
    expect(renderedComponent).toBeDefined()
  })
  it('should add option to selected', () => {
    expect(instance.state['selected'].size).toBe(0)
    instance.updateValues('Shipper ABC')
    expect(instance.state['selected'].size).toBe(1)
  })
  it('should remove option to selected', () => {
    instance.updateValues('Shipper ABC')
    expect(instance.state['selected'].size).toBe(1)
    instance.updateValues('Shipper ABC', false)
    expect(instance.state['selected'].size).toBe(0)
  })
  it ('should empty selected', () => {
    instance.updateValues('Shipper ABC')
    expect(instance.state['selected'].size).toBe(1)
    instance.clear()
    expect(instance.state['selected'].size).toBe(0)
  })
  it ('should search and populate Options', () => {
    expect(instance.state['options'].length).toBe(0)
    instance.searchOptions('Test')
    setTimeout(() => {
      expect(instance.state['options'].length).toBe(3)
    }, 100)
  })
})
