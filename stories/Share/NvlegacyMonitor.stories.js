import React from 'react'
import { storiesOf } from '@storybook/react'

import { NvlegacyMonitor } from '@app/components/NvlegacyMonitor/index'

storiesOf('Share', module)
  .add('NvlegacyMonitor', () => {
    const successIndicator = false
    return (<NvlegacyMonitor successIndicator={successIndicator} successsMessage='Success showing the notifications' />)
  }
  )
