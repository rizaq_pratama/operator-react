// @flow
import React from 'react'
import { Input } from 'antd'
import _ from 'lodash'

class FilterableHeaderInput extends React.Component<FilterableHeaderInputProps> {
  constructor(props) {
    super(props)

    this.state = {
      searchText: '',
    }

    this.onInputChange = this.onInputChange.bind(this)

    // have debounce so that it will not keep triggering the callback
    this.onSearchTextChange = _.debounce(this.onSearchTextChange, 250);
  }

  onSearchTextChange(text) {
    this.props.onSearchTextChange({
      dataKey: this.props.dataKey,
      text: text,
    })
  }

  onInputChange(e) {
    const text = e.target.value
    this.setState({
      searchText: text,
    })

    this.onSearchTextChange(text)
  }

  render() {
    return <Input
      onChange={this.onInputChange}
      value={this.state.searchText}
      placeholder="Find..."
    />
  }
}

type FilterableHeaderInputProps = {
  dataKey: string,
  onSearchTextChange: func,
}

export default FilterableHeaderInput