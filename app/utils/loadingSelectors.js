
export const createLoadingSelector = (actions, prefix = '') => state => {
  return actions.some(action => state.getIn(['loading', `${prefix}${action}`]))
}
