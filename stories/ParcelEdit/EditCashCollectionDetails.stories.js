import React from 'react'
import { EditCashCollectionDetails } from '@app/components/ParcelEdit/EditCashCollectionDetails'
import { TestIntlProviderWrapper } from '@internals/testing/test-helpers'
import { storiesOf } from '@storybook/react'

const sampleInfo = {
  parcelTrackingId: 'NINJ000238148',
  orderSource: 'API',
  createdOn: '2018-10-09 15:23:12',
}

const sampleDeliveryTypes = [
  'DELIVERY_THREE_DAYS_ANYTIME',
  'DELIVERY_FIVE_DAYS_ANYTIME',
  'DELIVERY_SIX_DAYS_ANYTIME',
]

const sampleFormData = {
  stampTrackingId: 'NVP-123425632',
  currentPriorityLevel: 80,
  deliveryType: sampleDeliveryTypes[0],
}

storiesOf('ParcelEdit', module).add('EditCashCollectionDetails', () => (
  <TestIntlProviderWrapper>
    <EditCashCollectionDetails
      visible
      cashCollectionInformation={sampleInfo}
      cashCollectionFormData={sampleFormData}
      deliveryTypes={sampleDeliveryTypes}
    />
  </TestIntlProviderWrapper>
))
