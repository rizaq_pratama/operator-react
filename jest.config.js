const config = {
  'globalSetup': './internals/testing/puppeteer-setup.js',
  'globalTeardown': './internals/testing/puppeteer-teardown.js',
  'collectCoverageFrom': [
    'app/**/*.{js,jsx}',
    '!app/**/*.test.{js,jsx}',
    '!app/*/RbGenerated*/*.{js,jsx}',
    '!app/app.js',
    '!app/global-styles.js',
    '!app/*/*/Loadable.{js,jsx}',
    '!app/containers/Home/*.{js,jsx}',
    '!app/configs/*.{js,jsx}',
  ],
  'coverageThreshold': {
    'global': {
      'statements': 90,
      'branches': 90,
      'functions': 90,
      'lines': 90,
    },
  },
  'testEnvironmentOptions': {
    'url': 'http://localhost:3001',
  },
  'globals': {
    '__DEV__': false,
    '__CONFIG__': {},
  },
  'moduleDirectories': [
    'node_modules',
    'app',
  ],
  'moduleNameMapper': {
    '.*\\.(css|styl|scss|sass)$': 'identity-obj-proxy',
    '^((?!vars\\.less).)*\\.less': 'identity-obj-proxy',
    '.*\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$': '<rootDir>/internals/mocks/image.js',
    '^@app(.*)$': '<rootDir>/app$1',
    '^@internals(.*)$': '<rootDir>/internals$1',
  },
  'setupTestFrameworkScriptFile': '<rootDir>/internals/testing/test-bundler.js',
  'testRegex': 'tests/.*\\.test(\\.e2e)?\\.js$',
  'transformIgnorePatterns': [
    '/node_modules/(?!(@nv/react-commons|mapbox-gl)/).+\\.js$',
  ],
  'transform': {
    '^.+\\.js?$': './custom-transformer.js',
    'vars\\.less$': 'jest-raw-loader',
  },
}

module.exports = config
