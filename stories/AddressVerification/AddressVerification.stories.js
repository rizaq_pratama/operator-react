import React from 'react'
import { storiesOf } from '@storybook/react'

import { AvfetchAddressForm } from '@app/components/AddressVerification/AvfetchAddressForm'
import { AvpoolFilter } from '@app/components/AddressVerification/AvpoolFilter'
import { AvstatsCard } from '@app/components/AddressVerification/AvstatsCard'
import AvCoordinateDialog from '@app/components/AddressVerification/AvCoordinateDialog'

const address = {
  address: 'Jalan Kilang Barat 30 Singapore SG 142000',
  lat: 1.2843839,
  lng: 103.8072115,
}

const zones = [
  {
    id: 1,
    name: 'KOOB',
  },
  {
    id: 2,
    name: 'LOREM',
  },
  {
    id: 3,
    name: 'IPSUM',
  },
  {
    id: 4,
    name: 'LORLOR',
  },
  {
    id: 5,
    name: 'SUMSUM',
  },
]

const routeGroups = [
  {
    id: 1,
    name: 'ROUTE GROUP 1',
  },
  {
    id: 2,
    name: 'ROUTE GROUP 2',
  },
  {
    id: 3,
    name: 'ROUTE GROUP 3',
  },
  {
    id: 4,
    name: 'ROUTE GROUP 4',
  },
]

const stats = [
  { 'size': 43, 'name': 'K-North (Ang Mo Kio)' },
  { 'size': 67, 'name': 'Tania Test Zone' },
  { 'size': 8, 'name': 'Zone Refactor' },
  { 'size': 266, 'name': 'JURONG VERSI 2' },
  { 'size': 11760, 'name': 'This Zone is Made to Test on Zone Refactor' },
  { 'size': 526, 'name': 'Z-Out of Zone' },
  { 'size': 34, 'name': 'JERI-TEST' },
  { 'size': 129, 'name': 'Atlantis' },
]

storiesOf('AddressVerification', module)
  .add('AvfetchAddressForm', () => {
    let isLoading = false
    return (<AvfetchAddressForm zones={zones} isLoading={isLoading} routegroups={routeGroups} />)
  }
  )
  .add('AvCoordinateDialog', () =>
    <AvCoordinateDialog mapBoxToken='pk.eyJ1IjoibmluamF2YW4iLCJhIjoiY2ludmFkbzNsMTQ2dHVrbTNscGMwdmRjdSJ9.cw0OWMamjLCuLB2kw6yFPg' address={address} onOk={() => alert('Address is Saved')} visible />)
  .add('AvpoolFilter', () => {
    let isLoading = false
    return (<AvpoolFilter isLoading={isLoading} />)
  }
  )
  .add('AvstatsCard', () =>
    <AvstatsCard zonestats={stats} />)
