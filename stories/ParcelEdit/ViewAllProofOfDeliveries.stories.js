import * as React from 'react';
import { ViewAllProofOfDeliveries } from '@app/components/ParcelEdit/ViewAllProofOfDeliveries'
import { TestIntlProviderWrapper } from '@internals/testing/test-helpers'
import { storiesOf } from '@storybook/react'

const data = [{
    id: 10,
    reservationTransaction: 'Transaction (87623429)',
    type: 'Pickup',
    status: 'Pending',
    distance: 1000,
    podTime: '2018-12-20 13:38:16',
    driver: 'Staff-RESCUE-Zaid Abdul Rahim 3',
    recipient: 'ROY AZMI KAMSANI',
    address: '04-288 BUKIT BARU STREET 321456',
  }, {
    id: 11,
    reservationTransaction: 'Reservation (333666999)',
    status: 'Success',
    distance: 900,
    podTime: '2018-01-09 20:00:00',
    driver: 'Zorro 007',
    recipient: 'BATMAN',
    address: 'Bruce Wayne\'s Basement',
  }];

storiesOf('ParcelEdit', module).add('ViewAllProofOfDeliveries', () => (
  <TestIntlProviderWrapper>
    <ViewAllProofOfDeliveries
      visible
      onCancel={() => alert('Cancel is clicked')}
      onViewPodClicked={podId => alert('Pod [Id:' + podId + '] clicked')}
      proofs={data} />
  </TestIntlProviderWrapper>
))
