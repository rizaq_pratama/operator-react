import { put } from 'redux-saga/effects'
import setting from '@app/services/settingService'
import { fromJS } from 'immutable'
import { REHYDRATE } from './constants'
import { Creators } from './redux'

export function * getUserSettings () {
  const userSetting = yield setting.getUserSettings()
  const domain = yield setting.getDomainSettings()
  const settingData = fromJS({
    userSetting: userSetting,
    domain: domain,
  })
  yield put(Creators.startupSuccess(settingData))
}

// Individual exports for testing
export default function * defaultSaga () {
  try {
    yield getUserSettings(REHYDRATE, getUserSettings)
  } catch (e) {
    // eslint-disable-next-line
    console.log('Problem getting user settings from iframe');
  }
}
