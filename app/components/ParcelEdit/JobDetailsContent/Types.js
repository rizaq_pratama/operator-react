// @flow
export type JobInformation = {
  jobId: number,
  jobType: string,
  priorityLevel: string,
  name: string,
  contact: string,
  email: string,
  address: string,
  endTime: string
}
export type RouteInformation = {
  routeId: number,
  routeDate: string,
  assignedTo: string,
  dpId: number,
  failureReason: string,
  dnr: string,
  link: string
}
