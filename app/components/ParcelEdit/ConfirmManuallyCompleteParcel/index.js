// @flow
import React, {useState} from 'react'
import { T } from '@app/components/Generic/T'
import { Modal, notification } from 'antd'
import { injectIntl, intlShape } from 'react-intl'
import NC from '@app/services/api/ninjaControlApi'
import selectors from '@app/containers/ParcelEdit/selectors'
import { connect } from 'react-redux'

type Props = {
  visible: boolean,
  onClose?: Function,
  intl: intlShape,
  parcelTrackingNumber: string,
}

export function ConfirmManuallyCompleteParcel(props: Props) {
  const [isLoading, setLoading] = useState(false)
  const f = props.intl.formatMessage
  return (
    <Modal
      okType='danger'
      visible={props.visible}
      title={f({id: 'container.parcel-edit.manually-complete-parcel'})}
      cancelText={f({id: 'commons.back'})}
      okText={f({id: 'container.parcel-edit.complete-parcel'})}
      onOk={onOk}
      okButtonProps={{
        disabled: isLoading,
      }}
      cancelButtonProps={{
        disabled: isLoading,
      }}
      closable={!isLoading}
      maskClosable={!isLoading}
      onCancel={props.onClose}>
      <T id='container.parcel-edit.manually-complete-order-desc' />
    </Modal>
  )

  async function onOk() {
    setLoading(true)
    const res = await NC.forceSuccessParcel(props.parcelTrackingNumber)
    if (res.ok !== false) {
      props.onClose()
      notification.success({
        message: 'Saved successfully',
      })
    }
    setLoading(false)
  }
}

const mapStateToProps = state => {
  const parcel = selectors.ParcelSelector(state)
  return { parcelTrackingNumber: parcel? parcel.trackingNumber: null }
}

export default injectIntl(connect(mapStateToProps)(ConfirmManuallyCompleteParcel))
