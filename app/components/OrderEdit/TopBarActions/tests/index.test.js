
import React from 'react'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'
import TopBarActions from '../index'
import 'jest-styled-components'
import { TestIntlProviderWrapper } from '../../../../../internals/testing/test-helpers'

describe('TopBarActions', () => {
  it('matches snapshot', () => {
    const component = renderer.create(
      <TestIntlProviderWrapper>
        <TopBarActions
          status={'abc'} />
      </TestIntlProviderWrapper>
    )
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('renders a ParcelTable', () => {
    const wrapper = shallow(
      <TopBarActions
        status={'abc'} />
    )
    expect(wrapper).toBeDefined()
  })
})
