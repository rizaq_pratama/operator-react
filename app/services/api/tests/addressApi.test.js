import IFrameHelpers from '@nv/react-commons/src/Migration_corev2/IFrameHelpers'
import AddressApi from '../addressApi'
import _ from 'lodash'

jest.mock('@nv/react-commons/src/Migration_corev2/IFrameHelpers')

const fakeRequest = jest.fn(() => Promise.resolve())
const testObject = { test: 'test' }
IFrameHelpers.IFrameClient = function () {
  this.request = fakeRequest
}

describe('addressApi', () => {
  it('getAvStats', () => {
    AddressApi.getAvStats()
    expect(fakeRequest).toBeCalledWith('address_getUnverifiedAddressStats')
  })
  it('fetchAddressByRouteGroup', () => {
    AddressApi.fetchAddressByRouteGroup(1)
    expect(fakeRequest).toBeCalledWith('address_fetchAddressVerificationByrouteGroup', 1)
  })
  it('initAv', () => {
    AddressApi.initAv(testObject)
    expect(fakeRequest).toBeCalledWith('address_initializeAddressVerification', testObject)
  })
  it('fetchAddress', () => {
    AddressApi.fetchAddress(testObject)
    expect(fakeRequest).toBeCalledWith('address_fetchAddressVerification', testObject)
  })
  it('fetchReverifiedAddress', () => {
    AddressApi.fetchReverifiedAddress()
    expect(fakeRequest).toBeCalledWith('address_getReverifiedAddresses', null)
  })
  it('searchAddress', () => {
    AddressApi.searchAddress('query_string')
    expect(fakeRequest).toBeCalledWith('address_searchAddress', 'query_string')
  })
  it('preAvAssign', () => {
    const payload = {
      zoneId: 1,
      waypointIds: [2],
    }
    AddressApi.preAvAssign(1, 2)
    expect(fakeRequest).toBeCalledWith('address_preAvAssign', payload)
  })
  it('archiveOrderJaroScore', () => {
    AddressApi.archiveOrderJaroScore(1)
    expect(fakeRequest).toBeCalledWith('address_archiveOrderJaroScore', 1)
  })
  it('verifyAddress', () => {
    AddressApi.verifyAddress(testObject)
    expect(fakeRequest).toBeCalledWith('address_verifyJaroScore', [testObject])
  })
  it('verifyEvent', () => {
    AddressApi.verifyEvent(testObject)
    expect(fakeRequest).toBeCalledWith('address_createAddressEvent', testObject)
  })
  it('getAllUnverifiedShipperAddress', () => {
    AddressApi.getAllUnverifiedShipperAddress()
    expect(fakeRequest).toBeCalledWith('address_allShipperAddress', { verified: false })
  })
  it('verifyShipperAddress', () => {
    AddressApi.verifyShipperAddress(_.cloneDeep(testObject))
    expect(fakeRequest).toBeCalledWith('addreess_updateShipperAddress',_.assign(testObject, { verified: true }))
  })
})
