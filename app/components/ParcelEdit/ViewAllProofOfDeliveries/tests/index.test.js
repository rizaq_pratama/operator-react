import * as React from 'react';
import { ViewAllProofOfDeliveries } from '../index'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'

describe('ViewAllProofOfDeliveries', () => {
  it('should render', () => {
    const proofs =   [
      {
        'id': 3,
        'type': 'Pickup',
        'status': 'Success',
        'receiver_info': {
          'name': 'George Ezra',
          'contact': '+6592961234',
        },
        'sign_coordinates_lat': -6.2405673,
        'sign_coordinates_lng': 106.8407915,
        'signature_url': 'https://cdn-qa.ninjavan.co/sg/pods/signature_d89055a2-4e6c-4a49-92a9-93ff8f6b22da.png',
        'metadata': {
          'parcel_pickup_quantity': 0,
          'driver_device_imei': '866400034905682',
          'driver_id': 1020498,
          'driver_name': 'Nikonewsg',
        },
        'failure_reason': {
          'failure_reason_code_id': 0,
          'failure_reason_id': 0,
        },
        'photos': [
          {
            'name': 'photo_1.png',
            'url': 'https://cdn-qa.ninjavan.co/sg/pods/signature_d89055a2-4e6c-4a49-92a9-93ff8f6b22da.png',
          },
        ],
        'commit_date': 1542784535000,
      },{
        'id': 3,
        'type': 'Pickup',
        'status': 'Success',
        'receiver_info': {
          'name': 'George Ezra',
          'contact': '+6592961234',
        },
        'sign_coordinates_lat': -6.2405673,
        'sign_coordinates_lng': 106.8407915,
        'signature_url': 'https://cdn-qa.ninjavan.co/sg/pods/signature_d89055a2-4e6c-4a49-92a9-93ff8f6b22da.png',
        'metadata': {
          'parcel_pickup_quantity': 0,
          'driver_device_imei': '866400034905682',
          'driver_id': 1020498,
          'driver_name': 'Nikonewsg',
        },
        'failure_reason': {
          'failure_reason_code_id': 0,
          'failure_reason_id': 0,
        },
        'photos': [
          {
            'name': 'photo_1.png',
            'url': 'https://cdn-qa.ninjavan.co/sg/pods/signature_d89055a2-4e6c-4a49-92a9-93ff8f6b22da.png',
          },
        ],
        'commit_date': 1542784535000,
      },
    ]

    const wrapper = shallow(<ViewAllProofOfDeliveries visible proofs={proofs}/>)
    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
