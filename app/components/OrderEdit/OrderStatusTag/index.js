//  @flow
import React from 'react'
import { style } from '@app/components/ParcelEdit/GranularStatusTag'
import {
  RegularOrderStatus,
  AbnormalOrderStatus,
  TerminalOrderStatus,
} from '@nv/react-commons/src/Migration_corev2/Constants/OrderStatus'
import _ from 'lodash'

type OrderStatusTagProps = {
  status: string
}

class OrderStatusTag extends React.Component<OrderStatusTagProps> {
  render () {
    const status = _.toUpper(this.props.status)
    if (RegularOrderStatus.has(status)) {
      return <span style={style.regularStatusStyling}>{status}</span>
    } else if (AbnormalOrderStatus.has(status)) {
      return <span style={style.abnormalStatusStyling}>{status}</span>
    } else if (TerminalOrderStatus.has(status)) {
      return <span style={style.terminalStatusStyling}>{status}</span>
    }
    return null
  }
}

export default OrderStatusTag
