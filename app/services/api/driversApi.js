import iframeClient from '../iframeClient'

function searchDriverOne (id) {
  return iframeClient.request('driver_searchOne', id)
}

export default {
  searchDriverOne,
}
