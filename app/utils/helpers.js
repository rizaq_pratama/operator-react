// @flow
import _ from 'lodash'

export function calculateDistance (lat1, lng1, lat2, lng2) {
  const earthRadius = 6371000 // metres
  const pi = 3.1415926535897

  const dLat = (lat2 - lat1) * (pi / 180)
  const dLng = (lng2 - lng1) * (pi / 180)
  const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos((lat1) * (pi / 180)) * Math.cos((lat2) * (pi / 180)) * Math.sin(dLng / 2) * Math.sin(dLng / 2)
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
  const dist = earthRadius * c

  return dist
}

export function getDefaultLatLngForDomain (domain) {
  domain = domain || 'sg'
  switch (domain) {
    case 'sg' : return { lat: 1.3597220659709373, lng: 103.82701942695314 }
    case 'my' : return { lat: 3.1333333, lng: 101.697806 }
    case 'id' : return { lat: -6.229728, lng: 106.689430 }
    case 'vn' : return { lat: 10.8046341, lng: 106.6299261 }
    case 'ph' : return { lat: 11.5563477, lng: 113.5779244 }
    case 'th' : return { lat: 13.0003028, lng: 96.9924234 }
    default : return { lat: 1.3, lng: 103.8311393 }
  }
}

export function generateAPIActions (verb, resource, ...args) {
  return {
    [_.camelCase(`${verb}${resource}Request`)]: [...args],
    [_.camelCase(`${verb}${resource}Success`)]: ['data'],
    [_.camelCase(`${verb}${resource}Error`)]: ['error'],
  }
}

export function formatParcelStatus(status) {
  const words = status.split('-').map(w => _.startCase(w))
  return words.filter(w => w !== 'Parcel').join(' ')
}

// if key is a tuple, second element in tuple denotes the new name
export function pick(obj, keys: [string | string[]][]) {
  const ret = {}
  if (!obj) return ret
  for (const k of keys) {
    let key = null, alias = null
    if (Array.isArray(k)) {
      [key, alias] = k
    } else {
      key = k
      alias = key
    }
    if (key in obj) {
      ret[alias] = obj[key]
    }
  }
  return ret
}

export function padLeft(str, len, fill) {
  str = str + ''
  const fillLen = len - str.length
  if (fillLen <= 0) {
    return str
  }
  return Array(fillLen).fill(fill).join('') + str
}

