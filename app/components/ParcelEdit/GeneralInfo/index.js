// @flow
import React from 'react'
import { ParcelEditCard } from '../Common/Card'
import { Row, Col } from 'antd'
import { formatParcelStatus } from '@app/utils/helpers'
import DataField from '@app/components/Generic/DataField'

type Props = {
  stampId: string,
  currentJob: Object,
  currentHub: string,
  destinationHub: string,
  orderTrackingId: string,
  latestEvent: string,
  zone: string,
  onCurrentJobClicked: Function,
  onOrderTrackingIdClicked: Function,
}

class GeneralInfo extends React.Component<Props> {
  static defaultProps = {
    stampId: '-',
    currentJob: null,
    currentHub: '-',
    destinationHub: '-',
    orderTrackingId: '-',
    latestEvent: '-',
    zone: '-',
  }
  constructor (props) {
    super(props)
    this.handleCurrentJobClick = this.handleCurrentJobClick.bind(this)
    this.handleOrderTrackingIdClick = this.handleOrderTrackingIdClick.bind(this)
  }

  handleCurrentJobClick (e) {
    e.preventDefault()
    // TODO handle show job modal
    // this.props.onCurrentJobClicked({ ...this.props })
  }

  handleOrderTrackingIdClick (e) {
    e.preventDefault()
    this.props.onOrderTrackingIdClicked({ ...this.props })
  }

  render () {
    const {stampId, currentJob} = this.props
    const contents = [
      {
        label: 'commons.stamp-id',
        data: stampId,
      }, {
        label: 'commons.current-job',
        data: currentJob? `${formatParcelStatus(currentJob.jobType)} (${currentJob.id})`: '-',
        onClick: this.handleCurrentJobClick,
      }, {
        label: 'container.shipment-management.current-hub',
        data: this.props.currentHub,
      }, {
        label: 'container.shipment-management.destination-hub',
        data: this.props.destinationHub,
      }, {
        label: 'container.shipment-management.order-tracking-id',
        data: this.props.orderTrackingId,
        onClick: this.handleOrderTrackingIdClick,
      }, {
        label: 'container.order.edit.latest-event',
        data: this.props.latestEvent,
      }, {
        label: 'container.parcel-sweeper.zone',
        data: this.props.zone,
      },
    ]

    return <ParcelEditCard>
      <Row type='flex'>
      {contents.map(info => {
        const txt = info.onClick
          ? <a onClick={info.onClick}>{info.data}</a>
          : info.data
        return <Col span={6} key={info.label}>
          <DataField label={info.label}>{txt}</DataField>
        </Col>
      })}
      </Row>
    </ParcelEditCard>
  }
}

export default GeneralInfo
