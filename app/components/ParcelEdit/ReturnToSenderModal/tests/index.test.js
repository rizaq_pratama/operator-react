import React from 'react'
import { mount, shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { RTSModal } from '../index'
import { TestIntlProviderWrapper } from '@internals/testing/test-helpers'
import NC from '@app/services/api/ninjaControlApi'

jest.mock('react-mapbox-gl', () => {
  function Map() {
    return function () {return null}
  }

  Map.Marker = function () {return null}

  return Map
})

jest.mock('@app/services/api/ninjaControlApi', () => {
  return {
    updateDeliveryDetails: jest.fn(() => ({ok: true})),
    rescheduleDeliveryJob: jest.fn(() => ({ok: true})),
    updateDeliveryAddress: jest.fn(() => ({ok: true})),
    returnToSender: jest.fn(() => ({ok: true})),
  }
})

describe('ReturnToSenderModal', () => {
  let pickupJob, deliveryJob

  deliveryJob = {
    'id': 3,
    'job_type': 'parcel-delivery',
    'priority_level': 123,
    'start_date': '2018-10-11',
    'end_date': '2018-10-11',
    'start_time': '12:00:00+08:00',
    'end_time': '15:00:00+08:00',
    'sla_date_time': '2018-10-11T15:00:00+08:00',
    'name': 'Shipper Name',
    'email': 'shipper@ninjavan.co',
    'contact': '90909090',
    'address1': 'GUIMERE S. ISABELITA #04-19 LUCKY PLAZA, 034 ORCHARD ROAD, SINGAPORE 238863',
    'address2': '#04-19 LUCKY PLAZA',
    'neighbourhood': 'SINGAPORE',
    'locality': 'SINGAPORE',
    'region': 'SINGAPORE',
    'country': 'SINGAPORE',
    'postcode': '238863',
    'latitude': 1.3308,
    'longitude': 103.908,
    'status': 'Pending',
    'parcel_id': 1,
    'cod_req_id': 121,
    'cod_req_amount': 99.78,
    'created_at': '2018-10-11T15:00:00',
    'updated_at': '2018-10-11T15:00:00',
  }
  pickupJob = {
    'id': 22,
    'job_type': 'parcel-pickup',
    'priority_level': 72,
    'service_type': 'Standard',
    'service_level': 'Scheduled',
    'start_date': '2018-10-11',
    'end_date': '2018-10-11',
    'start_time': '12:00:00+08:00',
    'end_time': '15:00:00+08:00',
    'sla_date_time': '2018-10-11T15:00:00+08:00',
    'name': 'Shipper Name',
    'email': 'shipper@ninjavan.co',
    'contact': '90909090',
    'address1': '30 Jln Kilang Barat, Singapore 159363',
    'address2': '#04-19 LUCKY PLAZA',
    'neighbourhood': 'SINGAPORE',
    'locality': 'SINGAPORE',
    'region': 'SINGAPORE',
    'country': 'SINGAPORE',
    'postcode': '238863',
    'latitude': 1.3308,
    'longitude': 103.908,
    'pickup_approx_volume': 'Less than 3 Parcels',
    'status': 'Completed',
    'parcel_id': 3,
    'cop_req_id': 120,
    'cop_req_amount': 90.88,
  }

  it('shallow render', () => {
    expect(toJson(shallow(
      <RTSModal
        pickup={pickupJob}
        delivery={deliveryJob}
        visible
        onClose={jest.fn()}
        instruction={'abc'}
      />
    ))).toMatchSnapshot()
  })

  describe('full render', () => {
    let wrapper
    beforeEach(() => {
      jest.useFakeTimers();
    });

    afterEach(() => {
      jest.useRealTimers();
    });
    it('should not submit without reason', () => {
      wrapper = mount(
        <TestIntlProviderWrapper>
          <RTSModal
            pickup={pickupJob}
            delivery={deliveryJob}
            visible
            onClose={jest.fn()}
          />
        </TestIntlProviderWrapper>
      )
      const spy = jest.spyOn(console, 'warn')
      wrapper.find('.ant-modal-footer [type="primary"]').simulate('click')
      expect(NC.updateDeliveryDetails).not.toHaveBeenCalled()
      expect(NC.rescheduleDeliveryJob).not.toHaveBeenCalled()
      expect(NC.updateDeliveryAddress).not.toHaveBeenCalled()
      expect(NC.returnToSender).not.toHaveBeenCalled()
      expect(spy.mock.calls[0][0]).toBe('async-validator:')
      expect(spy.mock.calls[0][1]).toEqual(['reason is required'])
    })

    it('should submit with reason', () => {
      wrapper = mount(
        <TestIntlProviderWrapper>
          <RTSModal
            pickup={pickupJob}
            delivery={deliveryJob}
            visible
            onClose={jest.fn()}
          />
        </TestIntlProviderWrapper>
      )
      wrapper.find('#reason .ant-select').simulate('click');
      jest.runAllTimers()

      const dropdownWrapper = mount(
        wrapper
          .find('#reason Trigger')
          .instance()
          .getComponent()
      )
      dropdownWrapper.find('MenuItem').first().simulate('click')

      wrapper.find('.ant-modal-footer [type="primary"]').simulate('click')
      expect(NC.updateDeliveryDetails).toHaveBeenCalled()
      expect(NC.rescheduleDeliveryJob).toHaveBeenCalled()
      expect(NC.updateDeliveryAddress).toHaveBeenCalled()
      expect(NC.returnToSender).toHaveBeenCalled()
    })
  })
})
