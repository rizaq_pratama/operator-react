import { persistStore } from 'redux-persist-immutable'
import reduxPersistConfig from '@app/configs/reduxPersistConfig'

export const updateReducers = store => {
  const { version, storeConfig } = reduxPersistConfig
  const { localStorage } = window

  // Check to ensure latest reducer version
  const localVersion = localStorage.getItem('version')
  if (localVersion !== version) {
    // Purge store
    persistStore(store, storeConfig).purge()
    localStorage.setItem('version', version)
  } else {
    persistStore(store, storeConfig)
  }
}
