import api from '@nv/react-commons/src/Migration_corev2/Services'
import { makeAPIProxy } from '../apiProxy'
import Config from 'configs'

const NinjaControl = api.NinjaControl

NinjaControl.BASE_URL = Config.BASE_URL + '/ninja-control'

export default makeAPIProxy(NinjaControl)
