// @flow
import React from 'react'
import 'react-virtualized/styles.css'
import {
  Table as VirtualizedTable,
  AutoSizer,
  Column,
  CellMeasurerCache,
  CellMeasurer,
  ScrollSync,
} from 'react-virtualized'
import { Checkbox, Spin } from 'antd'
import styled from 'styled-components'
import _ from 'lodash'
import Highlighter from 'react-highlight-words'

import { T } from '@app/components/Generic/T'
import SortableHeaderCell, { SortTypes } from './SortableHeaderCell'
import FilterableHeaderInput from './FilterableHeaderInput'
import CheckboxHeaderCell, { CheckboFilterOptions } from './CheckboxHeaderCell';
import Footer from './Footer'
import './style.less'

const mainTableHolderClass = 'main-table-holder'
const fixedRightTableHolderClass = 'fixed-right-table-holder'
const mainTableClass = 'main-table'
const fixedRightTableClass = 'fixed-right-table'
const cellLeftRightPaddingWidth = 10
const fixedRightTableBorderWidth = 3

const headerHeight = 30
const headerWithFilterHeight = 65
const counterColumnWidth = 50
const checkboxColumnWidth = 50
const actionsButtonMargin = 4
const actionsButtonWidth = 32
const footerHeight = 50

export const ActionsCellWidthByButton = {
  ONE: actionsButtonWidth + (cellLeftRightPaddingWidth * 2) + 1,
  TWO: (actionsButtonWidth * 2) + actionsButtonMargin + (cellLeftRightPaddingWidth * 2) + 1,
  THREE: (actionsButtonWidth * 3) + (actionsButtonMargin * 2) + (cellLeftRightPaddingWidth * 2) + 1,
  FOUR: (actionsButtonWidth * 4) + (actionsButtonMargin * 3) + (cellLeftRightPaddingWidth * 2) + 1,
}

const StyledVirtualizedTable = styled(VirtualizedTable)`
  &.virtualized-table {
    &.main-table {
      .table-content-grid {
        ${props => !props.showVerticalScrollbar ? `
          &::-webkit-scrollbar { display: none;}` : ''}
      }
    }

    .table-column {
      .cell {
        ${ props => props.isFixedRowHeight ? `
          height: 100% !important;
          overflow: auto;
        ` : ''}
      }
    }
  }
`

class Table extends React.Component<TableProps> {
  static defaultProps = {
    selectable: false,
    showCounter: false,
    overscanRowCount: 5,
  }

  constructor(props) {
    super(props)
    this.validate()

    this.mounted = false
    this.cellMeasurerCache = new CellMeasurerCache({
      fixedWidth: true,
      minHeight: 25,
    });
    this.originalList = props.list || []
    this.overscanStartIndex = 0
    this.overscanStopIndex = 0

    const searchTextMap = {}
    _.forEach(props.columns, (col) => {
      searchTextMap[col.key] = ''
    })

    this.state = {
      processedList: [...this.originalList],
      searchTextMap: searchTextMap,
      isShowOnlySelected: false,
      selectedIds: [],
      sortColumn: null,
      sortBy: null,
      isLoading: props.loading || false,
      isAbleToLoadMoreData: !!props.onLoadMore,
      mainTableClientWidth: 0,
      currentScrollingTable: null,
    }

    this.mainTableHolderRef = React.createRef()

    this.mainTableOnRowsRendered = this.mainTableOnRowsRendered.bind(this)
    this.mainTableNoRowsRenderer = this.mainTableNoRowsRenderer.bind(this)

    this.getRowClassName = this.getRowClassName.bind(this)
    this.getRowHeight = this.getRowHeight.bind(this)

    this.rowGetter = this.rowGetter.bind(this)
    this.headerRenderer = this.headerRenderer.bind(this)
    this.columnCellRenderer = this.columnCellRenderer.bind(this)
    this.onSortChange = this.onSortChange.bind(this)
    this.onSearchTextChange = this.onSearchTextChange.bind(this)
    this.onCheckboxHeaderMenuClicked = this.onCheckboxHeaderMenuClicked.bind(this)
    this.onLoadMoreCompleted = this.onLoadMoreCompleted.bind(this)
  }

  validate() {
    const errorMsgs = []

    const {
      columns,
      list,
      onLoadMore,
      selectable,
      rowId,
      rowHeight,
      onSelectableChange,
      actions,
      onRowsRendered,
    } = this.props

    if(_.isEmpty(columns)) {
      errorMsgs.push('Table must have columns props')
    }

    if(_.isUndefined(list) && _.isUndefined(onLoadMore)) {
      errorMsgs.push('Table must have list or onLoadMore props')
    }

    if(selectable) {
      if(_.isUndefined(rowId)) {
        errorMsgs.push('Must specify rowId props to enable selectable')
      }

      if(_.isUndefined(rowHeight)) {
        errorMsgs.push('Must specify rowHeight props to enable selectable')
      }

      if(_.isUndefined(onSelectableChange)) {
        errorMsgs.push('Must specify onSelectableChange props to enable selectable')
      }
    }

    if(actions) {
      if(_.isUndefined(rowId)) {
        errorMsgs.push('Must specify rowId props to enable actions column')
      }

      if(_.isUndefined(rowHeight)) {
        errorMsgs.push('Must specify rowHeight props to enable actions column')
      }

      if(_.isUndefined(actions.width) || _.isUndefined(actions.contentRenderer)) {
        errorMsgs.push('Must have width and contentRenderer for actions props')
      }
    }

    if(onRowsRendered && _.isUndefined(rowHeight)) {
      errorMsgs.push('Must specify rowHeight props to enable onRowsRendered')
    }

    if(_.size(this.getFixedRightTableColumns()) > 0 && _.isUndefined(rowHeight)) {
      errorMsgs.push('Must specify rowHeight props if the table contains fixed right column')
    }

    const containPreserveKeysColumns = _.filter(columns, column =>
      _.includes(['_index', '_counter', '_action'], column.key)
    )
    if(_.size(containPreserveKeysColumns) > 0) {
      errorMsgs.push('Cannot have _index / _counter / _action as key in column')
    }

    const missingPropsColumns = _.filter(columns, column =>
      !(column.key && column.label && column.width)
    )
    if(_.size(missingPropsColumns) > 0) {
      errorMsgs.push('Column must have key, label, and width props')
    }

    if(_.size(errorMsgs) > 0) {
      throw new Error(_.join(errorMsgs, '; '))
    }
  }

  componentDidMount() {
    this.mounted = true
    this.setState({
      mainTableClientWidth: _.get(this.mainTableHolderRef, 'current.clientWidth'),
    })

    const { onLoadMore, list } = this.props
    if(!_.isUndefined(onLoadMore) && _.isEmpty(list)) {
      this.onLoadMoreData()
    }
  }

  componentDidUpdate(prevProps) {
    const { list, onLoadMore, reloadId, selectable, rowId, loading } = this.props
    const isLoading = loading || false
    if(prevProps.loading !== loading && isLoading) {
      this.setState({
        isLoading,
      })
    } else if(prevProps.list !== list) {
      let selectedIds = [...this.state.selectedIds]
      this.originalList = this.props.list || []

      // process selected ids
      if(selectable) {
        selectedIds = _.intersection(_.map(this.originalList, rowId), selectedIds)
        this.setState({
          selectedIds,
        })
      }

      if(prevProps.reloadId !== reloadId && !_.isUndefined(onLoadMore)) {
        this.setState({
          processedList: this.sort(
            this.state.sortColumn,
            this.filterList(
              this.state.searchTextMap,
              this.state.isShowOnlySelected,
              selectedIds
            ),
            this.state.sortBy
          ),
          isAbleToLoadMoreData: true,
        })

        this.onLoadMoreData()
      } else {
        this.setState({
          isLoading,
          processedList: this.sort(
            this.state.sortColumn,
            this.filterList(
              this.state.searchTextMap,
              this.state.isShowOnlySelected,
              selectedIds
            ),
            this.state.sortBy
          ),
        })
      }
    }
  }

  componentWillUnmount() {
    this.mounted = false
  }

  shouldComponentUpdate() {
    this.cellMeasurerCache.clearAll()
    return true
  }

  tableOnMouseEnter(tableName) {
    this.setState({
      currentScrollingTable: tableName,
    })
  }

  tableOnMouseLeave() {
    this.setState({
      currentScrollingTable: null,
    })
  }

  mainTableNoRowsRenderer() {
    return (
      <div
        className="no-result">
        {this.state.isAbleToLoadMoreData || this.state.isLoading ? <Spin /> : <T id="directive.table.no-result" />}
      </div>
    )
  }

  mainTableOnRowsRendered({ overscanStartIndex, overscanStopIndex }) {
    this.overscanStartIndex = overscanStartIndex
    this.overscanStopIndex = overscanStopIndex
    this.onRowsRendered()

    if(this.isLastRowIndex(overscanStopIndex) &&
      !this.state.isLoading && this.state.isAbleToLoadMoreData) {
      this.onLoadMoreData()
    }
  }

  onRowsRendered() {
    if(this.props.onRowsRendered) {
      const renderedRows = []
      for(let i = this.overscanStartIndex; i <= this.overscanStopIndex; i++) {
        const row = this.rowGetter({ index: i });
        if(row) {
          renderedRows.push(row)
        }
      }

      this.props.onRowsRendered(renderedRows)
    }
  }

  getRowClassName({ index }) {
    if(index === -1) {
      return 'table-row header'
    }

    const classNames = ['table-row']
    classNames.push(index % 2 === 0 ? 'odd' : 'even')

    const row = this.rowGetter({ index })
    if(row.rowClassName) {
      classNames.push(row.rowClassName)
    }

    if(this.isLastRowIndex(index)) {
      classNames.push('last-row')
    }

    if(this.props.selectable && this.isRowSelected(row[this.props.rowId])) {
      classNames.push('selected')
    }

    return _.join(classNames, ' ')
  }

  getHeaderHeight() {
    const {
      columns,
    } = this.props

    const fixedRightTableColumns = _.filter(columns, (col) =>
      this.isFilterableColumn(col)
    )

    return _.size(fixedRightTableColumns) > 0 ? headerWithFilterHeight : headerHeight
  }

  getRowHeight(response) {
    let rowHeight
    if(this.props.rowHeight) {
      rowHeight = this.props.rowHeight
    } else {
      rowHeight = this.cellMeasurerCache.rowHeight(response)
    }

    if(this.isLastRowIndex(response.index)) {
      rowHeight += footerHeight
    }

    return rowHeight
  }

  rowGetter({ index }) {
    const { processedList } = this.state;
    return processedList[index];
  }

  getMainTableColumns() {
    return _.filter(this.props.columns, column => !column.isFixedRight)
  }

  getFixedRightTableColumns() {
    return _.filter(this.props.columns, 'isFixedRight')
  }

  hasFixedRightTableColumn() {
    return this.props.selectable ||
      _.size(this.getFixedRightTableColumns()) > 0 ||
      this.props.actions
  }

  isLastRowIndex(index) {
    return index >= (this.state.processedList.length - 1)
  }

  isFilterableColumn(column) {
    return _.isUndefined(column.filterable) || column.filterable === true
  }

  isLastColumn(tableClass, cols, colIndex) {
    const { selectable, actions } = this.props

    switch(tableClass) {
      case mainTableClass:
        return _.size(cols) - 1 === colIndex
      case fixedRightTableClass:
        return !selectable && !actions && _.size(cols) - 1 === colIndex
      default:
        return false
    }
  }

  async onLoadMoreData() {
    if(_.isUndefined(this.props.onLoadMore) || !this.mounted) {
      return
    }

    this.setState({ isLoading: true })
    try {
      const response = await this.props.onLoadMore(
        [...this.originalList],
        this.onLoadMoreCompleted
      )
      if(!this.mounted) {
        return
      }

      if(_.isEmpty(response)) {
        if(!this.props.loading) {
          this.setState({
            isLoading: false,
            isAbleToLoadMoreData: false,
          })
        }
        return
      }

      this.originalList = _.concat(this.originalList, response)

      this.setState({
        isLoading: false,
        processedList: this.filterList(
          this.state.searchTextMap,
          this.state.isShowOnlySelected,
          this.state.selectedIds
        ),
      })
    } catch {
      this.setState({
        isLoading: false,
        isAbleToLoadMoreData: false,
      })
    }
  }

  onLoadMoreCompleted(isAbleToLoadMoreData = true) {
    this.setState({
      isLoading: false,
      isAbleToLoadMoreData: isAbleToLoadMoreData,
    })
  }

  sort(dataKey, list, sortBy) {
    if(!dataKey) {
      return list
    }

    let processedList
    const column = _.find(this.props.columns, ['key', dataKey]);
    if(!_.isUndefined(column.sortFn)) {
      processedList = column.sortFn({
        list: list,
        sortBy: sortBy,
      })
    } else {
      processedList = _.orderBy(list, [dataKey], [sortBy]);
    }

    return processedList
  }

  filterList(searchTextMap, isShowOnlySelected, selectedIds) {
    let processedList = [...this.originalList]
    _.forEach(searchTextMap, (theText, theDataKey) => {
      const processedText = _.toLower(_.trim(theText))
      if(processedText === '') {
        return;
      }

      const column = _.find(this.props.columns, ['key', theDataKey]);
      if(!_.isUndefined(column.filterFn)) {
        processedList = column.filterFn({
          list: processedList,
          text: theText,
        })
      } else {
        processedList = _.filter(processedList, row =>
          _.toLower(row[theDataKey]).indexOf(processedText) > -1
        )
      }
    })

    if(isShowOnlySelected) {
      processedList = _.filter(processedList, row =>
        this.isRowSelected(row[this.props.rowId], selectedIds)
      )
    }

    return processedList
  }

  onSortChange(dataKey) {
    let sortBy = null
    if(this.state.sortColumn === dataKey) {
      sortBy = this.state.sortBy === SortTypes.ASC ? SortTypes.DESC : SortTypes.ASC
      this.setState({
        sortBy: sortBy,
      });
    } else {
      sortBy = SortTypes.ASC
      this.setState({
        sortColumn: dataKey,
        sortBy: sortBy,
      })
    }

    this.setState({
      processedList: this.sort(dataKey, this.state.processedList, sortBy),
    }, () => {
      this.onRowsRendered()
    })
  }

  onSearchTextChange({ dataKey, text }) {
    const searchTextMap = { ...this.state.searchTextMap }
    searchTextMap[dataKey] = text

    this.setState({
      searchTextMap: searchTextMap,
      processedList: this.sort(
        this.state.sortColumn,
        this.filterList(
          searchTextMap,
          this.state.isShowOnlySelected,
          this.state.selectedIds
        ),
        this.state.sortBy
      ),
    }, () => {
      this.onRowsRendered()
    })
  }

  onCheckboxHeaderMenuClicked({ id, value }) {
    let selectedIds
    switch(id) {
      case CheckboFilterOptions.SELECT_ALL_SHOWN.id:
        selectedIds = _.union(
          this.state.selectedIds,
          _.map(this.state.processedList, this.props.rowId)
        )

        if(this.state.isShowOnlySelected) {
          this.setState({
            selectedIds: selectedIds,
            processedList: this.sort(
              this.state.sortColumn,
              this.filterList(
                this.state.searchTextMap,
                this.state.isShowOnlySelected,
                selectedIds
              ),
              this.state.sortBy
            ),
          })
        } else {
          this.setState({
            selectedIds: selectedIds,
          })
        }
        break;
      case CheckboFilterOptions.DESELECT_ALL_SHOWN.id:
        selectedIds = _.pullAll(
          [...this.state.selectedIds],
          _.map(this.state.processedList, this.props.rowId)
        )


        if(this.state.isShowOnlySelected) {
          this.setState({
            selectedIds: selectedIds,
            processedList: this.sort(
              this.state.sortColumn,
              this.filterList(
                this.state.searchTextMap,
                this.state.isShowOnlySelected,
                selectedIds
              ),
              this.state.sortBy
            ),
          })
        } else {
          this.setState({
            selectedIds: selectedIds,
          })
        }
        break;
      case CheckboFilterOptions.CLEAR_CURRENT_SELECTION.id:
        selectedIds = []
        if(this.state.isShowOnlySelected) {
          this.setState({
            selectedIds: selectedIds,
            processedList: this.sort(
              this.state.sortColumn,
              this.filterList(
                this.state.searchTextMap,
                this.state.isShowOnlySelected,
                selectedIds
              ),
              this.state.sortBy
            ),
          })
        } else {
          this.setState({
            selectedIds: selectedIds,
          })
        }
        break;
      case CheckboFilterOptions.SHOW_ONLY_SELECTED.id:
        selectedIds = this.state.selectedIds
        this.setState({
          isShowOnlySelected: value,
          processedList: this.sort(
            this.state.sortColumn,
            this.filterList(
              this.state.searchTextMap,
              value,
              selectedIds
            ),
            this.state.sortBy
          ),
        })
        break;
      default:
    }

    if(!_.isEqual(selectedIds.sort(), this.state.selectedIds.sort())) {
      this.props.onSelectableChange(selectedIds)
    }
  }

  headerRenderer({
    dataKey,
    label,
  }) {
    const defaultCell = (
      <div className={`cell ${dataKey}`}>
        {label}
      </div>
    )
    const filterableCell = (
      <div className={`cell ${dataKey}`}>
        {label}
        <FilterableHeaderInput
          dataKey={dataKey}
          onSearchTextChange={this.onSearchTextChange}
        />
      </div>
    )

    let column
    let filterable
    switch(dataKey) {
      case '_counter':
      case '_action':
        return defaultCell
      case '_checkbox':
        return <CheckboxHeaderCell
          dataKey={dataKey}
          hasSelectedIds={_.size(this.state.selectedIds) > 0}
          onOptionClicked={this.onCheckboxHeaderMenuClicked}
        />
      default:
        column = _.find(this.props.columns, ['key', dataKey]);
        filterable = this.isFilterableColumn(column)

        if(_.isUndefined(column.sortable) || column.sortable === true) {
          let sortBy = null
          if(this.state.sortColumn === dataKey) {
            sortBy = this.state.sortBy
          }

          return <SortableHeaderCell
            dataKey={dataKey}
            label={label}
            sortBy={sortBy}
            onSortChange={this.onSortChange}
            filterable={filterable}
            onSearchTextChange={this.onSearchTextChange} />
        }

        if(filterable) {
          return filterableCell
        }

        return defaultCell
    }
  }

  renderCheckbox(row) {
    const { rowId } = this.props

    return (
      <Checkbox
        checked={this.isRowSelected(row[rowId])}
        onChange={e => this.onRowSelect(row[rowId], e.target.checked)}
      />
    )
  }

  isRowSelected(rowId, selectedIds = this.state.selectedIds) {
    return _.includes(selectedIds, rowId)
  }

  onRowSelect(rowId, isChecked) {
    const selectedIds = [...this.state.selectedIds]
    if(isChecked) {
      selectedIds.push(rowId)

      this.setState({
        selectedIds,
      })
    } else {
      _.pull(selectedIds, rowId)

      this.setState({
        selectedIds,
      })
    }

    if(this.state.isShowOnlySelected) {
      this.setState({
        selectedIds,
        processedList: this.sort(
          this.state.sortColumn,
          this.filterList(
            this.state.searchTextMap,
            this.state.isShowOnlySelected,
            selectedIds
          ),
          this.state.sortBy
        ),
      })
    } else {
      this.setState({
        selectedIds,
      })
    }

    this.props.onSelectableChange(selectedIds)
  }

  columnCellRenderer({ dataKey, parent, rowIndex, columnIndex }) {
    const row = this.rowGetter({ index: rowIndex });

    let content
    let column
    switch(dataKey) {
      case '_counter':
        content = rowIndex + 1
        break;
      case '_checkbox':
        content = this.renderCheckbox(row)
        break;
      case '_action':
        content = this.props.actions.contentRenderer(row[this.props.rowId])
        break;
      default:
        column = _.find(this.props.columns, ['key', dataKey])
        if(_.isUndefined(column.highlightable) || column.highlightable === true) {
          content = <Highlighter
            highlightClassName="highlight"
            searchWords={[this.state.searchTextMap[dataKey]]}
            textToHighlight={_.toString(row[dataKey])}
          />
        } else {
          content = row[dataKey]
        }
    }

    return (
      <CellMeasurer
        cache={this.cellMeasurerCache}
        columnIndex={columnIndex}
        key={dataKey}
        parent={parent}
        rowIndex={rowIndex}>
        <div className={`cell ${(row.cellClassName && row.cellClassName[dataKey]) || ''}`}>
          {content}
          {this.isLastRowIndex(rowIndex) && columnIndex === 0 && this.renderFooter()}
        </div>
      </CellMeasurer>
    );
  }

  renderFooter() {
    return (
      <Footer isLoading={this.state.isAbleToLoadMoreData || this.state.isLoading} />
    )
  }

  renderCounterColumn() {
    return (
      <Column
        className="table-column _counter"
        dataKey="_counter"
        label="No"
        width={counterColumnWidth}
        headerRenderer={this.headerRenderer}
        cellRenderer={this.columnCellRenderer}
      />
    )
  }

  renderCheckboxColumn() {
    return (
      <Column
        className="table-column _checkbox last-column"
        dataKey="_checkbox"
        label="checkbox"
        width={checkboxColumnWidth}
        headerRenderer={this.headerRenderer}
        cellRenderer={this.columnCellRenderer}
      />
    )
  }

  renderActionsColumn() {
    const classNames = ['table-column', '_action']
    if(!this.props.selectable) {
      classNames.push('last-column')
    }

    return (
      <Column
        className={_.join(classNames, ' ')}
        dataKey="_action"
        label="Actions"
        width={this.props.actions.width}
        headerRenderer={this.headerRenderer}
        cellRenderer={this.columnCellRenderer}
      />
    )
  }

  renderColumn(tableClass, cols, col, colIndex) {
    const classNames = ['table-column', col.key]
    let width = col.width
    if(this.isLastColumn(tableClass, cols, colIndex)) {
      classNames.push('last-column')

      if(tableClass === mainTableClass && this.hasFixedRightTableColumn()) {
        const totalNonLastColumnWidth = _.sum(
          _.map(
            _.filter(this.getMainTableColumns(), theCol =>
              theCol.key !== col.key
            ), 'width')
        )
        const desireWidth = this.state.mainTableClientWidth - totalNonLastColumnWidth
        if(desireWidth > width) {
          width = desireWidth
        }

      }
    }

    return (
      <Column
        className={_.join(classNames, ' ')}
        key={col.key}
        dataKey={col.key}
        label={col.label}
        width={width}
        headerRenderer={this.headerRenderer}
        cellRenderer={this.columnCellRenderer}
      />
    )
  }

  renderTable(options) {
    const {
      height: fixedHeight,
      rowHeight: fixedRowHeight,
      showCounter,
      selectable,
      actions,
    } = this.props

    const shouldRenderCounterColumn = options.class === mainTableClass && showCounter
    const shouldRenderCheckboxColumn = options.class === fixedRightTableClass && selectable
    const shouldRenderActionsColumn = options.class === fixedRightTableClass && actions

    return (
      <AutoSizer disableWidth={true} disableHeight={!!fixedHeight}>
        {({ height }) => (
          <StyledVirtualizedTable
            className={`virtualized-table ${options.class}`}
            gridClassName="table-content-grid"
            showVerticalScrollbar={options.showVerticalScrollbar}
            isFixedRowHeight={fixedRowHeight > 0}
            headerHeight={this.getHeaderHeight()}
            width={options.width}
            height={fixedHeight ? fixedHeight : height}
            headerClassName="table-column-header"
            rowClassName={this.getRowClassName}
            rowHeight={this.getRowHeight}
            rowGetter={this.rowGetter}
            rowCount={this.state.processedList.length}
            overscanRowCount={this.props.overscanRowCount}
            {...options.props}>
            {shouldRenderCounterColumn && this.renderCounterColumn()}

            {options.columns.map((col, colIndex) =>
              this.renderColumn(options.class, options.columns, col, colIndex)
            )}

            {shouldRenderActionsColumn && this.renderActionsColumn()}
            {shouldRenderCheckboxColumn && this.renderCheckboxColumn()}
          </StyledVirtualizedTable>
        )}
      </AutoSizer>
    )
  }

  onScroll(tableName, onScroll) {
    if(!this.state.currentScrollingTable || tableName === this.state.currentScrollingTable) {
      return onScroll
    }
  }

  scrollTop(tableName, scrollTop) {
    if(!this.state.currentScrollingTable || tableName !== this.state.currentScrollingTable) {
      return scrollTop
    }
  }

  renderMainTable({ onScroll, scrollTop }) {
    const { showCounter } = this.props

    const mainTableColumns = this.getMainTableColumns()

    let tableWidth = _.sumBy(mainTableColumns, 'width')
    if(showCounter) {
      tableWidth += counterColumnWidth
    }

    if(this.state.mainTableClientWidth > tableWidth) {
      tableWidth = this.state.mainTableClientWidth
    }

    return (
      <div
        ref={this.mainTableHolderRef}
        className={mainTableHolderClass}
        onMouseEnter={() => this.tableOnMouseEnter(mainTableClass)}
        onMouseLeave={() => this.tableOnMouseLeave()}>
        {this.renderTable({
          class: mainTableClass,
          columns: mainTableColumns,
          width: tableWidth + 1,
          props: {
            onRowsRendered: this.mainTableOnRowsRendered,
            noRowsRenderer: this.mainTableNoRowsRenderer,
            onScroll: this.onScroll(mainTableClass, onScroll),
            scrollTop: this.scrollTop(mainTableClass, scrollTop),
          },
          showVerticalScrollbar: !this.hasFixedRightTableColumn(),
        })}
      </div>
    )
  }

  renderFixedRightTable({ onScroll, scrollTop }) {
    if(this.hasFixedRightTableColumn()) {
      const { selectable, actions } = this.props
      const columns = this.getFixedRightTableColumns()

      let tableWidth = _.sumBy(columns, 'width')
      if(selectable) {
        tableWidth += checkboxColumnWidth
      }

      if(actions) {
        tableWidth += actions.width
      }

      return (
        <div
          className={fixedRightTableHolderClass}
          onMouseEnter={() => this.tableOnMouseEnter(fixedRightTableClass)}
          onMouseLeave={() => this.tableOnMouseLeave()}>
          {this.renderTable({
            class: fixedRightTableClass,
            columns: this.getFixedRightTableColumns(),
            width: tableWidth,
            props: {
              style: { maxWidth: `${tableWidth + fixedRightTableBorderWidth - 1}px` },
              onScroll: this.onScroll(fixedRightTableClass, onScroll),
              scrollTop: this.scrollTop(fixedRightTableClass, scrollTop),
            },
            showVerticalScrollbar: true,
          })}
        </div>
      )
    }
  }

  render() {
    let innerClassName = this.props.className || ''
    return (
      <ScrollSync>
        {({ onScroll, scrollTop }) => (
          <div className={innerClassName + ' flex-row'}>
            {this.renderMainTable({ onScroll, scrollTop })}
            {this.renderFixedRightTable({ onScroll, scrollTop })}
          </div>
        )}
      </ScrollSync>
    )
  }
}

type TableProps = {
  columns: Array<ColumnProps>,
  list?: Array<RowProps>,
  loading?: boolean,
  onLoadMore?: func,
  reloadId?: number,
  rowId?: string,
  className?: string,
  showCounter?: boolean,
  selectable?: boolean,
  onSelectableChange?: func,
  actions?: ActionsProps,
  width?: number,
  height?: number,
  rowHeight?: number,
  overscanRowCount?: number,
  onRowsRendered?: func,
}

type ColumnProps = {
  key: string,
  label: string,
  width: number,
  isFixedRight?: boolean,
  sortable?: boolean,
  sortFn?: func,
  filterable?: boolean,
  filterFn?: func,
  highlightable?: boolean,
}

type RowProps = {
  rowClassName?: string,
  ...Object
}

type ActionsProps = {
  width: number,
  contentRenderer: func,
}

export default Table
