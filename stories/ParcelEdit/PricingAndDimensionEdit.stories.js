import React from 'react'
import PricingAndDimensionEdit from '@app/components/ParcelEdit/PricingAndDimensionEdit'
import { storiesOf } from '@storybook/react'
import { TestIntlProviderWrapper } from '@internals/testing/test-helpers'

function submitHandler (props) {
  return new Promise((resolve, reject) => {
    function handleLoadingLag () {
      window.alert('Save Button is Clicked [FormValue:' + JSON.stringify(props) + ']')
      reject(new Error('Error when saving'))
    }
    setTimeout(handleLoadingLag, 2000)
  })
}

storiesOf('ParcelEdit', module).add('PricingAndDimensionEdit', () => (
  <TestIntlProviderWrapper>
    <PricingAndDimensionEdit
      deliveryFee={2.81}
      codFee={undefined}
      insuranceFee={0}
      handlingFee={0.1}
      gst={0.06}
      total='SGD 2.97'
      insuredValue={0.1}
      parcelSize='M'
      weight={0.1}
      dimensions={{ width: 1, height: 2, length: 3 }}
      currency='SGD'
      onSaveChangesButtonClicked={(props) => submitHandler(props)}
      visible />
  </TestIntlProviderWrapper>
))
