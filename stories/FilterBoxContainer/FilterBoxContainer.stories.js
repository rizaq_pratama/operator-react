/* eslint-disable no-console */
import React from 'react'
import { Button } from 'antd'
import { storiesOf } from '@storybook/react'
import FilterBoxContainer from '@app/components/Generic/FilterBoxContainer'

let allFilters = [
  {
    type: 'multipleText',
    label: 'Tracking IDs',
    key: 'tracking_ids_2',
  },
  {
    type: 'time',
    label: 'Creation Time',
    key: 'creation_time',
  },
  {
    type: 'autocomplete',
    options: [
      'Pending',
      'Cancelled',
      'Completed',
      'Delivery Fail',
    ],
    selectedOptions: ['Cancelled', 'Completed'],
    label: 'Granular Status',
    key: 'granular_status',
  },
  {
    type: 'time',
    label: 'Deletion Time',
    key: 'deletion_time',
  },
  {
    type: 'autocomplete',
    label: 'Shipper',
    key: 'shipper',
    options: (text) =>
      new Promise((resolve, reject) => {
        const options = [
          'Shipper ABC',
          'Shipper BCD',
          'Shipper CDE',
        ]
        resolve(options);
      }),
  },
  {
    type: 'date',
    label: 'Creation Date',
    key: 'creation_date',
  },
  {
    type: 'boolean',
    label: 'Archived Routes',
    property: 'Archived',
    key:'archived_routes',
  },
  {
    type: 'range',
    label: 'Number of Parcels',
    property: 'Parcels',
    startValue: 3,
    endValue: 7,
    min: 0,
    max: 10,
    key: 'no_of_parcels',
  },
  {
    type: 'multipleText',
    label: 'Tracking IDs',
    key: 'tracking_ids',
  },
  {
    type: 'single',
    label: 'Aged Days',
    key: 'aged_days',
  },
]

const testPossibleFilters = [
  'tracking_ids_2',
  'creation_time',
]

const filterBoxRef = React.createRef()

storiesOf('FilterBoxContainer', module)
  .add('FilterBox', () => {
    allFilters = allFilters.reduce((acc, f) => {
      acc[f.key] = f
      return acc
    }, {})

    return (
      <div style={{width: '100%'}}>
        <FilterBoxContainer
          ref={filterBoxRef}
          allFilters={allFilters}
          possibleFiltersKey={testPossibleFilters}
          selectedFiltersKey={Object.keys(allFilters).filter(k => testPossibleFilters.indexOf(k) === -1)}
        />
        <Button
          style={{ marginTop: '1em' }}
          type='primary'
          block
          onClick={() => alert(JSON.stringify(filterBoxRef.current.getValues(), null, 2))}>
          Submit
        </Button>
      </div>
    )
  })
