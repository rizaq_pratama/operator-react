// @flow
import React, {useState} from 'react'
import { Input, TimePicker, Radio, Col, Row, Card } from 'antd'
import { injectForm } from '@app/utils/injectForm'
import { PickupSchedule, DeliverySchedule } from '../Types'
import FormControl from '@app/components/Generic/FormControl'
import { padLeft } from '@app/utils/helpers'
import { T } from '@app/components/Generic/T'
import Moment from 'moment'
import { Colors } from '@app/themes'
import { CountryUtils } from '@nv/react-commons/src/Utils'
import { FormContext } from '@app/components/Generic/Contexts/Form'

const RadioGroup = Radio.Group

type PickupProps = {
  action: string,
  whom: string
} & PickupSchedule;

type DeliveryProps = {
  action: string,
  whom: string
} & DeliverySchedule;

function toTimeRange([f, t]) {
  return `${padLeft(f, 2, '0')}:00 ~ ${padLeft(t, 2, '0')}:00`
}

export function DeliverySlotsForm(props: DeliveryProps) {
  const { action } = props;
  const deliverySlots = CountryUtils.getDeliveryTimeslotInfo('SG').map(s => toTimeRange(s.range)).sort()
  return (
    <Card title={<T id={`commons.model.${action || 'delivery'}-schedule`} />}>
      <Row>
        <FormControl
          fieldName='commons.model.delivery-date'
          type='date'
          stateKey='slots.scheduledDate'
          span={8}
          options={{ initialValue: Moment.parseZone(props.scheduledDate) }}
        />
        <FormControl
          fieldName={'commons.model.delivery-timeslot'}
          type='dropdown'
          stateKey='slots.scheduledTimeslot'
          span={8}
          menu={deliverySlots}
          options={{ initialValue: props.selectedTimeslot }}
        />
      </Row>
      <Row>
        <FormControl
          fieldName={'commons.model.change-requested-by'}
          type='checkbox'
          stateKey={'slots.requestedByShipper'}
          span={8}
          options={{
            initialValue: props.requestedByShipper,
          }}
          checkboxText={'commons.model.shipper'}
        />
      </Row>
    </Card>
  )
}

export function StandardPickup(props: {scheduledDate: string, scheduledTimeslot: string, requestedByShipper: boolean}) {
  const {disabled} = props
  // TODO get country from somewhere
  const pickupSlots = CountryUtils.getPickupTimeslotInfo('SG').map(s => toTimeRange(s.range)).sort()
  return (
    <>
      <Row>
        <FormControl
          fieldName='commons.model.pickup-date'
          type='date'
          stateKey='slots.standardScheduledDate'
          disabled={disabled}
          span={8}
          options={{ initialValue: Moment.parseZone(props.scheduledDate) }}
        />
        <FormControl
          fieldName={'commons.model.delivery-timeslot'}
          type='dropdown'
          disabled={disabled}
          stateKey='slots.standardScheduledTimeslot'
          span={8}
          menu={pickupSlots}
          options={{ initialValue: props.scheduledTimeslot }}
        />
      </Row>
      <Row>
        <FormControl
          fieldName={'commons.model.change-requested-by'}
          type='checkbox'
          stateKey={'slots.standardRequestedByShipper'}
          disabled={disabled}
          span={8}
          options={{
            initialValue: props.requestedByShipper,
          }}
          checkboxText={'commons.model.shipper'}
        />
      </Row>
    </>
  )
}

export function PremiumPickup(props: Props) {
  const { disabled } = props
  const timeControlStyle = {
    display: 'inline-block',
    marginRight: '2px' ,
    marginTop: 0,
  }
  // TODO implement fixed 90 mins window
  return (
    <FormContext.Consumer>
      { form =>
        <>
          <Row type='flex'>
            <FormControl
              fieldName='commons.model.pickup-date'
              type='date'
              disabled={disabled}
              stateKey='slots.premiumScheduledDate'
              span={8}
              options={{ initialValue: Moment.parseZone(props.scheduledDate) }}
            />
            <FormControl
              type='custom'
              span={16}
              fieldName='container.parcel-edit.pickup-timeslot'
              value={
                <Row type='flex'>
                  <Col span={24}>
                    <label style={timeControlStyle} className='data-label'>Start</label>
                    {
                      form.getFieldDecorator('slots.scheduledTimeslotStart')(
                        <TimePicker format='HH:mma'
                                    disabled={disabled}></TimePicker>
                      )
                    }
                    <span style={{display: 'inline-block', width: '6px'}}></span>
                    <label className='data-label' style={timeControlStyle}>End</label>
                    {
                      form.getFieldDecorator('slots.scheduledTimeslotEnd')(
                        <TimePicker format='HH:mma'
                                    disabled={disabled}></TimePicker>
                      )
                    }
                  </Col>
                </Row>
              }
            />
          </Row>
          <Row>
            <FormControl
              fieldName={'commons.model.change-requested-by'}
              type='checkbox'
              stateKey='slots.premiumRequestedByShipper'
              disabled={disabled}
              span={8}
              options={{
                initialValue: props.requestedByShipper,
              }}
              checkboxText={'commons.model.shipper'}
            />
          </Row>
        </>
      }

    </FormContext.Consumer>
  )
}


export function PickupSlotsForm(props: PickupProps) {
  const [ type, setType ] = useState('standard')
  const commonStyle = {
    padding: '6px 20px',
    marginLeft: '20px',
    marginTop: '10px',
    marginBottom: '10px',
    border: `1px solid ${Colors.nvNeutral200}`,
    borderRadius: '4px',
  }
  const standardContainerStyle = {
    backgroundColor: type === 'standard' ? 'transparent': Colors.nvNeutral050,
    ...commonStyle,
  }
  const premiumContainerStyle = {
    backgroundColor: type === 'premium' ? 'transparent': Colors.nvNeutral050,
    ...commonStyle,
  }
  return (
    <FormContext.Consumer>
      {form =>
        <Card title={<T id={'commons.model.pickup-schedule'} />}>
          <Row>
            <RadioGroup onChange={onChange.bind(null, form)} value={type} style={{width: '100%'}}>
              <Radio value='standard' style={{fontWeight: type === 'standard' ? '500': '400'}}>
                Standard Pickup
              </Radio>
              <div style={standardContainerStyle}>
                <StandardPickup disabled={type!=='standard'} {...props} />
              </div>
              <Radio value='premium' style={{fontWeight: type === 'premium' ? '500': '400'}}>
                Premium Pickup
              </Radio>
              <div style={premiumContainerStyle}>
                <PremiumPickup disabled={type!=='premium'} {...props} />
              </div>
            </RadioGroup>
            {form.getFieldDecorator('slots.scheduleType')(<Input type='hidden'/>)}
          </Row>
        </Card>
      }
    </FormContext.Consumer>
  )

  function onChange(form, ev) {
    setType(ev.target.value)
    form.setFieldsValue({'slots.scheduleType': ev.target.value})
  }
}

export default {
  DeliverySlotsForm: injectForm(DeliverySlotsForm),
  PickupSlotsForm: injectForm(PickupSlotsForm),
}
