import React from 'react'
import { shallowWithIntl } from '@internals/testing/test-helpers'
import { AvpoolFilter } from '../index'
import moment from 'moment'

const testClick = jest.fn()
const testHandleUpdateFilter = jest.fn()

describe('<AvpoolFilter />', () => {

  let wrapper, instance
  beforeEach(() => {
    wrapper = shallowWithIntl(
      <AvpoolFilter
        handleUpdateFilter={testHandleUpdateFilter}
        handleClick={testClick} />
    )
    instance = wrapper.instance()
  })
  it('Should render without error', () => {
    expect(wrapper).toBeDefined()
  })
  it('should call click callback function', () => {
    instance.handleInitButton({
      inbounded_only: '',
      is_sameday: true,
      is_priority: true,
      date: moment(),
    })
    expect(testClick).toBeCalled()
  })
  it('should handle input change', () => {
    instance.handleInputChange({
      target: {
        checked: true,
        type: 'checkbox',
        name: 'test',
      },
    })
    expect(testHandleUpdateFilter).toBeCalled()
  })
  it('should handle date change', () => {
    instance.handleDateChange(moment())
    expect(testHandleUpdateFilter).toBeCalled()
  })
})
