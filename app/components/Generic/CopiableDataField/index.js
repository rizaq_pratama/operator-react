// @flow
import React from 'react'
import styled from 'styled-components'
import { Colors } from '@app/themes/index'
import { Col, Icon, Row } from 'antd'
import { CopyToClipboard } from 'react-copy-to-clipboard'

const StyledIcon = styled(Icon)`
  margin-left: 5px;
  font-size: 12px;
  margin-right: 10px;
`
const coverInnerStyle = {
  lineHeight: 'calc(100% + 16px)',
  margin: '-4px',
  display: 'inline-block',
  padding: '0 4px',
  width: '100px',
  textAlign: 'center',
  fontSize: '14px',
  borderBottomLeftRadius: '3px',
}


const coverStyle = {
  cursor: 'default',
  borderRadius: '3px',
  boxShadow: `
    0 1px 5px 0 rgba(0, 0, 0, 0.1),
    0 3px 1px -2px rgba(0, 0, 0, 0.12),
    0 2px 2px 0 rgba(0, 0, 0, 0.14)`,
  position: 'absolute',
  left: '-2px',
  top: '-4px',
  width: 'calc(100% + 4px)',
  minHeight: '100%',
  textAlign: 'right',
  padding: '4px',
}

const StyledInnerRow = styled(Row)`
  padding-left: 5px;
  padding-bottom: 4px;
`

type Props = {
  value: string | number,
  span: number,
  icon: string,
  fontSize: string,
  className: string,
}

class CopiableText extends React.Component<Props> {
  constructor (props) {
    super(props)
    this.state = {
      isHovered: false,
      isCopied: false,
    }
  }
  handleCopy () {
    this.setState({
      isCopied: true,
    })
  }
  handleMouseLeave () {
    this.setState({
      isHovered: false,
      isCopied: false,
    })
  }
  handleMouseEnter () {
    if (this.props.value == null || this.props.value === '') {
      return
    }
    this.setState({
      isHovered: true,
    })
  }
  render () {
    let icon = null
    if (this.props.icon) {
      icon = (
        <StyledIcon type={this.props.icon} />
      )
    }
    return (
      <CopyToClipboard text={this.props.value}
            onMouseEnter={this.handleMouseEnter.bind(this)}
            onMouseLeave={this.handleMouseLeave.bind(this)}
            className={this.props.className}
          >
        <div style={{position: 'relative'}}>
          <StyledInnerRow
            onClick={this.handleCopy.bind(this)}
            align='middle'
            type={'flex'} >
            {icon}
            <Col span={this.props.icon? 16 : 18}>
              {this.props.value}
            </Col>
          </StyledInnerRow>
          {this.state.isHovered?
              <div className='cover' style={coverStyle}>
                <span onClick={this.handleCopy.bind(this)}
                  style={{...coverInnerStyle, backgroundColor: this.state.isCopied ? Colors.nvCopyGreen : Colors.nvBackgroundGrey}} >
                  {this.state.isCopied ? 'Copied text!' : 'Click to copy'}
                </span>
              </div> :null}
        </div>
      </CopyToClipboard>
    )
  }
}

export default CopiableText
