import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import _ from 'lodash'

import { T } from '@app/components/Generic/T'
import { ApplicationStyles } from '@app/themes'

const StyledLabel = styled.label`
  ${ApplicationStyles.fontFamily};
  font-size: 14px;
  font-weight: normal;
  line-height: 1.5;
  color: #8a9096;
`

const StyledP = styled.p`
  ${ApplicationStyles.fontFamily};
  font-size: 14px;
  font-weight: normal;
  line-height: 1.5;
  color: #212125;
`

const StyledDiv = styled.div`
  ${ApplicationStyles.fontFamily};
  margin-bottom: 1em;
  font-size: 14px;
  font-weight: normal;
  line-height: 1.5;
  color: #212125;
`

class ContentWithLabel extends React.Component {
  render () {
    const content = !_.includes([undefined, null], this.props.content) ? this.props.content : '-'

    let styledContent = (
      <StyledP>
        { content }
      </StyledP>
    )
    if (typeof this.props.content === 'object') {
      styledContent = (
        <StyledDiv>
          { content }
        </StyledDiv>
      )
    }

    return (
      <div>
        <StyledLabel>
          <T id={this.props.label} />
        </StyledLabel>
        { styledContent }
      </div>
    )
  }
}

ContentWithLabel.propTypes = {
  label: PropTypes.string,
  content: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
  ]),
}

export default ContentWithLabel
