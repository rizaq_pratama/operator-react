import React from 'react'
import { shallow } from 'enzyme'
import renderer from 'react-test-renderer'
import { mountWithIntl, TestIntlProviderWrapper } from '@internals/testing/test-helpers'

import JobHistory from '../index'

let testFunctionInput
const onJobIdClicked = jest.fn()
const onPodIdClicked = jest.fn()
const onRouteIdClicked = jest.fn()
const testJobs = [
  {
    jobId: 5,
    routeId: 52,
    podId: 12,
  },
]

describe('JobHistory', () => {

  let wrapper, instance
  beforeEach(() => {
    testFunctionInput =  {
      preventDefault: () => {},
      target: {
        id: 123,
      },
    }
    wrapper = mountWithIntl(
      <JobHistory
        jobs={testJobs}
        onJobIdClicked={onJobIdClicked}
        onPodIdClicked={onPodIdClicked}
        onRouteIdClicked={onRouteIdClicked}
      />
    )
    instance = wrapper.instance()
  })
  it('should render', () => {
    const renderedComponent = shallow(<JobHistory />)
    expect(renderedComponent).toBeDefined()
  })

  it('should match snapshot', () => {
    const renderedComponent = renderer.create(
      <TestIntlProviderWrapper>
        <JobHistory />
      </TestIntlProviderWrapper>
    )
    expect(renderedComponent).toMatchSnapshot()
  })
  it('should call callback on job id click', () => {
    instance.handleOnJobIdClick(testFunctionInput)
    wrapper.find('a').find('.job-id').simulate('click', testFunctionInput)
    expect(onJobIdClicked).toHaveBeenCalledTimes(2)
  })
  it('should call callback on pod id click', () => {
    instance.handleOnPodIdClick(testFunctionInput)
    wrapper.find('a').find('.pod').simulate('click', testFunctionInput)
    expect(onPodIdClicked).toHaveBeenCalledTimes(2)
  })
  it('should call callback on route id click', () => {
    instance.handleOnRouteIdClick(testFunctionInput)
    wrapper.find('a').find('.route-id').simulate('click', testFunctionInput)
    expect(onRouteIdClicked).toHaveBeenCalledTimes(2)
  })
})
