// @flow
export type ParcelInformation = {
  id: number,
  orderId: number,
  parcelTrackingId: string,
  orderSource: string,
  createdOn: string
}

export type ParcelFormData = {
  stampTrackingId: string,
  deliveryType: string,
}
