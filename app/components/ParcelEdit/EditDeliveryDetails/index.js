//  @flow
import React, {useState} from 'react'
import { connect } from 'react-redux'
import SubmittableModal from '../../SubmittableModal'
import { notification } from 'antd'
import {DeliveryForm} from '../GenericEditDetails'
import omit from 'lodash/omit'
import selectors from '@app/containers/ParcelEdit/selectors'
import NC from '@app/services/api/ninjaControlApi'
import type { Job } from '@nv/react-commons/src/Migration_corev2/EntityTypes'

type Props = {
  visible: boolean,
  onClose: Function
} & Job

export function EditDeliveryDetails(props: Props) {
  const [isLoading, setIsLoading] = useState(false)

  return (
    <SubmittableModal
      visible={props.visible}
      disabled={isLoading}
      title={'commons.model.edit-delivery-details'}
      onSubmit={onSubmit}
      onClose={props.onClose} >
      <DeliveryForm {...omit(props, ['visible', 'onClose'])} />
    </SubmittableModal>
  )

  async function onSubmit(data) {
    const { slots, address, details } = data
    const slotsData = {
      date: slots.scheduledDate? slots.scheduledDate.format('YYYY-MM-DD'): '',
      timeSlot: slots.scheduledTimeslot,
      requestedByShipper: slots.requestedByShipper,
    }

    const updateDetails = NC.updateDeliveryDetails(props.id, details)
    const updateSlots = NC.rescheduleDeliveryJob(props.id, slotsData)
    const updateAddress = NC.updateDeliveryAddress(props.id, address)

    setIsLoading(true)

    const res = await Promise.all([updateDetails, updateSlots, updateAddress])

    if (res.every(r => !r.ok)) {
      notification.success({
        message: 'Saved successfully',
      })
      props.onClose()
    } else {
      // TODO show errors after we finalize response error format
    }

    setIsLoading(false)
  }
}

const mapStateToProps = state => {
  const props = selectors.DeliveryJobSelector(state)
  return props || {}
}

export default connect(mapStateToProps)(EditDeliveryDetails)
