import { fromJS } from 'immutable'
import reducer, { INITIAL_STATE, Creators } from '../redux'

describe('parcelEditReducer', () => {
  let initialState
  beforeEach(() => {
    initialState = fromJS(INITIAL_STATE)
  })

  it('returns the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState)
  })

  it('set parcel', () => {
    const newState = reducer(undefined, Creators.setParcel('abc'))
    expect(newState).toEqual(initialState.set('parcel', 'abc'))
  })

  it('set jobs', () => {
    const newState = reducer(undefined, Creators.setJobs('abc'))
    expect(newState).toEqual(initialState.set('jobs', 'abc'))
  })

  it('set order', () => {
    const newState = reducer(undefined, Creators.setOrder('abc'))
    expect(newState).toEqual(initialState.set('order', 'abc'))
  })

  it('set destination zone', () => {
    const newState = reducer(undefined, Creators.setDestinationZone('abc'))
    expect(newState).toEqual(initialState.set('destinationZone', 'abc'))
  })

  it('set current zone', () => {
    const newState = reducer(undefined, Creators.setCurrentRoute('abc'))
    expect(newState).toEqual(initialState.set('currentRoute', 'abc'))
  })

  it('set events', () => {
    const newState = reducer(undefined, Creators.setEvents('abc'))
    expect(newState).toEqual(initialState.set('events', 'abc'))
  })

  it('set tickets', () => {
    const newState = reducer(undefined, Creators.setTickets('abc'))
    expect(newState).toEqual(initialState.set('tickets', 'abc'))
  })

  it('set job to route mappping', () => {
    const newState = reducer(undefined, Creators.setJobToRouteMapping('abc'))
    expect(newState).toEqual(initialState.set('jobToRouteMapping', 'abc'))
  })

  it('set route to driver mapping', () => {
    const newState = reducer(undefined, Creators.setRouteToDriverMapping('abc'))
    expect(newState).toEqual(initialState.set('routeToDriverMapping', 'abc'))
  })

  it('set job to proof mapping', () => {
    const newState = reducer(undefined, Creators.setJobToProofsMapping('abc'))
    expect(newState).toEqual(initialState.set('jobToProofsMapping', 'abc'))
  })
})
