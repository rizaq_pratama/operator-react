//  @flow
import React from 'react'
import styled from 'styled-components'
import { Colors } from '@app/themes'
import { Icon } from 'antd'

const SuccessIcon = styled(Icon)`
  color: ${Colors.nvCheckGreen}
`
const PendingIcon = styled(Icon)``

const FailedIcon = styled(Icon)`
  color: ${Colors.nvPriRed}
`

const CancelledIcon = styled(Icon)`
  color: ${Colors.nvPriRed}
`

type StatusIconProps = {
  style: object,
  type: string
}

class StatusIcon extends React.Component<StatusIconProps> {
  render () {
    switch (this.props.type) {
      case 'Completed':
        return <SuccessIcon type='check' style={this.props.style} className='status-icon'/>
      case 'Pending':
      case 'Ready For Routing':
      case 'Routed':
      case 'Transferring of Control':
      case 'Locked':
      case 'In-Progress':
        return <PendingIcon type='loading-3-quarters' style={this.props.style} className='status-icon' />
      case 'On-Hold':
      case 'Failed':
        return <FailedIcon type='info-circle' style={this.props.style} className='status-icon' />
      case 'Cancelled':
        return <CancelledIcon type='close' style={this.props.style} className='status-icon' />
      default:
        return null
    }
  }
}

export default StatusIcon
