export function makeAPIProxy (...apis) {
  return new Proxy({}, {
    get: (obj, key) => {
      for (const api of apis) {
        const fn = api[key]
        if (typeof fn === 'function') {
          return (...rest) =>
            fn.call(null, ...rest)
            // TODO handle exception case
              .then(data => {
                if (data && 'ok' in data) {
                  // apisauce
                  if (data.ok) {
                    if ('data' in data) {
                      if ('data' in data.data) {
                        return data.data.data || {}
                      }
                      return data.data || {}
                    }
                  } else {
                    throw data
                  }
                }
                return data || {}
              })
        }
      }
      throw new Error('No function matched')
    },
  })
}
