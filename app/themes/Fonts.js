import { css } from 'styled-components'

const light = css`
  font-weight: 300;
`
const medium = css`
  font-weight: 500
`
const bold = css`
  font-weight: 700;
`
export const style = {
  light,
  medium,
  bold,
}
