import { addLocaleData } from 'react-intl'
import { withKnobs } from '@storybook/addon-knobs'
import enLocaleData from 'react-intl/locale-data/en'
import { translationMessages } from '@app/i18n'
import React from 'react'
import { configure, addDecorator } from '@storybook/react'
import { setIntlConfig, withIntl } from 'storybook-addon-intl'
import { withSmartKnobs } from 'storybook-addon-smart-knobs'
import '../app/themes/global.css'

import Container from './Container'


// Knobs Addon
addDecorator(withSmartKnobs)
addDecorator(withKnobs)

// React I18n Addon
addLocaleData(enLocaleData)
const getMessages = (locale) => translationMessages[locale]
setIntlConfig({
  locales: ['en'],
  defaultLocale: 'en',
  getMessages
})
addDecorator(withIntl)

// Styling
addDecorator(story => <Container story={story}/>)

// automatically import all files ending in *.stories.js
const req = require.context('../stories', true, /.stories.js$/)

function loadStories () {
  req.keys().forEach(filename => req(filename))
}

configure(loadStories, module)
