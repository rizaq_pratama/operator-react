// @flow
import React from 'react'
import { Row, Card, Form } from 'antd'
import { T } from '@app/components/Generic/T'
import FormControl from '@app/components/Generic/FormControl'

type Props = {
  form: Form,
  reason: string,
}

function RtsReason(props: Props) {
  return (
    <Card title={<T id='container.parcel-edit.return-to-sender-reason' />}>
      <Row>
        <FormControl
          type='dropdown'
          fieldName='commons.reason'
          stateKey='reason'
          span={18}
          selectShowSearch
          // TODO get list of rts reasons
          menu={[]}
          form={props.form}
          options={{
            rules: [{ required: true, message: 'Please Select The RTS Reason' }],
          }}
        />
      </Row>
    </Card>
  )
}

export default RtsReason
