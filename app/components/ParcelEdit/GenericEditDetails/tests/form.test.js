import React from 'react'
import { mount, shallow } from 'enzyme'
import { AddressForm } from '../Forms/AddressForm'
import { DetailsForm } from '../Forms/DetailsForm'
import RtsReason from '../Forms/RtsReason'
import { PickupSlotsForm, DeliverySlotsForm } from '../Forms/Slots'
import { TestIntlProviderWrapper } from '@internals/testing/test-helpers'
import toJson from 'enzyme-to-json'
import AddressFinderForm from '@app/components/AddressFinderForm/index';
import { DeliveryForm, PickupForm }from '../index'
import { FormContext } from '@app/components/Generic/Contexts/Form'

jest.mock('@nv/react-commons/src/Utils', () => {
  return {
    CountryUtils: {
      getPickupTimeslotInfo: jest.fn((country) => ([
        { id: 0, range: [9, 12] },
        { id: 1, range: [12, 15] },
        { id: 2, range: [15, 18] },
      ])),
      getDeliveryTimeslotInfo: jest.fn(country => ([
        { id: 0, range: [9, 12] },
        { id: 1, range: [12, 15] },
      ])),
    },
  }
})

describe('Forms', () => {
  let form, intl
  beforeEach(() => {
    form = {
      getFieldDecorator: jest.fn(() => jest.fn()),
    }
    intl = {
      formatMessage: jest.fn(),
    }
  })

  it('renders rts reason form', () => {
    const wrapper = mount(
      <TestIntlProviderWrapper>
        <FormContext.Provider value={form}>
          <RtsReason reason={'hello world'} form={form}/>
        </FormContext.Provider>
      </TestIntlProviderWrapper>
    )

    expect(toJson(wrapper.find(RtsReason))).toMatchSnapshot()
  })

  describe('Address form', () => {
    describe('With finder', () => {
      it('should shallow render', () => {
        const rendered = shallow(<AddressForm latitude={123}
                                              longitude={456}
                                              form={form}/>)
        expect(rendered.find(AddressFinderForm).length).toBe(1)

        expect(rendered).toBeDefined()
        expect(toJson(rendered)).toMatchSnapshot()
      })
    })

    describe('Without finder', () => {
      it('should shallow render', () => {
        const rendered = shallow(<AddressForm/>)
        expect(rendered).toBeDefined()

        expect(toJson(rendered)).toMatchSnapshot()
        expect(rendered.contains(<AddressFinderForm/>)).toBe(false)
      })
    })
  })

  describe('Slot form', () => {
    describe('DeliverySlotsForm', () => {
      let wrapper, schedule
      beforeEach(() => {
        schedule = {
          scheduledDate: '2018-12-12',
          scheduledTimeslot: '12PM-2PM',
          requestedByShipper: true,
        }
      })
      it('should match snapshot when requestedByShipper', () => {
        wrapper = mount(
          <TestIntlProviderWrapper>
            <FormContext.Provider value={form}>
              <DeliverySlotsForm {...schedule} form={form}/>
            </FormContext.Provider>
          </TestIntlProviderWrapper>
        )
        expect(toJson(wrapper.find(DeliverySlotsForm))).toMatchSnapshot()
      })
      it('should match snapshot when not requestedByShipper', () => {
        schedule.requestedByShipper = false
        wrapper = mount(
          <TestIntlProviderWrapper>
            <FormContext.Provider value={form}>
              <DeliverySlotsForm {...schedule} form={form}/>
            </FormContext.Provider>
          </TestIntlProviderWrapper>
        )
        expect(toJson(wrapper.find(DeliverySlotsForm))).toMatchSnapshot()
      })
    })

    describe('PickupSlotsForm', () => {
      let schedule, wrapper
      beforeEach(() => {
        schedule = {
          scheduleType: 'standard',
          scheduledDate: '2018-12-13',
          scheduledTimeslotFrom: '9pm',
          scheduledTimeslotTo: '11pm',
          requestedByShipper: false,
        }
      })

      it('should renders correct for Standard Type', () => {
        wrapper = mount(
          <TestIntlProviderWrapper>
            <FormContext.Provider value={form}>
              <PickupSlotsForm {...schedule} form={form}/>
            </FormContext.Provider>
          </TestIntlProviderWrapper>
        )
        expect(toJson(wrapper.find(PickupSlotsForm))).toMatchSnapshot()
      })

      it('should renders correct for Premium Type', () => {
        schedule.scheduleType = 'premium'
        wrapper = mount(
          <TestIntlProviderWrapper>
            <FormContext.Provider value={form}>
              <PickupSlotsForm {...schedule} form={form}/>
            </FormContext.Provider>
          </TestIntlProviderWrapper>
        )
        expect(toJson(wrapper.find(PickupSlotsForm))).toMatchSnapshot()
      })
    })
  })

  describe('Details form', () => {
    let wrapper, details
    beforeEach(() => {
      details = {
        name: 'Derek Zeng',
        contact: '28282828',
        email: 'dzeng@ninja.co',
        instruction: 'handle with care',
        whom: 'sender',
        action: 'delivery',
      }
    })
    it('should render correctly', () => {
      wrapper = mount(
        <TestIntlProviderWrapper>
          <FormContext.Provider value={form}>
            <DetailsForm {...details} form={form}/>
          </FormContext.Provider>
        </TestIntlProviderWrapper>
      )
      expect(toJson(wrapper.find(DetailsForm))).toMatchSnapshot()
    })
  })

  describe('Delivery form', () => {
    it('should contains sub components for delivery only', () => {
      const wrapper = mount(
        <TestIntlProviderWrapper>
          <FormContext.Provider value={form}>
            <DeliveryForm intl={intl}/>
          </FormContext.Provider>
        </TestIntlProviderWrapper>
      )
      expect(wrapper.find(DetailsForm)).toHaveLength(1)
      expect(wrapper.find(DeliverySlotsForm)).toHaveLength(1)
      expect(wrapper.find(AddressForm)).toHaveLength(1)
    })
  })

  describe('Pickup form', () => {
    it('should contains sub components for pickup only', () => {
      const wrapper = mount(
        <TestIntlProviderWrapper>
          <FormContext.Provider value={form}>
            <PickupForm intl={intl} form={form}/>
          </FormContext.Provider>
        </TestIntlProviderWrapper>
      )
      expect(wrapper.find(DetailsForm)).toHaveLength(1)
      expect(wrapper.find(PickupSlotsForm)).toHaveLength(1)
      expect(wrapper.find(AddressForm)).toHaveLength(1)
    })
  })
})
