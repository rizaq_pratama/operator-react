// @flow
import React from 'react'
import { connect } from 'react-redux'
import { notification } from 'antd'
import SubmittableModal from '@app/components/SubmittableModal'
import PullRouteForm from './Form'
import Route from '@app/services/api/routeApi'
import selectors from '@app/containers/ParcelEdit/selectors'

type Props = {
  visible: boolean,
  onClose?: Function,
  jobs: Object[],
}

export function ConfirmPullFromRoute (props: Props) {
  const formRef = React.createRef()
  return (
    <SubmittableModal
      ref={formRef}
      visible={props.visible}
      title='container.parcel-edit.pull-from-route'
      cancelText='commons.go-back'
      okText='container.parcel-edit.pull-from-route'
      onSubmit={onSubmit}
      onClose={props.onClose} >
      <PullRouteForm jobs={props.jobs}/>
    </SubmittableModal>
  )

  async function onSubmit(data: {jobsToPull: {id: number, jobType: string}[]}) {
    const {jobsToPull} = data
    if (jobsToPull && jobsToPull.length) {
      const promises = []
      for (const [id, type] of jobsToPull) {
        promises.push(Route.pullJobFromRoute(id, type))
      }
      const res = await Promise.all(promises)
      if (res.every(r => r.ok !== false)) {
        notification.success({
          message: 'Saved successfully',
        })
        props.onClose()
      }
    }
  }
}

const mapStateToProps = state => {
  return {
    jobs: selectors.ConfirmPullFromRouteModalSelector(state),
  }
}

export default connect(mapStateToProps)(ConfirmPullFromRoute)
