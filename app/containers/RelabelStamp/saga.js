import { takeEvery } from 'redux-saga/effects'
import ninjaControlApi from '@app/services/api/ninjaControlApi'
import shipperApi from '@app/services/api/shipperApi'
import printApi from '@app/services/api/printApi'
import { Types } from './redux'

export function * getOrderDetailsAndPrint (action) {
  const orderResponse = yield ninjaControlApi.getOrderDetailsFromScan(action.scan)
  const order = orderResponse.data
  const shipperResponse = yield shipperApi.getShipperById(order.shipperId)
  const shipper = shipperResponse.data
  const labelPayload = [{
    recipientName: order.toName,
    address1: order.toAddress1,
    address2: order.toAddress2,
    contact: order.toContact,
    country: order.toCountry,
    postcode: order.toPostcode,
    shipperName: shipper.shipperName,
    trackingId: action.scan,
    barcode: action.scan,
    routeId: '',
    driverName: '',
    template: 'hub_shipping_label_70X50',
  }]

  printApi.printByPost(labelPayload)
    .then(action.callback)
}

export default function * defaultSaga () {
  yield takeEvery(Types.GET_AND_PRINT_ORDER_REQUEST, getOrderDetailsAndPrint)
}
