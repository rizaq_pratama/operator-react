/**
 *
 * @jest-environment <rootDir>/internals/testing/puppeteer_environment.js
 */

describe('IFrame heartbeat test', () => {
    it('should be able to talk', async () => {
      await page.goto('http://localhost:8080')
      await expect(page.content()).resolves.toMatch('Passed');
    });
  }
);
