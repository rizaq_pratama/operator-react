import React from 'react'
import toJson from 'enzyme-to-json'
import { shallow, mount } from 'enzyme'
import { TestIntlProviderWrapper } from '@internals/testing/test-helpers'
import { CancelOrder } from '../index'
import NC from '@app/services/api/ninjaControlApi'

jest.mock('@app/services/api/ninjaControlApi', () => {
  return {
    cancelOrderById: jest.fn(() => {
      return { ok: true }
    }),
  }
})

describe('CancelOrderModal', () => {
  it('renders a CancelOrderModal component', () => {
    const wrapper = shallow(
      <CancelOrder
        intl={{ formatMessage: jest.fn() }}
        visible />
    )
    expect(wrapper).toBeDefined()
    expect(toJson(wrapper)).toMatchSnapshot()
  })
  it('should call NC api on modal submit', async () => {
    const onSubmit = jest.fn()
    const onClose = jest.fn()
    const wrapper = mount(
      <TestIntlProviderWrapper>
        <CancelOrder
          intl={{ formatMessage: jest.fn() }}
          onSubmit={onSubmit}
          onClose={onClose}
          visible />
      </TestIntlProviderWrapper>
    )
    const submitButton = wrapper.find('.ant-modal-footer [type="danger"]')
    submitButton.simulate('click')

    expect(NC.cancelOrderById).toHaveBeenCalled()
    await new Promise(setTimeout)
    expect(onClose).toHaveBeenCalled()
  })
})
