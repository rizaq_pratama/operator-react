import React from 'react'
import { shallow } from 'enzyme'
import { JobDetailsModal } from '../index'

const testJobInfo = {
  jobId: 12345678,
  jobType: 'Pickup',
  priorityLevel: null,
  name: 'Min Thu Ya Hlaing',
  contact: '+6585881948',
  email: 'minthuya@hotmail.com',
  address: 'Blk 62, #11-27 Woodlands Drive 16 Singapore 737895 SG',
  endTime: '5pm',
}

const testRouteInfo = {
  routeId: 433566,
  routeDate: '2018-10-11',
  assignedTo: 'F1 - Izman',
  dpId: null,
  failureReason: null,
  dnr: null,
  link: 'https://www.ninjavan.co/en-sg',
}

describe('JobDetailsModal', () => {
  it('renders a JobDetailsModal component', () => {
    const wrapper = shallow(
      <JobDetailsModal jobInformation={testJobInfo} routeInformation={testRouteInfo} visible />
    )
    expect(wrapper).toBeDefined()
  })
})
