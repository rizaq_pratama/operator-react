import { call, select, take, takeEvery, put, spawn } from 'redux-saga/effects'
import { Types, Creators } from './redux'
import { Types as GlobalTypes } from '@app/containers/Base/redux'
import JobType from '@nv/react-commons/src/Migration_corev2/Constants/JobType'
import NC from '@app/services/api/ninjaControlApi'
import Address from '@app/services/api/addressApi'
import Route from '@app/services/api/routeApi'
import Events from '@app/services/api/eventsApi'
import Ticketing from '@app/services/api/ticketingApi'
// import Driver from '@app/services/api/driversApi'
import selectors from './selectors'
import { MODAL_TYPES } from './constants'

export function * loadDestinationZone() {
  const transitJob = yield select(selectors.TransitJobSelector)
  let zone = null
  if (transitJob)  {
    try {
      zone = yield call(Address.getZoneInfo, transitJob.destinationSortingZoneId)
    } catch (e) {
      // ignore
    }
  }
  yield put(Creators.setDestinationZone(zone))
}

export function * loadParcelOrder(orderId) {
  let order
  try {
    order = yield call(NC.getOrderDetailById, orderId)
  } catch (e) {
    order = null
  }
  yield put(Creators.setOrder(order))
}

export function * loadJobRoute(job) {
  let jobRoute
  try {
    jobRoute = yield call(Route.searchJobs, job.jobType, [job.id])
  } catch (e) {
    jobRoute = []
  }
  let route = null
  if (jobRoute.length) {
    route = jobRoute[0]
    // yield spawn(loadDriver, route.route)
  }
  const mapping = yield select(selectors.JobToRouteMappingSelector)
  mapping[job.__uid__] = route
  yield put(Creators.setJobToRouteMapping(mapping))
}

/*function * loadDriver(route) {
  const mapping = yield select(selectors.RouteToDriverMappingSelector)
  if (!route.driverId) {
    mapping[route.id] = null
  } else {
    const driver = yield Driver.searchDriverOne(route.driverId)
    mapping[route.id] = driver
  }
  yield put(Creators.setRouteToDriverMapping(mapping))
}*/

export function * loadProofs(job) {
  let proofs
  try {
    yield call(NC.getProofsFromJob, job.id, job.jobType)
  } catch (e) {
    proofs = []
  }
  const mapping = yield select(selectors.JobToProofsMappingSelector)
  mapping[job.__uid__] = proofs
  // TODO load failure message
  yield put(Creators.setJobToProofsMapping(mapping))
}

export function * loadParcelAndRelated(id) {
  let parcel
  try {
    parcel = yield call(NC.getParcelById, id)
  } catch (e) {
    parcel = null
  }
  yield put(Creators.setParcel(parcel))

  if (parcel) {
    yield spawn(loadParcelOrder, parcel.orderId)
  }
}

export function * loadJobsAndRelated(parcelId) {
  let jobs
  try {
    jobs = yield call(NC.getJobsByParcelId, parcelId)
  } catch (e) {
    jobs = []
  }
  yield put(Creators.setJobs(jobs))
  if (jobs && jobs.length) {
    yield spawn(loadDestinationZone)
    for (const job of jobs) {
      job.__uid__ = `${job.jobType}/${job.id}`
      yield spawn(loadJobRoute, job)
      if (job.jobType === JobType.PARCEL_PICKUP || job.jobType === JobType.PARCEL_DELIVERY) {
        yield spawn(loadProofs, job)
      }
    }
  }
}

export function * loadTickets(parcelId) {
  let tickets
  try {
    yield call(Ticketing.getTicketsForParcel, [parcelId])
  } catch (e) {
    tickets = []
  }
  yield put(Creators.setTickets(tickets))
}

export function * loadEvents(parcelId) {
  let events
  try {
    events = yield call(Events.getEventsByParcelId, parcelId)
  } catch (e) {
    events = []
  }
  yield put(Creators.setEvents(events))
}

export function * loadParcelEdit({parcelId}) {
  yield spawn(loadParcelAndRelated, parcelId)
  yield spawn(loadJobsAndRelated, parcelId)
  yield spawn(loadTickets, parcelId)
  yield spawn(loadEvents, parcelId)
}

export function * watchModalSubmission({id}) {
  const action = yield take([GlobalTypes.CLOSE_MODAL, GlobalTypes.MODAL_FORM_SUBMITTED])
  if (action.type === GlobalTypes.MODAL_FORM_SUBMITTED) {
    // TODO modal form changed things
    // we need to update relevant models
    switch (id) {
      case MODAL_TYPES.JOB_DETAIL:
        break
    }
  }
}

export default function * defaultSaga () {
  yield takeEvery(Types.LOAD_PARCEL_EDIT_VIEW, loadParcelEdit)
  yield takeEvery(GlobalTypes.OPEN_MODAL, watchModalSubmission)
}
