import iframeClient from '../iframeClient'

function getHubs () {
  return iframeClient.request('hub_getHubs')
}

export default {
  getHubs,
}
