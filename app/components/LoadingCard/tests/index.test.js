import React from 'react'
import { shallowWithIntl } from '@internals/testing/test-helpers'
import { LoadingCard } from '../index'

describe('<LoadingCard />', () => {

  let wrapper, instance
  beforeEach(() => {
    wrapper = shallowWithIntl(
      <LoadingCard />
    )
    instance = wrapper.instance()
  })
  it('Should render without error', () => {
    expect(wrapper).toBeDefined()
  })
  it('should return a loading overlay component', () => {
    const loadingOverlay = instance.renderLoadingOverlay({})
    expect(loadingOverlay).toBeDefined()
  })
})
