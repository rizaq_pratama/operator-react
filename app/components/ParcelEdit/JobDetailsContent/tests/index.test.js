import React from 'react'
import { shallow } from 'enzyme'
import renderer from 'react-test-renderer'
import JobDetailsContent from '../index'
import { TestIntlProviderWrapper } from '../../../../../internals/testing/test-helpers'

const testJobInfo = {
  jobId: 12345678,
  jobType: 'Pickup',
  priorityLevel: null,
  name: 'Min Thu Ya Hlaing',
  contact: '+6585881948',
  email: 'minthuya@hotmail.com',
  address: 'Blk 62, #11-27 Woodlands Drive 16 Singapore 737895 SG',
  endTime: '5pm',
}

const testRouteInfo = {
  routeId: 433566,
  routeDate: '2018-10-11',
  assignedTo: 'F1 - Izman',
  dpId: null,
  failureReason: null,
  dnr: null,
  link: 'https://www.ninjavan.co/en-sg',
}

describe('JobDetailsContent', () => {
  it('matches snapshot', () => {
    const component = renderer.create(
      <div>
        <TestIntlProviderWrapper>
          <JobDetailsContent jobInformation={testJobInfo} routeInformation={testRouteInfo} />
        </TestIntlProviderWrapper>
      </div>
    )
    expect(component.toJSON()).toMatchSnapshot()
  })

  it('renders a JobDetailsContent component', () => {
    const renderedComponent = shallow(
      <JobDetailsContent jobInformation={testJobInfo} routeInformation={testRouteInfo} />
    )
    expect(renderedComponent).toBeDefined()
  })
})
