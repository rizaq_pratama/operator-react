const camelCase = require('lodash/camelCase')

function getLessVarsFromText(colorVarStr ) {
  const re = /@(.*): (.*);/
  const lines = colorVarStr.split('\n')
  const ret = {}
  for (const line of lines) {
    const m = line.match(re)
    if (m) {
      ret[m[1]] = m[2]
      ret[camelCase(m[1])] = m[2]
    }
  }
  return ret
}

exports.getLessVarsFromText = getLessVarsFromText
