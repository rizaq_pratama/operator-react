// @flow

export type SenderDetails = {
  name: string,
  contact: string,
  email: string,
  pickupInstructions: string
}

export type RecipientDetails = {
  name: string,
  contact: string,
  email: string,
  deliveryInstructions: string
}
export type PickupSchedule = {
  scheduleType: string,
  scheduledDate: string,
  scheduledTimeslotFrom: string,
  scheduledTimeslotTo: string,
  requestedByShipper: boolean
}
export type DeliverySchedule = {
  scheduledDate: string,
  scheduledTimeslot: string,
  requestedByShipper?: boolean
}
export type PickupAddress = Address
export type DeliveryAddress = Address

export type Address = {
  country: string,
  city: string,
  address1: string,
  address2: string,
  postalCode: number,
  latitude: number,
  longitude: number
}

export type PickupFormProps = SenderDetails & PickupSchedule & PickupAddress
export type DeliveryFormProps = RecipientDetails & DeliverySchedule & DeliveryAddress
export type RtsFormProps = SenderDetails & DeliverySchedule & Address & { reason: string, instruction: string }
