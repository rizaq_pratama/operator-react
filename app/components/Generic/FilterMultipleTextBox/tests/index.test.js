import React from 'react'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'
import FilterMultipleTextBox from '../index'
import { shallowWithIntl, TestIntlProviderWrapper } from '@internals/testing/test-helpers'
import 'jest-styled-components'

const testFilterMultipleTextBoxParams = {
  title: 'Tracking IDs',
}

describe('FilterMultipleTextBox', () => {
  let wrapper, instance, optionToAdd = {
    target: {
      value: 'Shipper ABC',
    },
  }
  beforeEach(() => {
    wrapper = shallowWithIntl(
      <FilterMultipleTextBox
        title={testFilterMultipleTextBoxParams.title}
      />
    )
    instance = wrapper.instance()
  })
  xit('matches snapshot', () => {
    const component = renderer.create(
      <TestIntlProviderWrapper>
        <FilterMultipleTextBox
          title={testFilterMultipleTextBoxParams.title}
        />
      </TestIntlProviderWrapper>
    )
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('renders a FilterMultipleTextBox component', () => {
    const renderedComponent = shallow(
      <TestIntlProviderWrapper>
        <FilterMultipleTextBox
          title={testFilterMultipleTextBoxParams.title}
        />
      </TestIntlProviderWrapper>
    )
    expect(renderedComponent).toBeDefined()
  })
  it('should add option to selected', () => {
    expect(instance.state['selected'].size).toBe(0)
    instance.addOption(optionToAdd)
    expect(instance.state['selected'].size).toBe(1)
  })
  it('should remove option to selected', () => {
    instance.addOption(optionToAdd)
    expect(instance.state['selected'].size).toBe(1)
    instance.removeOption('Shipper ABC')
    expect(instance.state['selected'].size).toBe(0)
  })
  it ('should empty selected', () => {
    instance.addOption(optionToAdd)
    expect(instance.state['selected'].size).toBe(1)
    instance.clear()
    expect(instance.state['selected'].size).toBe(0)
  })
})
