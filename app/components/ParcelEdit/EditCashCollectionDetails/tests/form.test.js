import React from 'react'
import { shallow } from 'enzyme'
import { EditCashForm } from '../Form'
import toJson from 'enzyme-to-json'
import FormControl from '@app/components/Generic/FormControl'


describe('Edit Cash Form', () => {
  let intl
  beforeEach(() => {
    intl = {
      formatMessage: jest.fn(),
    }
  })
  it('should shallow render', () => {
    const rendered = shallow(<EditCashForm intl={intl}/>)
    expect(rendered).toBeDefined()
    expect(rendered.find(FormControl).length).toBe(4)
    expect(toJson(rendered)).toMatchSnapshot()
  })
})
