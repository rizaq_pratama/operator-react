//  @flow
import React from 'react';
import { Card, Row, Form } from 'antd';
import { injectForm } from '@app/utils/injectForm'
import { T } from '@app/components/Generic/T';
import FormControl from '@app/components/Generic/FormControl';

type Props = {
  name: string,
  contact: string,
  email: string,
  instruction: string,
  form: Form,
  whom: string,
  action: string,
}

export function DetailsForm(props: Props) {
  const {name, contact, email, instruction, form, whom, action} = props
  return (
    <Card title={<T id={`commons.${whom}-details`}/>}>
      <Row>
        <FormControl
          fieldName={`commons.${whom}-name`}
          type='text'
          stateKey='details.name'
          span={8}
          form={form}
          options={{
            initialValue: name,
            rules: [
              {required: true, message: 'Please input the Recipient Name'},
            ],
          }}
        />
        <FormControl
          fieldName={`commons.${whom}-contact`}
          type='text'
          stateKey='details.contact'
          span={8}
          form={form}
          options={{
            rules: [
              {
                required: true,
                pattern: new RegExp('^(\\d|\\+\\d{2})[0-9\\s.\\/-]{6,20}$'),
                message:
                  'Valid Contact must be started with + or a number, and has length more than 6 digits',
              },
            ],
            initialValue: contact,
          }}
        />
        <FormControl
          fieldName={`commons.${whom}-email`}
          type='text'
          stateKey='details.email'
          span={8}
          form={form}
          options={{
            rules: [{
              type: 'email',
              message: 'The input is not valid E-mail!',
            },
              {
                required: true,
                message: 'Please input your E-mail!',
              },
            ],
            initialValue: email,
          }}
        />
      </Row>
      <Row>
        <FormControl
          fieldName={`commons.model.${action}-instructions`}
          type='text'
          stateKey='details.instruction'
          span={24}
          form={form}
          options={{initialValue: instruction}}
        />
      </Row>
    </Card>
  )
}

export default injectForm(DetailsForm)
