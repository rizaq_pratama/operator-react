//  @flow
import React, {useState} from 'react'
import { connect } from 'react-redux'
import SubmittableModal from '../../SubmittableModal'
import {PickupForm} from '../GenericEditDetails'
import selectors from '@app/containers/ParcelEdit/selectors'
import omit from 'lodash/omit'
import { notification } from 'antd'
import NC from '@app/services/api/ninjaControlApi'
import type { Job } from '@nv/react-commons/src/Migration_corev2/EntityTypes'

type Props = {
  visible: boolean,
  onClose: Function,
} & Job

export function EditPickupDetails(props: Props) {
  const [isLoading, setIsLoading] = useState(false)
  const formRef = React.createRef()

  return (
    <SubmittableModal
      visible={props.visible}
      title={'commons.model.edit-pickup-details'}
      onSubmit={onSubmit}
      onClose={props.onClose}
      disabled={isLoading}
      ref={formRef}
    >
      <PickupForm {...omit(props, ['visible', 'onClose'])} />
    </SubmittableModal>
  )

  async function onSubmit(data) {
    const { slots, address, details } = data
    const slotsData = {}
    if (slots.scheduleType === 'premium') {
      slotsData.date = slots.premiumScheduledDate?
        slots.premiumScheduledDate.format('YYYY-MM-DD'): ''
      slotsData.timeSlot = slots.scheduledTimeslotStart && slots.scheduledTimeslotEnd ?
        `${slots.scheduledTimeslotStart.format('HH:mm')} ~ ${slots.scheduledTimeslotEnd.format('HH:mm')}` : ''
      slotsData.requestedByShipper = !!slots.premiumRequestedByShipper
    } else {
      slotsData.date = slots.standardScheduledDate.format('YYYY-MM-DD')
      slotsData.timeSlot = slots.standardScheduledTimeslot
      slotsData.requestedByShipper = !!slots.standardRequestedByShipper
    }

    const updateDetails = NC.updatePickupDetails(props.id, details)
    const updateSlots = NC.reschedulePickupJob(props.id, slotsData)
    const updateAddress = NC.updatePickupAddress(props.id, address)

    setIsLoading(true)

    const res = await Promise.all([updateDetails, updateSlots, updateAddress])

    if (res.every(r => !r.ok)) {
      notification.success({
        message: 'Saved successfully',
      })
      props.onClose()
    } else {
      // TODO show errors after we finalize response error format
    }

    setIsLoading(false)
  }
}

const mapStateToProps = state => {
  const props = selectors.PickupJobSelector(state)
  return props || {}
}

export default connect(mapStateToProps)(EditPickupDetails)
