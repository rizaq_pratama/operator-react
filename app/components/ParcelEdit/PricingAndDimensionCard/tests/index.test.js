import React from 'react'
import { shallowWithIntl } from '@internals/testing/test-helpers'

import PricingAndDimensionCard from '../index'

const onEditPricingAndDimensions = jest.fn()

describe('PricingAndDimensionCard', () => {

  let wrapper, instance
  beforeEach(() => {
    wrapper = shallowWithIntl(
      <PricingAndDimensionCard
        onEditPricingAndDimensions={onEditPricingAndDimensions}
      />
    )
    instance = wrapper.instance()
  })
  it('should render', () => {
    expect(wrapper).toBeDefined()
  })
  it('should call callback on pricing and dimension edit', () => {
    instance.onEditPricingAndDimensions()
    expect(onEditPricingAndDimensions).toBeCalled()
  })
  it('should return formatted weight', () => {
    wrapper.setProps({weight: 5})
    expect(instance.printWeight()).toBe('5 kg')
  })
})
