export const ADDRESS_FILTER = {
  INBOUNDED_ONLY: 'inbounded-only',
  SAME_DAY_ORDERS: 'same-day-orders',
  PRIORITY_ORDERS: 'priority_orders',
}

export const PAGE_INDEX = {
  INIT: 0,
  AV_PROCESSS: 1,
}

export const PREFIX = 'containers/AddressVerification'
