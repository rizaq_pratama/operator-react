import React, { useState } from 'react'
import { LIABLE_DEPARTMENT, CLAIM_STATUS } from '../constants'
import FilterBoxContainer from '@app/components/Generic/FilterBoxContainer'
import _ from 'lodash'
import { SubmitButton, ClaimFilterContainer } from './styles'
const allFilters = [
  {
    type: 'single',
    label: 'Tracking ID',
    key: 'trackingId',
  },
  {
    type: 'single',
    label: 'Claim ID',
    key: 'uuid',
  },
  {
    type: 'single',
    label: 'Ticket ID',
    key: 'ticketId',
  },
  {
    type: 'time',
    label: 'Creation Time',
    key: 'creationTime',
  },
  {
    type: 'autocomplete',
    options: LIABLE_DEPARTMENT,
    selectedOptions: [],
    label: 'Liable Department',
    key: 'liableDept',
  },
  {
    type: 'autocomplete',
    options: ['PENDING', 'IN_PROGRESS', 'PAID'],
    selectedOptions: [],
    label: 'Status',
    key: 'status',
  },
  {
    type: 'single',
    label: 'Shipper ID',
    key: 'shipperId',
  },
  // {
  //   type: 'autocomplete',
  //   label: 'Shipper',
  //   key: 'shipperId',
  //   options: (text) =>
  //     _.map(CLAIM_STATUS, (c) => c.name),
  // },
]

const testPossibleFilters = []

const filterBoxRef = React.createRef()

const DATE_FORMAT = 'YYYY-MM-DD[T]hh:mm:ss[Z]'

export const Filter = ({ handleSearch, handleShowDialog }) => {
  const onSearch = () => {
    const values = filterBoxRef.current.getValues();
    //convert date
    let filterData = {};
    _.each(values, (v, k) => {
      filterData[allFilters[k].key] = v;
    })
    if (filterData.creationTime) {
      const { creationTime, ...rest } = filterData
      filterData = {
        ...rest,
        fromDate: creationTime[0].format(DATE_FORMAT),
        toDate: creationTime[1].format(DATE_FORMAT),
      }
    }
    return handleSearch(filterData)
  }

  return (

    <ClaimFilterContainer>

      <FilterBoxContainer
        ref={filterBoxRef}
        allFilters={allFilters}
        possibleFiltersKey={testPossibleFilters}
        selectedFiltersKey={Object.keys(allFilters).filter(k => testPossibleFilters.indexOf(k) === -1)}
      />
      <SubmitButton
        style={{ marginTop: '1em' }}
        type='primary'
        icon="search"
        block
        onClick={onSearch}>
        Search
        </SubmitButton>
    </ClaimFilterContainer>
  )
}
