import { NvApi } from '@nv/react-commons/src/Services'
import _ from 'lodash'
/**
 * using a bypass cors proxy
 * remove it when cors issue solved
 */
export const api = NvApi.create({
  url: 'https://api-hackday.lixionary.com',
  timeout: 20000,
}).api

const getClaims = ({ uuid, trackingId, ticketId, toDate, fromDate, shipperId, status, liableDept }) => {
  const payload = _.pickBy({
    uuid,
    tracking_id: trackingId,
    ticket_id: ticketId,
    to_date: toDate,
    from_date: fromDate,
    shipper_id: shipperId,
    status,
    liable_dept: liableDept,
  }, _.identity);
  return api.get(
    '/claimed_items',
    payload
  )
}
const updateClaim = (claimId, payload) => {
  return api.put(`/claimed_items/${claimId}`, payload)
}

const createClaim = (payload) => {
  return api.post('/claimed_items', payload)
}

const deleteClaim = (claimId) => {
  return api.delete(`/claimed_items/${claimId}`)
}

export default {
  getClaims,
  updateClaim,
  createClaim,
  deleteClaim,
}
