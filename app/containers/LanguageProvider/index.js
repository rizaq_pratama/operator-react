/*
 *
 * LanguageProvider
 *
 * this component connects the redux state language locale to the
 * IntlProvider component and i18n messages (loaded from `app/translations`)
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { createSelector } from 'reselect'
import { IntlProvider } from 'react-intl'
import { LocaleProvider } from 'antd'
import enUS from 'antd/lib/locale-provider/en_US'

import { makeSelectLocale } from './selectors'

export class LanguageProvider extends React.PureComponent {
  render () {
    return (
      <LocaleProvider locale={enUS}>
        <IntlProvider
          locale={this.props.locale}
          key={this.props.locale}
          messages={this.props.messages[this.props.locale]}
        >
          {React.Children.only(this.props.children)}
        </IntlProvider>
      </LocaleProvider>
    )
  }
}

LanguageProvider.propTypes = {
  locale: PropTypes.string,
  messages: PropTypes.object,
  children: PropTypes.element.isRequired,
}

const mapStateToProps = createSelector(makeSelectLocale(), locale => ({
  locale,
}))

export default connect(mapStateToProps)(LanguageProvider)
