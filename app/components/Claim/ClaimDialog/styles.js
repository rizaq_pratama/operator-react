import styled from 'styled-components'
import { Modal, Button } from 'antd';

export const MyDialog = styled(Modal)`
  &&{
    height: 500px;
    width: 500px;
  }

`

export const NoticeSpan = styled.span`
  font-size: 12px;
  color: gray;
`

export const InfoP = styled.p`
  font-size: 15px;
  font-weight: 500;
  color: #555;
`

export const FooterRow = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
`
export const MyButton = styled(Button)`
  margin: 5px;
`
