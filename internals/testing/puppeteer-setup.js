const PuppeteerEnvironment = require('jest-environment-puppeteer')
module.exports = function(...args) {
  return PuppeteerEnvironment.setup(...args)
};
