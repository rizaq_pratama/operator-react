import React from 'react'
import GeneralInfo from '@app/components/ParcelEdit/GeneralInfo'
import { storiesOf } from '@storybook/react'
import { TestIntlProviderWrapper } from '@internals/testing/test-helpers'

storiesOf('ParcelEdit', module).add('GeneralInfo', () => (
  <TestIntlProviderWrapper>
    <GeneralInfo
      stampId={'XKXK89889'}
      currentJob={{jobType: 'parcel-delivery', id: 123}}
      currentHub={'GW'}
      destinationHub={'Pandan'}
      orderTrackingId={'78224548'}
      latestEvent={'Hub Inbound Scan'}
      dnrGroup={'Normal'}
      zone={'L2'}
      onCurrentJobClicked={() => alert('Handle Current Job Click')}
      onOrderTrackingIdClicked={() => alert('Handle Order Tracking ID Click')}
    />
  </TestIntlProviderWrapper>
))
