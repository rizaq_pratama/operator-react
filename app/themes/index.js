import ApplicationStyles from './ApplicationStyles'
import * as Fonts from './Fonts'
import { Images } from './Images'
import { media, sizes } from './media'
import Metrics from './Metrics'
import { getLessVarsFromText } from './helper'
import varsText from './vars.less'

const Colors = getLessVarsFromText(varsText)

export { ApplicationStyles, Colors, Fonts, Images, Metrics, sizes, media }
