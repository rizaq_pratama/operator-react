import { createActions, createReducer } from 'reduxsauce'
import { fromJS } from 'immutable'
import { ADDRESS_FILTER, PREFIX } from './constants'
import { generateAPIActions } from '@app/utils/helpers'
import _ from 'lodash'

/**
 * use *SUCCESS* *ERROR* *REQUEST* suffix in the action name
 * to enable api loading state recorded in redux 'loading' key
 */
export const { Types, Creators } = createActions(_.assign({},
  generateAPIActions('get', 'Addresses', 'filter'),
  generateAPIActions('get', 'Zones'),
  generateAPIActions('get', 'RouteGroups'),
  generateAPIActions('get', 'AvStats'),
  generateAPIActions('get', 'AvInit', 'filter'),
  generateAPIActions('patch', 'PreAvAssign', 'waypointId', 'zoneId'),
  generateAPIActions('patch', 'VerifyAddress', 'latLong', 'address', 'selectedAddress'),
  generateAPIActions('patch', 'ArchiveAddress', 'ojsId'),
  generateAPIActions('get', 'ShipperUnverifiedAddresses'),
  generateAPIActions('get', 'AddressesRouteGroup', 'routeGroupId'),
  generateAPIActions('get', 'ReverifiedAddress'),
  {
    defaultAction: [],
    setAddresses: ['addresses'],
    resetAddress: [],
    setFetching: ['isFetching'],
    clearSuggestedAddress: [],
    setPage: ['page'],
    fetchStatus: ['isFetching'],
    fetchReverifiedAddress: [],
    asShipperAddress: ['asShipperAddress'],
    fetchAddresses: ['filter'],
    setFilter: ['filter'],
    getSuggestedAddresses: ['addresses'],
    putSuggestedAddresses: ['ojsId', 'suggestedAddresses'],
    setSameday: ['isSameday'],
  }), { prefix: PREFIX })

export const INITIAL_STATE = fromJS({
  addresses: [],
  addressFilters: [ADDRESS_FILTER.INBOUNDED_ONLY],
  zones: [],
  routeGroups: [],
  avStats: [],
  page: 0,
  suggestedAddresses: {},
  isSameday: false,
  isFetching: false,
  asShipperAddress: false,
  verifyError: null,
})

export const putSuggestedAddresses = (state, action) => {
  const suggested = _.assign({}, state.get('suggestedAddresses'), { [action.ojsId]: action.suggestedAddresses })
  return state.set('suggestedAddresses', suggested)
}

export const updateFilter = (state, action) => {
  const filter = _.assign({}, state.get('filter'), action.filter)
  return state.set('filter', filter)
}

export const onWaypointAssigned = (state, action) => {
  let addresses = _.cloneDeep(state.get('addresses'))
  let asShipperAddress = state.get('asShipperAddress')
  if (asShipperAddress) {
    _.pullAllBy(addresses, [{ id: action.data }], 'id')
  } else {
    _.pullAllBy(addresses, [{ waypointId: action.data }], 'waypointId')
  }
  return state
    .set('addresses', addresses)
    .set('verifyError', null)
}

export const onAddressArchived = (state, action) => {
  let addresses = _.cloneDeep(state.get('addresses'))
  _.pullAllBy(addresses, [{ orderJaroScoreId: action.data }], 'orderJaroScoreId')
  return state.set('addresses', addresses)
}

export default createReducer(INITIAL_STATE, {
  [Types.DEFAULT_ACTION]: state => state,
  [Types.GET_ADDRESSES_SUCCESS]: (state, action) => state.set('addresses', action.data),
  [Types.GET_ADDRESSES_ROUTE_GROUP_SUCCESS]: (state, action) => state.set('addresses', action.data),
  [Types.GET_SHIPPER_UNVERIFIED_ADDRESSES_SUCCESS]: (state, action) => state.set('addresses', action.data),
  [Types.GET_REVERIFIED_ADDRESS_SUCCESS]: (state, action) => state.set('addresses', action.data),
  [Types.GET_ZONES_SUCCESS]: (state, action) => state.set('zones', action.data),
  [Types.GET_AV_STATS_SUCCESS]: (state, action) => state.set('avStats', action.data),
  [Types.GET_AV_INIT_SUCCESS]: (state) => state.set('page', 1),
  [Types.GET_ROUTE_GROUPS_SUCCESS]: (state, action) => state.set('routeGroups', action.data),
  [Types.PATCH_VERIFY_ADDRESS_SUCCESS]: onWaypointAssigned,
  [Types.PATCH_VERIFY_ADDRESS_ERROR]: (state, action) => state.set('verifyError', action.error),
  [Types.PATCH_ARCHIVE_ADDRESS_SUCCESS]: onAddressArchived,
  [Types.PATCH_PRE_AV_ASSIGN_SUCCESS]: onWaypointAssigned,
  [Types.AS_SHIPPER_ADDRESS]: (state, action) => state.set('asShipperAddress', action.asShipperAddress),
  [Types.SET_PAGE]: (state, action) => state.set('page', action.page),
  [Types.FETCH_STATUS]: (state, action) => state.set('isFetching', action.isFetching),
  [Types.PUT_SUGGESTED_ADDRESSES]: putSuggestedAddresses,
  [Types.CLEAR_SUGGESTED_ADDRESS]: (state) => state.set('suggestedAddresses', {}),
  [Types.SET_FETCHING]: (state, action) => state.set('isFetching', action.isFetching),
  [Types.RESET_ADDRESS]: (state) => state.set('addresses', []),
  [Types.SET_SAMEDAY]: (state, action) => state.set('isSameday', action.isSameday),
  [Types.SET_FILTER]: updateFilter,
})
