// @flow
import React from 'react';
import { connect } from 'react-redux';
import { notification, Modal, Table } from 'antd';
import { T } from '@app/components/Generic/T';
import selectors from '@app/containers/ParcelEdit/selectors';
import { Proof } from '@nv/react-commons/src/Migration_corev2/EntityTypes'

type Props = {
  visible: boolean,
  proofs: Proof[],
  onClose: Function,
  onViewPodClicked: Function
}

const columns = [
  { title: 'POD ID', dataIndex: 'id', key: 'podId' },
  { title: 'Type', dataIndex: 'type', key: 'type' },
  { title: 'Status', dataIndex: 'status', key: 'status' },
  {
    title: 'Distance',
    dataIndex: 'distance',
    key: 'distance',
    render: (text, row) => text + ' m',
  },
  { title: 'POD Time', dataIndex: 'commitDate', key: 'podTime' },
  { title: 'Driver', dataIndex: 'driver', key: 'driver' },
  { title: 'Recipient', dataIndex: 'receiverInfo.name', key: 'recipient' },
  { title: 'Address', dataIndex: 'address', key: 'address' },
  {
    title: 'Action',
    dataIndex: 'action',
    key: 'action',
    fixed: 'right',
    width: 100,
    // eslint-disable-next-line
    render: (text, row) => (
      <a
        href='javascript:;'
        id={row.podId}
        onClick={() => notification.warn({ message: 'Not implemented yet' })}
      >
        <T id='container.shipper-pickups.dialog.view-pod' />
      </a>
    ),
  },
];

export function ViewAllProofOfDeliveries(props: Props) {
  return (
    <Modal
      width={970}
      visible={props.visible}
      footer={null}
      hoverable={false}
      onCancel={props.onClose}
      title={<T id='container.parcel-edit.view-proof-of-delivery' />}
    >
      <Table columns={columns} dataSource={props.proofs} pagination={false} />
    </Modal>
  )
}

const mapStateToProps = state => {
  return { proofs: selectors.ViewAllProofOfDeliveriesSelector(state) };
}

export default connect(mapStateToProps)(ViewAllProofOfDeliveries);
