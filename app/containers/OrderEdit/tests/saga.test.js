import { expectSaga, testSaga } from 'redux-saga-test-plan'
import defaultSaga, {
  getOrderAndParcelInfo,
  setIsLoading,
} from '../saga'
import { Types } from '../redux'
import NC from '@app/services/api/ninjaControlApi'

jest.mock('@app/services/api/ninjaControlApi', () => ({
  getOrderDetailById: jest.fn((orderId) => {
    if (orderId === 1) {
      return {
        reference: {
          merchantOrderRef: 'test',
        },
        internalReference: {
          source: 'test',
        },
      }
    } else {
      throw new Error('err')
    }

  }),
  getParcelsByOrderId: jest.fn((orderId) => {
    if (orderId === 1) {
      return mockParcelInfo
    } else {
      throw new Error('err')
    }
  }),
}))

const mockParcelInfo = { test: 'mockParcelInfo' }

describe('orderEditSagas', () => {
  it('should change loading state on call', () => {
    return expectSaga(setIsLoading, { isLoading: true })
      .put({
        type: Types.SET_LOADING,
        isLoading: true,
      })
      .run()
  })
  it('should fetch order info from NC on request', () => {
    const saga = expectSaga(getOrderAndParcelInfo, { orderId: 1 })
      .put.like({
        action: {
          type: Types.GET_ORDER_INFO_SUCCESS,
        },
      })
      .put({
        type: Types.SET_PARCEL_INFO,
        data: mockParcelInfo,
      })
      .put({
        type: Types.SET_LOADING,
        isLoading: false,
      })
      .run()
    expect(NC.getOrderDetailById).toBeCalledWith(1)
    expect(NC.getParcelsByOrderId).toBeCalledWith(1)
    return saga
  })
  it('should set order and parcel info to null', () => {
    return expectSaga(getOrderAndParcelInfo, { orderId: 2 })
      .put({
        type: Types.GET_ORDER_INFO_SUCCESS,
        data: null,
      })
      .put({
        type: Types.SET_PARCEL_INFO,
        data: null,
      })
      .put({
        type: Types.SET_LOADING,
        isLoading: false,
      })
      .run()

  })
  it('should call appropriate functions when an action is dispatched', () => {
    testSaga(defaultSaga)
      .next()
      .takeEvery(Types.GET_ORDER_INFO_REQUEST, getOrderAndParcelInfo)
      .finish()
      .isDone()
  })
})
