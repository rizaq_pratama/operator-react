// @flow
export type EditFormTypes = {
  pickupJobId: number,
  deliveryJobId: number,
  codReqId: number,
  codReqAmount: number,
  copReqId: number,
  copReqAmount: number,
}

