import React from 'react'
import { mount, shallow } from 'enzyme'
import NC from '@app/services/api/ninjaControlApi'
import toJson from 'enzyme-to-json'
import { ResumeParcelModal } from '../index'
import { TestIntlProviderWrapper } from '@internals/testing/test-helpers'

jest.mock('@app/services/api/ninjaControlApi', () => {
  return {
    resumeParcel: jest.fn(() => ({ok: true})),
  }
})

describe('ConfirmResumeParcel', () => {
  let intl
  beforeEach(() => {
    intl = {
      formatMessage: jest.fn(),
    }
  })

  it('renders correctly', () => {
    const wrapper = shallow(
      <ResumeParcelModal parcelTrackingNumber='123456' visible intl={intl}/>
    )
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  describe('full render', () => {
    let submitButton, wrapper
    beforeEach(() => {
      wrapper = mount(
        <TestIntlProviderWrapper>
          <ResumeParcelModal parcelTrackingNumber='123456' visible intl={intl} onClose={jest.fn()}/>
        </TestIntlProviderWrapper>
      )

      submitButton = wrapper.find('[type="primary"]')
    })


    it('submit', async () => {
      submitButton.simulate('click')
      expect(NC.resumeParcel.mock.calls[0][0]).toBe('123456')
      await new Promise(setTimeout)
    })
  })
})
