export const DEFAULT_LOCALE = 'en'
export const REHYDRATE = 'persist/REHYDRATE'
export const ROUTES = {
  HOME: '/',
  ADDRESS_VERIFICATION: '/address-verification',
  RELABEL_STAMP: '/relabel-stamp',
  PARCEL_EDIT: '/parcel-edit',
  ORDER_EDIT: '/order-edit',
  CLAIM: '/claim',
}
