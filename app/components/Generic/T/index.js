// @flow
import React from 'react'
import { FormattedMessage } from 'react-intl-phraseapp'

function T (props: {id: string}) {
  const {id} = props
  if (id) {
    return <FormattedMessage id={id} />
  }
  // eslint-disable-next-line
  console.warn('Passed in invalid id', props);
  return null
}

export { T }
