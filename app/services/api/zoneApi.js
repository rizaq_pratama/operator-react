import iframeClient from '../iframeClient'

function getZones () {
  return iframeClient.request('zone_getZones')
}

export default {
  getZones,
}
