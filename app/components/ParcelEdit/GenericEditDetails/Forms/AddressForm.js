// @flow
import React, { useState } from 'react';
import { Input, Card, Button, Icon, Col, Row, Form } from 'antd';
import { injectForm } from '@app/utils/injectForm'
import FormControl from '@app/components/Generic/FormControl/index';
import { T } from '@app/components/Generic/T/index';
import AddressFinderForm from '@app/components/AddressFinderForm/index';
import DataField from '@app/components/Generic/DataField/index';
import { Address } from '@app/containers/ParcelEdit/Types'

type Props = {
  form: Form,
  title?: string,
} & Address

export function AddressForm(props: Props) {
  const form = props.form
  const [showAddressFinder, setShowAddressFinder] = useState(false)
  const [latlng, setLatlng] = useState([props.latitude, props.longitude])
  return (
    <>
      <Card title={props.title || 'Edit address'}>
        <Row>
          <FormControl
            fieldName='commons.model.country'
            type='text'
            stateKey='address.country'
            span={8}
            options={{ initialValue: props.country }}
          />
          <FormControl
            fieldName='commons.city'
            type='text'
            stateKey='address.city'
            span={8}
            options={{ initialValue: props.city }}
          />
        </Row>
        <Row>
          <FormControl
            fieldName='commons.model.address1'
            type='text'
            stateKey='address.address1'
            span={16}
            options={{ initialValue: props.address1 }}
          />
          <FormControl
            fieldName='commons.model.postalCode'
            type='text'
            stateKey='address.postalCode'
            span={8}
            options={{ initialValue: props.postalCode }}
          />
        </Row>
        <Row>
          <FormControl
            fieldName='commons.model.address2'
            type='text'
            stateKey='address.address2'
            span={16}
            options={{ initialValue: props.address2 }}
          />
        </Row>
        {props.latitude ? (
          <Row>
            <DataField
              label='commons.model.lat-lon'
              span={8}
              content={latlng.join(',')}
            />
            <Col span={8}>
              <Row>
                <Button
                  onClick={() => setShowAddressFinder(!showAddressFinder)}
                >
                  <Icon type='edit' />
                  <T id={'commons.model.edit-lat-lon'} />
                </Button>
              </Row>
            </Col>

            {form.getFieldDecorator('address.latitude', {
              initialValue: props.latitude,
            })(<Input type='hidden' />)}

            {form.getFieldDecorator('address.longitude', {
              initialValue: props.longitude,
            })(<Input type='hidden' />)}

          </Row>
        ) : null}
      </Card>
      {props.latitude ? (
        <AddressFinderForm
          visible={showAddressFinder}
          latitude={props.latitude}
          longitude={props.longitude}
          handleSaveCoordinates={handleSaveCoords.bind(null, form)}
        />
      ) : null}
    </>
  )

  function handleSaveCoords(form, lat, lng) {
    form.setFieldsValue({
      'address.latitude': lat,
      'address.longitude':lng,
    })
    setLatlng([lat, lng])
  }
}

export default injectForm(AddressForm)
