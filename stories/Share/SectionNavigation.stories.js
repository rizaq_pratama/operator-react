import React from 'react'
import { storiesOf } from '@storybook/react'

import { SectionNavigation } from '@app/components/SectionNavigation'

const navItems = [
  {
    id: 0,
    displayName: 'container.address-verification.initialize',
  },
  {
    id: 1,
    displayName: 'container.address-verification.verify-addresses',
  },
]

const navItems2 = [
  {
    id: 0,
    displayName: 'container.address-verification.initialize',
  },
  {
    id: 1,
    displayName: 'container.address-verification.verify-addresses',
  },
  {
    id: 2,
    displayName: 'Head 2',
  },
  {
    id: 3,
    displayName: 'head 3',
  },
  {
    id: 4,
    displayName: 'head 4',
  },

]

storiesOf('Share', module)
  .add('Navigation', () =>
    <SectionNavigation navItems={navItems} />)
  .add('Navigation (more items)', () =>
    <SectionNavigation navItems={navItems2} />)
