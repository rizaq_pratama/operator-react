import React from 'react'
import { EditDeliveryDetails } from '@app/components/ParcelEdit/EditDeliveryDetails'
import { storiesOf } from '@storybook/react'
import { TestIntlProviderWrapper } from '@internals/testing/test-helpers'

const testRecipientDetails = {
  name: 'Min Thu Ya Hlaing',
  contact: '+6585881948',
  email: 'minthuya@hotmail.com',
  deliveryInstructions: 'Code to enter warehouse is 999',
  internalNotes: 'Convey special request to ',
}

const testDeliverySchedule = {
  deliveryDate: new Date(),
  deliveryTimeslots: ['All Day (9 AM - 10 PM)', 'Half Day (9 AM - 4 PM)', 'Half Day (4 PM - 10 PM)'],
  selectedTimeslot: 'All Day (9 AM - 10 PM)',
  requestedByShipper: true,
}

const testDeliveryAddress = {
  country: 'Singapore',
  city: '-',
  address: 'Blk 62, #11-27 Woodlands Drive 16',
  postalCode: 737895,
  latitude: 1.3308,
  longitude: 103.908,
}

storiesOf('ParcelEdit', module).add('EditDeliveryDetailsModal', () => (
  <TestIntlProviderWrapper>
    <EditDeliveryDetails
      visible
      {...Object.assign({}, testRecipientDetails, testDeliveryAddress, testDeliverySchedule)}
    />
  </TestIntlProviderWrapper>
))
