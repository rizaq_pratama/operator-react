# Operator React Frontend

### Requirements
- `yarn` or `npm` (yarn is preferred)

### Local Setups

#### Development Setup
```bash
yarn load
yarn start
# open http://localhost:3000 in your browser
```

#### Storybook Setup
```bash
yarn storybook
# open http://localhost:6006 in your browser
```

### Builds

#### Build Assets
```bash
yarn build
```

Built assets can be found in the `build` folder

#### Build Storybook Assets
```bash
yarn build-storybook
```

Built assets can be found in the `storybook-static` folder

### Libraries
- [Ant Design](http://2x.ant.design/docs/react/introduce)
- [React Boilerplate](https://github.com/react-boilerplate/react-boilerplate")
- [Storybook for React](https://storybook.js.org/basics/guide-react/)

### Notes
- **Don't** move the babel config in the `package.json` into a `.babelrc` file, or else it would break the `build-storybook` script.

  https://storybook.js.org/configurations/custom-babel-config/
