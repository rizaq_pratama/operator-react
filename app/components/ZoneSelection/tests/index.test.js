import React from 'react'
import { shallow } from 'enzyme'
import { ZoneSelection } from '../index'

const testZones = [
  {
    name: 'ABC',
    id: 1,
  },
  {
    name: 'DEF',
    id: 2,
  },
]

describe('ZoneSelection', () => {
  let wrapper, instance
  beforeEach(() => {
    wrapper = shallow(
      <ZoneSelection
        wpId={1}
      />
    )
    instance = wrapper.instance()
  })
  it('Should render without error', () => {
    expect(wrapper).toBeDefined()
  })
  it('should return list of rendered options', () => {
    expect(instance.renderZoneOptions(testZones)).toHaveLength(testZones.length)
  })
  it('should return list of rendered options for null wpId', () => {
    wrapper.setProps({ wpId: null })
    expect(instance.renderZoneOptions(testZones)).toHaveLength(testZones.length)
  })
})
