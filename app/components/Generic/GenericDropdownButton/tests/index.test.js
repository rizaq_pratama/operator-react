import React from 'react'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'
import GenericDropdownButton from '../index'
import { TestIntlProviderWrapper } from '../../../../../internals/testing/test-helpers'

describe('GenericDropdownButton', () => {
  it('should match snapshot', () => {
    const component = renderer.create(
      <TestIntlProviderWrapper>
        <GenericDropdownButton menu={<span />} buttonName='test' />
      </TestIntlProviderWrapper>
    )
    expect(component).toMatchSnapshot()
  })

  it('should render', () => {
    const component = shallow(<GenericDropdownButton />)
    expect(component).toBeDefined()
  })
})
