// @flow
import React from 'react'
import GenericJobCard from '../GenericJobCard'
import { Row, Col } from 'antd'
import { ParcelEditCard } from '../Common/Card'
import JobStatus from '@nv/react-commons/src/Migration_corev2/Constants/JobStatus'

type RouteInformationProps = {
  routeId: number,
  routeDate: string,
  waypointId: number,
  driverId: number,
  failedAttempts: number,
  instructions: string
}

type PickupDeliveryJobProps = {
  pickupJobInfo: {
    jobStatus: $Keys<typeof JobStatus>,
    shipperName: string,
    phoneNumber: string,
    email: string,
    address: string,
    routeInformation: RouteInformationProps
  },
  deliveryJobInfo: {
    jobStatus: $Keys<typeof JobStatus>,
    customerName: string,
    phoneNumber: string,
    email: string,
    address: string,
    routeInformation: RouteInformationProps
  },
  onPickupDetailsClicked: Function,
  onDeliveryDetailsClicked: Function
}

class PickupDeliveryJob extends React.Component<PickupDeliveryJobProps> {
  static defaultProps = {
    pickupJobInfo: {
      jobStatus: JobStatus.IN_PROGRESS,
      shipperName: '',
      phoneNumber: '',
      email: '',
      address: '',
      routeInformation: {
        routeId: -1,
        routeDate: '',
        waypointId: -1,
        driverId: -1,
        failedAttempts: 0,
        instructions: '',
      },
    },
    deliveryJobInfo: {
      jobStatus: JobStatus.IN_PROGRESS,
      customerName: '',
      phoneNumber: '',
      email: '',
      address: '',
      routeInformation: {
        routeId: -1,
        routeDate: '',
        waypointId: -1,
        driverId: -1,
        failedAttempts: 0,
        instructions: '',
      },
    },
  }

  render () {
    return (
      <ParcelEditCard>
        <Row type='flex' gutter={10}>
          <Col sm={24} md={12} style={{marginBottom: '10px'}}>
            <GenericJobCard type={'pickup'} jobInfo={this.props.pickupJobInfo} onClick={this.props.onPickupDetailsClicked} />
          </Col>
          <Col sm={24} md={12}>
            <GenericJobCard type={'delivery'} jobInfo={this.props.deliveryJobInfo} onClick={this.props.onDeliveryDetailsClicked} />
          </Col>
        </Row>
      </ParcelEditCard>
    )
  }
}

export default PickupDeliveryJob
