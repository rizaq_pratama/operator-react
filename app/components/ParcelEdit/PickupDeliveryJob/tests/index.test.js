import React from 'react'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'
import PickupDeliveryJob from '../index'
import { TestIntlProviderWrapper } from '../../../../../internals/testing/test-helpers'
import 'jest-styled-components'

const testPickupJobInfo = {
  jobStatus: 'Completed',
  shipperName: 'Amazon.com Services, Inc.',
  phoneNumber: '+6566028271',
  email: 'support@ninjavan.co',
  address: '30 Jln Kilang Barat Singapore 159363 SG',
  routeInformation: {
    routeId: 567162,
    routeDate: '2018-10-09',
    waypointId: 44519426,
    driverId: 100083,
    failedAttempts: 0,
    instructions: null,
  },
}

const testDeliveryJobInfo = {
  jobStatus: 'Pending',
  customerName: 'Min Thu Ya Hlaing',
  phoneNumber: '+6585881948',
  email: 'minthuya@hotmail.com',
  address: 'Blk 62, #11-27 Woodlands Drive 16 Singapore 737895 SG',
  routeInformation: {
    routeId: null,
    routeDate: null,
    waypointId: null,
    driverId: null,
    failedAttempts: null,
    instructions: null,
  },
}

describe('PickupDeliveryJob', () => {
  it('matches snapshot', () => {
    const component = renderer.create(
      <TestIntlProviderWrapper>
        <PickupDeliveryJob
          pickupJobInfo={testPickupJobInfo}
          deliveryJobInfo={testDeliveryJobInfo} />
      </TestIntlProviderWrapper>
    )
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('renders a PickupDeliveryJob component', () => {
    const renderedComponent = shallow(
      <PickupDeliveryJob
        pickupJobInfo={testPickupJobInfo}
        deliveryJobInfo={testDeliveryJobInfo} />
    )
    expect(renderedComponent).toBeDefined()
  })
})
