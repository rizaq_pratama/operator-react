// @flow
import React, {useState} from 'react'
import { connect } from 'react-redux'
import SubmittableModal from '@app/components/SubmittableModal'
import { notification } from 'antd'
import EditForm from './Form'
import selectors from '@app/containers/ParcelEdit/selectors'
import { pick } from '@app/utils/helpers'
import './style.less'
import { EditFormTypes } from './Types'
import NC from '@app/services/api/ninjaControlApi'

type Props = {
  intl: intlShape,
  visible: boolean,
  onClose: Function,
} & EditFormTypes

export function EditCashCollectionDetails(props: Props) {
  const [isLoading, setLoading] = useState(false)

  const modalRef = React.createRef()
  return (
    <SubmittableModal
      visible={props.visible}
      title={'commons.model.edit-cash-collection-details'}
      disabled={isLoading}
      ref={modalRef}
      onSubmit={onSubmit}
      onClose={props.onClose}
    >
      <EditForm {...pick(props, ['pickupJobId', 'deliveryJobId', 'codReqId', 'codReqAmount', 'copReqId', 'copReqAmount'])}/>
    </SubmittableModal>
  )

  async function onSubmit(data: {cod: boolean, cop: boolean, codAmount: number, copAmount: number}) {
    const form = modalRef.current
    const { pickupJobId, deliveryJobId } = props
    if (!data.cod && !data.cop) {
      notification.warning({
        message: 'Nothing is updated.',
      })
      return
    }
    setLoading(true)

    const [updateCod, updateCop] = await Promise.all([
      data.cod? NC.updateCod(deliveryJobId, props.codReqId, {amount: data.codAmount, currency: 'SGD'}): {},
      data.cop? NC.updateCop(pickupJobId, props.copReqId, { amount: data.copAmount, currency: 'SGD' }) : {},
    ])

    if (updateCod.ok === false) {
      form.setFields({
        codAmount: {
          errors: [new Error(updateCod.data.error.message)],
        },
      })
    }

    if (updateCop.ok === false) {
      form.setFields({
        copAmount: {
          errors: [new Error(updateCop.data.error.message)],
        },
      })
    }

    if (![updateCop, updateCod].some(r => r.ok === false)) {
      props.onClose()
      notification.success({
        message: 'Saved successfully',
      })
    }

    setLoading(false)
  }
}

const mapStateToProps = state => {
  return selectors.EditCashCollectionModalSelector(state)
}

export default connect(mapStateToProps)(EditCashCollectionDetails)
