import { createActions } from 'reduxsauce'
import { PREFIX } from './constants'
import _ from 'lodash'

 export const { Types, Creators } = createActions(_.assign({},
   {
      getAndPrintOrderRequest: ['scan', 'callback'],
   }
 ), { prefix: PREFIX })
