import { createActions, createReducer } from 'reduxsauce'
import { PREFIX } from './constants'
import { generateAPIActions } from '@app/utils/helpers'
import _ from 'lodash'


export const { Types, Creators } = createActions(_.assign({},
  generateAPIActions('get', 'Claims', 'filter'),
  generateAPIActions('put', 'Claim', 'id', 'data', 'callback'),
  {
    defaultAction: [],
  }), { prefix: PREFIX })

export const INITIAL_STATE = {
  claims: [],
}

export const claimSuccess = (state, action) => {
  const claims = _.clone(state.claims)
  const idx = _.findIndex(claims, { id: action.data.id })
  claims[idx] = action.data
  state.claims = claims;
  return state
}

export default createReducer(INITIAL_STATE, {
  [Types.DEFAULT_ACTION]: state => state,
  [Types.GET_CLAIMS_SUCCESS]: (state, action) => ({
    ...state, claims: action.data,
  }),
  [Types.PUT_CLAIM_SUCCESS]: claimSuccess,
})
