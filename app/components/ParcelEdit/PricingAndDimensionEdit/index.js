// @flow
import React, {useState} from 'react'
import { connect } from 'react-redux'
import { notification } from 'antd'
import SubmittableModal from '@app/components/SubmittableModal'

import PricingAndDimensionForm from './form'
import { PricingAndDimensionModalSelector } from './selectors'

type Props = {
  visible: boolean,
  onClose: Function
}

export function PricingAndDimensionEdit(props: Props) {
  const [isLoading, setIsLoading] = useState(false)

  return (
    <SubmittableModal
      title={'commons.edit-pricing-and-dimensions'}
      visible={props.visible}
      isFormDisabled={isLoading}
      onClose={props.onClose}
      onSubmit={onSubmit}
    >
      <PricingAndDimensionForm {...props}/>
    </SubmittableModal>
  )

  async function onSubmit(data) {
    // TODO call the service to save pricing
    setIsLoading(true)
    notification.warn({
      message: 'API endpoint is not fixed yet',
    })
    setIsLoading(false)
    props.onClose()
  }
}

const mapStateToProps = state => {
  return PricingAndDimensionModalSelector(state)
}

export default connect(mapStateToProps)(PricingAndDimensionEdit)
