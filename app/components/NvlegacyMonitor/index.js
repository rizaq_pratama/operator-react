/**
*
* NvlegacyMonitor
*
*/

import React from 'react'
import PropTypes from 'prop-types'
import { notification } from 'antd'

class NvlegacyMonitor extends React.Component {
  componentDidUpdate (prevProps) {
    if (!this.props.successIndicator && prevProps.successIndicator) {
      notification.success({
        message: this.props.successMessage,
        description: this.props.successDescription,
      })
    }

    if (!this.props.errorIndicator && prevProps.errorIndicator) {
      notification.error({
        message: this.props.errorMessage,
      })
    }
  }
  render () {
    return null
  }
}

NvlegacyMonitor.propTypes = {
  successMessage: PropTypes.string,
  successDescription: PropTypes.string,
  errorMessage: PropTypes.string,
  successIndicator: PropTypes.bool,
  errorIndicator: PropTypes.bool,
}

NvlegacyMonitor.defaultProps = {
}

export { NvlegacyMonitor }
