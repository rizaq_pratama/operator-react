import React from 'react'
import toJson from 'enzyme-to-json'
import { shallow } from 'enzyme'
import { OrderEdit, mapDispatchToProps } from '../index'
import { Types } from '../redux'

const testOrderInfo = {
  shipperId: 14904,
  shipperName: 'Amazon',
  orderSource: 'API',
  orderCreated: '2018-10-08 23:53 HRS',
  orderBillingStatus: 'Billed',
  shipperReference: '114-0503199-7110637',
  shipperOrderData: 'SHIPPA ODDA DATAAAAAAAAAAAA',
  parcelCount: 4,
  codAmount: 50,
  codCurrency: 'SGD',
}

const testParcelInfo = []
const openModal = jest.fn()

describe('OrderEdit', () => {
  let wrapper, instance
  beforeEach(() => {
    wrapper = shallow(
      <OrderEdit
        openModal={openModal}
        setLoading={jest.fn()}
        getOrderInfo={jest.fn()}
        parcelInfo={testParcelInfo}
        orderInfo={testOrderInfo}
        match={{ params: { orderId: 1 } }}
        visible
      />
    )
    instance = wrapper.instance()
  })
  it('renders a OrderEdit', () => {
    expect(wrapper).toBeDefined()
    expect(toJson(wrapper)).toMatchSnapshot()
  })
  it('opens a modal on modal click', () => {
    instance.onModalClicked({
      key: 'abc',
    })
    expect(openModal).toBeCalledWith('abc')
  })
  it('tests various dispatch actions', () =>{
    const dispatch = jest.fn()
    mapDispatchToProps(dispatch).getOrderInfo(1)
    mapDispatchToProps(dispatch).setLoading(true)
    mapDispatchToProps(dispatch).openModal('key')
    mapDispatchToProps(dispatch).closeModal()
    expect(dispatch.mock.calls[0][0]).toEqual({ type: Types.GET_ORDER_INFO_REQUEST, orderId: 1 })
    expect(dispatch.mock.calls[1][0]).toEqual({ type: Types.SET_LOADING, isLoading: true })
    expect(dispatch.mock.calls[2][0]).toEqual({ type: 'global/OPEN_MODAL', id: 'key' })
    expect(dispatch.mock.calls[3][0]).toEqual({ type: 'global/CLOSE_MODAL' })
  })
})
