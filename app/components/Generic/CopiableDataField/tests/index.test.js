import React from 'react'
import renderer from 'react-test-renderer'
import { shallowWithIntl, TestIntlProviderWrapper } from '@internals/testing/test-helpers'
import CopiableText from '../index'

const testValue = 'test'
const testType = 'house'

describe('CopiableText', () => {

  let wrapper, instance
  beforeEach(() => {
    wrapper = shallowWithIntl(
      <CopiableText
        span={9}
        value={testValue}
        type={testType} />
    )
    instance = wrapper.instance()
  })
  it('matches snapshot', () => {
    const component = renderer.create(
      <TestIntlProviderWrapper>
        <CopiableText
          span={9}
          value={testValue}
          type={testType} />
      </TestIntlProviderWrapper>
    )
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('renders a CopiableText component', () => {
    expect(wrapper).toBeDefined()
  })
  it('changes copy state', () => {
    instance.handleCopy()
    expect(instance.state['isCopied']).toBe(true)
  })
  it('changes mouse leave state', () => {
    instance.handleMouseLeave()
    expect(instance.state['isHovered']).toBe(false)
    expect(instance.state['isCopied']).toBe(false)
  })
  it('changes mouse enter state', () => {
    instance.handleMouseEnter()
    expect(instance.state['isHovered']).toBe(true)
  })
})
