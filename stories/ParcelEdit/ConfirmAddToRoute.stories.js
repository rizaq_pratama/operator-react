import React from 'react'
import Route from '@app/services/api/routeApi'
import { ConfirmAddToRoute } from '@app/components/ParcelEdit/ConfirmAddToRoute'
import { storiesOf } from '@storybook/react'
import { TestIntlProviderWrapper } from '@internals/testing/test-helpers'

Route.getAllTags = () => Promise.resolve([{id: 123, name: 'abc'}])

storiesOf('ParcelEdit', module).add('ConfirmAddToRoute', () => (
  <TestIntlProviderWrapper className='parcel-edit-main'>
    <ConfirmAddToRoute visible jobs={[]}/>
  </TestIntlProviderWrapper>
))
