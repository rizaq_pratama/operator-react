import { transformAngularResponseToApisauce } from '../migrationHelper'

describe('migrationHelper', () => {
  describe('transformAngularResponseToApisauce', () => {
    it('should get succeeded response', () => {
      const angularResponse = {
        data: { abc: 1 },
        status: 200,
        config: {},
        statusText: '',
      }
      const apiSauceResponse = transformAngularResponseToApisauce(angularResponse)
      expect(apiSauceResponse).toEqual({
        ok: true,
        problem: null,
        data: { abc: 1 },
        headers: {},
        config: {},
        duration: null,
      })
    })
    it('should set problem when the response is unsuccessful', () => {
      const angularResponse = {
        data: {
          message: 'Server has encountered an error',
        },
        status: 500,
        config: {},
        statusText: '',
      }
      const apiSauceResponse = transformAngularResponseToApisauce(angularResponse)
      expect(apiSauceResponse).toEqual({
        ok: false,
        problem: 'SERVER_ERROR',
        data: {
          message: 'Server has encountered an error',
        },
        headers: {},
        config: {},
        duration: null,
      })
    })

  it('returns client error problem for 4** request', () => {
    const response = {
      status: 450,
    }
    const modifiedResponse = transformAngularResponseToApisauce(response)
    expect(modifiedResponse.problem).toBe('CLIENT_ERROR')
  })
  it('returns server error problem for 5** request', () => {
    const response = {
      status: 550,
    }
    const modifiedResponse = transformAngularResponseToApisauce(response)
    expect(modifiedResponse.problem).toBe('SERVER_ERROR')
  })
  it('returns network error problem for network error', () => {
    const response = {
      status: 0,
      statusText: 'Network Error',
    }
    const modifiedResponse = transformAngularResponseToApisauce(response)
    expect(modifiedResponse.problem).toBe('NETWORK_ERROR')
  })
  it('returns timeout error problem for timeout', () => {
    const response = {
      status: 0,
      statusText: 'ECONNABORTED',
    }
    const modifiedResponse = transformAngularResponseToApisauce(response)
    expect(modifiedResponse.problem).toBe('TIMEOUT_ERROR')
  })
  it('returns unknown error when reason is not known', () => {
    const response = {
      status: 0,
      statusText: 'none',
    }
    const modifiedResponse = transformAngularResponseToApisauce(response)
    expect(modifiedResponse.problem).toBe('UNKNOWN_ERROR')
})
  })
})
