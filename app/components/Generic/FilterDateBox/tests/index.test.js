import React from 'react'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'
import FilterDateBox from '../index'
import { shallowWithIntl, TestIntlProviderWrapper } from '@internals/testing/test-helpers'
import 'jest-styled-components'
import moment from 'moment'

const testFilterDateBoxParams = {
  title: 'Creation Date',
}

describe('FilterDateBox', () => {
  let wrapper, instance
  beforeEach(() => {
    wrapper = shallowWithIntl(
      <FilterDateBox
        title={testFilterDateBoxParams.title}
        onChange={jest.fn()}
      />
    )
    instance = wrapper.instance()
  })
  xit('matches snapshot', () => {
    const component = renderer.create(
      <TestIntlProviderWrapper>
        <FilterDateBox
          title={testFilterDateBoxParams.title}
          onChange={() => {}}
        />
      </TestIntlProviderWrapper>
    )
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('renders a FilterDateBox component', () => {
    const renderedComponent = shallow(
      <FilterDateBox
        title={testFilterDateBoxParams.title}
        onChange={() => {}}
      />
    )
    expect(renderedComponent).toBeDefined()
  })
  it ('should change date', () => {
    expect(instance.state['startDate']).toBeInstanceOf(moment)
    instance.dateChangeHandler([1,2])
    expect(instance.state['startDate']).toBe(1)
  })
})
