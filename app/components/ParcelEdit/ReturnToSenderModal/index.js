// @flow
import React, {useState} from 'react'
import SubmittableModal from '@app/components/SubmittableModal'
import RtsForm from './Form'
import { notification } from 'antd'
import { connect } from 'react-redux'
import selectors from '@app/containers/ParcelEdit/selectors'
import NC from '@app/services/api/ninjaControlApi'
import { Job } from '@nv/react-commons/src/Migration_corev2/EntityTypes'

type Props = {
  onClose: Function,
  visible: boolean,
  instruction: string,
  pickup: Job,
  delivery: Job,
}

export function RTSModal(props: Props) {
  const [isLoading, setIsLoading] = useState(false)

  return (
    <SubmittableModal
      onClose={props.onClose}
      visible={props.visible}
      disabled={isLoading}
      isFormDi
      onSubmit={onSubmit}
      title='commons.return-to-sender'>
      <RtsForm  { ...{...props.pickup, instruction: props.instruction} } />
    </SubmittableModal>
  )

  async function onSubmit(data) {
    const {slots, address, recipient, reason} = data
    const {delivery: deliveryJob} = props

    const slotsData = {
      date: slots.scheduledDate? slots.scheduledDate.format('YYYY-MM-DD'): '',
      timeSlot: slots.scheduledTimeslot,
      requestedByShipper: slots.requestedByShipper,
    }

    const updateDetails = NC.updateDeliveryDetails(deliveryJob.id, recipient)
    const updateSlots = NC.rescheduleDeliveryJob(deliveryJob.id, slotsData)
    const updateAddress = NC.updateDeliveryAddress(deliveryJob.id, address)
    const setRts = NC.returnToSender(deliveryJob.id, {actionMetaData: {comment: reason}})

    setIsLoading(true)

    const res = await Promise.all([updateDetails, updateSlots, updateAddress, setRts])

    if (res.every(r => !r.ok)) {
      notification.success({
        message: 'Saved successfully',
      })
      props.onClose()
    } else {
      // TODO show errors after we finalize response error format
    }

    setIsLoading(false)
  }
}

const mapStateToProps = state => {
  return selectors.EditRtsDetailsModalSelector(state)
}

export default connect(mapStateToProps)(RTSModal)
