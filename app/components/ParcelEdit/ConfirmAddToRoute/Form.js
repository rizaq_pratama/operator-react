// @flow
import React from 'react'
import { injectForm } from '@app/utils/injectForm'
import FormControl from '@app/components/Generic/FormControl';
import { message, Select, Form, Row, Col, Card, Button } from 'antd'
import { T } from '@app/components/Generic/T';
import Route from '@app/services/api/routeApi'
import type { Job, RouteTag } from '@nv/react-commons/src/Migration_corev2/EntityTypes'

type Props = {
  tags: RouteTag[],
  targetJob: Job,
  jobs: Job[],
  form: Form,
}

export function AddToRouteForm(props: Props) {
  const form = props.form
  const jobs = props.jobs || []
  const tags = props.tags || []
  return (
    <Form layout="vertical">
      <Row style={{ marginTop: 4 }}>
        <Col>
          <T id="container.parcel-edit.add-to-route-desc" />
        </Col>
      </Row>
      <Row style={{ marginTop: 16 }}>
        <FormControl
          type="text"
          fieldName="commons.model.route-id"
          stateKey={'routeId'}
          span={12}
          options={{
            rules: [
              { required: true, message: <T id='container.parcel-edit.validation.required-route-id'/> },
            ],
            initialValue: null,
          }}
        />
        <FormControl
          type="dropdown"
          fieldName="commons.model.type"
          stateKey={'type'}
          span={12}
          menu={jobs.map(job => job.jobType)}
          options={{ initialValue: props.targetJob && props.targetJob.jobType,
            rules: [{required: true, message: <T id='container.parcel-edit.validation.required-job-type'/>}] }}
        />
      </Row>
      <Row style={{ marginTop: 20 }}>
        <Col span={24}>
          <Card title={<T id="container.parcel-edit.route-finder" />}>
            <Row style={{ marginTop: 4 }} type="flex" align="bottom">
              <Col style={{ flex: '1 1' }}>
                <Form.Item label='Route Tags' className='form-control'>
                  {form.getFieldDecorator('tags')(
                    <Select
                      optionFilterProp='children'
                      mode='tags'
                    >
                      {tags.map(t=> (<Select.Option key={t.name} value={t.id+''}>{t.name}</Select.Option>))}
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col>
                <Button
                  onClick={onSuggestRouteClicked}
                  type="primary"
                  style={{marginBottom: '18px'}}>
                  <T id="container.parcel-edit.suggest-route" />
                </Button>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
    </Form>
  )
  function onSuggestRouteClicked() {
    form.validateFields(['type'], async (err, values) => {
      if (!err) {
        const type = values.type
        const tags: number[] = form.getFieldValue('tags')
        const selectedJob = props.jobs.find(j => j.jobType === type)

        if (selectedJob) {
          const routes = await Route.suggestRoute(type, selectedJob.id, tags)
          form.setFieldsValue({routeId: routes[0].route.id})
          message.success('Suggested a route')
        }
      }
    })
  }
}

export default injectForm(AddToRouteForm)
