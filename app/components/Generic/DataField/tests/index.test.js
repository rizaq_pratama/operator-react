import React from 'react'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'
import DataField from '../index'
import { TestIntlProviderWrapper } from '../../../../../internals/testing/test-helpers'
import 'jest-styled-components'

const testFieldName = 'testing.field-name'
const testFieldValue = 'testFieldValue'
const testSpan = 8

describe('DataField', () => {
  it('matches snapshot', () => {
    const component = renderer.create(
      <TestIntlProviderWrapper>
        <DataField label={testFieldName} content={testFieldValue} span={testSpan} />
      </TestIntlProviderWrapper>
    )
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('renders a DataField component', () => {
    const renderedComponent = shallow(
      <DataField label={testFieldName} content='abc' span={testSpan} />
    )
    expect(renderedComponent).toBeDefined()
  })
})
