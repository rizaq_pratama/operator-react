const fs = require('fs')
const path = require('path')
const _ = require('lodash')

module.exports = function (filename) {
  const value = fs.readFileSync(path.resolve(__dirname, '../../', filename), 'utf-8')

  return _(value)
    .split('\n')
    .compact()
    .map(line => _.split(line, '='))
    .map(([key, value]) => ({ [key]: value }))
    .reduce(_.assign, {})
}
