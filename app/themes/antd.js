// List of possibly modified vars:
// https://github.com/ant-design/ant-design/blob/master/components/style/themes/default.less
const { getLessVarsFromText } = require('./helper')

const fs = require('fs')
const path = require('path')
const varsText = fs.readFileSync(path.resolve(__dirname, 'vars.less'), {encoding: 'utf8'})

module.exports = Object.assign({}, getLessVarsFromText(varsText), {
  'primary-color': '#4c6ef5',
  'font-size-base': '14px',
  'text-color': '#212125',
  'font-family': 'Roboto Regular, Helvetica Neue, Helvetica, Arial, sans-serif',
  'card-radius': '8px',
  'form-item-margin-bottom': '10px',
})
