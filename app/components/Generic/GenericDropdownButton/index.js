// @flow
import * as React from 'react'
import { Menu, Dropdown, Button, Icon } from 'antd'
import style from 'styled-components'
import { T } from '@app/components/Generic/T'

const StyledButton = style(Button)`
  margin: 0em 0.5em;
`

type GenericDropdownButtonProps = {
  menu: React.Element<typeof Menu>,
  buttonName: string
}

class GenericDropdownButton extends React.Component<GenericDropdownButtonProps > {
  render () {
    return (
      <Dropdown overlay={this.props.menu}>
        <StyledButton>
          <T id={this.props.buttonName} /> <Icon type='down' />
        </StyledButton>
      </Dropdown>
    )
  }
}

export default GenericDropdownButton
