import React from 'react'
import { shallow } from 'enzyme'
import { ParcelEdit } from '../index'
import toJson from 'enzyme-to-json'

describe('<AddressVerification />', () => {
  it('Should render without error', () => {
    const wrapper = shallow(
      <ParcelEdit
        match={{params: {parcelId: 123}}}
        openModal={jest.fn()}
        closeModal={jest.fn()}
        loadParcelEditView={jest.fn()}/>
    )
    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
