// @flow
import React, { useContext } from 'react'
import { FormContext } from '@app/components/Generic/Contexts/Form'
import { Input, Form, Row, Col, Table } from 'antd'
import { T } from '@app/components/Generic/T'

type Props = {
  jobs: Object[],
}

function PullRouteForm(props: Props) {
  const form = useContext(FormContext)
  const { jobs } = props

  const columns = [{
    title: 'Route ID',
    dataIndex: 'routeId',
  }, {
    title: 'Job Type',
    dataIndex: 'jobType',
    width: 480,
  }];

  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      form.setFieldsValue({
        jobsToPull: selectedRows.map(r => [r.id, r.jobType]),
      })
    },
    getCheckboxProps: record => ({
      disabled: record.name === 'Disabled User', // Column configuration not to be checked
      name: record.name,
    }),
  }

  return (
    <Form>
      <Row style={{ marginTop: 4 }}>
        <Col>
          <T id='container.parcel-edit.pull-from-route-desc' />
        </Col>
      </Row>
      <Row style={{ marginTop: 16 }}>
        <Table
          rowKey='jobType'
          pagination={false}
          rowSelection={rowSelection }
          dataSource={jobs} columns={columns} />
      </Row>
      {form.getFieldDecorator('jobsToPull')(<Input type='hidden'/>)}
    </Form>
  )
}

export default PullRouteForm
