import React from 'react'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'
import FilterSingleTextBox from '../index'
import { shallowWithIntl, TestIntlProviderWrapper } from '@internals/testing/test-helpers'
import 'jest-styled-components'

const testFilterSingleTextBoxParams = {
  title: 'Aged Days',
}

describe('FilterSingleTextBox', () => {
  let wrapper, instance
  beforeEach(() => {
    wrapper = shallowWithIntl(
      <FilterSingleTextBox
        title={testFilterSingleTextBoxParams.title}
        onChange={jest.fn()}
        onDelete={jest.fn()} />
    )
    instance = wrapper.instance()
  })
  xit('matches snapshot', () => {
    const component = renderer.create(
      <TestIntlProviderWrapper>
        <FilterSingleTextBox
          title={testFilterSingleTextBoxParams.title}
          onChange={() => {}}
          onDelete={() => {}} />
      </TestIntlProviderWrapper>
    )
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('renders a FilterSingleTextBox component', () => {
    const renderedComponent = shallow(
      <TestIntlProviderWrapper>
        <FilterSingleTextBox
          title={testFilterSingleTextBoxParams.title}
          onChange={() => {}}
          onDelete={() => {}} />
      </TestIntlProviderWrapper>
    )
    expect(renderedComponent).toBeDefined()
  })
  it ('should change text', () => {
    expect(instance.state['text']).toBe('')
    instance.handleSingleTextChange({
      target: {
        value: 'test',
      },
    })
    expect(instance.state['text']).toBe('test')
  })
})
