import React from 'react'
import { shallowWithIntl } from '@internals/testing/test-helpers'
import AvCoordinateDialog from '../index'

const mockAddress = {
  lat: 1,
  lng: 1,
}
const token = 'pk.eyJ1IjoibmluamF2YW4iLCJhIjoiY2ludmFkbzNsMTQ2dHVrbTNscGMwdmRjdSJ9.cw0OWMamjLCuLB2kw6yFPg'
const testCancel = jest.fn()
const testOk = jest.fn()

describe('<AvCoordinateDialog />', () => {

  let wrapper, instance
  beforeEach(() => {
    wrapper = shallowWithIntl(
      <AvCoordinateDialog
        onOk={testOk}
        onCancel={testCancel}
        address={mockAddress}
        mapBoxToken={token} />
    )
    instance = wrapper.instance()
  })
  it('Should render without error', () => {
    expect(wrapper).toBeDefined()
  })
  it('should set new longitude', () => {
    const testLon = 110.02
    const testInvalidLong = '-'
    instance.handleLongitudeChange({
      preventDefault: jest.fn(),
      target: {
        value: testLon,
      },
    })
    expect(instance.state['lng']).toBe(testLon)
    instance.handleLongitudeChange({
      preventDefault: jest.fn(),
      target: {
        value: testInvalidLong,
      },
    })
    expect(instance.state['lng']).toBe(testInvalidLong)
  })
  it('should set new latitude', () => {
    const testLat = 80.23
    const testInvalidLat = '-'
    instance.handleLatitudeChange({
      preventDefault: jest.fn(),
      target: {
        value: testLat,
      },
    })
    expect(instance.state['lat']).toBe(testLat)
    instance.handleLatitudeChange({
      preventDefault: jest.fn(),
      target: {
        value: testInvalidLat,
      },
    })
    expect(instance.state['lat']).toBe(testInvalidLat)
  })
  it('should set a new latitude & longitude', () => {
    const testValueObject = {
      preventDefault: jest.fn(),
      target: {
        value: '1.20, 1.21',
      },
    }
    instance.handleLatLongChange(testValueObject)
    expect(instance.state['lat']).toBe(1.20)
    expect(instance.state['lng']).toBe(1.21)
    const invalidLatObject = {
      preventDefault: jest.fn(),
      target: {
        value: '190.24, 2.41',
      },
    }
    const invalidLonObject = {
      preventDefault: jest.fn(),
      target: {
        value: '10.2, 190.24',
      },
    }
    instance.handleLatLongChange(invalidLatObject)
    expect(instance.state['lat']).not.toBe(190.24)
    instance.handleLatLongChange(invalidLonObject)
    expect(instance.state['lng']).not.toBe(150.24)
  })
  it('should handle cancel', () => {
    instance.handleCancel()
    expect(testCancel).toBeCalled()
  })
  it('should handle call to ok function', () => {
    instance.handleOk()
    expect(testOk).toBeCalled()
  })
  it('should reset coordinates', () => {
    instance.handleResetCoordinate()
    expect(instance.state['lat']).toBe(mockAddress.lat)
    expect(instance.state['lng']).toBe(mockAddress.lng)
  })
})
