import React from 'react'
import { storiesOf } from '@storybook/react'
import { Select } from 'antd'

import { LoadingCard } from '@app/components/LoadingCard/index'

function onOk () {

}
storiesOf('Share', module)
  .add('LoadingCard', () => <LoadingCard title='Sample' onOk={onOk} style={{ width: '300px', height: '300px', background: 'red' }} isLoading />
  )

storiesOf('Share', module)
  .add('LoadingCard (select)', () => {
    const Option = Select.Option
    const data = [{
      name: 'Rizaq',
      value: 1,
    },
    {
      name: 'Rizaq 2',
      value: 2,
    },
    {
      name: 'Rizaq 3',
      value: 3,
    }]
    const options = data.map((d) => (<Option key={d.value} value={d.value}>{d.name}</Option>))

    return (
      <Select defaultActiveFirstOption style={{ width: '300px' }}>
        {options}
      </Select>
    )
  })
