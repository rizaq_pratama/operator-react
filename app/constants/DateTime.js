export const DATE_FORMAT = 'DD/MM/YYYY'
export const HOURS = Array(24).fill('0').map((z, n) => n < 10 ? '0' + n : n.toString())
export const MINUTES = ['00','30']

