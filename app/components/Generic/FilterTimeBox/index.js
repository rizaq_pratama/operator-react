//  @flow
import React from 'react'
import { Input, Row, Col, DatePicker, TimePicker } from 'antd'
import styled from 'styled-components'
import FilterContainer from '../FilterContainer'
import moment from 'moment'
import { FilterComponent } from '../Types'

type FilterTimeBoxProps = {
  id: string,
  label: string,
}

const InputGroup = styled(Input.Group)`
  && {
    width: 100%;
  }
`

class FilterTimeBox extends React.Component<FilterTimeBoxProps> implements FilterComponent {
  constructor (props) {
    super(props)
    this.state = {
      start: moment(),
      end: moment(),
    }
  }
  componentDidMount() {
    this.reset()
  }
  reset() {
    const start = moment()
    start.subtract(start.minute() % 30, 'minutes')
    const end = moment()
    end.add(30 - end.minute() % 30, 'minutes')
    this.setState({ start, end })
  }
  onTimeChange(key, value) {
    const m = this.state[key]
    m.hour(value.hour())
    m.minute(value.minute())
    m.second(0)
    this.setState({[key]: m})
  }
  onDateChange(key, value) {
    const m = this.state[key]
    m.year(value.year())
    m.month(value.month())
    m.date(value.date())
    this.setState({[key]: m})
  }
  serialize(): moment[] {
    return [this.state.start, this.state.end]
  }
  deserialize(datetimes: moment[]) {
    this.setState({
      start: datetimes[0],
      end: datetimes[1],
    })
  }
  clear() {
    this.reset()
  }
  render () {
    return (
      <FilterContainer label={this.props.label} id={this.props.id}>
        <Row type='flex'>
          <Col style={{flex: 'none'}}>
            <InputGroup compact>
              <DatePicker
                onChange={this.onDateChange.bind(this, 'start')}
                value={this.state.start}
                />
              <TimePicker
                onChange={this.onTimeChange.bind(this, 'start')}
                format='HH:mm'
                value={this.state.start}
                minuteStep={30}
              />
            </InputGroup>
          </Col>
          <Col span={2}>
            to
          </Col>
          <Col style={{flex: 'none'}}>
            <InputGroup compact>
              <DatePicker
                onChange={this.onDateChange.bind(this, 'end')}
                value={this.state.end}
              />
              <TimePicker
                onChange={this.onTimeChange.bind(this, 'end')}
                value={this.state.end}
                format='HH:mm'
                minuteStep={30}
              />
            </InputGroup>
          </Col>
        </Row>
      </FilterContainer>
    )
  }
}

export default FilterTimeBox
