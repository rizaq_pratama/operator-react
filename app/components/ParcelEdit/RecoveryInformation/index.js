// @flow
import React from 'react'
import type { Ticket } from '@app/containers/ParcelEdit/Types'
import { Card } from 'antd'
import { ParcelEditCard } from '../Common/Card'
import _ from 'lodash'
import { T } from '@app/components/Generic/T'

const Grid = Card.Grid

type Props = {
  daysSinceOrderCreated: number,
  daysSinceInbounded: number,
  tickets: Ticket[],
  onTicketLinkClicked: Function
}

class RecoveryInformation extends React.Component<Props> {
  static defaultProps = {
    daysSinceOrderCreated: 0,
    daysSinceInbounded: 0,
    tickets: [],
  }

  constructor (props) {
    super(props)
    this.handleTicketClick = this.handleTicketClick.bind(this)
  }

  handleTicketClick (e) {
    e.preventDefault()
    this.props.onTicketLinkClicked(e.target.id)
  }

  render () {
    const { tickets } = this.props
    const contents = _.map(tickets, (ticket) => {
      return (
        <tr key={ticket.id}>
          <td>
            <a href='#' onClick={this.handleTicketClick} id={ticket.id}>{ticket.id} {ticket.type}</a>
          </td>
          <td width='150px'>
            | {ticket.status}
          </td>
        </tr>
      )
    })

    const gridStyle = {
      textAlign: 'left',
      boxShadow: 'none',
      padding: '0.5em',
    }

    return (
      <ParcelEditCard title='Recovery Information'>
        <Grid style={{ ...gridStyle, width: '50%' }} >
          <p className='title'><T id='commons.recovery-tickets' /></p>
          <table>
            <tbody>
              {contents}
            </tbody>
          </table>
        </Grid>
        <Grid style={{ ...gridStyle, width: '25%' }}>
          <p className='title'><T id='container.order.edit.days-since-order-created' /></p>
          <div>{this.props.daysSinceOrderCreated || '-'}</div>
        </Grid>
        <Grid style={{ ...gridStyle, width: '25%' }}>
          <p className='title'><T id='container.order.edit.days-since-inbounded' /></p>
          <div>{this.props.daysSinceInbounded || '-'}</div>
        </Grid>
      </ParcelEditCard>
    )
  }
}

export default RecoveryInformation
