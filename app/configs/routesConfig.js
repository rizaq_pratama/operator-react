// @flow
import React from 'react'
import { ROUTES } from 'containers/Base/constants'
const HomeContainer = React.lazy(() =>
  import(/* webpackChunkName: "HOME" */ '@app/containers/Home/index')
)

const AddressVerificationContainer = React.lazy(() =>
  import(/* webpackChunkName: "ADRESS_VERIFICATION" */'@app/containers/AddressVerification/index')
)

const RelabelStampContainer = React.lazy(() =>
  import(/* webpackChunkName: "RELABEL_STAMP" */ '@app/containers/RelabelStamp/index')
)

const ParcelEditContainer = React.lazy(() =>
  import(/* webpackChunkName: "PARCEL_EDIT" */'@app/containers/ParcelEdit/index')
)

const OrderEditContainer = React.lazy(() =>
  import(/* webpackChunkName: "ORDER_EDIT" */'@app/containers/OrderEdit/index')
)

const ClaimController = React.lazy(() => import(/* webpackChunkName: "CLAIM" */'@app/containers/Claim/index'))

export const routes = [
  {
    path: `${ROUTES.PARCEL_EDIT}/:parcelId`,
    main: Wrapper(ParcelEditContainer),
  },
  {
    path: ROUTES.ADDRESS_VERIFICATION,
    main: Wrapper(AddressVerificationContainer),
  },
  {
    path: ROUTES.RELABEL_STAMP,
    main: Wrapper(RelabelStampContainer),
  }, {
    path: `${ROUTES.ORDER_EDIT}/:orderId`,
    main: Wrapper(OrderEditContainer),
  },
  {
    path: ROUTES.CLAIM,
    main: Wrapper(ClaimController),
  },
]

type ErrorBoundaryProps = {
  children: React.Node
}

class ErrorBoundary extends React.Component<ErrorBoundaryProps> {
  constructor(props) {
    super(props)
    this.state = { hasError: false, error: null }
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    // eslint-disable-next-line
    console.error(error);
    return { hasError: true, error }
  }

  componentDidCatch(error, info) {
    // You can also log the error to an error reporting service
  }

  render() {
    if (this.state.hasError) {
      if (__DEV__) {
        return <div>
          <h4>Something went wrong. </h4>
          {this.state.error.stack.split('\n').map((l, i) => <div key={i}>{l}</div>)}
        </div>
      } else {
        return <h4>Something went wrong.</h4>
      }
    }

    return this.props.children
  }
}

function Wrapper(Comp) {
  return function RouteComponent(props) {
    return (
      <div>
        <ErrorBoundary>
          <React.Suspense fallback={<div>Loading...</div>}>
            <Comp {...props} />
          </React.Suspense>
        </ErrorBoundary>
      </div>
    )
  }
}
