import React from 'react'
import { mount, shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { ConfirmDPDropOffSettings } from '../index'
import FormControl from '@app/components/Generic/FormControl'
import { TestIntlProviderWrapper } from '@internals/testing/test-helpers'

describe('ConfirmDPDropOffSettings', () => {
  it('renders a ConfirmDPDropOffSettings component', () => {
    const wrapper = shallow(
      <ConfirmDPDropOffSettings visible />
    )
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('calls api to get dp ids', async () => {
    const wrapper = mount(
      <TestIntlProviderWrapper>
        <ConfirmDPDropOffSettings visible />
      </TestIntlProviderWrapper>
    )
    await new Promise(setTimeout)
    // State changed, force re-render
    wrapper.update()
    expect(wrapper.find(FormControl).first().prop('menu')).toEqual(
      [{ id: 123, label: 'JKB' }, { id: 23, label: 'Greenwich' }])
    wrapper.unmount()
  })
})
