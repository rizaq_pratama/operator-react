import IFrameHelpers from '@nv/react-commons/src/Migration_corev2/IFrameHelpers'
import Config from 'configs'

let _client
let client = {}

Object.defineProperty(client, 'request', {
  get () {
    if (_client === undefined) {
      _client = new IFrameHelpers.IFrameClient(window, Config.HOST_URL)
    }
    return _client.request.bind(_client)
  },
})

export default client
