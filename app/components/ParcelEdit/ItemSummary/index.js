/**
 * Parcel Edit - Item Summary
 */

// @flow
import React from 'react'
import styled from 'styled-components'
import { Modal, Button, Row, Col } from 'antd'
import { T } from '@app/components/Generic/T'
import { ApplicationStyles } from '@app/themes'
import { ParcelEditCard } from '@app/components/ParcelEdit/Common/Card'
import DataField from '@app/components/Generic/DataField'

const StyledItemSummaryCard = styled(ParcelEditCard)`
  height: 100%;
  width: 100%;
  && .ant-card-head-title {
    ${ApplicationStyles.fontFamily};
    font-size: 16px;
    font-weight: 500;
    line-height: 1.31;
    letter-spacing: 0.3px;
    color: #212125;
  }
  
  .ant-row-flex {
  }

  && .ant-card-extra {
    ${ApplicationStyles.fontFamily};
  }

  && .ant-card-body {
    ${ApplicationStyles.fontFamily};
  }
`

type Props = {
  id: number,
  shipperId: number,
  orderSource: string,
  shipperReference: string,
  international: boolean,
  multiParcel: boolean,
  createdTimestamp: string,
  chargeTo: string,
  deliveryType: string,
  shipperOrderData: string,
  orderType: string,
  onEditButtonClicked: Function,
  onShipperOrderDataClicked: Function
}

class ItemSummary extends React.Component<Props> {
  static defaultProps = {
    id: 0,
    shipperId: 0,
    orderSource: '-',
    shipperReference: '-',
    international: false,
    multiParcel: false,
    createdTimestamp: '-',
    chargeTo: '-',
    deliveryType: '-',
    orderType: '-',
  }
  constructor (props) {
    super(props)
    this.state = {
      isShipperOrderDataOpened: false,
      formattedShipperOrderData: '',
    }
  }

  handleEditParcelDetailsClick = () =>  {
    this.props.onEditButtonClicked({ ...this.props })
  }

  handleShipperOrderDataClick = (e) => {
    e.preventDefault()
    this.setState({ isShipperOrderDataOpened: true })
    this.props.onShipperOrderDataClicked()
  }

  handleShipperOrderDataCancel = (e) => {
    this.setState({ isShipperOrderDataOpened: false })
  }

  render () {
    const {
      shipperId,
      orderSource,
      shipperOrderData,
      shipperReference,
      international,
      multiParcel,
      createdTimestamp,
      chargeTo,
      deliveryType,
      orderType,
    } = this.props

    const { isShipperOrderDataOpened, formattedShipperOrderData } = this.state

    return (
      <StyledItemSummaryCard
        title="Parcel Summary"
        extra={
          <Button onClick={this.handleEditParcelDetailsClick} icon="edit">
            <T id="container.parcel-edit.edit-parcel-details" />
          </Button>
        }
      >
        <Row type="flex" gutter={8}>
          <Col span={12}>
            <DataField label='commons.shipper-id'>
              {shipperId || '-'}
            </DataField>
          </Col>
          <Col span={12}>
            <DataField label='container.parcel-edit.shipper-reference'>
              {shipperReference || '-'}
            </DataField>
          </Col>
        </Row>
        <Row type="flex" gutter={8}>
          <Col span={12}>
            <DataField label='commons.shipper-order-data'>
              {shipperOrderData}
            </DataField>
          </Col>
          <Col span={12}>
            <DataField label='container.parcel-edit.order-created'>
              {createdTimestamp || '-'}
            </DataField>
          </Col>
        </Row>
        <Row type="flex" gutter={8}>
          <Col span={12}>
            <DataField label='container.driver-type-management.delivery-type'>
              {deliveryType || '-'}
            </DataField>
          </Col>
          <Col span={12}>
            <DataField label='container.pricing-scripts.description-order-type'>
              {orderType || '-'}
            </DataField>
          </Col>
        </Row>
        <Row type="flex" gutter={8}>
          <Col span={12}>
            <DataField label='commons.model.order-source'>
              {orderSource || '-'}
            </DataField>
          </Col>
          <Col span={12}>
            <DataField label='commons.international'>
              {international || '-'}
            </DataField>
          </Col>
        </Row>
        <Row type="flex" gutter={8}>
          <Col span={12}>
            <DataField label='commons.multi-parcel'>
              {multiParcel? 'Yes': 'No'}
            </DataField>
          </Col>
          <Col span={12}>
            <DataField label='container.parcel-edit.charge-to'>
              {chargeTo || '-'}
            </DataField>
          </Col>
        </Row>
        <Modal
          width={640}
          visible={isShipperOrderDataOpened}
          footer={null}
          hoverable={false}
          onCancel={this.handleShipperOrderDataCancel}
          title={<T id="commons.shipper-order-data" />}>
          <pre>{formattedShipperOrderData}</pre>
        </Modal>
      </StyledItemSummaryCard>
    )
  }
}

export default ItemSummary
