/**
*
* AvpoolFilter
*
*/

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import moment from 'moment'
import { Checkbox, DatePicker, Button, Row, Col, Form } from 'antd'
import { T } from '@app/components/Generic/T'
import { LoadingCard } from '@app/components/LoadingCard'
import { Colors } from '@app/themes'
import pick from 'lodash/pick'
import assign from 'lodash/assign'

const FormItem = Form.Item
const StyledAvpoolFilter = styled(LoadingCard)`
  margin: 5px;
  .ant-card-head-title{
    font-weight: bold;
  }
`

const InitButton = styled(Button)`
  && {
    width: 184px;
    height: 32px;
    background-color: ${Colors.nvPriBlue};
    color: white;
  }
  font-family: roboto;
`
const StyledRow = styled(Row)`
  margin-bottom: 10px;
`
const StyledDatePicker = styled(DatePicker)`
  && {
    width : 132px;
  }
`

class AvpoolFilter extends React.Component {
  constructor (props) {
    super(props)
    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleDateChange = this.handleDateChange.bind(this)
    this.handleInitButton = this.handleInitButton.bind(this)
    this.state = assign({
      inbounded_only: false,
      is_sameday: false,
      is_priority: false,
      date: moment(),
      isLoading: false,
    }, this.props.poolFilter)
  }
  static getDerivedStateFromProps (nextProps, prevState) {
    if (nextProps.isLoading !== prevState.isLoading) {
      return { isLoading: nextProps.isLoading }
    }
    return null
  }

  handleInputChange (event) {
    const target = event.target
    const value = target.type && target.type === 'checkbox' ? target.checked : target.value
    const name = target.name

    this.setState({
      [name]: value,
    })
    this.props.handleUpdateFilter({ [name]: value })
  }

  handleInitButton (data) {
    const { handleClick } = this.props
    this.setState({ isLoading: true })
    const _data = assign({},
      pick(data, ['inbounded_only', 'is_sameday', 'is_priority']),
      { date: data.date.format('YYYY-MM-DD') })
    handleClick(_data)
  }

  handleDateChange (date) {
    this.setState({ date: date })
    this.props.handleUpdateFilter({ date: date })
  }
  render () {
    return (
      <StyledAvpoolFilter {...pick(this.props, ['children'])} isLoading={this.state.isLoading} style={{ height: '220px', width: '896px' }} title={<T id='container.address-verification.initialize-pool' />}>
        <Form layout='inline'>
          <StyledRow>
            <Col span='4' offset='6'>
              <Checkbox key='inbounded_only' checked={this.state.inbounded_only} name='inbounded_only' onChange={this.handleInputChange}>
                <T id='container.address-verification.inbounded-only' />
              </Checkbox>
            </Col>
            <Col span='4'>
              <Checkbox key='is_sameday' checked={this.state.is_sameday} name='is_sameday' onChange={this.handleInputChange} >
                <T id='container.address-verification.same-day-orders' />
              </Checkbox>
            </Col>
            <Col span='4'>
              <Checkbox key='is_priority' checked={this.state.is_priority} name='is_priority' onChange={this.handleInputChange}>
                <T id='container.address-verification.priority-orders' />
              </Checkbox>
            </Col>
          </StyledRow>
          <StyledRow>
            <Col offset='6'>
              <FormItem label={<T id='container.address-verification.orders-created-before' />}>
                <StyledDatePicker id='av-pool-datepicker' value={this.state.date} name='date' onChange={this.handleDateChange} />
              </FormItem>
            </Col>
          </StyledRow>
          <StyledRow>
            <Col span='8' offset='9'>
              <InitButton loading={this.state.isLoading} onClick={() => this.handleInitButton(this.state)}>
                <T id='container.address-verification.initialize-address-pool' />
              </InitButton>
            </Col>
          </StyledRow>
        </Form>
      </StyledAvpoolFilter>
    )
  }
}

AvpoolFilter.propTypes = {
  onClickFilter: PropTypes.func,
  isLoading: PropTypes.bool,
  poolFilter: PropTypes.object,
  handleClick: PropTypes.func,
}

AvpoolFilter.defaultProps = {
  isLoading: false,
}
export { AvpoolFilter }
