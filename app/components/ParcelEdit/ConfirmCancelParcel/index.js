// @flow
import React, {useState} from 'react'
import { compose } from 'redux'
import { notification, Form, Modal, Input } from 'antd'
import { injectIntl, intlShape } from 'react-intl'
import NC from '@app/services/api/ninjaControlApi'
import selectors from '@app/containers/ParcelEdit/selectors'
import { connect } from 'react-redux'

const FormItem = Form.Item

type Props = {
  parcelTrackingNumber: string,
  form: Form,
  visible: boolean,
  onClose?: Function,
  intl: intlShape,
}

export function ConfirmCancelParcel(props: Props) {
  const [isLoading, setLoading] = useState(false)
  const f = props.intl.formatMessage
  const form = props.form
  return (
    <Modal
      visible={props.visible}
      title={f({id: 'container.parcel-edit.cancel-parcel'})}
      cancelText={f({id: 'commons.back'})}
      okType='danger'
      okText={f({id: 'container.parcel-edit.cancel-parcel'})}
      onOk={onSubmit}
      okButtonProps={{
        disabled: isLoading,
      }}
      cancelButtonProps={{
        disabled: isLoading,
      }}
      closable={!isLoading}
      maskClosable={!isLoading}
      onCancel={props.onClose} >

      <Form hideRequiredMark>
        <FormItem label={f({id: 'container.parcel-edit.cancel-parcel-desc'})} colon={false}>
          {form.getFieldDecorator('reason', {
            rules: [{ required: true, message: 'Please Input The Cancellation Reason' }],
          })(
            <Input
              type='text'
              id='reason'
              placeholder={f({id: 'container.parcel-edit.cancellation-input-placeholder'})}/>
          )}
        </FormItem>
      </Form>

    </Modal>
  )
  function onSubmit() {
    form.validateFields(async (err, values) => {
      if (!err) {
        setLoading(true)
        const res = await NC.cancelParcel(props.parcelTrackingNumber, values.reason)
        if (res.ok !== false) {
          props.onClose()
          notification.success({
            message: 'Saved successfully',
          })
        }
        setLoading(false)
      }
    })
  }
}

const mapStateToProps = state => {
  const parcel = selectors.ParcelSelector(state)
  return { parcelTrackingNumber: parcel? parcel.trackingNumber: null }
}

export default compose(
  injectIntl,
  Form.create(),
  connect(mapStateToProps)
)(ConfirmCancelParcel)
