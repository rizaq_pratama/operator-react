// @flow
import React from 'react'
import { Input, Select, Icon, Radio, Row, Col } from 'antd'
import { T } from '@app/components/Generic/T'
import _ from 'lodash'

type EventHistoryBarProps = {
  events: Array,
  onRadioButtonSortChange: Function,
  onSearchInputChange: Function,
  onEventFilterChange: Function
}

class EventHistoryBar extends React.Component<EventHistoryBarProps> {
  static defaultProps = {
    events: [],
  }
  constructor (props) {
    super(props)
    this.handleOnRadioButtonSortChange = this.handleOnRadioButtonSortChange.bind(this)
    this.handleOnSearchChange = this.handleOnSearchChange.bind(this)
    this.handleEventFilterChange = this.handleEventFilterChange.bind(this)
  }

  handleOnRadioButtonSortChange = (e) => {
    this.props.onRadioButtonSortChange(e.target.value)
  }

  handleOnSearchChange = (e) => {
    this.props.onSearchInputChange(e.target.value)
  }

  handleEventFilterChange = (value, events) => {
    this.props.onEventFilterChange(value)
  }

  render () {
    const Option = Select.Option

    const RadioButton = Radio.Button
    const RadioGroup = Radio.Group

    const { events } = this.props

    const optionEvents = _.map(events, (option) => {
      return (
        <Option key={option.value} value={option.value}>{option.label}</Option>
      )
    })

    return (
      <Row type='flex' justify='space-between'>
        <Col xs={24} sm={12} md={6} lg={3} xl={3} style={{ marginBottom: '8px' }}>
          <T id='container.order.edit.event-history' />
        </Col>
        <Col xs={24} sm={12} md={6} lg={4} xl={4} style={{ marginBottom: '8px' }} className='bold-light'>
          <Select defaultValue='AllEvents' style={{ width: 150 }} onSelect={this.handleEventFilterChange}>
            <Option value='AllEvents'><T id='container.order.edit.all-events' /></Option>
            {optionEvents}
          </Select>
        </Col>
        <Col xs={24} sm={12} md={12} lg={{ span: 6, offset: 1 }} xl={{ span: 6, offset: 1 }} style={{ marginBottom: '8px' }} className='bold-light'>
          <RadioGroup onChange={this.handleOnRadioButtonSortChange} style={{ 'whiteSpace': 'nowrap' }} defaultValue='ascend'>
            <RadioButton value='ascend'><T id='container.order.edit.sort-oldest-to-latest' /></RadioButton>
            <RadioButton value='descend'><T id='container.order.edit.sort-latest-to-oldest' /></RadioButton>
          </RadioGroup>
        </Col>
        <Col xs={24} sm={12} md={12} lg={{ span: 8, offset: 2 }} xl={{ span: 8, offset: 2 }} style={{ marginBottom: '8px' }} className='bold-light'>
          <Input
            prefix={<Icon type='search' style={{ color: 'rgba(0,0,0,.25)' }} />}
            placeholder='Search...'
            onChange={this.handleOnSearchChange}
          />
        </Col>
      </Row>
    )
  }
}

export default EventHistoryBar
