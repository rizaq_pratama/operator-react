import { createSelector } from 'reselect'

export const JobDetailModalSelector = createSelector([], () => {
  return {
    jobInformation: {},
    routeInformation: {},
  }
})
