import api from '@nv/react-commons/src/Migration_corev2/Services'
import { makeAPIProxy } from '../apiProxy'
import Config from 'configs'

const Events = api.Events

Events.BASE_URL = Config.BASE_URL + '/events'

export default makeAPIProxy(Events)


