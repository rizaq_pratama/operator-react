//  @flow
import React from 'react'
import { Card, Select, Row, Col, Button } from 'antd'
import FilterAutocompleteBox from '../FilterAutocompleteBox'
import FilterBooleanBox from '../FilterBooleanBox'
import FilterDateBox from '../FilterDateBox'
import FilterMultipleTextBox from '../FilterMultipleTextBox'
import FilterRangeBox from '../FilterRangeBox'
import FilterSingleTextBox from '../FilterSingleTextBox'
import FilterTimeBox from '../FilterTimeBox'
import { FilterBoxContainerContext } from '../Contexts/Filters'
import _ from 'lodash'
import type { FilterComponent } from '../Types'

const FILTER_TYPES = {
  AUTOCOMPLETE: 'autocomplete',
  BOOLEAN: 'boolean',
  DATE: 'date',
  MULTIPLE_TEXT: 'multipleText',
  RANGE: 'range',
  SINGLE: 'single',
  TIME: 'time',
}

//  All filters must abide by some of these properties
type FilterProps = {
  type: $Values<filterTypes>,
  label: string,
  key: string,
  property: string,
  options: string[],
  selectedOptions: string[],
  searchFunction: Function,
  searchKey: string,
  startValue: number,
  endValue: number,
  onDelete: Function,
}

type FilterBoxContainerProps = {
  allFilters: {string: FilterProps},
  possibleFiltersKey: string[],
  selectedFilterskey: string[],
  onChange: ({string: any}) => {}
}

function getRealComp(filterRef) {
  if (filterRef.current) {
    return typeof filterRef.current.getWrappedInstance === 'function' ?
      filterRef.current.getWrappedInstance() : filterRef.current
  }
  return null
}

class FilterBoxContainer extends React.Component<FilterBoxContainerProps> {
  constructor (props) {
    super(props)
    const filterSwitches = {}
    for (const key of this.props.possibleFiltersKey) {
      filterSwitches[key] = false
    }
    for (const key of this.props.selectedFiltersKey) {
      filterSwitches[key] = true
    }
    this.filterComponentRefs = {}
    this.state = {
      filterSwitches,
      clearFilter: id => {
        const refComp = getRealComp(this.filterComponentRefs[id])
        if (refComp && typeof refComp.clear === 'function') {
          refComp.clear()
        }
      },
      switchFilter: id => {
        const switches = this.state.filterSwitches
        switches[id] = !switches[id]
        this.setState({filterSwitches: switches})
      },
    }
  }
  filterAddHandler (key) {
    this.setState(Object.assign(this.state.filterSwitches, {[key]: true}))
  }
  filterClearHandler () {
    const clearState = {}
    for (const key in this.state.filterSwitches) {
      clearState[key] = false
    }
    this.setState(Object.assign(this.state.filterSwitches, clearState))
  }
  getFilterComponent (filterKey): FilterComponent {
    const filter = this.props.allFilters[filterKey]
    if (!filter) return null

    let component
    switch (filter.type) {
      case FILTER_TYPES.AUTOCOMPLETE:
        component = <FilterAutocompleteBox {...filter}/>
        break
      case FILTER_TYPES.BOOLEAN:
        component = <FilterBooleanBox {...filter}/>
        break
      case FILTER_TYPES.DATE:
        component = <FilterDateBox {...filter}/>
        break
      case FILTER_TYPES.MULTIPLE_TEXT:
        component = <FilterMultipleTextBox {...filter}/>
        break
      case FILTER_TYPES.RANGE:
        component = <FilterRangeBox {...filter} />
        break
      case FILTER_TYPES.SINGLE:
        component = <FilterSingleTextBox {...filter}/>
        break
      case FILTER_TYPES.TIME:
        component = <FilterTimeBox {...filter}/>
        break
      default:
        return null
    }
    const ref = React.createRef()
    component = React.cloneElement(component, {
      id: filterKey,
      ref,
    })
    this.filterComponentRefs[filterKey] = ref
    return component
  }
  getValues() {
    const ret = {}
    for (const id in this.filterComponentRefs) {
      if (this.state.filterSwitches[id] === true) {
        const refComp = getRealComp(this.filterComponentRefs[id])
        if (refComp && typeof refComp.serialize === 'function') {
          ret[id] = refComp.serialize()
        }
      }
    }
    return ret
  }
  render () {
    const selectedFilters: Col = _.map(
      this.state.filterSwitches,
      (on, key) => on?
        <Col xl={12} xs={24} key={key}>
          {this.getFilterComponent(key)}
        </Col> : null
    )

    const possibleFilters: Select.Option = _.map(this.state.filterSwitches, (on, key) => {
      if (on) return null

      const filter = this.props.allFilters[key]
      if (!filter) return null
      return <Select.Option key={key} value={key}>{filter.label}</Select.Option>
    })
    return (
      <Card style={{minWidth: '600px'}}>
        <Row type='flex'>
          <FilterBoxContainerContext.Provider value={this.state}>
            {selectedFilters}
          </FilterBoxContainerContext.Provider>
        </Row>
        <div style={{display: 'flex', alignItems: 'center', flex: 'none', margin: '10px'}}>
          Add filter
          <div style={{flex: '1 1', marginLeft: '4px'}}>
            <Select
              value={undefined}
              style={{width: '200px'}}
              placeholder='Select filters'
              optionFilterProp='children'
              autoClearSearchValue={true}
              onChange={this.filterAddHandler.bind(this)}
              filterOption={(input, option) =>
                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0} >
              {possibleFilters}
            </Select>
          </div>
          <Button
            style={{flex: 'none'}}
            type='danger'
            ghost
            icon='close'
            onClick={this.filterClearHandler.bind(this)}>
            Clear All Filters
          </Button>
        </div>
      </Card>
    )
  }
}

export default FilterBoxContainer
