//  @flow
import React from 'react'
import styled from 'styled-components'
import ReactMapboxGL, { Marker } from 'react-mapbox-gl'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMapMarkerAlt } from '@fortawesome/pro-solid-svg-icons'
import { Card, Row, Col, Button, Icon } from 'antd'
import { Colors } from '@app/themes'
import { T } from '@app/components/Generic/T'
import FormControl from '@app/components/Generic/FormControl'
import Config from 'configs'

const AddressFinderButton = styled(Button)`
  && {
    margin-top: 2em;
  }
`
const linkStyle = {
  marginBottom: '18px',
  marginRight: '20px',
  color: Colors.nvLinkBlue,
  marginTop: '2.5em',
  display: 'inline-block',
}

let Map = ReactMapboxGL(
  {
    accessToken: Config.MAPBOX_TOKEN,
    interactive: true,
  }
)

type AddressFinderFormProps = {
  latitude: number,
  longitude: number,
  visible: boolean,
  form: Class<Form>,
  handleSaveCoordinates: Function
}

class AddressFinderForm extends React.Component<AddressFinderFormProps> {
  map: Map
  constructor (props) {
    super(props)
    this.state = {
      visible: props.visible,
      lat: props.latitude,
      lon: props.longitude,
    }
    this.handleStyleLoad = this.handleStyleLoad.bind(this)
    this.mapRef = React.createRef()
  }
  handleReset () {
    const { setFields } = this.props.form
    setFields({
      latitude: {
        value: this.props.latitude,
      },
      longitude: {
        value: this.props.longitude,
      },
    })
    this.setState({
      lat: this.props.latitude,
      lon: this.props.longitude,
    })
  }
  componentDidUpdate() {
    if (this.props.visible) {
      if (this.mapRef.current.state.map) {
        this.mapRef.current.state.map.resize()
      }
    }
  }
  //  Handle incorrect loading of canvas size when map container is loaded
  handleStyleLoad (map) {
    map.resize()
  }
  //  Check value and set latitude for map depending on input
  setLat (lat) {
    if (lat < -90 || lat > 90) {
      return this.state.lat
    }
    this.setState({ lat: lat })
    return lat
  }
  //  Check value and set longitude for map depending on input
  setLon (lon) {
    if (lon < -180 || lon > 180) {
      return this.state.lon
    }
    this.setState({ lon: lon })
    return lon
  }
  // Split string and check respective values
  setLatLon (e) {
    let latlon = e.target.value.replace(' ', '').split(',')
    let lat = this.setLat(latlon[0])
    let lon = this.setLon(latlon[1])
    return lat + ', ' + lon
  }
  saveCoordinates () {
    this.props.handleSaveCoordinates(this.state.lat, this.state.lon)
  }
  render () {
    const { form } = this.props
    return (
      <Card title={<T id={'commons.address-finder'} />} style={{ display: this.props.visible ? null : 'none' }} >
        <Row type='flex' align='bottom'>
          <FormControl fieldName={'commons.model.lat-lon'}
            type={'text'}
            stateKey={'latlong'}
            span={6}
            form={form}
            options={{ initialValue: this.state.lat + ', ' + this.state.lon,
              getValueFromEvent: this.setLatLon.bind(this),
            }} />
          <FormControl fieldName={'commons.latitude'}
            type={'number'}
            stateKey={'latitude'}
            span={5}
            form={form}
            options={{ initialValue: this.state.lat,
              getValueFromEvent: this.setLat.bind(this),
            }} />
          <FormControl fieldName={'commons.longitude'}
            type={'number'}
            stateKey={'longitude'}
            span={5}
            form={form}
            options={{ initialValue: this.state.lon,
              getValueFromEvent: this.setLon.bind(this),
            }} />
          <Col span={6}>
            <AddressFinderButton type='primary'
              style={{
                marginBottom: '18px',
                backgroundColor: Colors.nvPriBlue,
                borderColor: Colors.nvPriBlue }}
              onClick={this.saveCoordinates.bind(this)}>
              <Icon type='check' />
              <T id={'commons.save-coordinates'} />
            </AddressFinderButton>
          </Col>
          <Col span={2}>
            <Button style={linkStyle} href='javascript:;' onClick={this.handleReset.bind(this)}>
              <T id={'commons.reset'} />
            </Button>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <Map
              style='mapbox://styles/mapbox/streets-v9'
              center={[this.state.lon, this.state.lat]}
              containerStyle={{
                height: '400px',
                width: '100%',
              }}
              ref={this.mapRef}
              onStyleLoad={this.handleStyleLoad}>
              <Marker
                coordinates={[this.state.lon, this.state.lat]}
                anchor='bottom'>
                <FontAwesomeIcon icon={faMapMarkerAlt} size='2x' style={{ color: 'red' }} />
              </Marker>
            </Map>
          </Col>
        </Row>
      </Card>
    )
  }
}

export default AddressFinderForm
