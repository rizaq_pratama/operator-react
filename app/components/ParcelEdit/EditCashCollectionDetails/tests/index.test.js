import React from 'react'
import { mount, shallow } from 'enzyme'
import { EditCashCollectionDetails } from '../index'
import NC from '@app/services/api/ninjaControlApi'
import toJson from 'enzyme-to-json'
import { TestIntlProviderWrapper } from '@internals/testing/test-helpers'

jest.mock('@app/services/api/ninjaControlApi', () => {
  return {
    updateCop: jest.fn(() => ({ok: true})),
    updateCod: jest.fn(() => ({ok: true})),
  }
})

describe('EditCashCollectionDetails', () => {
  it('renders a EditCashCollectionDetails component', () => {
    const wrapper = shallow(
      <EditCashCollectionDetails visible
                                 pickupJobId={123}
                                 deliveryJobId={456}
                                 codReqId={8}
                                 codReqAmount={0}
                                 copReqId={12}
                                 copReqAmount={1}
      />)
    expect(wrapper).toBeDefined()
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  describe('full render', () => {
    let wrapper, intl, submitButton
    beforeEach(() => {
      intl = {
        formatMessage: jest.fn(),
      }
      wrapper = mount(
        <TestIntlProviderWrapper>
          <EditCashCollectionDetails
            intl={intl}
            visible
            pickupJobId={123}
            deliveryJobId={456}
            codReqId={8}
            codReqAmount={0}
            copReqId={12}
            copReqAmount={1}
            onClose={jest.fn()}
          />
        </TestIntlProviderWrapper>
      )
      submitButton = wrapper.find('.ant-modal-footer [type="primary"]')
      NC.updateCop.mockClear()
      NC.updateCod.mockClear()
    })

    afterEach(() => wrapper.unmount())

    it('should not submit', () => {
      submitButton.simulate('click')
      expect(NC.updateCop).not.toHaveBeenCalled()
      expect(NC.updateCod).not.toHaveBeenCalled()
    })

    it('should submit cop', async () => {
      wrapper.find('input[id="cop"]').simulate('change', {target: {checked: true}})
      // invalid data type
      wrapper.find('input[id="copAmount"]').simulate('change', {target: {value: 'abc'}})
      submitButton.simulate('click')
      expect(NC.updateCop).not.toHaveBeenCalled()
      expect(NC.updateCod).not.toHaveBeenCalled()

      wrapper.find('input[id="copAmount"]').simulate('change', {target: {value: '123'}})
      submitButton.simulate('click')
      expect(NC.updateCop.mock.calls[0][0]).toBe(123)
      expect(NC.updateCop.mock.calls[0][1]).toBe(12)
      expect(NC.updateCop.mock.calls[0][2]).toEqual({amount: 123, currency: 'SGD'})
      expect(NC.updateCod).not.toHaveBeenCalled()
      await new Promise(setTimeout)
    })

    it('should submit cod', async () => {
      wrapper.find('input[id="cod"]').simulate('change', {target: {checked: true}})
      // invalid data type
      wrapper.find('input[id="codAmount"]').simulate('change', {target: {value: 'abc'}})
      submitButton.simulate('click')
      expect(NC.updateCop).not.toHaveBeenCalled()
      expect(NC.updateCod).not.toHaveBeenCalled()

      wrapper.find('input[id="codAmount"]').simulate('change', {target: {value: '123'}})
      submitButton.simulate('click')
      expect(NC.updateCod.mock.calls[0][0]).toBe(456)
      expect(NC.updateCod.mock.calls[0][1]).toBe(8)
      expect(NC.updateCod.mock.calls[0][2]).toEqual({amount: 123, currency: 'SGD'})
      expect(NC.updateCop).not.toHaveBeenCalled()
      await new Promise(setTimeout)
    })

  })
})
