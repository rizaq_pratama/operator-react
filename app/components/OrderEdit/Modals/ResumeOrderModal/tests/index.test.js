import React from 'react'
import toJson from 'enzyme-to-json'
import { mount, shallow } from 'enzyme'
import { TestIntlProviderWrapper } from '@internals/testing/test-helpers'
import { ResumeOrder } from '../index'
import NC from '@app/services/api/ninjaControlApi'

jest.mock('@app/services/api/ninjaControlApi', () => {
  return {
    resumeOrderById: jest.fn(() => {
      return { ok: true }
    }),
  }
})

describe('ResumeOrderModal', () => {
  it('renders a ResumeOrderModal component', () => {
    const wrapper = shallow(
      <ResumeOrder visible />
    )
    expect(wrapper).toBeDefined()
    expect(toJson(wrapper)).toMatchSnapshot()
  })
  it('should call NC api on modal submit', async () => {
    const onSubmit = jest.fn()
    const onClose = jest.fn()
    const wrapper = mount(
      <TestIntlProviderWrapper>
        <ResumeOrder
          intl={{ formatMessage: jest.fn() }}
          onSubmit={onSubmit}
          onClose={onClose}
          visible />
      </TestIntlProviderWrapper>
    )
    const submitButton = wrapper.find('.ant-modal-footer [type="primary"]')
    submitButton.simulate('click')

    expect(NC.resumeOrderById).toHaveBeenCalled()
    await new Promise(setTimeout)
    expect(onClose).toHaveBeenCalled()
  })
})
