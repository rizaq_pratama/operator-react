// @flow
import React from 'react'
import { Layout } from 'antd'

const { Content, Header } = Layout

export function StyleGuideLayout(props: {title: string, children: React.Children}) {
  return <Layout>
    <Header style={{backgroundColor: '#ad1533', display: 'flex'}}>
      <span className="logo" style={{flex: '1 1'}}>
        <img src="https://static1.squarespace.com/static/5678e71ec647ad2a4bc8f1fb/t/5a60228c53450a9fb2e303d4/1547435411568/?format=1500w" width='100px' />
      </span>
      <span style={{color: 'white'}}>
        {props.title}
      </span>
    </Header>
    <Content style={{padding: '50px', backgroundColor: 'white', border: '1px solid #aaa'}}>
      {props.children}
    </Content>
  </Layout>
}
