// @flow
import React from 'react'
import { T } from '@app/components/Generic/T/index'

type Props = {
  span?: number,
  label: string,
  content?: string | React.Node,
  children?: React.Node,
}

function DataField (props: Props) {
  return (
    <>
      <label className='data-label'>
        <T id={props.label}/>
      </label>
      {props.content || props.children || '-'}
    </>
  )
}

export default DataField
