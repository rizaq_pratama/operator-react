import React from 'react'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'
import FormControl from '../index'
import { TestIntlProviderWrapper } from '@internals/testing/test-helpers'
import { FormContext } from '@app/components/Generic/Contexts/Form'

describe('FormControl', () => {
  let form = null
  const testFieldName = 'testing.field-name'
  const testKey = 'testKey'
  const testSpan = 8
  const testItems = ['A', 'B', 'C']

  beforeEach(() => {
    form = {
      getFieldDecorator: () => jest.fn(() => null),
    }
  })

  it('matches snapshot for text type', () => {
    const component = renderer.create(
      <TestIntlProviderWrapper>
        <FormContext.Provider value={form}>
          <FormControl fieldName={testFieldName} stateKey={testKey} span={testSpan} type={'text'}/>
        </FormContext.Provider>
      </TestIntlProviderWrapper>
    )
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('renders a FormControl with text type inputs', () => {
    const renderedComponent = shallow(
      <FormControl stateKey={testKey} span={testSpan} type={'text'} />
    )
    expect(renderedComponent).toBeDefined()
  })
  it('matches snapshot for number type', () => {
    const component = renderer.create(
      <TestIntlProviderWrapper>
        <FormContext.Provider value={form}>
          <FormControl fieldName={testFieldName} stateKey={testKey} span={testSpan} type={'number'} />
        </FormContext.Provider>
      </TestIntlProviderWrapper>
    )
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('renders a FormControl with number type inputs', () => {
    const renderedComponent = shallow(
      <FormControl stateKey={testKey} span={testSpan} type={'number'} />
    )
    expect(renderedComponent).toBeDefined()
  })
  it('matches snapshot for dropdown type', () => {
    const component = renderer.create(
      <TestIntlProviderWrapper>
        <FormContext.Provider value={form}>
          <FormControl fieldName={testFieldName} stateKey={testKey} span={testSpan} type={'dropdown'} menu={testItems} />
        </FormContext.Provider>
      </TestIntlProviderWrapper>
    )
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('renders a FormControl with dropdown type inputs', () => {
    const renderedComponent = shallow(
      <FormControl stateKey={testKey} span={testSpan} type={'dropdown'} menu={testItems} />
    )
    expect(renderedComponent).toBeDefined()
  })
  it('matches snapshot for date type', () => {
    const component = renderer.create(
      <TestIntlProviderWrapper>
        <FormContext.Provider value={form}>
          <FormControl fieldName={testFieldName} stateKey={testKey} span={testSpan} type={'date'} />
        </FormContext.Provider>
      </TestIntlProviderWrapper>
    )
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('renders a FormControl with date type inputs', () => {
    const renderedComponent = shallow(
      <FormControl fieldName={testFieldName} stateKey={testKey} span={testSpan} type={'date'} />
    )
    expect(renderedComponent).toBeDefined()
  })
  it('matches snapshot for checkbox type', () => {
    const component = renderer.create(
      <TestIntlProviderWrapper>
        <FormContext.Provider value={form}>
          <FormControl fieldName={testFieldName} stateKey={testKey} span={testSpan} type={'date'} checkboxText={'testText'} />
        </FormContext.Provider>
      </TestIntlProviderWrapper>
    )
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('renders a FormControl with checkbox type inputs', () => {
    const renderedComponent = shallow(
      <FormControl fieldName={testFieldName} stateKey={testKey} span={testSpan} type={'date'} checkboxText={'testText'} />
    )
    expect(renderedComponent).toBeDefined()
  })
})
