// @flow
import React from 'react'
import styled from 'styled-components'
import { Card, Row, Modal, Col } from 'antd'
import { T } from '@app/components/Generic/T'
import { Currency } from '@nv/react-commons/src/Migration_corev2/Constants/Currency'
import DataField from '@app/components/Generic/DataField'
import ParcelTable from '../ParcelTable'
import moment from 'moment'
import _ from 'lodash'

type Props = {
  orderInfo: {
    shipperId: number,
    shipperName: string,
    createdAt: string,
    billingStatus: string,
    orderSource: string,
    shipperReference: string,
    shipperOrderData: string,
    codAmount: number,
    codCurrency: $Keys<typeof Currency>,
  },
  parcelInfo: Array<{
    parcelTrackingId: string,
    currentJob: string,
    currentJobStatus: string,
    currentGranularStatus: string,
  }>,
}

const InnerCard = styled(Card)`
  && {
    border-radius: 0.3em;
    min-width: 50em;
  }
`

class OrderInfoCard extends React.Component<Props> {
  constructor(props) {
    super(props)
    this.state = {
      orderDataVisible: false,
    }
  }
  toggleOrderDataVisibility = () => {
    this.setState({
      orderDataVisible: !this.state.orderDataVisible,
    })
  }
  render () {
    return (
      <InnerCard title={<T id='commons.model.order-info' />}>
          <Row>
            <Col span={6}>
              <DataField
              label={'commons.shipper-id'}
              content={_.get(this.props.orderInfo, 'shipperId')} />
            </Col>
            <Col span={6}>
              <DataField
              label={'commons.model.order-source'}
              content={_.get(this.props.orderInfo, 'orderSource')} />
            </Col>
            <Col span={6}>
              <DataField
              label={'commons.model.order-created'}
              content={moment(new Date(_.get(this.props.orderInfo, 'createdAt'))).format('llll')} />
            </Col>
            <Col span={6}>
              <DataField
              label={'commons.model.order-billing-status'}
              content={_.get(this.props.orderInfo, 'billingStatus')} />
            </Col>
          </Row>
          <Row>
            <Col span={6}>
              <DataField
              label={'commons.model.shipper-reference'}
              content={_.get(this.props.orderInfo, 'shipperReference')} />
            </Col>
            <Col span={6}>
              <DataField
              label={'commons.model.shipper-order-data'}
              content={<a
                href='javascript:;'
                onClick={this.toggleOrderDataVisibility.bind(this)}>
                Click to view
              </a>} />
            </Col>
            <Col span={6}>
              <DataField
              label={'commons.model.parcel-number-in-order'}
              content={_.get(this.props.parcelInfo, 'length')} />
            </Col>
            <Col span={6}>
              <DataField
              label={'commons.model.cod-amount'}
              content={`${_.get(this.props.orderInfo, 'codCurrency')}
              ${_.get(this.props.orderInfo, 'codAmount')}`} />
            </Col>
          </Row>
          <ParcelTable data={this.props.parcelInfo} />
      <Modal
        title={<T id='commons.model.shipper-order-data' />}
        visible={this.state.orderDataVisible}
        footer={null}
        onCancel={this.toggleOrderDataVisibility.bind(this)} >
        {_.get(this.props.orderInfo, 'shipperOrderData')}
      </Modal>
      </InnerCard>
    )
  }
}

export default OrderInfoCard
