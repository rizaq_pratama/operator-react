import React from 'react'
import { MyDialog, NoticeSpan, FooterRow, MyButton, InfoP } from './styles'
import PropTypes from 'prop-types'
import { Form, Row, Col, Input, Select, Radio } from 'antd';
import TextArea from 'antd/lib/input/TextArea';

const FormItem = Form.Item

const ClaimDialog = ({ visible, claim = {}, handleOk, handleCancel, form }) => {
  const { getFieldDecorator, validateFields } = form
  const { status } = claim

  const handleSubmit = (event) => {
    event.preventDefault()
    validateFields((err, values) => {
      if (!err) {
        const newData = { ...claim, ...values }
        return handleOk(newData)
      }
      alert('Error validatio')
    })
  }
  const formEnabled = () => {
    return status === 'PAID'
  }

  return (
    <MyDialog
      visible={visible}
      onOk={handleOk}
      onCancel={handleCancel}
      footer={null}
      title="Edit Claim">
      <Form onSubmit={handleSubmit} >
        <Row gutter={16}>
          <Col span={6}>
            <span>Claim ID</span>
            <InfoP>{claim.claimId}</InfoP>
          </Col>
          <Col span={6}>
            <span>Tracking ID</span>
            <InfoP>{claim.trackingId}</InfoP>
          </Col>
          <Col span={6}>
            <span>Ticket ID</span>
            <InfoP>{claim.ticketId}</InfoP>
          </Col>
          <Col span={6}>
            <span>Shipper ID</span>
            <InfoP>{claim.shipperId}</InfoP>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={12}>
            <FormItem label="Amount">
              {
                getFieldDecorator('amount', { initialValue: claim.amount })(
                  <Input disabled={formEnabled()} />
                )
              }
            </FormItem></Col>
          <Col span={12}>
            <FormItem label="Liable Department">
              {
                getFieldDecorator('liableDept', { initialValue: claim.liableDept })(

                  <Select disabled={formEnabled()} >
                    <Select.Option value="FIRST_MILE">First Mile</Select.Option>
                    <Select.Option value="LAST_MILE">Last Mile</Select.Option>
                    <Select.Option value="MIDDLE_MILE">Middle Mile</Select.Option>
                    <Select.Option value="WAREHOUSE">Warehouse</Select.Option>
                    <Select.Option value="DRIVER">Driver</Select.Option>
                  </Select>
                )
              }
            </FormItem>
          </Col>
        </Row>
        <FormItem label="Status">
          {
            getFieldDecorator('status', { initialValue: claim.status })(
              <Radio.Group disabled={formEnabled()} >
                <Radio value="PENDING">Pending</Radio>
                <Radio value="IN_PROGESS">In Progress</Radio>
                <Radio value="PAID">Paid</Radio>
              </Radio.Group>
            )
          }
        </FormItem>
        <Row gutter={16}>
          <Col>
            <NoticeSpan>
              Note: You will not be able to edit this Claim item after you update it to Paid status
            </NoticeSpan>
          </Col>
        </Row>
        <FormItem label="Assignee">
          {
            getFieldDecorator('assignee', { initialValue: claim.assignee })(
              <Input placeholder="Claim assignee" disabled={formEnabled()} />
            )
          }
        </FormItem>
        <FormItem label="Comments">
          {
            getFieldDecorator('comments', { initialValue: claim.comments })(
              <TextArea disabled={formEnabled()}  >{claim.comments}</TextArea>
            )
          }
        </FormItem>

        <Row gutter={16}>
          <Col span={12}>
            <FormItem label="Account Number">
              {
                getFieldDecorator('accountNumber', { initialValue: claim.accountNumber })(
                  <Input disabled={formEnabled()} />
                )
              }
            </FormItem>

          </Col>
          <Col span={12}>
            <FormItem label="Bank Name">
              {
                getFieldDecorator('bankName', { initialValue: claim.bankName })(
                  <Input disabled={formEnabled()} />
                )
              }
            </FormItem>
          </Col>
        </Row>
        <FooterRow>
          <FormItem>
            <MyButton onClick={handleCancel}>Cancel</MyButton>
            <MyButton disabled={formEnabled()} type="primary" htmlType="submit">Submit</MyButton>
          </FormItem>
        </FooterRow>

      </Form>

    </MyDialog>)
}
ClaimDialog.propTypes = {
  visible: PropTypes.bool,
  claim: PropTypes.object,
  handleOk: PropTypes.func,
  handleCancel: PropTypes.func,
  form: PropTypes.object,
}

const ClaimDialogWithForm = Form.create({ name: 'claim' })(ClaimDialog)
export { ClaimDialogWithForm as ClaimDialog }
