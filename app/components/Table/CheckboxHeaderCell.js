// @flow
import React from 'react'
import { Dropdown, Menu, Checkbox } from 'antd'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCaretDown } from '@fortawesome/pro-solid-svg-icons'
import styled from 'styled-components'
import _ from 'lodash'

import { T } from '@app/components/Generic/T'

export const CheckboFilterOptions = {
  SELECT_ALL_SHOWN: {
    id: 'select-all-shown',
    label: <T id="directive.table.select-all-shown" />,
  },
  DESELECT_ALL_SHOWN: {
    id: 'deselect-all-shown',
    label: <T id="directive.table.deselect-all-shown" />,
  },
  CLEAR_CURRENT_SELECTION: {
    id: 'clear-current-selection',
    label: <T id="directive.table.clear-selection" />,
  },
  SHOW_ONLY_SELECTED: {
    id: 'show-only-selected',
    label: <T id="directive.table.show-only-selected" />,
  },
}

const StyledDiv = styled.div`
  cursor: pointer;
  display: inline-block;
  border: 2px solid grey;
  border-radius: 3px;
  line-height: normal;
  width: 20px;
  height: 20px;

  &.has-selected {
    background-color: lightblue;
  }
`

const StyledMenuCheckbox = styled(Checkbox)`
  margin-right: 4px !important;
`

class CheckboxHeaderCell extends React.Component<CheckboxHeaderCellProps> {
  constructor(props) {
    super(props)

    this.state = {
      isSelected: false,
    }

    this.handleClick = this.handleClick.bind(this)
  }

  handleClick({ key }) {
    const response = {
      id: key,
    }

    if(key === CheckboFilterOptions.SHOW_ONLY_SELECTED.id) {
      const isSelected = !this.state.isSelected;
      this.setState({
        isSelected: isSelected,
      })

      response.value = isSelected
    }

    this.props.onOptionClicked(response)
  }

  menu() {
    return (
      <Menu onClick={this.handleClick}>
        {_.values(CheckboFilterOptions).map((CheckboFilterOption) => {
          return (
            <Menu.Item key={CheckboFilterOption.id}>
              <a href="javascript:;">
                {this.showCheckbox(CheckboFilterOption.id)}{CheckboFilterOption.label}
              </a>
            </Menu.Item>
          )
        })}
      </Menu>
    )
  }

  showCheckbox(id) {
    if(id === CheckboFilterOptions.SHOW_ONLY_SELECTED.id) {
      return <StyledMenuCheckbox
        checked={this.state.isSelected}
      />
    }
  }

  render() {
    const { dataKey, hasSelectedIds } = this.props

    return (
      <div className={`cell ${dataKey}`}>
        <Dropdown overlay={this.menu()} trigger={['click']}>
          <StyledDiv className={hasSelectedIds ? 'has-selected' : ''}>
            <FontAwesomeIcon icon={faCaretDown} />
          </StyledDiv>
        </Dropdown>
      </div>
    )
  }
}

type CheckboxHeaderCellProps = {
  dataKey: string,
  hasSelectedIds: boolean,
  onOptionClicked: func,
}

export default CheckboxHeaderCell
