import React, { useState } from 'react';
import { createStructuredSelector } from 'reselect'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { SelectorUtils } from '@nv/react-commons/src/Utils'
import { Filter } from '@app/components/Claim/Filter'
import { ClaimTable } from '@app/components/Claim/ClaimTable'
import { Container } from './styles'
import reducer, { Creators } from './redux'
import saga from './saga'
import PropTypes from 'prop-types'
import injectSaga from 'utils/injectSaga'
import injectReducer from 'utils/injectReducer'
import { ClaimDialog } from '../../components/Claim/ClaimDialog';

const debug = false;
const { selector } = SelectorUtils

const Claim = ({ dispatch, getClaims, updateClaim, claims }) => {
  const handleSearch = (filter) => {
    getClaims(filter);
  }
  const [showDialog, setShowDialog] = useState(false)
  const [editingClaim, setEditingClaim] = useState({})

  const handleShowDialog = (editingClaim) => {
    if (editingClaim) {
      setEditingClaim(editingClaim)
      setShowDialog(true)
    }

  }

  const handleCancelDialog = () => {
    setShowDialog(false)
  }

  const handleOkDialog = (claim) => {
    updateClaim(claim.id, claim, () => (setShowDialog(false)))
  }

  return (
    <>
      <Container>
        {debug && <div>
          Claim: {JSON.stringify(claims)}
        </div>}
        <Filter
          handleSearch={handleSearch}
          handleShowDialog={setShowDialog} />
        <ClaimTable data={claims} handleShowDialog={handleShowDialog} />
      </Container>
      {showDialog &&
        <ClaimDialog
          visible={showDialog}
          claim={editingClaim}
          handleOk={handleOkDialog}
          handleCancel={handleCancelDialog}
        />}
    </>
  )
}

const mapStateToProps = createStructuredSelector(
  {
    claims: selector('claim', 'claims')(),
  }
)

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch,
    getClaims: (filter) => dispatch(Creators.getClaimsRequest(filter)),
    updateClaim: (id, data, callback) => dispatch(Creators.putClaimRequest(id, data, callback)),
  }
}

const withConnect = connect(mapStateToProps, mapDispatchToProps)
const withReducer = injectReducer({ key: 'claim', reducer })
const withSaga = injectSaga({ key: 'claim', saga })

Claim.propTypes = {
  dispatch: PropTypes.func,
  claims: PropTypes.array,
}
export default compose(withReducer, withSaga, withConnect)(Claim)
