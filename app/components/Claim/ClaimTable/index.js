import React from 'react'
import { TableCard, Table } from './styles'
import PropTypes from 'prop-types'
import _ from 'lodash'
import {
  Input, Button, Icon,
} from 'antd';
import Highlighter from 'react-highlight-words';

class ClaimTable extends React.Component {
  state = {
    searchText: '',
  };

  getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys, selectedKeys, confirm, clearFilters,
    }) => (
        <div style={{ padding: 8 }}>
          <Input
            ref={node => { this.searchInput = node; }}
            placeholder={`Search ${dataIndex}`}
            value={selectedKeys[0]}
            onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
            onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
            style={{ width: 188, marginBottom: 8, display: 'block' }}
          />
          <Button
            type="primary"
            onClick={() => this.handleSearch(selectedKeys, confirm)}
            icon="search"
            size="small"
            style={{ width: 90, marginRight: 8 }}
          >
            Search
        </Button>
          <Button
            onClick={() => this.handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
        </Button>
        </div>
      ),
    filterIcon: filtered => <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilter: (value, record) => record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: (text) => (
      // <Highlighter
      //   highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
      //   searchWords={[this.state.searchText]}
      //   autoEscape
      //   textToHighlight={text}
      // />
      <span>{text}</span>
    ),
  })

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  }

  handleReset = (clearFilters) => {
    clearFilters();
    this.setState({ searchText: '' });
  }



  render() {

    const COLUMNS = [
      {
        title: 'Claim ID',
        dataIndex: 'claimId',
        key: 'claimId',
        width: '150px',
        fixed: 'left',
      },
      {
        title: 'Tracking ID',
        dataIndex: 'trackingId',
        key: 'trackingId',
        width: '200px',
      },
      {
        title: 'Ticket ID',
        dataIndex: 'ticketId',
        key: 'ticketId',
        width: '200px',
      },
      {
        title: 'Shipper ID',
        dataIndex: 'shipperId',
        key: 'shipperId',
        width: '200px',
      },
      {
        title: 'Amount',
        dataIndex: 'amount',
        key: 'amount',
        width: '200px',
      },
      {
        title: 'Liable Department',
        dataIndex: 'liableDept',
        key: 'liableDept',
        width: '200px',
      },
      {
        title: 'Status',
        dataIndex: 'status',
        key: 'status',
        width: '200px',
      },
      {
        title: 'Paid Date',
        dataIndex: 'paidDate',
        key: 'paidDate',
        width: '200px',
      },
      {
        title: 'Action',
        dataIndex: 'action',
        key: 'action',
        width: '150px',
        fixed: 'right',
        // eslint-disable-next-line react/display-name
        render: (text, record) => (
          <div>
            <Button icon="edit" onClick={() => this.props.handleShowDialog(record)}>Edit</Button>
          </div>
        ),
      },
    ]

    const COLUMN_WITH_SEARCH = _.map(COLUMNS, (c) => {
      if (c.key === 'action') {
        return c
      }

      return {
        ...c,
        ...this.getColumnSearchProps(c.key),
      }
    })
    return (
      <TableCard>
        <Table scroll={{ x: 'max-content' }} columns={COLUMN_WITH_SEARCH} dataSource={this.props.data} />
      </TableCard >
    )
  }
}

ClaimTable.propTypes = {
  data: PropTypes.array,
  handleShowDialog: PropTypes.func,
}

ClaimTable.defaultProps = {
  data: [],
}

export {
  ClaimTable,
}
