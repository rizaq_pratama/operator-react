//  @flow
import { takeEvery, put, fork } from 'redux-saga/effects'
import NC from '@app/services/api/ninjaControlApi'
import { Types, Creators } from './redux'
import _ from 'lodash'

export function * getOrderInfo(orderId: number) {
  let orderInfo
  try {
    orderInfo = yield NC.getOrderDetailById(orderId)
    orderInfo.shipperReference = _.get(orderInfo, 'reference.merchantOrderRef')
    orderInfo.orderSource = _.get(orderInfo, 'internalReference.source')
  } catch (err) {
    orderInfo = null
  }
  yield put(Creators.getOrderInfoSuccess(orderInfo))
  yield put(Creators.setLoading(false))
}

export function * getParcelInfo(orderId: number) {
  let parcelInfo
  try {
    parcelInfo = yield NC.getParcelsByOrderId(orderId)
  } catch (err) {
    parcelInfo = null
  }

  yield put(Creators.setParcelInfo(parcelInfo))
}

export function * getOrderAndParcelInfo(action: { orderId: number }) {
  yield fork(getOrderInfo, action.orderId)
  yield fork(getParcelInfo, action.orderId)
}

export function * setIsLoading(action: { isLoading: boolean }) {
  yield put(Creators.setLoading(action.isLoading))
}

export default function * defaultSaga() {
  yield takeEvery(Types.GET_ORDER_INFO_REQUEST, getOrderAndParcelInfo)
}
