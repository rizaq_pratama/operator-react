// @flow
import React from 'react'
import styled from 'styled-components'
import { Row, Col, Button } from 'antd'
import { ParcelEditCard } from '../Common/Card'
import { CountryUtils } from '@nv/react-commons/src/Utils'
import { FormattedNumber } from 'react-intl'
import _ from 'lodash'

import { T } from '@app/components/Generic/T'
import DataField from '@app/components/Generic/DataField'
import { ApplicationStyles } from '@app/themes'

const StyledCard = styled(ParcelEditCard)`
  && .ant-card-head-title {
    ${ApplicationStyles.fontFamily};
    font-size: 16px;
    font-weight: 500;
    line-height: 1.31;
    letter-spacing: 0.3px;
    color: #212125;
  }

  && .ant-card-extra {
    ${ApplicationStyles.fontFamily};
  }

  && .ant-card-body {
    ${ApplicationStyles.fontFamily};
  }
`

type PricingAndDimensionCardProps = {
  deliveryFee: number,
  codFee: number,
  insuranceFee: number,
  handlingFee: number,
  gst: number,
  insuredValue: number,
  parcelSize: string,
  weight: number,
  dimensions: Object,
  country: string,
  onEditPricingAndDimensions: Function,
}

class PricingAndDimensionCard extends React.Component<PricingAndDimensionCardProps> {

  static defaultProps = {
    deliveryFee: 0,
    codFee: 0,
    insuranceFee: 0,
    handlingFee: 0,
    gst: 0,
    insuredValue: 0,
    weight: 0,
    country: 'SG',
  }

  constructor (props) {
    super(props)
    this.onEditPricingAndDimensions = this.onEditPricingAndDimensions.bind(this)
  }

  printWeight () {
    if (!this.props.weight) {
      return null
    }

    return `${this.props.weight} kg`
  }

  printTotal () {
    const total = _.sum([
      this.props.deliveryFee,
      this.props.codFee,
      this.props.insuranceFee,
      this.props.handlingFee,
      this.props.gst,
    ])
    const currency = CountryUtils.getCurrencyCode(this.props.country)

    return (
      <FormattedNumber
        value={total}
        style='currency'
        currency={currency}
      />
    )
  }

  printDimensions () {
    const dimensions = this.props.dimensions
    if (!dimensions ||
      !_.isNumber(dimensions.width) ||
      !_.isNumber(dimensions.height) ||
      !_.isNumber(dimensions.length)) {
      return null
    }

    return `${[dimensions.length, dimensions.width, dimensions.height].join(' x ')} cm`
  }

  onEditPricingAndDimensions () {
    this.props.onEditPricingAndDimensions()
  }

  render () {
    return (
      <StyledCard
        title={<T id='commons.pricing-and-dimensions' />}
        extra={
          <Button
            icon='edit'
            onClick={this.onEditPricingAndDimensions}>
            <T id='commons.edit-pricing-and-dimensions' />
          </Button>
        }>
        <Row gutter={8}>
          <Col md={12} sm={24}>
            <DataField
              label='commons.delivery-fee'
              content={this.props.deliveryFee || '-'}
            />
            <DataField
              label='commons.cod-fee'
              content={this.props.codFee || '-'}
            />
            <DataField
              label='commons.insurance'
              content={this.props.insuranceFee || '-'}
            />
            <DataField
              label='commons.handling'
              content={this.props.handlingFee || '-'}
            />
            <DataField
              label='commons.tax'
              content={this.props.gst || '-'}
            />
            <DataField
              label='commons.total'
              content={this.printTotal()}
            />
          </Col>
          <Col md={12} sm={24}>
            <DataField
              label='commons.insured-value'
              content={this.props.insuredValue || '-'}
            />
            <DataField
              label='commons.size'
              content={this.props.parcelSize}
            />
            <DataField
              label='commons.weight'
              content={this.printWeight() || '-'}
            />
            <DataField
              label='commons.dimensions-lwh'
              content={this.printDimensions() || '-'}
            />
          </Col>
        </Row>
      </StyledCard>
    )
  }
}

export default PricingAndDimensionCard
