const path = require('path')
const genBaseConfig = require('../internals/webpack/webpack.base.babel.js')

module.exports = (baseConfig, env, defaultConfig) => {
  const config = genBaseConfig(baseConfig, env)

  defaultConfig.resolve.modules.push('app')
  defaultConfig.resolve.alias = config.resolve.alias
  defaultConfig.resolve.extensions.push('.js', '.jsx', '.react.js')
  defaultConfig.plugins = config.plugins

  const rules = config.module.rules.filter(rule => {
    return !rule.test || rule.test.toString() !== /\.css$/.toString() && rule.test.toString() !== /\.(jpg|png|gif)$/.toString()
  })

  defaultConfig.module.rules.push(...rules)

  return defaultConfig
}
