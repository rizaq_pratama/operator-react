import IFrameHelpers from '@nv/react-commons/src/Migration_corev2/IFrameHelpers'
import ZoneApi from '../zoneApi'

jest.mock('@nv/react-commons/src/Migration_corev2/IFrameHelpers')

const fakeRequest = jest.fn()

IFrameHelpers.IFrameClient = function () {
  this.request = fakeRequest
}

describe('zoneApi', () => {
  it('getZones', () => {
    ZoneApi.getZones()
    expect(fakeRequest).toBeCalledWith('zone_getZones')
  })
})
