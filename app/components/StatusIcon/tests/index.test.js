import React from 'react'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'
import StatusIcon from '../index'
import { TestIntlProviderWrapper } from '../../../../internals/testing/test-helpers'
import 'jest-styled-components'

describe('StatusIcon', () => {
  it('matches snapshot', () => {
    const component = renderer.create(
      <TestIntlProviderWrapper>
        <StatusIcon type='Success' />
      </TestIntlProviderWrapper>
    )
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('renders a StatusIcon component for success status', () => {
    const wrapper = shallow(
      <StatusIcon type='Success' />
    )
    expect(wrapper).toBeDefined()
  })
  it('renders a StatusIcon component for failed/on-hold status', () => {
    const wrapper = shallow(
      <StatusIcon type='Failed' />
    )
    expect(wrapper).toBeDefined()
  })
  it('renders a StatusIcon component for cancelled status', () => {
    const wrapper = shallow(
      <StatusIcon type='Cancelled' />
    )
    expect(wrapper).toBeDefined()
  })
})
