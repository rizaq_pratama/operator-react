/**
 * i18n.js
 *
 * This will setup the i18n language files and locale data for your app.
 *
 */
import { addLocaleData } from 'react-intl'
import enLocaleData from 'react-intl/locale-data/en'
import { initializePhraseAppEditor } from 'react-intl-phraseapp'

import { DEFAULT_LOCALE } from './containers/Base/constants'

import enTranslationMessages from './translations/en.json'

addLocaleData(enLocaleData)

export const appLocales = ['en']

export const formatTranslationMessages = (locale, messages) => {
  const defaultFormattedMessages =
    locale !== DEFAULT_LOCALE
      ? formatTranslationMessages(DEFAULT_LOCALE, enTranslationMessages)
      : {}
  return Object.keys(messages).reduce((formattedMessages, key) => {
    const formattedMessage =
      !messages[key] && locale !== DEFAULT_LOCALE
        ? defaultFormattedMessages[key]
        : messages[key]
    return Object.assign(formattedMessages, { [key]: formattedMessage })
  }, {})
}

export const translationMessages = {
  en: formatTranslationMessages('en', enTranslationMessages),
}

// Configure react-intl-phraseapp

let config = {
  projectId: 'aa9d5bdd442352def9a43457107e7739',
  phraseEnabled: false, // TODO: ZF- Enable In-Context Editor dynamically for translators
  prefix: '[[__',
  suffix: '__]]',
  fullReparse: true,
}

initializePhraseAppEditor(config)
