// @flow
import React from 'react'
import { Menu, notification } from 'antd'
import { T } from '@app/components/Generic/T'
import GenericDropdownButton from '../../Generic/GenericDropdownButton'
import { injectIntl, intlShape } from 'react-intl'
import { MODAL_TYPES } from '@app/containers/ParcelEdit/constants'

type TopBarActionsProps = {
  onClick: Function,
  intl: intlShape,
}

function TopBarActions(props: TopBarActionsProps) {
  const orderInfoMenu = (
    <Menu onClick={props.onClick}>
      <Menu.Item key={MODAL_TYPES.PARCEL_EDIT}>
        <T id='container.parcel-edit.edit-parcel-details' />
      </Menu.Item>
      <Menu.Item key={MODAL_TYPES.CASH_COLLECTION_DETAILS}>
        <T id='container.parcel-edit.edit-cash-collection-details' />
      </Menu.Item>
      <Menu.Item key={MODAL_TYPES.MANUALLY_COMPLETE_PARCEL}>
        <T id='container.parcel-edit.manually-complete-parcel' />
      </Menu.Item>
      <Menu.Item key={MODAL_TYPES.CANCEL_PARCEL}>
        <T id='container.parcel-edit.cancel-parcel' />
      </Menu.Item>
      <Menu.Item key={MODAL_TYPES.RESUME_PARCEL}>
        <T id='container.parcel-edit.resume-parcel' />
      </Menu.Item>
      <Menu.Item key={ MODAL_TYPES.RESCHEDULE_JOB} disabled>
        <T id='container.parcel-edit.reschedule-job' />
      </Menu.Item>
      <Menu.Item disabled>
        <T id='container.parcel-edit.create-recovery-ticket' />
      </Menu.Item>
      <Menu.Item key={MODAL_TYPES.RECOVERY_TICKET} disabled>
        <T id='container.parcel-edit.edit-recovery-ticket' />
      </Menu.Item>
      <Menu.Divider />
    </Menu>
  )

  const pickupMenu = (
    <Menu onClick={props.onClick}>
      <Menu.Item key={MODAL_TYPES.PICKUP_DETAILS}>
        <T id='container.parcel-edit.edit-pickup-details' />
      </Menu.Item>
      <Menu.Item onClick={() => notifyReverifyPickupAddress()}>
        <T id='container.parcel-edit.reverify-delivery-address' />
      </Menu.Item>
      <Menu.Item key={MODAL_TYPES.PULL_FROM_ROUTE}>
        <T id='container.parcel-edit.pull-from-route' />
      </Menu.Item>
      <Menu.Item key={MODAL_TYPES.ADD_PICKUP_JOB_TO_ROUTE}>
        <T id='container.parcel-edit.add-to-route' />
      </Menu.Item>
    </Menu>
  )

  const deliveryMenu = (
    <Menu onClick={props.onClick}>
      <Menu.Item key={MODAL_TYPES.DELIVERY_DETAILS}>
        <T id='container.parcel-edit.edit-delivery-details' />
      </Menu.Item>
      <Menu.Item onClick={() => notifyReverifyDeliveryAddress()}>
        <T id='container.parcel-edit.reverify-delivery-address' />
      </Menu.Item>
      <Menu.Item key={MODAL_TYPES.PULL_FROM_ROUTE}>
        <T id='container.parcel-edit.pull-from-route' />
      </Menu.Item>
      <Menu.Item key={MODAL_TYPES.ADD_DELIVERY_JOB_TO_ROUTE}>
        <T id='container.parcel-edit.add-to-route' />
      </Menu.Item>
      <Menu.Item key={MODAL_TYPES.RETURN_TO_SENDER}>
        <T id='commons.return-to-sender' />
      </Menu.Item>
      <Menu.Item key={MODAL_TYPES.DP_DROP_OFF_SETTINGS}>
        <T id='container.parcel-edit.edit-dp-management' />
      </Menu.Item>
    </Menu>
  )

  const shipmentMenu = (
    <Menu onClick={props.onClick}>
      <Menu.Item disabled>
        <T id='container.parcel-edit.edit-current-shipment' />
      </Menu.Item>
    </Menu>
  )

  // TODO link print airway bill
  // report service
  // TODO link print shipping label
  // report service
  // TODO link shipper dashboard
  const viewPrintMenu = (
    <Menu onClick={props.onClick}>
      <Menu.Item key={MODAL_TYPES.ALL_PROOF_OF_DELIVERIES}>
        <T id='container.parcel-edit.view-all-pods' />
      </Menu.Item>
      <Menu.Item disabled>
        <T id='container.parcel-edit.print-airway-bill' />
      </Menu.Item>
      <Menu.Item disabled>
        <T id='container.parcel-edit.print-shipping-label' />
      </Menu.Item>
      <Menu.Item disabled>
        <T id='container.parcel-edit.login-to-shipper-dashboard' />
      </Menu.Item>
    </Menu>
  )

  return (
    <div>
      <GenericDropdownButton
        menu={orderInfoMenu}
        buttonName='container.parcel-edit.parcel-settings'
      />
      <GenericDropdownButton
        menu={pickupMenu}
        buttonName='commons.model.pickup'
      />
      <GenericDropdownButton
        menu={deliveryMenu}
        buttonName='commons.delivery'
      />
      <GenericDropdownButton
        menu={shipmentMenu}
        buttonName='commons.shipment'
      />
      <GenericDropdownButton
        menu={viewPrintMenu}
        buttonName='container.parcel-edit.view-or-print'
      />
    </div>
  )

  function notifyReverifyDeliveryAddress() {
    notification.success({
      message: props.intl.formatMessage({id: 'container.address-verification.reverification-triggered'}),
      description: props.intl.formatMessage({id: 'container.parcel-edit.delivery-address-reverification-success'}),
    })
  }
  function notifyReverifyPickupAddress() {
    notification.success({
      message: props.intl.formatMessage({id: 'container.address-verification.reverification-triggered'}),
      description: props.intl.formatMessage({id: 'container.parcel-edit.pickup-address-reverification-success'}),
    })
  }
}

export default injectIntl(TopBarActions)
