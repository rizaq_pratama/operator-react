import IFrameHelpers from '@nv/react-commons/src/Migration_corev2/IFrameHelpers'
import RouteGroupApi from '../routeGroupApi'

jest.mock('@nv/react-commons/src/Migration_corev2/IFrameHelpers')

const fakeRequest = jest.fn()

IFrameHelpers.IFrameClient = function () {
  this.request = fakeRequest
}
describe('routeGroupApi', () => {
  it('getRouteGroups', () => {
    RouteGroupApi.getRouteGroups()
    expect(fakeRequest).toBeCalledWith('routeGroup_read')
  })
})
