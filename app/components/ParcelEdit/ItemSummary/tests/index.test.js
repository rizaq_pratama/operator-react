import React from 'react'
import renderer from 'react-test-renderer'
import ItemSummary from '../index'
import { TestIntlProviderWrapper, shallowWithIntl } from '@internals/testing/test-helpers'

const onEditButtonClicked = jest.fn()
const onShipperOrderDataClicked = jest.fn()

describe('ItemSummary', () => {

  let wrapper, instance
  beforeEach(() => {
    wrapper = shallowWithIntl(
      <ItemSummary
        id={1}
        shipperId={10}
        orderSource='API'
        shipperReference='114-053199-7110637'
        createdTimestamp='2018-10-08 23:53 HRS'
        deliveryType='DELIVERY_ONE_DAY_ANYTIME'
        orderType='Normal'
        onEditButtonClicked={onEditButtonClicked}
        onShipperOrderDataClicked={onShipperOrderDataClicked}
      />
    )
    instance = wrapper.instance()
  })
  it('renders correctly', () => {
    const tree = renderer.create(<TestIntlProviderWrapper><ItemSummary /></TestIntlProviderWrapper>).toJSON()
    expect(tree).toMatchSnapshot()
  })
  it('renders a ItemSummary', () => {
    expect(
      wrapper
    ).toBeDefined()
  })
  it('should call callback function when edit parcel details is clicked', () => {
    instance.handleEditParcelDetailsClick()
    expect(onEditButtonClicked).toBeCalled()
  })
  it('should call callback function when shipper order data is clicked', () => {
    instance.handleShipperOrderDataClick({
      preventDefault: () => {},
    })
    expect(onShipperOrderDataClicked).toBeCalled()
  })
})
