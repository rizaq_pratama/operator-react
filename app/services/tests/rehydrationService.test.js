import * as rpi from 'redux-persist-immutable'
import { updateReducers } from '../rehydrationService'

jest.mock('redux-persist-immutable')

describe('rehydrationService', () => {
  it('updateReducers', () => {
    const store = {
      subscribe: jest.fn(),
    }

    rpi.persistStore = jest.fn(() => {
      return {
        purge () {},
      }
    })

    updateReducers(store)

    expect(rpi.persistStore).toBeCalled()
  })
})
