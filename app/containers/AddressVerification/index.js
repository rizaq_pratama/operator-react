// @flow
/**
 *
 * AddressVerification
 *
 */

import React from 'react'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { compose } from 'redux'
import { SelectorUtils } from '@nv/react-commons/src/Utils'
import styled from 'styled-components'
import { T } from '@app/components/Generic/T'
import { SectionNavigation } from '@app/components/SectionNavigation'
import { AvpoolFilter } from '@app/components/AddressVerification/AvpoolFilter'
import { AvstatsCard } from '@app/components/AddressVerification/AvstatsCard'
import { AvfetchAddressForm } from '@app/components/AddressVerification/AvfetchAddressForm'
import { ZoneSelection } from '@app/components/ZoneSelection'
import { NvlegacyMonitor } from '@app/components/NvlegacyMonitor'
import AvCoordinateDialog from 'components/AddressVerification/AvCoordinateDialog'
import { Card, Table, Alert, Button, Select, Dropdown, Menu, Icon, message } from 'antd'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import Config from 'configs'
import injectSaga from 'utils/injectSaga'
import injectReducer from 'utils/injectReducer'
import { injectIntl, intlShape } from 'react-intl'
import reducer, { Creators } from './redux'
import saga from './saga'
import { PAGE_INDEX, PREFIX } from './constants'
import './style.css'
import { createLoadingSelector } from '@app/utils/loadingSelectors'
import _ from 'lodash'
import { getDefaultLatLngForDomain } from '@app/utils/helpers'

const { selector } = SelectorUtils

const navItems = [
  {
    id: 0,
    displayName: 'container.address-verification.initialize',
  },
  {
    id: 1,
    displayName: 'container.address-verification.verify-addresses',
  },
]

const AssignLink = styled.a`
  margin-left: 12px;
`

const AddressVerificationContainer = styled.div`
  && {
    width: 896px;
    margin: 10px auto;
  }
`

const AvCard = styled(Card)`
  && {
    margin-top: 10px;
    width: 896px;
  }
`

const FetchAlert = styled(Alert)`
  && {
    margin-bottom: 10px;
    padding: 5px;
  }
`
const AvTable = styled(Table)`
  width: 100%;
  & table th div {
    font-family: Roboto;
    font-size: 14px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: 0.2px;
    color: #212b36;
  }
`

type AddressVerificationProps = {
  fetchZones: Function,
  dispatch: Function,
  fetchAddresses: Function,
  assignZone: Function,
  archiveAddress: Function,
  zones: Array,
  routeGroups: Array,
  page: Number,
  initAv?: Function,
  suggestedAddresses: Object,
  addresses: Array,
  isSameday: boolean,
  asShipperAddress: boolean,
  verifyAddress: Function,
  setPage: Function,
  setSameday: Function,
  setFilter: Function,
  fetchAddressesByRouteGroupId: Function,
  getSuggestedAddresses: Function,
  intl: intlShape,
  getAvStats: Function,
  getRouteGroups: Function,
  avStats: any,
  fetchShipperUnverifiedAddress: Function,
  fetchReverifiedAddress: Function,
  poolFilter: any,
  isInitAv: boolean,
  isFetching: boolean,
  preAvFlag: boolean,
  isArchiving: boolean,
  isVerifying: boolean,
  verifyError: any,
  settings: any,
}

export class AddressVerification extends React.Component<AddressVerificationProps> {
  constructor(props) {
    super(props)
    this.handleNavClick = this.handleNavClick.bind(this)
    this.handleInit = this.handleInit.bind(this)
    this.handleUpdateFilter = this.handleUpdateFilter.bind(this)
    this.handleFetchByRgId = this.handleFetchByRgId.bind(this)
    this.handleFetch = this.handleFetch.bind(this)
    this.handleTableChange = this.handleTableChange.bind(this)
    this.handleTableZoneSelect = this.handleTableZoneSelect.bind(this)
    this.handleTableEdit = this.handleTableEdit.bind(this)
    this.handleTableSave = this.handleTableSave.bind(this)
    this.handleTableEditClose = this.handleTableEditClose.bind(this)
    this.handleOnCopy = this.handleOnCopy.bind(this)
    this.handleTableAssign = this.handleTableAssign.bind(this)
    this.handleTableArchive = this.handleTableArchive.bind(this)
    this.handleSuggestedAddressSelect = this.handleSuggestedAddressSelect.bind(this)
    this.handleTableUseSuggestedAddress = this.handleTableUseSuggestedAddress.bind(this)
    this.getCountry = this.getCountry.bind(this)
    this.state = {
      editingAddress: {},
      dialogVisible: false,
      updateZoneMap: {},
      selectedAddressMap: {},
    }
  }

  handleTableZoneSelect(e) {
    const currentMap = this.state.updateZoneMap
    const wpZoneIdPair = _.split(e, ',')
    const updatedMap = _.assign({}, currentMap, { [wpZoneIdPair[0]]: wpZoneIdPair[1] })
    this.setState({ updateZoneMap: updatedMap })
  }

  handleSuggestedAddressSelect(e) {
    const currentMap = this.state.selectedAddressMap
    const wpSuggestedAddressPair = _.split(e, ',')
    const updatedMap = _.assign({}, currentMap, { [wpSuggestedAddressPair[0]]: wpSuggestedAddressPair[1] })
    this.setState({ selectedAddressMap: updatedMap })
  }

  handleTableEdit(address) {
    this.setState({ editingAddress: address })
    this.setState({ dialogVisible: true })
  }

  handleTableEditClose() {
    this.setState({ dialogVisible: false })
  }

  handleTableUseSuggestedAddress(address) {
    let id = this.props.asShipperAddress ? address.id : address.waypointId
    let selectedAddress = this.state.selectedAddressMap[id]
    if (!selectedAddress) {
      // user use the default selected address
      const suggestedAddress = this.props.suggestedAddresses[id]
      selectedAddress = _.first(suggestedAddress)
    }
    this.props.verifyAddress(null, address, selectedAddress)
  }

  handleTableSave(data, address) {
    this.setState({ dialogVisible: false })
    this.props.verifyAddress(data, address, null)
  }

  handleTableArchive(address) {
    if (this.props.asShipperAddress) {
      this.props.verifyAddress({ lat: getDefaultLatLngForDomain().lat, lng: getDefaultLatLngForDomain().lng }, address, null)
    } else {
      this.props.archiveAddress(address.orderJaroScoreId)
    }
  }

  handleTableAssign(wpId) {
    const updateZoneMap = this.state.updateZoneMap
    if (updateZoneMap && updateZoneMap[wpId]) {
      this.props.assignZone(wpId, updateZoneMap[wpId])
      let updated = _.cloneDeep(updateZoneMap)
      _.unset(updated, wpId)
      this.setState({ updateZoneMap: updated })
    } else {
      message.error('Please select a zone')
    }
  }

  handleNavClick(idx) {
    this.props.setPage(idx)
  }

  handleInit(filter) {
    this.props.setSameday(filter.is_sameday)
    this.props.initAv(filter)
  }

  handleUpdateFilter(filter) {
    this.props.setFilter(filter)
  }

  handleFetchByRgId(rgId) {
    this.props.fetchAddressesByRouteGroupId(rgId)
  }

  handleFetch(param) {
    this.props.fetchAddresses(param)
  }

  handleTableChange(pagination, filters, sorter, extra) {
    const newPageData = _.slice(extra.currentDataSource, ((pagination.current - 1) *
      pagination.pageSize), (pagination.current * pagination.pageSize))
    this.props.getSuggestedAddresses(newPageData)
  }

  handleOnCopy() {
    message.info(this.props.intl.formatMessage({ id: 'commons.copied-to-clipboard' }))
  }

  componentDidMount() {
    this.props.fetchZones()
    this.props.getAvStats()
    this.props.getRouteGroups()
  }

  prepareTableData(addresses) {
    return _.map(addresses, (a, idx) => {
      const address = _.assign(a, {
        key: idx,
        address: `${a.address1} ${a.address2 || ''} ${a.city || ''} ${a.country || ''} ${a.postcode}`,
      })
      return address
    })
  }
  getSelectionValue(waypointId) {
    if (this.state.updateZoneMap[waypointId]) {
      return `${waypointId},${this.state.updateZoneMap[waypointId]}`
    }
    return null
  }

  renderSuggestedAddressOption(wpId, suggestedAddresses) {
    function generateAddressString(address) {
      let addressString
      if (address.street) {
        addressString = `${address.building || ''} ${address.building_no} ${address.street} ${address.city} ${address.country} ${address.postcode}`;
      } else {
        addressString = address.raw_address;
      }
      return `${_.round(address.jaro_winkler_score, 1)} - ${addressString}`
    }
    const firstValue = _.first(suggestedAddresses)
    let defaultValue = ''
    if (firstValue) {
      defaultValue = `${wpId},${firstValue.id}`
    }
    return (
      <Select value={defaultValue} dropdownClassName='suggestedAddressDropDown' style={{ width: '188px', fontSize: '12px' }} onChange={this.handleSuggestedAddressSelect} >
        {
          _.map(suggestedAddresses, (address) => {
            return (
              <Select.Option className='suggestedAddress' key={`${wpId},${address.id}`}>
                {generateAddressString(address)}
              </Select.Option>
            )
          })
        }
      </Select>
    )
  }

  getCountry(settings) {
    return settings && settings.domain && settings.domain.current ? settings.domain.current : null
  }

  render() {
    const { avStats, page, addresses, zones, routeGroups, asShipperAddress, suggestedAddresses, intl } = this.props
    let collapsible = page === PAGE_INDEX.AV_PROCESSS
    const columns = [
      {
        title: '',
        dataIndex: 'address',
        key: 'addressCopy',
        width: '56px',
        render: text => (
          <CopyToClipboard text={text} onCopy={this.handleOnCopy}>
            <Button size='default' icon='copy' />
          </CopyToClipboard>
        ),
      },
      {
        title: 'Address',
        dataIndex: 'address',
        key: 'address',
        width: '200px',
      },
      {
        title: 'Suggested Address',
        dataIndex: 'waypointId',
        key: 'suggestedAddress',
        width: '188px',
        render: (text, record) => {
          const id = record.waypointId || record.id
          return (
            <div>
              {this.renderSuggestedAddressOption(id, suggestedAddresses[id])}
            </div>
          )
        },
      },
      {
        title: 'Actions',
        dataIndex: 'waypointId',
        key: 'actions',
        width: '120px',
        render: (text, record) => (
          <div>
            <a href='javascript:void(0)' onClick={() => this.handleTableEdit(record)}>
              <T id='commons.edit' />
            </a>
            <div style={{ marginBottom: '-2px', display: 'inline-block', width: '5px', borderRight: '1px solid', marginRight: '5px', height: '15px' }} />
            <Dropdown overlay={menu(record)}>
              <a className='ant-dropdown-link' href='javascript:void(0)'>
                <T id='commons.more' />
                <Icon type='down' />
              </a>
            </Dropdown>
          </div>
        ),
      },
    ]
    // include zone assign if not a shipper address
    if (!asShipperAddress) {
      columns.push(
        {
          title: 'Assign to Zone',
          dataIndex: 'zoneId',
          key: 'assignToZone',
          render: (text, record) => (
            <div>
              <ZoneSelection zones={zones} value={this.getSelectionValue(record.waypointId)} wpId={record.waypointId} style={{ width: '146px' }} onChange={this.handleTableZoneSelect} />
              <AssignLink href='javascript:void(0)' onClick={() => this.handleTableAssign(record.waypointId)}>
                <T id='container.address-verification.assign' />
              </AssignLink>
            </div>
          ),
        }
      )
    }

    const menu = record => (
      <Menu>
        <Menu.Item key='0' onClick={() => this.handleTableUseSuggestedAddress(record)}>
          <Icon type='save' theme='filled' />
          <T id='container.address-verification.save-address' />
        </Menu.Item>
        <Menu.Item key='1' onClick={() => this.handleTableArchive(record)}>
          <Icon type='delete' theme='filled' />
          <T id='container.address-verification.archive-address' />
        </Menu.Item>
      </Menu>
    )

    const callbackMap = {
      fetchByRg: this.handleFetchByRgId,
      fetchFromPool: this.handleFetch,
      fetchShipperUnverifiedAddress: this.props.fetchShipperUnverifiedAddress,
      fetchReverifiedAddress: this.props.fetchReverifiedAddress,
    }

    return (
      <AddressVerificationContainer className='AddressVerification'>
        <SectionNavigation navItems={navItems} active={page} handleClick={this.handleNavClick} />
        {(page === PAGE_INDEX.INIT) &&
          (<AvpoolFilter poolFilter={this.props.poolFilter} handleUpdateFilter={this.handleUpdateFilter} handleClick={this.handleInit} isLoading={this.props.isInitAv} />)
        }
        {
          (page === PAGE_INDEX.AV_PROCESSS) &&
          (<AvfetchAddressForm
            zones={zones}
            isLoading={this.props.isFetching}
            routegroups={routeGroups}
            callbackmap={callbackMap} />
          )
        }
        <AvstatsCard zonestats={avStats} collapsible={collapsible} />
        {
          (page === PAGE_INDEX.AV_PROCESSS) && (
            <AvCard id='address-verification-table-container'
              title={<T id='container.address-verification.address-verification' />}>
              <FetchAlert message={addresses.length > 0 ? `${addresses.length} addresses fetched` : 'No addresses in pool yet'} type='success' />
              <AvTable columns={columns} dataSource={this.prepareTableData(addresses)} onChange={this.handleTableChange} />
            </AvCard>
          )
        }
        <AvCoordinateDialog
          destroyOnClose
          address={this.state.editingAddress}
          onOk={this.handleTableSave}
          onCancel={this.handleTableEditClose}
          mapBoxToken={Config.MAPBOX_TOKEN}
          country={this.getCountry(this.props.settings)}
          visible={this.state.dialogVisible} />
        <NvlegacyMonitor
          successIndicator={this.props.isInitAv}
          successMessage={intl.formatMessage({ id: 'container.address-verification.address-pool-initialized' })} />
        <NvlegacyMonitor
          successIndicator={this.props.preAvFlag}
          successMessage={intl.formatMessage({ id: 'container.address-verification.success-assign-to-zone' })} />
        <NvlegacyMonitor
          successIndicator={this.props.isArchiving}
          successMessage={intl.formatMessage({ id: 'container.address-verification.success-archive-address' })} />
        <NvlegacyMonitor
          successIndicator={!!this.props.isVerifying || !!this.props.verifyError}
          successDescription={intl.formatMessage({ id: 'container.address-verification.waypoint-succesfully-updated' })}
          successMessage={intl.formatMessage({ id: 'container.address-verification.address-event-created' })}
          errorMessage={this.props.verifyError}
          errorIndicator={!!this.props.isVerifying || !this.props.verifyError} />
      </AddressVerificationContainer>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  addressverification: selector('addressVerification')(),
  addresses: selector('addressVerification', 'addresses')(),
  verifyError: selector('addressVerification', 'verifyError')(),
  zones: selector('addressVerification', 'zones')(),
  avStats: selector('addressVerification', 'avStats')(),
  page: selector('addressVerification', 'page')(),
  routeGroups: selector('addressVerification', 'routeGroups')(),
  isSameday: selector('addressVerification', 'isSameday')(),
  suggestedAddresses: selector('addressVerification', 'suggestedAddresses')(),
  asShipperAddress: selector('addressVerification', 'asShipperAddress')(),
  isInitAv: createLoadingSelector(['GET_AV_INIT'], PREFIX),
  isFetching: createLoadingSelector(['GET_ADDRESSES', 'GET_ADDRESSES_ROUTE_GROUP', 'GET_SHIPPER_UNVERIFIED_ADDRESSES', 'GET_REVERIFIED_ADDRESS'], PREFIX),
  isFetchingAddress: createLoadingSelector(['FETCH_ADDRESS'], PREFIX),
  preAvFlag: createLoadingSelector(['PATCH_PRE_AV_ASSIGN'], PREFIX),
  isArchiving: createLoadingSelector(['PATCH_ARCHIVE_ADDRESS'], PREFIX),
  isVerifying: createLoadingSelector(['PATCH_VERIFY_ADDRESS'], PREFIX),
  poolFilter: selector('addressVerification', 'filter')(),
  settings: selector('global', 'settings')(),
})

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    fetchAddresses: (filter) => dispatch(Creators.getAddressesRequest(filter)),
    fetchAddressesByRouteGroupId: (routeGroupId) => dispatch(Creators.getAddressesRouteGroupRequest(routeGroupId)),
    fetchShipperUnverifiedAddress: () => dispatch(Creators.getShipperUnverifiedAddressesRequest()),
    fetchReverifiedAddress: () => dispatch(Creators.getReverifiedAddressRequest()),
    assignZone: (wpId, zoneId) => dispatch(Creators.patchPreAvAssignRequest(wpId, zoneId)),
    archiveAddress: (ojsId) => dispatch(Creators.patchArchiveAddressRequest(ojsId)),
    verifyAddress: (latLong, address, selectedAddress) => dispatch(Creators.patchVerifyAddressRequest(latLong, address, selectedAddress)),
    fetchZones: () => dispatch(Creators.getZonesRequest()),
    getAvStats: () => dispatch(Creators.getAvStatsRequest()),
    setPage: (data) => dispatch(Creators.setPage(data)),
    initAv: (filter) => dispatch(Creators.getAvInitRequest(filter)),
    getRouteGroups: () => dispatch(Creators.getRouteGroupsRequest()),
    getSuggestedAddresses: (addresses) => dispatch(Creators.getSuggestedAddresses(addresses)),
    resetAddress: () => dispatch(Creators.resetAddress()),
    setSameday: (isSameday) => dispatch(Creators.setSameday(isSameday)),
    setFilter: (filter) => dispatch(Creators.setFilter(filter)),
  }
}

const withConnect = connect(mapStateToProps, mapDispatchToProps)

const withReducer = injectReducer({ key: 'addressVerification', reducer })
const withSaga = injectSaga({ key: 'addressVerification', saga })

export default compose(
  withReducer,
  withSaga,
  withConnect,
  injectIntl
)(AddressVerification)
