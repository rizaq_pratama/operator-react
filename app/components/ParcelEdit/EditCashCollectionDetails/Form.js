// @flow
import React, {useState} from 'react'
import FormControl from '@app/components/Generic/FormControl'
import { Card, Row, Form } from 'antd'
import { injectIntl, intlShape } from 'react-intl'
import { EditFormTypes } from './Types'

type Props = {
  form: Form,
  intl: intlShape,
} & EditFormTypes

export function EditCashForm(props: Props) {
  const { form, intl } = props
  const [useCop, setUseCop] = useState(props.copReqAmount != null)
  const [useCod, setUseCod] = useState(props.codReqAmount != null)

  return (
    <Form className='edit-cash-collection-details'>
      <Card title={intl.formatMessage({id: 'commons.model.cash-collection-info'})} size='small'>
        <Row type='flex' align='bottom'>
          <FormControl
            type='checkbox'
            checkboxText='container.parcel-edit.cash-on-pickup'
            stateKey='cop'
            form={form}
            onChange={() => setUseCop(!useCop)}
            checked={useCop}
            options={{initialValue: false}}
          />
          <FormControl
            type='currency'
            postfix='SGD'
            disabled={!useCop}
            stateKey='copAmount'
            fieldName='container.parcel-edit.cash-on-pickup-amount'
            options={{initialValue: props.copReqAmount, rules: [
                {required: useCop, message: 'Cash value cannot be empty'},
                {type: 'number'},
              ]}}
            form={form}/>
        </Row>
        <Row type='flex' align='bottom'>
          <FormControl
            type='checkbox'
            checkboxText='container.parcel-edit.cash-on-delivery'
            stateKey='cod'
            checked={useCod}
            form={form}
            onChange={() => setUseCod(!useCod)}
            options={{initialValue: false}}
          />
          <FormControl
            type='currency'
            postfix='SGD'
            disabled={!useCod}
            stateKey='codAmount'
            options={{initialValue: props.codReqAmount, rules: [
                {required: useCod, message: 'Cash value cannot be empty'},
                {type: 'number'},
              ]}}
            fieldName='container.parcel-edit.cash-on-delivery-amount'
            form={form}/>
        </Row>
      </Card>
    </Form>
  )
}

export default injectIntl(EditCashForm)
