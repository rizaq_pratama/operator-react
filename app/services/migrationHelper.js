// @flow
import { UNKNOWN_ERROR, SERVER_ERROR, TIMEOUT_ERROR, CLIENT_ERROR, NETWORK_ERROR } from 'apisauce'

/***
 * From Angular doc
 * The response object has these properties:
 *
 * data – {string|Object} – The response body transformed with the transform functions.
 * status – {number} – HTTP status code of the response.
 * headers – {function([headerName])} – Header getter function.
 * config – {Object} – The configuration object that was used to generate the request.
 * statusText – {string} – HTTP status text of the response.
 *
 * A response status code between 200 and 299 is considered a success status and will result in the
 * success callback being called. Any response status code outside of that range is considered an
 * error status and will result in the error callback being called. Also, status codes less than -1
 * are normalized to zero. -1 usually means the request was aborted, e.g. using a config.timeout.
 * Note that if the response is a redirect, XMLHttpRequest will transparently follow it, meaning
 * that the outcome (success or error) will be determined by the final response status code.
 *
 * @param angularResponse
 *
 *
 * From Apisauce
 *
 * A response will always have these 2 properties:
 * ok      - Boolean - True is the status code is in the 200's; false otherwise.
 * problem - String  - One of 6 different values (see below - problem codes)
 * If the request made it to the server and got a response of any kind, response will also have these properties:

 * data     - Object - this is probably the thing you're after.
 * status   - Number - the HTTP response code
 * headers  - Object - the HTTP response headers
 * config   - Object - the `axios` config object used to make the request
 * duration - Number - the number of milliseconds it took to run this request
 * Sometimes on different platforms you need access to the original axios error that was thrown:

 * originalError - Error - the error that axios threw in case you need more info
 *
 * Constant        VALUE               Status Code   Explanation
 * ----------------------------------------------------------------------------------------
 * NONE             null               200-299       No problems.
 * CLIENT_ERROR     'CLIENT_ERROR'     400-499       Any non-specific 400 series error.
 * SERVER_ERROR     'SERVER_ERROR'     500-599       Any 500 series error.
 * TIMEOUT_ERROR    'TIMEOUT_ERROR'    ---           Server didn't respond in time.
 * CONNECTION_ERROR 'CONNECTION_ERROR' ---           Server not available, bad dns.
 * NETWORK_ERROR    'NETWORK_ERROR'    ---           Network not available.
 * CANCEL_ERROR     'CANCEL_ERROR'     ---           Request has been cancelled. Only possible if `cancelToken` is provided in config, see axios `Cancellation`.
 */

type AngularResponse = {
  data: Object,
  status: number,
  headers: Function,
  config: Object,
  statusText: string
}

type ApiSauceResponse = {
  ok: boolean,
  problem: string,
  data: Object,
  status: number,
  headers: Object,
  config: Object,
  duration: number,
}

function transformAngularResponseToApisauce (angularResponse: AngularResponse): ApiSauceResponse {
  const { data, status, config, statusText } = angularResponse
  const ok = status >= 200 && status < 300
  let problem = null

  if (!ok && status >= 400) {
    if (status < 500) {
      problem = CLIENT_ERROR
    } else if (status < 600) {
      problem = SERVER_ERROR
    }
  }

  if (status === 0) {
    if (statusText.indexOf('Network Error') >= 0) {
      problem = NETWORK_ERROR
    } else if (statusText.indexOf('ECONNABORTED') >= 0) {
      problem = TIMEOUT_ERROR
    } else {
      problem = UNKNOWN_ERROR
    }
  }

  const response = {
    ok,
    problem,
    duration: null,
    headers: {}, // angular headers is a getter function instead of an POJO
    data,
    config,
  }

  return response
}

export {
  transformAngularResponseToApisauce,
}
