import { createSelector } from 'reselect'

const GlobalSelector = state => state.get('global', {}).toJS()
const ModalIdSelector = createSelector(GlobalSelector, global => global.modal.id)

export default {
  GlobalSelector,
  ModalIdSelector,
}
