// @flow
import React from 'react'
import { Row, Col, Input, InputNumber, Select, DatePicker, Checkbox, Form, Tooltip, Icon } from 'antd'
import { T } from '@app/components/Generic/T/index'
import { FormDisableContext } from '../../SubmittableModal/index'
import { FormContext } from '@app/components/Generic/Contexts/Form'

const inputStyling = {
  width: '100%',
  fontWeight: 400,
  marginRight: '10px',
}

type FormControlProps = {
  stateKey: string,
  fieldName: string,
  selectMode?: 'multiple' | 'tags' | 'default',
  selectShowSearch?: boolean,
  isLoading: boolean,
  disabled?: boolean,
  type: 'custom' | 'number' | 'text' | 'password' | 'dropdown' | 'dropdown-label' | 'date' | 'checkbox',
  deliveryTypes: string[],
  menu?: string[],
  checkboxText?: string,
  checked?: boolean,
  span?: number,
  offset?: number,
  size?: string,
  value?: any,
  options?: any,
  placeholder?: string,
  onSelectChange?: Function,
  onChange?: Function,
  postfix?: string,
}

class FormControl extends React.Component<FormControlProps> {
  static defaultProps = {
    menu: [],
    postfix: null,
  }
  constructor(props) {
    super(props)
    this.state = {
      passwordVisible: false,
    }
  }

  handlePasswordSuffixClick = () => {
    // Toggle the Password Suffix
    this.setState({ passwordVisible: !this.state.passwordVisible })
  }

  renderPasswordSuffix = () => {
    return (
      <Tooltip title="" onClick={this.handlePasswordSuffixClick}>
        {this.state.passwordVisible ? (
          <Icon type='eye' />
        ) : (
          <Icon type='eye-invisible' />
        )}
      </Tooltip>
    )
  }
  getInputComponent = (type, placeholder) => {
    let disabled = this.props.disabled === true || this.context === true
    switch (type) {
      case 'number':
        return <InputNumber style={inputStyling} disabled={disabled} />
      case 'currency':
        return (
          <InputNumber
            style={inputStyling}
            disabled={disabled}
            precision={2}
          />
        )
      case 'password':
        var passwordType = this.state.passwordVisible ? 'text' : 'password'
        return (
          <Input
            type={passwordType}
            style={inputStyling}
            placeholder={placeholder || '-'}
            suffix={this.renderPasswordSuffix()}
            disabled={disabled}
          />
        )
      case 'text':
        return (
          <Input
            style={inputStyling}
            placeholder={placeholder || '-'}
            disabled={disabled}
          />
        )
      case 'dropdown': {
        const menuItems = this.props.menu
          ? this.props.menu.map(option => (
              <Select.Option style={inputStyling} key={option} value={option}>
                {option}
              </Select.Option>
            ))
          : null
        return (
          <Select
            onChange={this.props.onSelectChange}
            showSearch={this.props.selectShowSearch}
            suffixIcon={this.props.isLoading ? <Icon type='loading' /> : null}
            style={inputStyling}
            mode={this.props.selectMode ? this.props.selectMode : 'default'}
            size={this.props.size ? this.props.size : 'default'}
            disabled={disabled}
          >
            {menuItems}
          </Select>
        )
      }
      case 'dropdown-label': {
        const menuItems = this.props.menu
          ? this.props.menu.map(option => (
              <Select.Option
                style={inputStyling}
                key={option.id}
                value={option.id}
              >
                {option.label}
              </Select.Option>
            ))
          : null
        return (
          <Select
            placeholder={placeholder || ''}
            onChange={this.props.onSelectChange}
            showSearch={this.props.selectShowSearch}
            style={inputStyling}
            mode={this.props.selectMode ? this.props.selectMode : 'default'}
            size={this.props.size ? this.props.size : 'default'}
            disabled={disabled}
          >
            {menuItems}
          </Select>
        )
      }
      case 'date':
        return <DatePicker style={inputStyling} disabled={disabled} />
      case 'checkbox':
        return (
          <Checkbox onChange={this.props.onChange} disabled={disabled}>
            {this.props.checkboxText ? (
              <T id={this.props.checkboxText} />
            ) : null}
          </Checkbox>
        )
    }
  }
  render() {
    const {
      placeholder,
      type,
      span,
      fieldName,
      value,
      stateKey,
      options,
      postfix,
    } = this.props

    if (type === 'custom') {
      return (
        <Col span={span} className='form-control'>
          <Form.Item label={fieldName ? <T id={fieldName} /> : null} id={this.props.stateKey}>
            {value}
          </Form.Item>
        </Col>
      )
    } else {
      const component = this.getInputComponent(type, placeholder)
      if (postfix) {
        return (
          <FormContext.Consumer>
            {form => (
              <Col span={span} className='form-control'>
                <div>
                  <label className='data-label'>
                    <T id={fieldName} />
                  </label>
                  <Row type='flex' align='middle' style={{ marginTop: '4px' }}>
                    <Col style={{ flex: '1 1' }}>
                      {component
                        ? form.getFieldDecorator(stateKey, options)(component)
                        : null}
                    </Col>
                    <Col>
                      <label
                        className='data-label'
                        style={{ margin: 'auto 15px auto 4px' }}
                      >
                        {postfix}
                      </label>
                    </Col>
                  </Row>
                </div>
              </Col>
            )}
          </FormContext.Consumer>
        )
      } else {
        return (
          <FormContext.Consumer>
            {form => (
              <Col span={span} className='form-control'>
                <Form.Item label={fieldName ? <T id={fieldName} /> : null}>
                  {component
                    ? form.getFieldDecorator(stateKey, options)(component)
                    : null}
                </Form.Item>
              </Col>
            )}
          </FormContext.Consumer>
        )
      }
    }
  }
}

FormControl.contextType = FormDisableContext

export default FormControl
