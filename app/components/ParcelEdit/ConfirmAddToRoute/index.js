// @flow
import React, {useState, useEffect} from 'react';
import { connect } from 'react-redux'
import SubmittableModal from '@app/components/SubmittableModal'
import Route from '@app/services/api/routeApi'
import { notification } from 'antd'
import './style.less';
import selectors from '@app/containers/ParcelEdit/selectors'
import AddToRouteForm from './Form'

type Props = {
  visible: boolean,
  onClose?: Function,
  targetJob?: Job,
  jobs: Job[],
};

export function ConfirmAddToRoute (props: Props) {
  const [tags, setTags] = useState([])
  useEffect(() => {
    loadTags()
  })
  return (
    <SubmittableModal
      visible={props.visible}
      title="container.parcel-edit.add-to-route"
      cancelText="commons.go-back"
      okText="container.parcel-edit.add-to-route"
      onSubmit={onSubmit}
      onClose={props.onClose}
    >
      <AddToRouteForm targetJob={props.targetJob} tags={tags} jobs={props.jobs} />
    </SubmittableModal>
  )

  async function onSubmit(data: {routeId: number, type: string, jobId: number}) {
    const { jobs } = props
    const job = jobs.find(job => job.jobType === data.type)
    const res = await Route.addJobToRoute(job.id, job.jobType, data.routeId)

    if (res.ok !== false) {
      props.onClose()
      notification.success({
        message: 'Saved successfully',
      })
    } else {
      // handle error
    }
  }

  async function loadTags() {
    const tags = await Route.getAllTags()
    setTags(tags.tags)
  }
}

const mapDeliveryStateToProps = state => {
  return {
    jobs: selectors.JobsSelector(state),
    targetJob: selectors.DeliveryJobSelector(state),
  }
}

const mapPickupStateToProps = state => {
  return {
    jobs: selectors.JobsSelector(state),
    targetJob: selectors.PickupJobSelector(state),
  }
}

export const ConfirmAddDeliveryJobToRoute = connect(mapDeliveryStateToProps)(ConfirmAddToRoute)
export const ConfirmAddPickupJobToRoute = connect(mapPickupStateToProps)(ConfirmAddToRoute)


