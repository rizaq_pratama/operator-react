import React from 'react'
import { shallowWithIntl } from '@internals/testing/test-helpers'
import EventHistoryBar from '../index'

const testEvents = [{
  label: 'Sort',
  value: 'sort',
}, {
  label: 'Scan',
  value: 'scan',
}, {
  label: 'Pickup',
  value: 'pickup',
}]
const onRadioButtonSortChange = jest.fn()
const onSearchInputChange = jest.fn()
const onEventFilterChange = jest.fn()
const testValueObject = {
  target: {
    value: '',
  },
}

describe('EventHistoryBar', () => {

  let wrapper, instance
  beforeEach(() => {
    wrapper = shallowWithIntl(
      <EventHistoryBar
        events={testEvents}
        onRadioButtonSortChange={onRadioButtonSortChange}
        onSearchInputChange={onSearchInputChange}
        onEventFilterChange={onEventFilterChange}
        />
    )
    instance = wrapper.instance()
  })
  it('renders a EventHistoryBar', () => {
    expect(wrapper).toBeDefined()
  })
  it ('calls callback on radio button sort change', () => {
    instance.handleOnRadioButtonSortChange(testValueObject)
    expect(onRadioButtonSortChange).toBeCalled()
  })
  it('calls callback on search input change', () => {
    instance.handleOnSearchChange(testValueObject)
    expect(onSearchInputChange).toBeCalled()
  })
  it('calls callback on event filter change', () => {
    instance.handleEventFilterChange('')
    expect(onEventFilterChange).toBeCalled()
  })
})
