export default {
  NOBODY_AT_ADDRESS: 'container.parcel-edit.rts-reason.nobody-at-address',
  UNABLE_TO_FIND_ADDRESS: 'container.parcel-edit.rts-reason.unable-to-find-address',
  ITEM_REFUSED_AT_DOORSTEP: 'container.parcel-edit.rts-reason.item-refused-at-doorstep',
  REFUSED_TO_PAY_COD: 'container.parcel-edit.rts-reason.refused-to-pay-cod',
  CUSTOMER_DELAYED_BEYOND_DELIVERY_PERIOD: 'container.parcel-edit.rts-reason.customer-delayed-beyond-delivery-period',
  CANCELLED_BY_SHIPPER: 'container.parcel-edit.rts-reason.cancelled-by-shipper',
  DANGEROUS_GOODS: 'container.parcel-edit.rts-reason.dangerous-goods',
  OUT_OF_COVERAGE: 'container.parcel-edit.rts-reason.out-of-coverage',
  OVERSIZED_PARCEL: 'container.parcel-edit.rts-reason.oversized-parcel',
  OTHER_REASON: 'container.parcel-edit.rts-reason.other-reason',
}

