//  @flow
import React from 'react'
import { T } from '../../Generic/T'
import { Menu } from 'antd'
import GenericDropdownButton from '@app/components/Generic/GenericDropdownButton'
import { MODAL_TYPES } from '@app/containers/OrderEdit/constants'
import OrderStatus from '@nv/react-commons/src/Migration_corev2/Constants/OrderStatus'
import _ from 'lodash'

type TopBarActionsProps = {
  onClick: Function,
  status: string,
}

class TopBarActions extends React.Component<TopBarActionsProps> {
  constructor (props) {
    super(props)
  }

  render () {
    const orderIsCancelled = _.upperCase(this.props.status) === OrderStatus.CANCELLED

    const orderSettingsMenu = (
      <Menu onClick={this.props.onClick}>
        <Menu.Item key={MODAL_TYPES.MANUALLY_COMPLETE_ORDER}>
          <T id='container.order.edit.manually-complete-order' />
        </Menu.Item>
        <Menu.Item key={MODAL_TYPES.CANCEL_ORDER} disabled={orderIsCancelled}>
          <T id='container.order.edit.cancel-order' />
        </Menu.Item>
        <Menu.Item key={MODAL_TYPES.RESUME_ORDER} disabled={!orderIsCancelled}>
          <T id='container.order.edit.resume-order' />
        </Menu.Item>
      </Menu>
    )

    const orderBillingMenu = (
      <Menu onClick={this.props.onClick}>
        <Menu.Item key={MODAL_TYPES.RECALCULATE_PRICING}>
          <T id='container.order.list.recalculate-pricing' />
        </Menu.Item>
      </Menu>
    )

    return (
      <div style={{ float: 'left' }}>
        <GenericDropdownButton
          menu={orderSettingsMenu}
          buttonName='container.order.edit.order-settings'
        />
        <GenericDropdownButton
          menu={orderBillingMenu}
          buttonName='container.order.edit.billing'
        />
      </div>
    )
  }
}

export default TopBarActions
