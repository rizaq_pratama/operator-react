import React from 'react'
import renderer from 'react-test-renderer'
import TopBarActions from '../index'
import { shallowWithIntl, TestIntlProviderWrapper } from '@internals/testing/test-helpers'


let onClicked

describe('TopBarActions', () => {

  let wrapper
  beforeEach(() => {
    onClicked = jest.fn()

    wrapper = shallowWithIntl(
      <TopBarActions onClick={onClicked}/>
    )
  })
  it('should match snapshot', () => {
    const component = renderer.create(
      <TestIntlProviderWrapper>
        <TopBarActions />
      </TestIntlProviderWrapper>
    )
    expect(component).toMatchSnapshot()
  })
  it('should render', () => {
    expect(wrapper).toBeDefined()
  })
})
