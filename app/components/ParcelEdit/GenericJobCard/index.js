//  @flow
import React from 'react'
import styled from 'styled-components'
import './style.less'
import { Col, Card, Row, Button } from 'antd'
import { Colors } from '@app/themes'
import StatusIcon from '../../StatusIcon'
import { T } from '@app/components/Generic/T'
import CopiableText from '../../Generic/CopiableDataField'
import JobStatus from '@nv/react-commons/src/Migration_corev2/Constants/JobStatus'
import { RouteInfoCard } from '../Common/Card'

const RouteInformationCard = styled(RouteInfoCard)`
  && .ant-card-head {
    background-color: ${Colors.nvBackgroundGrey};
  }
  && .ant-card-head-title {
  }
  && .ant-card-body {
    background-color: ${Colors.nvBackgroundGrey}
  }

  .value {
    font-weight: 300;
  }

  .info-row >div {
    margin-bottom: 13px;
  }
`

type GenericJobCardProps = {
  type: 'pickup' | 'delivery',
  style: object,
  jobInfo: {
    jobStatus: $Keys<typeof JobStatus>,
    shipperName: string,
    customerName: string,
    phoneNumber: string,
    email: string,
    address: string,
    routeInformation: {
      routeId: number,
      routeDate: string,
      waypointId: number,
      driverId: number,
      failedAttempts: number,
      instructions: string
    }
  },
  onClick: Function
}

class GenericJobCard extends React.Component<GenericJobCardProps> {
  render () {
    const { jobStatus, customerName, shipperName, phoneNumber, email, routeInformation, address } = this.props.jobInfo
    const { routeId, routeDate, waypointId, driverId, failedAttempts, instructions } = routeInformation
    return (
      <Card title={<T id={'commons.model.' + this.props.type + '-job'} />} className='height-100 generic-job-card'
        style={this.props.style}
        extra={
          <Button type='default' icon='edit' size='small' onClick={this.props.onClick}>
            <T id={'commons.model.edit-' + this.props.type + '-details'} />
          </Button>
        } >
        <Row type='flex' align='middle' justify='start' className='job-status'>
          <Col>
            <T id={'commons.model.job-status'}/>:
          </Col>
          <Col>
          <span style={{marginLeft: '10px'}}>
            {jobStatus}
          </span>
          </Col>
          <StatusIcon type={jobStatus} />
        </Row>

        <CopiableText value={shipperName || customerName} className='shipper-name' />
        <CopiableText icon='phone' value={phoneNumber} />
        <CopiableText icon='mail' value={email} />
        <CopiableText icon='environment' value={address} />

        <RouteInformationCard title={<T id={'commons.model.route-info'} />}
          bordered={false} >
          <Row className='info-row'>
            <Col span={6}>
              <T id='commons.model.route-id' className='color-nvNeutral600'/>
              <div >
                {routeId||'-'}
              </div>
            </Col>
            <Col span={6}>
              <T id='commons.model.route-date' className='color-nvNeutral600'/>
              <div >
                {routeDate||'-'}
              </div>
            </Col>
            <Col span={6}>
              <T id='commons.waypoint-id' className='color-nvNeutral600'/>
              <div >
                {waypointId||'-'}
              </div>
            </Col>
            <Col span={6}>
              <T id='commons.model.driver-id' className='color-nvNeutral600'/>
              <div >
                {driverId||'-'}
              </div>
            </Col>
            <Col span={6}>
              <T id='commons.model.failed-attempts' className='color-nvNeutral600'/>
              <div >
                {failedAttempts||'-'}
              </div>
            </Col>
            <Col span={18}>
              <T id='commons.model.instructions' className='color-nvNeutral600'/>
              <div >
                {instructions||'-'}
              </div>
            </Col>
          </Row>
        </RouteInformationCard>
      </Card>
    )
  }
}

export default GenericJobCard
