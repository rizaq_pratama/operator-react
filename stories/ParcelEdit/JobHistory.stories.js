/* global alert */
import React from 'react'
import { storiesOf } from '@storybook/react'
import JobHistory from '@app/components/ParcelEdit/JobHistory/index'

const data = [{
  endTime: '-',
  jobType: 'Delivery',
  jobStatus: 'Pending',
  jobId: '1234567',
  routeId: '-',
  routeDate: '-',
  assignedTo: '-',
  dpId: '-',
  failureReason: '-',
  priorityLevel: '0',
  dnr: '-',
  podId: 123,
}, {
  endTime: '2018-10-10 12:34:56',
  jobType: 'Pickup',
  jobStatus: 'Success',
  jobId: '1234567',
  routeId: '1234567',
  routeDate: '2018-10-10',
  assignedTo: 'F1 - Jon Snow',
  dpId: '-',
  failureReason: '-',
  priorityLevel: '0',
  dnr: '-',
}]

storiesOf('ParcelEdit', module)
  .add('JobHistory', () => <JobHistory
    jobs={data}
    onJobIdClicked={() => alert('On Job ID Clicked')}
    onRouteIdClicked={() => alert('On Route ID Clicked')}
    onPodIdClicked={() => alert('On Pod ID Clicked')}
  />)
