// @flow
import React, { useState } from 'react'
import { connect } from 'react-redux'

import SubmittableModal from '@app/components/SubmittableModal/index'
import { EditParcelDetailModalSelector } from './selectors'
import EditParcelDetailsContent from './Form/index'
import { ParcelInformation, ParcelFormData } from './Types'
import { notification } from 'antd'
import NC from '@app/services/api/ninjaControlApi'

type EditParcelDetailsModalProps = {
  parcelInformation: ParcelInformation,
  parcelFormData: ParcelFormData,
  visible: boolean,
  onClose: Function
}

export function EditParcelDetailsModal(props: EditParcelDetailsModalProps) {
  const [isLoading, setLoading] = useState(false)
  const modalRef = React.createRef()
  return (
    <SubmittableModal
      visible={props.visible}
      ref={modalRef}
      disabled={isLoading}
      title={'commons.model.edit-parcel-details'}
      onSubmit={onSubmit}
      onClose={props.onClose}
    >
      <EditParcelDetailsContent
        parcelInformation={props.parcelInformation}
        parcelFormData={props.parcelFormData}
      />
    </SubmittableModal>
  )

  async function onSubmit(data: {stampId: string, selectedDeliveryType: string}) {
    const form = modalRef.current
    setLoading(true)

    const [updateParcel, updateOrder] = await Promise.all([
      data.stampId? NC.updateParcelDetails(props.parcelInformation.id, {stampId: data.stampId}): {},
      data.selectedDeliveryType? NC.updateOrderDetails(props.parcelInformation.orderId, {serviceLevel: data.selectedDeliveryType}) : {},
    ])

    if (updateParcel.ok === false) {
      form.setFields({
        stampId: {
          errors: [new Error(updateParcel.data.error.message)],
        },
      })
    }

    if (updateOrder.ok === false) {
      form.setFields({
        selectedDeliveryType: {
          errors: [new Error(updateOrder.data.error.message)],
        },
      })
    }

    if (![updateOrder, updateParcel].some(r => r.ok === false)) {
      props.onClose()
      notification.success({
        message: 'Saved successfully',
      })
    }

    setLoading(false)
  }
}

const mapStateToProps = state => {
  return EditParcelDetailModalSelector(state)
}

export default connect(mapStateToProps)(EditParcelDetailsModal)
