//  @flow
import React from 'react'
import styled from 'styled-components'
import { Table, Card, Input, Icon } from 'antd'
import { T } from '@app/components/Generic/T'
import _ from 'lodash'

type ParceTableProps = {
  data: Array<{
    trackingId: string,
    parcelMetadata: {
      currentJobType: string,
      currentJobStatus: string,
    },
    granularStatus: string,
  }>
}

const InnerCard = styled(Card)`
  && {
    border-radius: 0.3em;
    margin-top: 1em;
  }
`

const footerStyling = {
  textAlign: 'center',
}

const columns = [{
  title: <T id='commons.model.parcel-tracking-id' />,
  dataIndex: 'trackingId',
  key: 'trackingId',
  //  eslint-disable-next-line
  render: text => <a href="javascript;">{text}</a>,
}, {
  title: <T id='commons.model.current-job' />,
  dataIndex: 'parcelMetadata.currentJobType',
  key: 'parcelMetadata.currentJobType',
}, {
  title: <T id='commons.model.current-job-status' />,
  dataIndex: 'parcelMetadata.currentJobStatus',
  key: 'parcelMetadata.currentJobStatus',
}, {
  title: <T id='commons.model.current-granular-status' />,
  dataIndex: 'granularStatus',
  key: 'granularStatus',
}]

class ParcelTable extends React.Component<ParceTableProps> {
  constructor (props) {
    super(props)
    this.state = {
      searchTerm: '',
    }
    this.searchTermEqualityCheck = this.searchTermEqualityCheck.bind(this)
    this.searchParcelHandler = this.searchParcelHandler.bind(this)
  }
  searchTermEqualityCheck(parentString) {
    return _.includes(_.lowerCase(parentString), _.lowerCase(this.state.searchText))
  }
  searchParcelHandler (e) {
    this.setState({
      searchText: e.target.value,
    })
  }
  render () {
    const dataToRender = this.props.data ? _.filter(this.props.data, (parcel) =>
      this.searchTermEqualityCheck(parcel.trackingId) ||
      this.searchTermEqualityCheck(parcel.parcelMetadata.currentJobType) ||
      this.searchTermEqualityCheck(parcel.parcelMetadata.currentJobStatus) ||
      this.searchTermEqualityCheck(parcel.granularStatus)).map((data, i) => {
        return Object.assign(data, { key: i })
    }) : [];

    return (
      <InnerCard title={<T id='commons.model.parcels-in-order' />}
        extra={
          <Input prefix={
              <Icon type='search' />
            }
            placeholder='Search Parcels'
            onChange={this.searchParcelHandler}
          />}>
        <Table
          pagination={false}
          columns={columns}
          dataSource={dataToRender}
          footer={() =>
            <div style={footerStyling}>End of Table</div>
          }/>
      </InnerCard>
    )
  }
}

export default ParcelTable
