import React from 'react'
import { shallow } from 'enzyme'
import PriorityLevelTag from '../index'

describe('PriorityLevelTag', () => {
  it('renders a volcano PriorityLevelTag component', () => {
    const wrapper = shallow(
      <PriorityLevelTag
        priorityLevel={59} />
    )
    expect(wrapper).toBeDefined()
  })
  it('renders a red PriorityLevelTag component', () => {
    const wrapper = shallow(
      <PriorityLevelTag
        priorityLevel={95} />
    )
    expect(wrapper).toBeDefined()
  })
})
