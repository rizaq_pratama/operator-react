import React from 'react'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'
import GenericJobCard from '../index'
import { TestIntlProviderWrapper } from '../../../../../internals/testing/test-helpers'
import 'jest-styled-components'

const testJobInfo = {
  jobStatus: 'Success',
  shipperName: 'Amazon.com Services, Inc.',
  phoneNumber: '+6566028271',
  email: 'support@ninjavan.co',
  address: '30 Jln Kilang Barat Singapore 159363 SG',
  routeInformation: {
    routeId: 567162,
    routeDate: '2018-10-09',
    waypointId: 44519426,
    driverId: 100083,
    failedAttempts: 0,
    instructions: null,
  },
}

describe('GenericJobCard', () => {
  it('matches snapshot', () => {
    const component = renderer.create(
      <TestIntlProviderWrapper>
        <GenericJobCard
          type={'pickup'}
          jobInfo={testJobInfo} />
      </TestIntlProviderWrapper>
    )
    expect(component.toJSON()).toMatchSnapshot()
  })
  it('renders a GenericJobCard component', () => {
    const renderedComponent = shallow(
      <GenericJobCard
        type={'pickup'}
        jobInfo={testJobInfo} />
    )
    expect(renderedComponent).toBeDefined()
  })
})
