import { fromJS } from 'immutable'
import addressVerificationReducer, { INITIAL_STATE } from '../redux'

describe('addressVerificationReducer', () => {
  it('returns the initial state', () => {
    expect(addressVerificationReducer(undefined, {})).toEqual(fromJS(INITIAL_STATE))
  })
})
