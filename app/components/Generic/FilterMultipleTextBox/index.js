//  @flow
import React from 'react'
import { Tag, Input } from 'antd'
import { FILTER_TAG_COLORS } from '@app/constants/Colors'
import FilterContainer from '../FilterContainer'
import _ from 'lodash'
import type { FilterComponent } from '../Types'
import { injectIntl, intlShape } from 'react-intl'

type FilterMultipleTextBoxProps = {
  id: string,
  label: string,
  intl: intlShape
}

class FilterMultipleTextBox extends React.Component<FilterMultipleTextBoxProps> implements FilterComponent {
  constructor (props) {
    super(props)
    this.state = {
      selected: new Set(),
    }
  }
  addOption (e) {
    this.state.selected.add(e.target.value)
    this.setState({})
  }
  removeOption (value) {
    this.state.selected.delete(value)
    this.setState({})
  }
  clear() {
    this.setState({
      selected: new Set(),
    })
  }
  serialize() {
    return [...this.state.selected].join(',')
  }
  deserialize(values) {
    this.setState({selected: new Set(values)})
  }
  render () {
    const addedOptions = _.map([...this.state.selected], (addedOption, i) =>
      <Tag
        color={FILTER_TAG_COLORS[i%FILTER_TAG_COLORS.length]}
        closable
        onClose={this.removeOption.bind(this, addedOption)}
        key={i}
      >
        {addedOption}
      </Tag>
    )
    return (
      <FilterContainer label={this.props.label} items={addedOptions} id={this.props.id}>
        <Input
          placeholder={
            this.props.intl.formatMessage({id: 'container.filter-box.search-term'})
          }
          onPressEnter={this.addOption.bind(this)} />
      </FilterContainer>
    )
  }
}

export default injectIntl(FilterMultipleTextBox, {withRef: true})
