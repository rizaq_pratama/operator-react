import React from 'react'
import { JobDetailsModal } from '@app/components/ParcelEdit/JobDetailsModal'
import { storiesOf } from '@storybook/react'
import { TestIntlProviderWrapper } from '@internals/testing/test-helpers'

const sampleJobInfo = {
  jobId: 12345678,
  jobType: 'Pickup',
  priorityLevel: null,
  name: 'Min Thu Ya Hlaing',
  contact: '+6585881948',
  email: 'minthuya@hotmail.com',
  address: 'Blk 62, #11-27 Woodlands Drive 16 Singapore 737895 SG',
  endTime: '5pm',
}

const sampleRouteInfo = {
  routeId: 433566,
  routeDate: '2018-10-11',
  assignedTo: 'F1 - Izman',
  dpId: null,
  failureReason: null,
  dnr: null,
  link: 'https://www.ninjavan.co/en-sg',
}

storiesOf('ParcelEdit', module).add('JobDetailsModal', () => (
  <TestIntlProviderWrapper>
    <JobDetailsModal visible jobInformation={sampleJobInfo} routeInformation={sampleRouteInfo} />
  </TestIntlProviderWrapper>
))
