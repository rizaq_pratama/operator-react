/**
 * COMMON WEBPACK CONFIGURATION
 */
const _ = require('lodash')
const path = require('path')
const webpack = require('webpack')
const readConfig = require('../scripts/readConfig')

// Remove this line once the following warning goes away (it was meant for webpack loader authors not users):
// 'DeprecationWarning: loaderUtils.parseQuery() received a non-string value which can be problematic,
// see https://github.com/webpack/loader-utils/issues/56 parseQuery() will be replaced with getOptions()
// in the next major version of loader-utils.'
process.noDeprecation = true

const SUPPORTED_ENV = ['local', 'dev', 'qa', 'prod']
let environment = process.env.ENVIRONMENT
if (!_.includes(SUPPORTED_ENV, environment)) {
  environment = 'dev'
}

const ENV_FILE = `.env.${environment}`

let transpile = ['react-intl-phraseapp', '@nv/react-commons']
const nodeModulePath = path.resolve(__dirname, '../../node_modules')
transpile.map((m, i) => {
  transpile[i] = nodeModulePath + '/' + m
})

module.exports = options => ({
  mode: options.mode,
  entry: options.entry,
  output: Object.assign(
    {
      // Compile into js/build.js
      path: path.resolve(process.cwd(), 'build'),
      publicPath: '/',
    },
    options.output
  ), // Merge with env dependent settings
  optimization: options.optimization,
  module: {
    rules: [
      {
        test: /\.js$/, // Transform all .js files required somewhere with Babel
        include: transpile,
        use: {
          loader: 'babel-loader',
          options: options.babelQuery,
        },
      },
      {
        test: /\.js$/, // Transform all .js files required somewhere with Babel
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: options.babelQuery,
        },
      },
      {
        // Preprocess our own .css files
        // This is the place to add your own loaders (e.g. sass/less etc.)
        // for a list of loaders, see https://webpack.js.org/loaders/#styling
        test: /\.css$/,
        exclude: /node_modules/,
        use: ['style-loader', 'css-loader'],
      },
      {
        // Preprocess 3rd party .css files located in node_modules
        test: /\.css$/,
        include: /node_modules/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(eot|svg|otf|ttf|woff|woff2)$/,
        use: 'file-loader',
      },
      {
        test: /\.(jpg|png|gif)$/,
        use: {
          loader: 'file-loader',
          options: { name: '[name].[hash:16].[ext]' },
        },
      },
      {
        test: /\.html$/,
        use: 'html-loader',
      },
      {
        test: /\.(mp4|webm)$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 10000,
          },
        },
      },
      {
        test: /vars\.less$/,
        use: 'raw-loader',
      },
      {
        test: /\.less$/,
        exclude: /vars\.less/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          {
            loader: 'less-loader',
            options: {
              modifyVars: require('../../app/themes/antd.js'),
              javascriptEnabled: true,
            },
          },
        ],
      },
    ],
  },
  plugins: options.plugins.concat([
    // Always expose NODE_ENV to webpack, in order to use `process.env.NODE_ENV`
    // inside your code for any environment checks; UglifyJS will automatically
    // drop any unreachable code.
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV),
      },
      __DEV__: process.env.NODE_ENV !== 'production',
      __CONFIG__: JSON.stringify(readConfig(ENV_FILE)),
    }),
  ]),
  resolve: {
    modules: ['app', 'node_modules'],
    alias: {
      '@app': path.resolve(__dirname, '../../app'),
      '@internals': path.resolve(__dirname, '../../internals'),
      moment$: 'moment/moment.js',
    },
    extensions: ['.js', '.jsx', '.react.js'],
    mainFields: ['browser', 'jsnext:main', 'main'],
  },
  devtool: options.devtool,
  target: 'web', // Make web variables accessible to webpack, e.g. window
  performance: options.performance || {},
})
