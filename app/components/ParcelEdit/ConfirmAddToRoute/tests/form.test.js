import React from 'react'
import {Button} from 'antd'
import { mount, shallow } from 'enzyme'
import toJSON from 'enzyme-to-json'
import { TestIntlProviderWrapper } from '@internals/testing/test-helpers'
import { AddToRouteForm } from '../Form'
import { FormContext } from '@app/components/Generic/Contexts/Form'
import Route from '@app/services/api/routeApi'

jest.mock('@app/services/api/routeApi', () => {
  return {
    suggestRoute: jest.fn(),
  }
})

describe('AddToRouteForm', () => {
  let form = null
  beforeEach(() => {
    form = {
      getFieldDecorator: () => jest.fn(() => null),
      validateFields: jest.fn((arr, cb) => cb(null, {type: 'parcel-delivery'})),
      getFieldValue: jest.fn(() => []),
    }
  })
  it('renders a AddToRouteForm component', () => {
    const wrapper = shallow(
      <AddToRouteForm form={form}/>
    )
    expect(wrapper).toBeDefined()
    expect(toJSON(wrapper)).toMatchSnapshot()
  })
  it('click button', () => {
    const jobs = [
      {id: 1, jobType: 'parcel-delivery'},
      {id: 2, jobType: 'parcel-pickup'},
    ]
    const job = jobs[0]
    const wrapper = mount(
      <TestIntlProviderWrapper>
        <FormContext.Provider value={form}>
          <AddToRouteForm form={form} jobs={jobs} targetJob={job} tags={[{id: 2, name: 'tag'}]}/>
        </FormContext.Provider>
      </TestIntlProviderWrapper>
    )

    wrapper.find(Button).simulate('click')
    expect(Route.suggestRoute).toHaveBeenCalled()
    expect(form.validateFields).toHaveBeenCalled()
  })
})
