/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react'
import styled from 'styled-components'
import { Switch, Route } from 'react-router-dom'

import { routes } from '@app/configs/routesConfig'
import { compose } from 'redux'
import sagas from '@app/services/saga'

import GlobalStyles from '../../themes/global-styles'

const AppWrapper = styled.div`
  max-width: 100%;
  width: 100%;
  margin: 0 auto;
  display: flex;
  min-height: 100%;
  flex-direction: column;
`
const MainContainer = styled.div`
  display: flex;
  flex: 1;
  min-height: 100%;
  background: #F4F6F8;
  >div {
    width: 100%;
  }
`

function renderRoutes (path) {
  return (
    <Switch>
      {routes.map((route, index) => (
        <Route
          key={index}
          path={route.path}
          exact={route.exact}
          component={route[path]}
        />
      ))}
    </Switch>
  )
}

function renderMain () {
  return renderRoutes('main')
}

export const App = () => {
  return (
    <AppWrapper>
      <GlobalStyles />
      <MainContainer>{renderMain()}</MainContainer>
    </AppWrapper>
  )
}

export default compose(...sagas)(App)
