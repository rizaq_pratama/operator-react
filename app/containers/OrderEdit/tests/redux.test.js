import orderEditReducer, { Types } from '../redux'
import { fromJS } from 'immutable'

describe('orderEditReducer', () => {
  let state
  beforeEach(() => {
    state = fromJS({
      parcelInfo: [],
      orderInfo: {},
      isLoading: false,
    })
  })
  it('sets order info to appropriate value', () => {
    const orderInfo = { test: 'test' }
    const newState = orderEditReducer(state, { type: Types.GET_ORDER_INFO_SUCCESS, data: orderInfo })
    expect(newState).toEqual(state.set('orderInfo', orderInfo))
  })
  it('sets parcel info to appropriate value', () => {
    const parcelInfo = { test: 'test' }
    const newState = orderEditReducer(state, { type: Types.SET_PARCEL_INFO, data: parcelInfo })
    expect(newState).toEqual(state.set('parcelInfo', parcelInfo))
  })
  it('sets is loading attribute', () => {
    const newState = orderEditReducer(state, { type: Types.SET_LOADING, isLoading: true })
    expect(newState).toEqual(state.set('isLoading', true))
  })
})
