import React from 'react'
import { mount, shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { ConfirmManuallyCompleteParcel } from '../index'
import NC from '@app/services/api/ninjaControlApi'
import { TestIntlProviderWrapper } from '@internals/testing/test-helpers'

jest.mock('@app/services/api/ninjaControlApi', () => {
  return {
    forceSuccessParcel: jest.fn(),
  }
})

describe('ConfirmManuallyCompleteParcel', () => {
  let intl
  beforeEach(() => {
    intl = {
      formatMessage: () => {},
    }
  })
  it('renders a ConfirmManuallyCompleteParcel component', () => {
    const wrapper = shallow(
      <ConfirmManuallyCompleteParcel visible intl={intl}/>
    )
    expect(toJson(wrapper)).toMatchSnapshot()
  })
  it('submit the form ', () => {
    const wrapper = mount(
      <TestIntlProviderWrapper>
        <ConfirmManuallyCompleteParcel visible intl={intl} parcelTrackingNumber='123456' onClose={jest.fn()}/>
      </TestIntlProviderWrapper>
    )

    NC.forceSuccessParcel.mockReturnValue({ok: true})
    wrapper.find('[type="danger"]').simulate('click')
    expect(NC.forceSuccessParcel.mock.calls[0][0]).toBe('123456')
  })
})
