import React from 'react'
import InformationBar from '@app/components/ParcelEdit/InformationBar'
import { storiesOf } from '@storybook/react'
import { select, number, text, boolean } from '@storybook/addon-knobs'
import { GranularStatus } from '@nv/react-commons/Migration_corev2/Constants'
import _ from 'lodash'

const statusOptions = _.map(_.values(GranularStatus.default), s => [s, s])

storiesOf('ParcelEdit', module).add('InformationBar', () => (
  <InformationBar
    currency={text('Currency', 'SGD')}
    trackingNumber={text('Tracking Number', 'NVSG0000000123')}
    cod={number('COD', 59)}
    priorityLevel={number('Priority Level', 39)}
    status={select('Status', _.fromPairs(statusOptions), 'PENDING PICKUP', 'granular_status')}
    isRts={boolean('Is RTS', true)}
  />
))
