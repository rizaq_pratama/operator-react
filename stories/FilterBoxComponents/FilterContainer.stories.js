import React from 'react'
import { storiesOf } from '@storybook/react'
import FilterContainer from '@app/components/Generic/FilterContainer'

storiesOf('FilterBoxComponents', module).add(
  'FilterContainer', () => (
    <FilterContainer label="Granular Status" selectedCount={100000}>
      <select>
      </select>
    </FilterContainer>
))

