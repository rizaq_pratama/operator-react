import React from 'react'
import toJson from 'enzyme-to-json'
import { shallow } from 'enzyme'
import OrderStatusTag from '../index'

describe('OrderStatusTag', () => {
  it('renders a in-progress OrderStatusTag', () => {
    const pendingWrapper = shallow(
      <OrderStatusTag
        status='In-progress'
      />
    )
    expect(pendingWrapper).toBeDefined()
    expect(toJson(pendingWrapper)).toMatchSnapshot()
  })
  it('renders a pending OrderStatusTag', () => {
    const pendingWrapper = shallow(
      <OrderStatusTag
        status='pending'
      />
    )
    expect(pendingWrapper).toBeDefined()
  })
  it('renders a cancelled OrderStatusTag', () => {
    const cancelledWrapper = shallow(
      <OrderStatusTag
        status='cancelled'
      />
    )
    expect(cancelledWrapper).toBeDefined()
  })
  it('does not render a component', () => {
      const wrapper = shallow(
        <OrderStatusTag
          status='meh'
        />
      )
      expect(wrapper.type()).toEqual(null)
  })
})
