import React from 'react'
import { ConfirmCancelParcel } from '@app/components/ParcelEdit/ConfirmCancelParcel'
import { storiesOf } from '@storybook/react'
import { TestIntlProviderWrapper } from '@internals/testing/test-helpers'
import messages from '@app/translations/en.json'

function formatMessage({id}) {
  return messages[id]
}

storiesOf('ParcelEdit', module).add('ConfirmCancelOrder', () => (
  <TestIntlProviderWrapper>
    <ConfirmCancelParcel visible intl={{formatMessage}} form={{getFieldDecorator: () => (e) => e}}/>
  </TestIntlProviderWrapper>
))
