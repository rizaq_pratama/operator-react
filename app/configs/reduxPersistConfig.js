import { createTransform } from 'redux-persist-immutable'
import { Set } from 'immutable'

/**
 * blacklist sample:
 * return state
 *       .removeIn(['addresses'])
 *       .removeIn(['isInitializing'])
 *       .removeIn(['isFetching'])
 * whitelist sample:
 * return state.filter(keyIn('zones', 'routeGroups'))
 */

function customBlacklist(state, key) {
  switch (key) {
    case 'addressVerification':
      return null
    case 'global':
      return state.remove('modal')
    case 'loading':
      return null
    case 'claim':
      return null
    default:
      return state
  }
}

function keyIn(...keys) {
  var keySet = Set(keys)
  return function (v, k) {
    return keySet.has(k)
  }
}

const CONFIG = {
  version: '1',
  storeConfig: {
    key: 'root',
    blacklist: [''],
    transforms: [createTransform(customBlacklist, customBlacklist)],
  },
}

export default CONFIG
