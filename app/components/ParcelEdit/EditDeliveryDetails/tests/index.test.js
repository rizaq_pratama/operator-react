import React from 'react'
import { mount, shallow } from 'enzyme'
import { EditDeliveryDetails } from '../index'
import toJSON from 'enzyme-to-json'
import { TestIntlProviderWrapper } from '@internals/testing/test-helpers'
import NC from '@app/services/api/ninjaControlApi'

jest.mock('react-mapbox-gl', () => {
  function Map() {
    return function () {return null}
  }

  Map.Marker = function () {return null}

  return Map
})

jest.mock('@app/services/api/ninjaControlApi', () => {
  return {
    updateDeliveryDetails: jest.fn(() => ({ok: true})),
    rescheduleDeliveryJob: jest.fn(() => ({ok: true})),
    updateDeliveryAddress: jest.fn(() => ({ok: true})),
  }
})

describe('EditDeliveryDetails', () => {
  let job
  beforeEach(() => {
    job = {
      'id': 3,
      'job_type': 'parcel-delivery',
      'priority_level': 123,
      'start_date': '2018-10-11',
      'end_date': '2018-10-11',
      'start_time': '12:00:00+08:00',
      'end_time': '15:00:00+08:00',
      'sla_date_time': '2018-10-11T15:00:00+08:00',
      'name': 'Shipper Name',
      'email': 'shipper@ninjavan.co',
      'contact': '90909090',
      'address1': 'GUIMERE S. ISABELITA #04-19 LUCKY PLAZA, 034 ORCHARD ROAD, SINGAPORE 238863',
      'address2': '#04-19 LUCKY PLAZA',
      'neighbourhood': 'SINGAPORE',
      'locality': 'SINGAPORE',
      'region': 'SINGAPORE',
      'country': 'SINGAPORE',
      'postcode': '238863',
      'latitude': 1.3308,
      'longitude': 103.908,
      'status': 'Pending',
      'parcel_id': 1,
      'cod_req_id': 121,
      'cod_req_amount': 99.78,
      'created_at': '2018-10-11T15:00:00',
      'updated_at': '2018-10-11T15:00:00',
    }
  })
  it('renders a EditDeliveryDetails', () => {
    const wrapper = shallow(
      <EditDeliveryDetails
        visible
        {...job}
      />
    )
    expect(toJSON(wrapper)).toMatchSnapshot()
  })

  describe('full render', () => {
    let wrapper
    beforeEach(() => {
      wrapper = mount(
        <TestIntlProviderWrapper>
          <EditDeliveryDetails visible {...job}/>
        </TestIntlProviderWrapper>
      )
    })

    it('should submit', async () => {
      wrapper.find('.ant-modal-footer [type="primary"]').simulate('click')
      expect(NC.updateDeliveryDetails).toHaveBeenCalled()
      expect(NC.updateDeliveryAddress).toHaveBeenCalled()
      expect(NC.rescheduleDeliveryJob).toHaveBeenCalled()
      await new Promise(setTimeout)
    })
  })
})
