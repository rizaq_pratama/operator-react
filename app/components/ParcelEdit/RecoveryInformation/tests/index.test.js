import React from 'react'
import renderer from 'react-test-renderer'
import RecoveryInformation from '../index'
import { shallowWithIntl, TestIntlProviderWrapper } from '@internals/testing/test-helpers'

const tickets = [
  { 'id': 244604, 'description': 'Shipper Issue', 'status': 'Pending' },
  { 'id': 212543, 'description': 'Delivery Issue', 'status': 'Pending' },
]
const onTicketLinkClicked = jest.fn()

describe('RecoveryInformation', () => {
  let wrapper, instance
  beforeEach(() => {
    wrapper = shallowWithIntl(
      <RecoveryInformation
        tickets={tickets}
        onTicketLinkClicked={onTicketLinkClicked} />
    )
    instance = wrapper.instance()
  })
  it('renders correctly', () => {
    const tree = renderer.create(<TestIntlProviderWrapper><RecoveryInformation /></TestIntlProviderWrapper>).toJSON()
    expect(tree).toMatchSnapshot()
  })
  it('renders a RecoveryInformation', () => {
    expect(
      wrapper
    ).toBeDefined()
  })
  it('should call callback function on ticket click', () => {
    instance.handleTicketClick({
      preventDefault: () => {},
      target: {
        id: '',
      },
    })
    expect(onTicketLinkClicked).toBeCalled()
  })
})
