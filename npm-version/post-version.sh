#!/usr/bin/env bash -e

# Push the new Release Branch
branch=$(git rev-parse --abbrev-ref HEAD)

if [ "$branch" == 'master' ]; then
  git push --set-upstream origin master
else
  PACKAGE_VERSION=$(cat package.json | grep version | head -1 | awk -F: '{ print $2 }' | sed 's/[\",]//g' | tr -d '[[:space:]]')
  git branch -m release/temp_$(git rev-parse --short HEAD^) release/$PACKAGE_VERSION
  # Push branch and tags
  git push --set-upstream origin release/$PACKAGE_VERSION
  git push origin --tags
fi
