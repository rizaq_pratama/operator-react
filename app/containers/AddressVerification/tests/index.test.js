import React from 'react'
import { shallowWithIntl } from '@internals/testing/test-helpers'
import { AddressVerification } from '../index'

const addresses = [
  {
    address1: 'A',
    address2: 'B',
    city: 'C',
    country: 'D',
    postcode: 0,
  },
  {
    address1: 'A',
    address2: 'B',
    city: 'C',
    country: 'D',
    postcode: 0,
  },
  {
    address1: 'A',
    address2: 'B',
    city: 'C',
    country: 'D',
    postcode: 0,
  },
]

const zones = [
  {
    id: 1,
    name: 'KOOB',
  },
  {
    id: 2,
    name: 'LOREM',
  },
  {
    id: 3,
    name: 'IPSUM',
  },
  {
    id: 4,
    name: 'LORLOR',
  },
  {
    id: 5,
    name: 'SUMSUM',
  },
]

const routeGroups = [
  {
    id: 1,
    name: 'ROUTE GROUP 1',
  },
  {
    id: 2,
    name: 'ROUTE GROUP 2',
  },
  {
    id: 3,
    name: 'ROUTE GROUP 3',
  },
  {
    id: 4,
    name: 'ROUTE GROUP 4',
  },
]

const verifyAddress = jest.fn()
const archiveAddress = jest.fn()
const assignZone = jest.fn()
const setPage = jest.fn()
const setSameday = jest.fn()
const initAv = jest.fn()
const setFilter = jest.fn()
const fetchAddressesByRouteGroupId = jest.fn()
const fetchAddresses = jest.fn()
const getSuggestedAddresses = jest.fn()

describe('AddressVerification', () => {

  let wrapper, instance
  beforeEach(() => {
    wrapper = shallowWithIntl(
      <AddressVerification
        verifyAddress={verifyAddress}
        archiveAddress={archiveAddress}
        assignZone={assignZone}
        setPage={setPage}
        setSameday={setSameday}
        setFilter={setFilter}
        fetchAddressesByRouteGroupId={fetchAddressesByRouteGroupId}
        fetchAddresses={fetchAddresses}
        getSuggestedAddresses={getSuggestedAddresses}
        addresses={addresses}
        routeGroups={routeGroups}
        zones={zones}
        initAv={initAv}
        fetchZones={jest.fn()}
        dispatch={jest.fn()}
        updateAddress={jest.fn()}
        getAvStats={jest.fn()}
        getRouteGroups={jest.fn()}
        settings={{domain: {current: 'abc'}}}
        asShipperAddress={true} />
    )
    instance = wrapper.instance()
  })
  it('should render without error', () => {
    expect(wrapper).toBeDefined()
  })
  it('should call callback function for using suggested address', () => {
    instance.setState({
      selectedAddressMap: {
        1: ['test address'],
      },
    })
    instance.handleTableUseSuggestedAddress({
      id: 1,
      waypointId: 23,
    })
    expect(verifyAddress).toBeCalled()
  })
  it('should call callback function for table save', () => {
    instance.handleTableSave('test', 'test')
    expect(verifyAddress).toHaveBeenCalledTimes(2)
  })
  it('should call callback functions for table archive', () => {
    instance.handleTableArchive('test')
    expect(verifyAddress).toHaveBeenCalledTimes(3)
    wrapper.setProps({ asShipperAddress: false })
    instance.handleTableArchive({ orderJaroScoreId: 'test' })
    expect(archiveAddress).toBeCalled()
  })
  it('should call callback function for table assign', () => {
    wrapper.setState({
      updateZoneMap: {
        1: 'test',
      },
    })
    instance.handleTableAssign(1)
    expect(assignZone).toBeCalled()
  })
  it('should call callback function on nav click', () => {
    instance.handleNavClick(1)
    expect(setPage).toBeCalled()
  })
  it('should call callback functions on filter init', () => {
    instance.handleInit({ is_sameday: true })
    expect(setSameday).toBeCalled()
    expect(initAv).toBeCalled()
  })
  it('should call callback function on filter update', () => {
    instance.handleUpdateFilter('test')
    expect(setFilter).toBeCalled()
  })
  it('should call callback function on fetch by region id', () => {
    instance.handleFetchByRgId(1)
    expect(fetchAddressesByRouteGroupId).toBeCalled()
  })
  it('should call callback function on fetch', () => {
    instance.handleFetch('test')
    expect(fetchAddresses).toBeCalled()
  })
  it('should call callback function on table change', () => {
    instance.handleTableChange({ current: 4, pageSize: 100 }, null, null, { currentDataSource: [] })
    expect(getSuggestedAddresses).toBeCalled()
  })
  it('should return prepared table data', () => {
    const tableData = [
      {
        address1: 'ABC',
        address2: 'DEF',
        city : 'test',
        country: 'sg',
        postcode: 12425,
      },
      {
        address1: 'ABC',
        address2: 'DEF',
        city : 'test',
        country: 'sg',
        postcode: 12425,
      },
    ]
    expect(instance.prepareTableData(tableData)).toHaveLength(2)
  })
  it('should update zone map on table zone select', () => {
    wrapper.setState({ updateZoneMap: {
      2: null,
    }})
    instance.handleTableZoneSelect('5, 21')
    expect(Object.keys(instance.state['updateZoneMap'])).toHaveLength(2)
  })
  it('should update selected address map on suggested address select', () => {
    wrapper.setState({ selectedAddressMap: {
      2: 525,
      3: 1255,
    }})
    instance.handleSuggestedAddressSelect('5, 5125')
    expect(Object.keys(instance.state['selectedAddressMap'])).toHaveLength(3)
  })
  it('should update editing address when editing table', () => {
    instance.handleTableEdit('test')
    expect(instance.state['editingAddress']).toBe('test')
  })
  it('should set dialog visibility to false for table edit close', () => {
    instance.handleTableEditClose()
    expect(instance.state['dialogVisible']).toBe(false)
  })
})
