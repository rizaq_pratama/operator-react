import React from 'react'
import { ConfirmManuallyCompleteParcel } from '@app/components/ParcelEdit/ConfirmManuallyCompleteParcel'
import { storiesOf } from '@storybook/react'
import { TestIntlProviderWrapper } from '@internals/testing/test-helpers'
import messages from '@app/translations/en.json'

function formatMessage({id}) {
  return messages[id]
}

storiesOf('ParcelEdit', module).add('ConfirmManuallyCompleteParcel', () => (
  <TestIntlProviderWrapper>
    <ConfirmManuallyCompleteParcel
      visible
      intl={{formatMessage}}
      onCancel={() => alert('go Back is Clicked')} />
  </TestIntlProviderWrapper>
))
