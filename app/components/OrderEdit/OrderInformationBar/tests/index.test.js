import React from 'react'
import toJson from 'enzyme-to-json'
import { shallow } from 'enzyme'
import OrderInformationBar from '../index'

describe('OrderInformationBar', () => {
  it('renders a OrderInformationBar', () => {
    const wrapper = shallow(
      <OrderInformationBar
        trackingNumber={'TEST'}
        stastus={'Pending'} />
    )
    expect(wrapper).toBeDefined()
    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
