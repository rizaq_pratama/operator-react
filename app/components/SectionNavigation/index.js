// @flow
/**
*
* SectionNavigation
*
*/

import React from 'react'
import styled from 'styled-components'
import { T } from '@app/components/Generic/T'

const width = 896

const firstItem = {
  borderRadius: '4px 0 0 4px',
}

const lastItem = {
  borderRadius: '0 4px 4px 0',
}

const SectionHeader = styled.div`
  width: 400px;
  height: 32px;
  color: #a4a7aa;
  text-align: center;
  padding: 4px;
  cursor: pointer;
  border: solid 1px #c1c6cb;
  background-color: #ffffff;
  font-family: Roboto;
  float: left;
`

const SectionHeaderActive = styled(SectionHeader)`
  background-color: #F0F9FF;
  color: #588ACA;
  border-color: #588ACA;
`

const StyledSectionNavigation = styled.div`
  && {
    width: 100%;
    height: 34px;
    margin-bottom: 20px;
    margin-top: 40px;
    text-align: center;
  }
`

type SectionNavigationProps = {
  children: React.Node,
  active: number,
  handleClick: Function,
  navItems: Array
}

class SectionNavigation extends React.Component<SectionNavigationProps> {
  static defaultProps = {
    navItems: [],
    active: 0,
  }

  constructor (props) {
    super(props)
    this.handleNavClick = this.handleNavClick.bind(this)
    this.state = {
      active: props.active,
      childCount: props.navItems.length,
    }
  }

  static getDerivedStateFromProps (nextProps, prevState) {
    if (nextProps.active !== prevState.active) {
      return { active: nextProps.active }
    }
    return null
  }

  handleNavClick (id) {
    this.setState({ active: id })
    this.props.handleClick(id)
  }

  render () {
    const { navItems } = this.props
    const childSize = width / this.state.childCount
    const items = navItems.map((item, idx) => {
      let className
      if (idx === 0) {
        className = firstItem
      } else if (idx === this.state.childCount - 1) {
        className = lastItem
      }
      let element
      if (item.id === this.state.active) {
        element =
          <SectionHeaderActive
            style={{ width: childSize, ...className }} onClick={this.handleNavClick.bind(this, item.id)} key={item.id}>
            <T id={item.displayName} />
          </SectionHeaderActive>
      } else {
        element =
          <SectionHeader style={{ width: childSize, ...className }} onClick={this.handleNavClick.bind(this, item.id)} key={item.id}>
            <T id={item.displayName} />
          </SectionHeader>
      }

      return element
    })

    return (
      <StyledSectionNavigation>
        {items}
      </StyledSectionNavigation>
    )
  }
}

export { SectionNavigation }
