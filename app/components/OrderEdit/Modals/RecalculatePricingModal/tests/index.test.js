import React from 'react'
import toJson from 'enzyme-to-json'
import { shallow, mount } from 'enzyme'
import { TestIntlProviderWrapper } from '@internals/testing/test-helpers'
import { RecalculatePricing } from '../index'
import NC from '@app/services/api/ninjaControlApi'

jest.mock('@app/services/api/ninjaControlApi', () => {
  return {
    recalculateOrderPriceById: jest.fn(() => {
      return { ok: true }
    }),
  }
})

describe('RecalculatePricingModal', () => {
  it('renders a RecalculatePricingModal component', () => {
    const wrapper = shallow(
      <RecalculatePricing />
    )
    expect(wrapper).toBeDefined()
    expect(toJson(wrapper)).toMatchSnapshot()
  })
  it('should call NC api on modal submit', async () => {
    const onSubmit = jest.fn()
    const onClose = jest.fn()
    const wrapper = mount(
      <TestIntlProviderWrapper>
        <RecalculatePricing
          intl={{ formatMessage: jest.fn() }}
          onSubmit={onSubmit}
          onClose={onClose}
          visible />
      </TestIntlProviderWrapper>
    )
    const submitButton = wrapper.find('.ant-modal-footer [type="primary"]')
    submitButton.simulate('click')

    expect(NC.recalculateOrderPriceById).toHaveBeenCalled()
    await new Promise(setTimeout)
    expect(onClose).toHaveBeenCalled()
  })
})
