import React from 'react'
import { shallowWithIntl } from '@internals/testing/test-helpers'

import { SectionNavigation } from '../index'

const testNavItems = [
  {id: 1},
  {id: 2},
  {id: 3},
  {id: 4},
  {id: 5},
]
const handleClick = jest.fn()

describe('SectionNavigation', () => {

  let wrapper, instance
  beforeEach(() => {
    wrapper = shallowWithIntl(
      <SectionNavigation
        handleClick={handleClick}
        navItems={testNavItems}
      />
    )
    instance = wrapper.instance()
  })
  it('Should render without error', () => {
    expect(wrapper).toBeDefined()
  })
  it('should call callback for nav click', () => {
    instance.handleNavClick(1)
    expect(handleClick).toBeCalledWith(1)
  })
})
