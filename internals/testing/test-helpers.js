// @flow
import React from 'react'
import { Provider } from 'react-redux'
import { IntlProvider, intlShape } from 'react-intl';
import { shallow, mount } from 'enzyme';
import { createStore } from 'redux'
import { FormContext } from '@app/components/Generic/Contexts/Form'
import messages from '@app/translations/en.json'

export function TestIntlProviderWrapper(props: {children: React.Node}) {
  return (
    <Provider store={createStore(() => {})}>
      <IntlProvider locale={'en'} key={'en'} messages={messages}>
        {props.children}
      </IntlProvider>
    </Provider>
  )
}

export function TestFormProviderWrapper(props: {children: React.Node}) {
  const mockForm = {
    getFieldDecorator: () => (el) => el,
  }
  return (
    <FormContext.Provider value={mockForm}>
      {props.children}
    </FormContext.Provider>
  )
}

// Create the IntlProvider to retrieve context for wrapping around.
const intlProvider = new IntlProvider({ locale: 'en', messages }, {});
const { intl } = intlProvider.getChildContext();

/**
 * When using React-Intl `injectIntl` on components, props.intl is required.
 */
function nodeWithIntlProp(node) {
  return React.cloneElement(node, { intl });
}

export function shallowWithIntl(node, { context, ...additionalOptions } = {}) {

  if (node.type.name === 'InjectIntl') {
    const unwrappedType = node.type.WrappedComponent;
    node = React.createElement(unwrappedType, node.props);
  }
  return shallow(
    nodeWithIntlProp(node),
    {
      context: Object.assign({}, context, {intl, store: createStore(() => {}, {})}),
      ...additionalOptions,
    }
  );
}

export function mountWithIntl(node, { context, ...additionalOptions } = {}) {
  if (node.type.name === 'InjectIntl') {
    const unwrappedType = node.type.WrappedComponent;
    node = React.createElement(unwrappedType, node.props);
  }
  return mount(
    nodeWithIntlProp(node),
    {
      childContextTypes: { intl: intlShape },
      context: Object.assign({}, context, {intl}),
      ...additionalOptions,
    }
  );
}

export function stringifyEqual(a, b) {
  expect(JSON.stringify(a) === JSON.stringify(b))
}

