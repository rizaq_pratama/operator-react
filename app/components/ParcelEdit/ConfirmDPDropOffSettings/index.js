// @flow
import React, { useState, useEffect } from 'react';
import FormControl from '@app/components/Generic/FormControl';
import SubmittableModal from '@app/components/SubmittableModal';
import { Col, Form, Row, Icon } from 'antd';

type Props = {
  visible: boolean,
  onClose: Function
}

export function ConfirmDPDropOffSettings(props: Props) {
  const [state, setState] = useState({
    menu: [],
    isLoading: false,
  });

  useEffect(() => {
    getDps();
  }, [props.visible]);

  return (
    <SubmittableModal
      visible={props.visible}
      title="container.parcel-edit.edit-dp-management"
      cancelText="commons.cancel"
      okText="commons.save-changes"
      onSubmit={onSubmit}
      onClose={props.onClose}
    >
      <Form layout="vertical">
        {state.isLoading ? (
          <Icon type="loading" />
        ) : (
          <Row style={{ marginTop: '17px' }} type='flex'>
            <Col style={{ flex: '1 1' }}>
              <FormControl
                type="dropdown-label"
                placeholder="Search or select drop off DP ID"
                selectShowSearch
                fieldName="container.parcel-edit.edit-dp-management-dp-id"
                stateKey="dpId"
                menu={state.menu}
                span={24}
              />
            </Col>
            <Col>
              <FormControl
                stateKey="dropoffDate"
                fieldName="container.parcel-edit.edit-dp-management-dropoff-date"
                type="date"
              />
            </Col>
          </Row>
        )}
      </Form>
    </SubmittableModal>
  );

  function onSubmit(data) {
    props.onClose()
    // TODO dp service is not P1 focus
  }

  async function getDps() {
    if (props.visible) {
      setState(Object.assign(state, { isLoading: true }));
      // TODO call service to get dps
      const dps = await new Promise(resolve =>
        setTimeout(
          () =>
            resolve([{ id: 123, name: 'JKB' }, { id: 23, name: 'Greenwich' }])
        )
      );

      state.menu = dps.map(dp => ({ id: dp.id, label: dp.name }));
      state.isLoading = false;
      setState(Object.assign({}, state));
    }
  }
}

export default ConfirmDPDropOffSettings
