import React from 'react'
import { EditPickupDetails } from '@app/components/ParcelEdit/EditPickupDetails'
import { storiesOf } from '@storybook/react'
import { TestIntlProviderWrapper } from '@internals/testing/test-helpers'

const testSenderDetails = {
  senderName: 'Min Thu Ya Hlaing',
  senderContact: '+6585881948',
  senderEmail: 'minthuya@hotmail.com',
  pickupInstructions: 'Code to enter warehouse is 999',
  internalNotes: 'Convey special request to ',
}

const testPickupSchedule = {
  pickupDate: new Date(),
  pickupTimeslots: ['All Day (9 AM - 10 PM)', 'Half Day (9 AM - 4 PM)', 'Half Day (4 PM - 10 PM)'],
  selectedTimeslot: 'All Day (9 AM - 10 PM)',
  requestedByShipper: true,
}

const testPickupAddress = {
  country: 'Singapore',
  city: '-',
  address: 'Blk 62, #11-27 Woodlands Drive 16',
  postalCode: 737895,
  latitude: 1.3308,
  longitude: 103.908,
}

storiesOf('ParcelEdit', module).add('EditPickupDetailsModal', () => (
  <TestIntlProviderWrapper>
    <EditPickupDetails
      visible
      {...Object.assign({}, testSenderDetails, testPickupAddress, testPickupSchedule)}
    />
  </TestIntlProviderWrapper>
))
