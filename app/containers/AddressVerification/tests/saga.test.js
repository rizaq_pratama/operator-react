/**
 * Test  sagas
 */

jest.mock('@app/services/api/addressApi', () => {
  return {
    getAvStats: jest.fn(),
    initAv: jest.fn(),
    fetchAddressByRouteGroup: jest.fn(),
    fetchAddress: jest.fn(),
    searchAddress: jest.fn(),
  }
})

describe('defaultSaga Saga', () => {
  it('Expect to have unit tests specified', () => {
    expect(true).toEqual(true)
  })
})
