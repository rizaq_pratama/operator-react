import api from '@nv/react-commons/src/Migration_corev2/Services'
import { makeAPIProxy } from '../apiProxy'
import Config from 'configs'

const Routes = api.Routes

Routes.BASE_URL = Config.BASE_URL + '/route'

export default makeAPIProxy(Routes)
