//  @flow
import React from 'react'
import { Slider } from 'antd'
import FilterContainer from '../FilterContainer'
import type { FilterComponent } from '../Types'

type FilterRangeBoxProps = {
  id: string,
  label: string,
  property: string,
  startValue: number,
  endValue: number,
  min: number,
  max: number,
}

class FilterRangeBox extends React.Component<FilterRangeBoxProps> implements FilterComponent {
  constructor (props) {
    super (props)
    this.state = {
      startValue: this.props.startValue,
      endValue: this.props.endValue,
    }
  }
  rangeChangeHandler (values) {
    this.setState({
      startValue: values[0],
      endValue: values[1],
    })
  }
  clear() {
    this.setState({
      startValue: this.props.startValue,
      endValue: this.props.endValue,
    })
  }
  serialize(): number[] {
    const {startValue, endValue} = this.state
    return [startValue, endValue]
  }
  deserialize(values: number[]) {
    this.setState({
      startValue: values[0],
      endValue: values[1],
    })
  }
  render () {
    const marks = {
      [this.state.startValue]: {
        label: <strong>{this.state.startValue}</strong>,
      },
      [this.state.endValue]: {
        label: <strong>{this.state.endValue}</strong>,
      },
    }
    return (
      <FilterContainer label={this.props.label} id={this.props.id}>
        <div> {/* for layouting children */}
          <Slider
            marks={marks}
            value={this.serialize()}
            style={{margin: '20px 40px'}}
            min={this.props.min || 0}
            max={this.props.max || 100}
            range={true}
            onChange={this.rangeChangeHandler.bind(this)} />
        </div>
      </FilterContainer>
    )
  }
}

export default FilterRangeBox
