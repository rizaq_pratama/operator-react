/* eslint-disable no-undef */
import React from 'react'
import EventHistory from '@app/components/ParcelEdit/EventHistory'
import { storiesOf } from '@storybook/react'

const events = [{
  label: 'Sort',
  value: 'sort',
}, {
  label: 'Scan',
  value: 'scan',
}, {
  label: 'Pickup',
  value: 'pickup',
}]

const data = [{
  eventId: 1,
  eventTime: '2018-10-11 11:24:45',
  eventTags: 'Sort, Scan',
  eventName: 'Hub Inbound Scan',
  userType: 'Client Credentials',
  userId: 'Go_Dev',
  hubName: 'GW',
  description: 'Inbounded at Hub 3 from Shipper 31438\nWeight updated: 0.69\nLength updated: 17.7\nWidth updated: 16.6\nHeight updated: 13.8\nParcel Size ID: 0\nSet Aside: false',
}, {
  eventId: 2,
  eventTime: '2018-10-10 15:22:12',
  eventTags: 'Scan, Pickup',
  eventName: 'Driver Pickup Scan',
  userType: 'Client Credentials',
  userId: 'Driver Driver',
  routeId: '606002',
  description: 'Driver 1074367 pickup from Shipper 31438 on Route 606002 at waypoint 47915758',
}, {
  eventId: 3,
  eventTime: '2018-10-10 13:51:34',
  eventTags: 'System Action',
  eventName: 'Pricing Change',
  description: 'Script ID: 1910 Version 2\nOld Price:0, New Price: 4.17',
}]

storiesOf('ParcelEdit', module)
  .add('EventHistory', () =>
    <EventHistory data={data} events={events} onRouteIdLinkClicked={routeId => alert('Route [Id:' + routeId + '] clicked')} />
  )
