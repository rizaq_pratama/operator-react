//  @flow
import React, { useState } from 'react'
import { T } from '@app/components/Generic/T'
import { notification, Form, Row } from 'antd'
import { injectIntl, intlShape } from 'react-intl'
import SubmitModal from '@app/components/SubmittableModal'
import FormControl from '@app/components/Generic/FormControl'
import NC from '@app/services/api/ninjaControlApi'

type ModalProps = {
  visible: boolean,
  orderId: number,
  onClose: Function,
  intl: intlShape
}

export function CancelOrder (props: ModalProps) {

  const [isLoading, setLoading] = useState(false)

  return (
    <SubmitModal
      visible={props.visible}
      title='container.order.edit.cancel-order'
      cancelText='commons.go-back'
      okText='container.order.edit.cancel-order'
      onSubmit={onSubmit}
      onClose={props.onClose}
      isConfirmButton={true}
      okType={'danger'}
      disabled={isLoading}>
      <Form>
        <T id={'container.order.edit.cancel-order-text'} />
          <Row style={{ marginTop: '15px', marginBottom: '-25px' }}>
            <FormControl
              type='text'
              stateKey='cancellationReason'
              span={24}
              placeholder={ props.intl.formatMessage({ id: 'container.order.edit.cancel-reason' })}
            />
          </Row>
      </Form>
    </SubmitModal>
  )

  async function onSubmit (data: { cancellationReason: string }) {
    setLoading(true)
    try {
      await NC.cancelOrderById(props.orderId, data.cancellationReason)
      props.onClose()
      notification.success({
        message: props.intl.formatMessage({ id: 'commons.update-success' }),
      })
    } catch (err) {
      //  handle error
    }
    setLoading(false)
  }
}

export default injectIntl(CancelOrder)
