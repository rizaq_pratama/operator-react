import {
  calculateDistance,
  getDefaultLatLngForDomain,
} from '../helpers'


describe('helpers', () => {
  it('correctly calculates distance between two coordinates', () => {

    const distance = calculateDistance(1.284, 103.809, 1.284, 110.00)
    expect(distance).toBeCloseTo(688234.76,1)
  })
  it('returns correct default coordinates for different countries', () => {
    const countryCoordinates = [
      {
        country: 'sg',
        coordinates: { lat: 1.3597, lng: 103.827 },
      },
      {
        country: 'my',
        coordinates: { lat: 3.1333, lng: 101.697 },
      },
      {
        country: 'id',
        coordinates: { lat: -6.2297, lng: 106.689 },
      },
      {
        country: 'vn',
        coordinates: { lat: 10.8046, lng: 106.629 },
      },
      {
        country: 'ph',
        coordinates: { lat: 11.5563, lng: 113.577 },
      },
      {
        country: 'th',
        coordinates: { lat: 13.0003, lng: 96.992 },
      },
    ]
    countryCoordinates.forEach((c) => {
      const returnedCoords = getDefaultLatLngForDomain(c.country)
      expect(returnedCoords.lat).toBeCloseTo(c.coordinates.lat, 3)
      expect(returnedCoords.lng).toBeCloseTo(c.coordinates.lng, 2)
    })
  })
})
