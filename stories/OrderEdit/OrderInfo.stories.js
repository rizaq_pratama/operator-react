import React from 'react'
import OrderInfoCard from '@app/components/OrderEdit/OrderInfoCard'
import OrderInformationBar from '@app/components/OrderEdit/OrderInformationBar'
import TopBarActions from '@app/components/OrderEdit/TopBarActions'
import { storiesOf } from '@storybook/react'

const testParcelInfo = [{
  parcelTrackingId: 'NINJ000238148',
  currentJob: 'Pickup',
  currentJobStatus: 'In-Progress',
  currentGranularStatus: 'En-route to Sorting Hub',
},{
  parcelTrackingId: 'NINJ000239148',
  currentJob: 'Delivery',
  currentJobStatus: 'In-Progress',
  currentGranularStatus: 'En-route to Sorting Hub',
},{
  parcelTrackingId: 'NINJ000240148',
  currentJob: 'Pickup',
  currentJobStatus: 'In-Progress',
  currentGranularStatus: 'En-route to Sorting Hub',
},{
  parcelTrackingId: 'NINJ000242148',
  currentJob: 'Delivery',
  currentJobStatus: 'In-Progress',
  currentGranularStatus: 'En-route to Sorting Hub',
}]

const testOrderInfo = {
  shipperId: 14904,
  shipperName: 'Amazon',
  orderSource: 'API',
  orderCreated: '2018-10-08 23:53 HRS',
  orderBillingStatus: 'Billed',
  shipperReference: '114-0503199-7110637',
  shipperOrderData: 'SHIPPA ODDA DATAAAAAAAAAAAA',
  parcelCount: 4,
  codAmount: 50,
  codCurrency: 'SGD',
}

const testTrackingNumber = 'NJVO1223873487'
const testOrderStatus = 'Pending'

storiesOf('OrderView', module)
  .add('OrderInfoCard', () => (
    <div>
      <OrderInfoCard
        orderInfo={testOrderInfo}
        parcelInfo={testParcelInfo}
      />
    </div>
  ))
  .add('OrderInformationBar', () => (
    <div>
      <OrderInformationBar
        trackingNumber={testTrackingNumber}
        orderStatus={testOrderStatus} />
    </div>
  ))
  .add('TopBarActions', () => (
    <div>
      <TopBarActions
        onClick={e => alert(`Order menu clicked key ${e.key}`)}
      />
    </div>
  ))
