// @flow
import React from 'react'
import { connect } from 'react-redux'

import SubmittableModal from '@app/components/SubmittableModal'

import JobDetailsContent from '../JobDetailsContent'
import { JobDetailModalSelector } from './selectors'

type Props = {
  jobInformation: {
    jobId: number,
    jobType: string,
    priorityLevel: string,
    name: name,
    contact: string,
    email: string,
    address: string,
    endTime: string,
  },
  routeInformation: {
    routeId: number,
    routeDate: string,
    assignedTo: string,
    dpId: string,
    failureReason: string,
    dnr: string,
    link: string,
  },
  visible: boolean,
  onClose: Function,
}

export function JobDetailsModal (props: Props) {
  return (
    <SubmittableModal
      visible={props.visible}
      onSubmit={onSubmit}
      onClose={props.onClose}
    >
      <JobDetailsContent jobInformation={props.jobInformation} routeInformation={props.routeInformation} />
    </SubmittableModal>
  )

  function onSubmit() {
  }
}

const mapStateToProps = state => {
  return JobDetailModalSelector(state)
}

export default connect(mapStateToProps)(JobDetailsModal)
