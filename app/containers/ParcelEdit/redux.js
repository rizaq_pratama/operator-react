import { createActions, createReducer } from 'reduxsauce'
import { fromJS } from 'immutable'
import { PREFIX } from './constants'

export const { Types, Creators } = createActions({
  loadParcelEditView: ['parcelId'],
  setParcel: ['data'],
  setJobs: ['data'],
  setOrder: ['data'],
  setDestinationZone: ['data'],
  setCurrentRoute: ['data'],
  setEvents: ['data'],
  setTickets: ['data'],
  setJobToRouteMapping: ['data'],
  setRouteToDriverMapping: ['data'],
  setJobToProofsMapping: ['data'],
}, {prefix: PREFIX})

export const INITIAL_STATE = fromJS({
  parcel: null,
  jobs: [],
  order: null,
  destinationZone: null,
  currentRoute: null,
  events: [],
  tickets: [],
  jobToRouteMapping: null,
  routeToDriverMapping: null,
  jobToProofsMapping: null,
})

export default createReducer(INITIAL_STATE, {
  [Types.SET_PARCEL]: (state, action) =>
    state.set('parcel', action.data),
  [Types.SET_JOBS]: (state, action) =>
    state.set('jobs', action.data),
  [Types.SET_ORDER]: (state, action) =>
    state.set('order', action.data),
  [Types.SET_DESTINATION_ZONE]: (state, action) =>
    state.set('destinationZone', action.data),
  [Types.SET_CURRENT_ROUTE]: (state, action) =>
    state.set('currentRoute', action.data),
  [Types.SET_EVENTS]: (state, action) =>
    state.set('events', action.data),
  [Types.SET_TICKETS]: (state, action) =>
    state.set('tickets', action.data),
  [Types.SET_JOB_TO_ROUTE_MAPPING]: (state, action) =>
    state.set('jobToRouteMapping', action.data),
  [Types.SET_ROUTE_TO_DRIVER_MAPPING]: (state, action) =>
    state.set('routeToDriverMapping', action.data),
  [Types.SET_JOB_TO_PROOFS_MAPPING]: (state, action) =>
    state.set('jobToProofsMapping', action.data),
})
