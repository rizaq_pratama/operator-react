// @flow
import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSortUp, faSortDown, faSort } from '@fortawesome/pro-solid-svg-icons'
import styled from 'styled-components'

import FilterableHeaderInput from './FilterableHeaderInput'

export const SortTypes = {
  ASC: 'asc',
  DESC: 'desc',
}

const StyledCell = styled.div`
  cursor: pointer;
  user-select: none;
`

const StyledFontAwesomeIcon = styled(FontAwesomeIcon)`
  margin-right: 4px;
`

class SortableHeaderCell extends React.Component<SortableHeaderCellProps> {
  constructor(props) {
    super(props)

    this.onSortChange = this.onSortChange.bind(this)
  }

  onSortChange() {
    this.props.onSortChange(this.props.dataKey)
  }

  onInputClick(e) {
    e.stopPropagation()
  }

  getSortIcon() {
    switch(this.props.sortBy) {
      case SortTypes.ASC:
        return faSortUp
      case SortTypes.DESC:
        return faSortDown
      default:
        return faSort;
    }
  }

  renderFilterableHeaderInput() {
    const { filterable, dataKey, onSearchTextChange } = this.props

    if(filterable) {
      return (
        <div onClick={this.onInputClick}>
          <FilterableHeaderInput
            dataKey={dataKey}
            onSearchTextChange={onSearchTextChange}
          />
        </div>
      )
    }
  }

  render() {
    const { dataKey, label } = this.props

    return (
      <StyledCell className={`cell ${dataKey}`} onClick={this.onSortChange}>
        <StyledFontAwesomeIcon icon={this.getSortIcon()} />
        {label}
        {this.renderFilterableHeaderInput()}
      </StyledCell>
    )
  }
}

type SortableHeaderCellProps = {
  dataKey: string,
  label: string,
  sortBy: string,
  onSortChange: func,
  filterable?: boolean,
  onSearchTextChange?: func,
}

export default SortableHeaderCell