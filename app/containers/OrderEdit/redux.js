import { createActions, createReducer } from 'reduxsauce'
import { fromJS } from 'immutable'
import { PREFIX } from './constants'
import { generateAPIActions } from '@app/utils/helpers'
import _ from 'lodash'

export const { Types, Creators } = createActions(_.assign({},
  generateAPIActions('get', 'OrderInfo', 'orderId'),
  {
    setLoading: ['isLoading'],
    setParcelInfo: ['data'],
  }), { prefix: PREFIX }
)

export const INITIAL_STATE = fromJS({
  orderInfo: {
    id: -1,
    shipperId: 0,
    shipperName: null,
    orderSource: null,
    orderCreated: null,
    orderBillingStatus: null,
    shipperReference: null,
    shipperOrderData: null,
    codAmount: 0,
    codCurrency: 'SGD',
    trackingNumber: 'ORDER NOT FOUND',
    status: 'Pending',
  },
  parcelInfo: [],
  isLoading: false,
})

  export default createReducer(INITIAL_STATE, {
    [Types.GET_ORDER_INFO_SUCCESS]: (state, action) => state.set('orderInfo', action.data),
    [Types.SET_PARCEL_INFO]: (state, action) => state.set('parcelInfo', action.data),
    [Types.SET_LOADING]: (state, action) => state.set('isLoading', action.isLoading),
  })
