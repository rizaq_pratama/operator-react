/* global alert */
import React from 'react'
import { storiesOf } from '@storybook/react'
import TopBarActions from '@app/components/ParcelEdit/TopBarActions'

storiesOf('ParcelEdit', module).add('TopBarActions', () => (
  <TopBarActions
    onOrderInfoMenuClicked={e => alert(`Order info menu clicked key ${e.key}`)}
    onPickupMenuClicked={e => alert(`Pickup menu clicked key ${e.key}`)}
    onDeliveryMenuClicked={e => alert(`Delivery menu clicked key ${e.key}`)}
    onStorageMenuClicked={e => alert(`Storage menu clicked key ${e.key}`)}
    onShipmentMenuClicked={e => alert(`Shipment menu clicked key ${e.key}`)}
    onViewPrintMenuClicked={e => alert(`View/Print menu clicked key ${e.key}`)}
  />
))
