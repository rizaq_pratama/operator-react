import { css } from 'styled-components'

import { media } from './media'
import Metrics from './Metrics'

const ApplicationStyles = {
  container: css`
    width: 100%;
    ${media.min.tablet`
      padding: 30px 120px;
      max-width: ${props => props.maxWidth ? '100%' : `${Metrics.maxWidth}px`};
      margin: 0 auto;
    `}
    ${media.max.tablet`
      padding: 15px 24px;
     `};
  `,

  fontFamily: css`
   font-family: "Roboto", "Helvetica Neue", Helvetica, Arial, sans-serif;
   `,
}

export default ApplicationStyles
