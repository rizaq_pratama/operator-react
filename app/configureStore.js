/**
 * Create the store with dynamic reducers
 */

import { createStore, applyMiddleware } from 'redux'
import { fromJS } from 'immutable'
import { routerMiddleware } from 'react-router-redux'
import { autoRehydrate } from 'redux-persist-immutable'
import { REHYDRATE } from 'redux-persist/lib/constants'

import createSagaMiddleware from 'redux-saga'
import createActionBuffer from 'redux-action-buffer'

import createReducer from './reducers'
import { updateReducers } from './services/rehydrationService'
import { composeWithDevTools } from 'remote-redux-devtools'

const sagaMiddleware = createSagaMiddleware()

export default function configureStore (initialState = {}, history) {
  // Create the store with two middlewares
  // 1. sagaMiddleware: Makes redux-sagas work
  // 2. routerMiddleware: Syncs the location/URL path to the state
  const middlewares = [
    sagaMiddleware,
    routerMiddleware(history),
    createActionBuffer(REHYDRATE),
  ]

  const enhancers = [applyMiddleware(...middlewares), autoRehydrate()]

  // Use redux devtools
  const composeEnhancers = composeWithDevTools({
    hostname: 'localhost', realtime: true,
  })

  const store = createStore(
    createReducer(),
    fromJS(initialState),
    composeEnhancers(...enhancers)
  )
  updateReducers(store)

  // Extensions
  store.runSaga = sagaMiddleware.run
  store.injectedReducers = {} // Reducer registry
  store.injectedSagas = {} // Saga registry

  // Make reducers hot reloadable, see http://mxs.is/googmo
  /* istanbul ignore next */
  if (module.hot) {
    module.hot.accept('./reducers', () => {
      store.replaceReducer(createReducer(store.injectedReducers))
    })
  }

  return store
}
