import React from 'react'
import { mount, shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { ConfirmCancelParcel } from '../index'
import NC from '@app/services/api/ninjaControlApi'

jest.mock('@app/services/api/ninjaControlApi', () => {
  return {
    cancelParcel: jest.fn(),
  }
})

describe('ConfirmCancelOrder', () => {
  let intl, form
  beforeEach(() => {
    intl = {
      formatMessage: () => {},
    }

    form = {
      getFieldDecorator: jest.fn(() => e => e),
      validateFields: jest.fn(),
    }
  })
  it('renders a ConfirmCancelOrder component', () => {
    const wrapper = shallow(
      <ConfirmCancelParcel intl={intl} form={form} visible parcelTrackingNumber='123456'/>
    )
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('requires reason to cancel an order', () => {
    const wrapper = mount(
      <ConfirmCancelParcel intl={intl} form={form} visible parcelTrackingNumber='123456'/>
    )
    wrapper.find('[type="danger"]').simulate('click')
    expect(form.validateFields).toHaveBeenCalled()
    expect(NC.cancelParcel).not.toHaveBeenCalled()
  })

  it('calls API with a reason', () => {
    NC.cancelParcel.mockReturnValue({ok: true})
    const wrapper = mount(
      <ConfirmCancelParcel intl={intl} form={form} visible parcelTrackingNumber='123456' onClose={jest.fn()}/>
    )
    form.validateFields.mockImplementationOnce((cb) => {
      cb(null, {reason: 'some reason'})
    })
    wrapper.find('[type="danger"]').simulate('click')
    expect(form.validateFields).toHaveBeenCalled()
    expect(NC.cancelParcel.mock.calls[0][0]).toBe('123456')
    expect(NC.cancelParcel.mock.calls[0][1]).toBe('some reason')
  })
})
