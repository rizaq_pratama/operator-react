import { createSelector } from 'reselect'
import parcelEditSelectors from '@app/containers/ParcelEdit/selectors'

const { ParcelSelector, OrderSelector } = parcelEditSelectors

export const EditParcelDetailModalSelector = createSelector(
  [ParcelSelector, OrderSelector],
  (parcel, order) => {
    return {
      parcelInformation: {
        orderId: parcel? parcel.orderId: null,
        id: parcel? parcel.id: null,
        parcelTrackingId: parcel? parcel.trackingNumber: null,
        orderSource: order? order.internalReference.source: null,
        createdOn: order? order.createdAt: null,
      },
      parcelFormData: {
        deliveryType: order? order.serviceLevel: null,
        stampId: parcel? parcel.stampId: null,
      },
    }
})

