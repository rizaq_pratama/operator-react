// @flow
import React from 'react'
import { Table } from 'antd'
import { ParcelEditCard } from '../Common/Card'
import { T } from '@app/components/Generic/T'
import style from 'styled-components'
import { Fonts } from '@app/themes'

const StyledCard = style(ParcelEditCard)`
  .ant-card-head {
    ${Fonts.style.bold}
  }
  && .ant-card-body {
    padding: 1em 2em;
  }
`

const StyledTable = style(Table)`
  .ant-table {
    ${Fonts.style.regular}
    white-space: nowrap;
  }
  && .ant-table-thead > tr > th {
    ${Fonts.style.bold}
  }
  && .ant-table-body {
    overflow: scroll;
  }
  && .ant-table-footer {
    text-align: center;
    padding: 0.5em;
  }
`

const A = style.a`
  {
    color: #647EEE;
  }
`

type JobRow = {
  endTime: string,
  jobType: string,
  jobStatus: string,
  jobId: number,
  routeId: number,
  routeDate: string,
  assignedTo: string,
  dpId: number,
  failureReason: string,
  priorityLevel: number,
  podId: number
}

type JobHistoryProps = {
  jobs: JobRow[],
  onJobIdClicked: Function,
  onRouteIdClicked: Function,
  onPodIdClicked: Function,
}

class JobHistory extends React.Component<JobHistoryProps> {
  constructor (props) {
    super(props)
    this.handleOnJobIdClick = this.handleOnJobIdClick.bind(this)
    this.handleOnRouteIdClick = this.handleOnRouteIdClick.bind(this)
    this.handleOnPodIdClick = this.handleOnPodIdClick.bind(this)
  }

  handleOnJobIdClick (e) {
    e.preventDefault()
    this.props.onJobIdClicked(e.target.id)
  }

  handleOnPodIdClick (e) {
    e.preventDefault()
    this.props.onPodIdClicked()
  }

  handleOnRouteIdClick (e) {
    e.preventDefault()
    this.props.onRouteIdClicked(e.target.id)
  }

  render () {
    const columns = [
      { dataIndex: 'endTime', key: 'endTime', title: <T id={'commons.end-time'} /> },
      { dataIndex: 'jobType', key: 'jobType', title: <T id={'commons.parcel-edit.job-type'} /> },
      { dataIndex: 'jobStatus', key: 'jobStatus', title: <T id={'commons.parcel-edit.job-status'} /> },
      { dataIndex: 'jobId',
        key: 'jobId',
        title: <T id={'commons.parcel-edit.job-id'} />,
        render: text => text !== '-' ? <A className='job-id' id={text} onClick={this.handleOnJobIdClick}>{text}</A> : text,
      },
      {
        dataIndex: 'routeId',
        key: 'routeId',
        title: <T id={'commons.model.route-id'} />,
        render: text => text !== '-' ? <A className='route-id' id={text} onClick={this.handleOnRouteIdClick}>{text}</A> : text,
      },
      { dataIndex: 'routeDate', key: 'routeDate', title: <T id={'commons.model.route-date'} /> },
      { dataIndex: 'assignedTo', key: 'assignedTo', title: <T id={'commons.parcel-edit.assigned-to'} /> },
      { dataIndex: 'dpId', key: 'dpId', title: <T id={'commons.distribution-point-id-abbr'} /> },
      { dataIndex: 'failureReason', key: 'failureReason', title: <T id={'container.failed-delivery-management.failure-reason'} /> },
      { dataIndex: 'priorityLevel', key: 'priorityLevel', title: <T id={'commons.model.priority-level'} /> },
      {
        dataIndex: 'podId',
        key: 'podId',
        title: <T id={'commons.model.pod'} />,
        render: id => id ? <A className='pod' onClick={this.handleOnPodIdClick}><T id={'container.parcel-edit.view-pod'} /></A> : '-',
      },
    ]

    return (
      <StyledCard title={'Job History'}>
        <StyledTable
          rowKey='jobId'
          pagination={false}
          footer={() => 'End of table'}
          dataSource={this.props.jobs}
          columns={columns}
        />
      </StyledCard>
    )
  }
}

export default JobHistory
