import invariant from 'invariant'
import _ from 'lodash'
import { REHYDRATE } from 'redux-persist/lib/constants'
import { getStoredState } from 'redux-persist-immutable'
import persistConfig from '@app/configs/reduxPersistConfig'

import checkStore from './checkStore'
import createReducer from '../reducers'

export function injectReducerFactory (store, isValid) {
  return function injectReducer (key, reducer) {
    if (!isValid) checkStore(store)

    invariant(
      _.isString(key) && !_.isEmpty(key) && _.isFunction(reducer),
      '(app/utils...) injectReducer: Expected `reducer` to be a reducer function'
    )

    // Check `store.injectedReducers[key] === reducer` for hot reloading when a key is the same but a reducer is different
    if (
      Reflect.has(store.injectedReducers, key) &&
      store.injectedReducers[key] === reducer
    ) { return }

    store.injectedReducers[key] = reducer // eslint-disable-line no-param-reassign
    store.replaceReducer(createReducer(store.injectedReducers))
    getStoredState(persistConfig).then(storedState => {
      store.dispatch({
        type: REHYDRATE,
        key: persistConfig.key,
        payload: storedState,
      })
    })
  }
}

export default function getInjectors (store) {
  checkStore(store)

  return {
    injectReducer: injectReducerFactory(store, true),
  }
}
