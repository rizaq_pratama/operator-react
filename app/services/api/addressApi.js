import _ from 'lodash'
import iframeClient from '../iframeClient'
import api from '@nv/react-commons/src/Migration_corev2/Services'
import { makeAPIProxy } from '../apiProxy'
import Config from 'configs'

const Addressing = api.Addressing

Addressing.BASE_URL = Config.BASE_URL + '/addressing'

function getAvStats () {
  return iframeClient.request('address_getUnverifiedAddressStats')
}

function fetchAddressByRouteGroup (routeGroupId) {
  return iframeClient.request('address_fetchAddressVerificationByrouteGroup', routeGroupId)
}

function initAv (filter) {
  return iframeClient.request('address_initializeAddressVerification', filter)
}

function fetchAddress (filter) {
  return iframeClient.request('address_fetchAddressVerification', filter)
}

function fetchReverifiedAddress () {
  return iframeClient.request('address_getReverifiedAddresses', null)
}

function searchAddress (query) {
  return iframeClient.request('address_searchAddress', query)
}

function preAvAssign (zoneId, waypointId) {
  const payload = {
    zoneId: zoneId,
    waypointIds: _.castArray(waypointId),
  }
  return iframeClient.request('address_preAvAssign', payload)
}

function archiveOrderJaroScore (ojsId) {
  return iframeClient.request('address_archiveOrderJaroScore', ojsId)
}

function verifyAddress (data) {
  return iframeClient.request('address_verifyJaroScore', _.castArray(data))
}

function verifyEvent (data) {
  return iframeClient.request('address_createAddressEvent', data)
}

function getAllUnverifiedShipperAddress () {
  return iframeClient.request('address_allShipperAddress', { verified: false })
}

function verifyShipperAddress (data) {
  const _data = _.assign(data, { verified: true })
  return iframeClient.request('addreess_updateShipperAddress', _data)
}

export default makeAPIProxy(Addressing, {
  getAvStats,
  initAv,
  fetchAddressByRouteGroup,
  fetchAddress,
  searchAddress,
  preAvAssign,
  archiveOrderJaroScore,
  verifyAddress,
  verifyEvent,
  getAllUnverifiedShipperAddress,
  fetchReverifiedAddress,
  verifyShipperAddress,
})
