import { createSelector } from 'reselect'
import parcelEditSelectors from '@app/containers/ParcelEdit/selectors'

const { ParcelSelector, OrderSelector } = parcelEditSelectors

export const EditParcelDetailModalSelector = createSelector(
  [ParcelSelector, OrderSelector],
  (parcel, order) => {
    return {
      parcelInformation: {
        parcelTrackingId: parcel? parcel.trackingNumber: null,
        orderSource: order? order.internalReference.source: null,
        createdOn: order? order.createdAt: null,
      },
      parcelFormData: {},
    }
  })

export const PricingAndDimensionModalSelector = createSelector([],
  (parcelEdit) => {

    return {
      deliveryFee: 1.2,
      codFee: 2.3,
      insuranceFee: 1,
      handlingFee: 3,
      gst: 0.3,
      insuredValue: 123,
      parcelSize: 'L',
      weight: 54,
      dimension: {
        width: 12,
        height: 12,
        length: 12,
      },
      currency: 'SG',
    }
  })
