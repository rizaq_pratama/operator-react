import iframeClient from './iframeClient'

function getUserSettings () {
  return iframeClient.request('settings_getUserSetting')
}

function getDomainSettings () {
  return iframeClient.request('settings_getDomain')
}

export default {
  getUserSettings,
  getDomainSettings,
}
