// @flow
import React from 'react'
import styled from 'styled-components'
import EventHistoryBar from './EventHistoryBar'
import { Table } from 'antd'
import { Fonts } from '@app/themes'
import { T } from '@app/components/Generic/T'
import { ParcelEditCard } from '@app/components/ParcelEdit/Common/Card'

const StyledEventHistoryCard = styled(ParcelEditCard)`
  .ant-row-flex{
    width: 100%;
  }
  .ant-card-head {
    ${Fonts.style.bold}
  }
  && .ant-card-body {
    padding: 1em 2em;
  }
`

const StyledTable = styled(Table)`
  height: auto;
  width: auto;
  .ant-table-column-sorter{
    display: none;
  }
  .ant-table {
    ${Fonts.style.regular}
    white-space: nowrap;
  }
  && .ant-table-thead > tr > th {
    ${Fonts.style.bold}
  }
  .ant-description-column {
    white-space: pre;
  }
  && .ant-table-body {
    overflow: scroll;
  }
`



type EventHistoryProps = {
  data: any[],
  events: any[],
  onRouteIdLinkClicked: Function
}

class EventHistory extends React.Component<EventHistoryProps > {
  static defaultProps = {
    data: [],
    events: [],
  }

  constructor (props) {
    super(props)
    this.state = { eventTimeSortOrder: 'ascend', searchFilter: null }
    this.handleRouteIdLinkClicked = this.handleRouteIdLinkClicked.bind(this)
  }

  eventTimeSorting = (a, b) => {
    return new Date(a.eventTime).getTime() - new Date(b.eventTime).getTime()
  }

  handleRouteIdLinkClicked (e) {
    e.preventDefault()
    this.props.onRouteIdLinkClicked(e.target.id)
  }

  render () {
    const { eventTimeSortOrder, searchFilter, eventTagFilter } = this.state

    const columns = [
      {
        title: 'Event Time',
        dataIndex: 'eventTime',
        key: 'eventTime',
        widthdefaultSortOrder: eventTimeSortOrder,
        sortOrder: eventTimeSortOrder,
        sorter: (a, b) => this.eventTimeSorting(a, b),
      },
      {
        title: 'Event Tags',
        dataIndex: 'eventTags',
        key: 'eventTags',
        filteredValue: eventTagFilter == null || eventTagFilter === 'AllEvents' ? null : ([eventTagFilter] || null),
        onFilter: (value, record) => record.eventTags && record.eventTags.toLowerCase().includes(value.toLowerCase()),
      },
      { title: 'Event Name', dataIndex: 'eventName', key: 'eventName' },
      { title: 'User Type', dataIndex: 'userType', key: 'userType', render: text => text || '-' },
      { title: 'User ID', dataIndex: 'userId', key: 'userId', render: text => text || '-' },
      { title: 'Route ID', dataIndex: 'routeId', key: 'routeId', render: text => text ? <a href='javascript:;' onClick={this.handleRouteIdLinkClicked} id={text}>{text}</a> : '-' },
      { title: 'Hub Name', dataIndex: 'hubName', key: 'hubName', render: text => text || '-' },
      {
        title: 'Description',
        dataIndex: 'description',
        key: 'description',
        className: 'ant-description-column',
        filteredValue: searchFilter == null ? null : ([searchFilter] || null),
        onFilter: (value, record) =>
          (record.description && record.description.includes(value)) ||
          (record.hubName && record.hubName.includes(value)) ||
          (record.routeId && record.routeId.includes(value)) ||
          (record.userId && record.userId.includes(value)) ||
          (record.userType && record.userType.includes(value)) ||
          (record.eventName && record.eventName.includes(value)) ||
          (record.eventTags && record.eventTags.includes(value)) ||
          (record.eventTime && record.eventTime.includes(value)),
      },
    ]

    const { data, events } = this.props

    return (
      <StyledEventHistoryCard
        title={
          <EventHistoryBar
            events={events}
            onRadioButtonSortChange={(sortValue) => this.setState({ eventTimeSortOrder: sortValue })}
            onSearchInputChange={(inputValue) => this.setState({ searchFilter: inputValue })}
            onEventFilterChange={(eventTagFilter) => this.setState({ eventTagFilter: eventTagFilter })}
          />
        }
      >
        <StyledTable
          columns={columns}
          dataSource={data}
          rowKey='eventId'
          pagination={false}
          footer={() => <div style={{ 'textAlign': 'center' }}><T id='directive.table.end-of-table' /></div>}
          scroll={{ x: '100%' }}
        />
      </StyledEventHistoryCard>
    )
  }
}

export default EventHistory
