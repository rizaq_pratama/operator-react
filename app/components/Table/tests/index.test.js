import React from 'react'
import _ from 'lodash'
import { shallowWithIntl } from '@internals/testing/test-helpers'

import Table from '../index'

const onSelectableChange = jest.fn()

describe('Table', () => {
  const columns = [{
    key: 'name',
    label: 'Name',
    width: 300,
  }, {
    key: 'age',
    label: 'Age',
    width: 80,
  }]

  const list = [{
    id: 10,
    name: 'Name 1',
    age: 30,
  }, {
    id: 11,
    name: 'Name 2',
    age: 20,
  }, {
    id: 12,
    name: 'Name 3',
    age: 10,
  }]

  let wrapper, instance
  beforeEach(() => {
    wrapper = shallowWithIntl(
      <Table
        rowId="id"
        rowHeight={80}
        selectable={true}
        onSelectableChange={onSelectableChange}
        columns={columns}
        list={list}
      />
    )
    instance = wrapper.instance()
  })

  it('should render', () => {
    expect(wrapper).toBeDefined()
  })

  describe('getRowClassName', () => {
    it('every row should have "table-row" class name', () => {
      expect(instance.getRowClassName({ index: 0 }).indexOf('table-row') >= 0).toBeTruthy()
      expect(instance.getRowClassName({ index: 1 }).indexOf('table-row') >= 0).toBeTruthy()
    })

    it('every row should have "odd" / "even" class name', () => {
      expect(instance.getRowClassName({ index: 0 }).indexOf('odd') >= 0).toBeTruthy()
      expect(instance.getRowClassName({ index: 1 }).indexOf('even') >= 0).toBeTruthy()
    })

    it('should have "last-row" class name for last row', () => {
      expect(instance.getRowClassName({ index: _.size(instance.state.processedList) - 1 }).indexOf('last-row') >= 0).toBeTruthy()
    })

    it('should have "selected" class name for selected row', () => {
      const firstRow = _.head(list)
      instance.onRowSelect(firstRow.id, true)
      expect(instance.getRowClassName({ index: 0 }).indexOf('selected') >= 0).toBeTruthy()

      instance.onRowSelect(firstRow.id, false)
      expect(instance.getRowClassName({ index: 0 }).indexOf('selected') >= 0).toBeFalsy()
    })
  })

  describe('isRowSelected', () => {
    const firstRow = _.head(list)

    it('return false if not selected', () => {
      expect(instance.isRowSelected(firstRow.id)).toBeFalsy()
    })

    it('return true if selected (default check by this.state.selectedIds)', () => {
      instance.onRowSelect(firstRow.id, true)
      expect(instance.isRowSelected(firstRow.id)).toBeTruthy()
    })

    it('return true if selected (by passing in selectedIds)', () => {
      expect(instance.isRowSelected(firstRow.id, [firstRow.id])).toBeTruthy()
    })
  })

  describe('onRowSelect', () => {
    const firstRow = _.head(list)

    it('should call callback on row selected/deselect', () => {
      instance.onRowSelect(firstRow.id, true)
      expect(onSelectableChange).toBeCalled()
    })

    it('should update selectedIds list if select/deselect the row', () => {
      instance.onRowSelect(firstRow.id, true)
      expect(instance.state.selectedIds).toEqual([firstRow.id])

      instance.onRowSelect(firstRow.id, false)
      expect(instance.state.selectedIds).toEqual([])
    })
  })
})