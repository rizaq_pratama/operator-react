import { ApiHelper } from '@nv/react-commons/src/Services'
import baseSaga from '@app/containers/Base/saga'
import injectSaga from '@app/utils/injectSaga'

export default [
  injectSaga({ key: 'global', saga: ApiHelper.saga }),
  injectSaga({ key: 'settings', saga: baseSaga }),
]
