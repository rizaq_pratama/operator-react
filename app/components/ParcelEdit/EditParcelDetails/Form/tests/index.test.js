import React from 'react'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'
import EditParcelDetailsContent from '../index'
import { TestIntlProviderWrapper } from '@internals/testing/test-helpers'
import { FormContext } from '@app/components/Generic/Contexts/Form'

const testParcelInfo = {
  parcelTrackingId: 'NINJ000238148',
  orderSource: 'API',
  createdOn: '2018-10-09 15:23:12',
}

const testDeliveryTypes = [
  'DELIVERY_THREE_DAYS_ANYTIME',
  'DELIVERY_FIVE_DAYS_ANYTIME',
  'DELIVERY_SIX_DAYS_ANYTIME',
]

const testParcelFormData = {
  stampTrackingId: 'NVP-123425632',
  currentPriorityLevel: 80,
  deliveryType: testDeliveryTypes[0],
}

describe('EditParcelDetailsContent', () => {
  let form = null
  beforeEach(() => {
    form = {
      getFieldDecorator: () => jest.fn(() => null),
    }
  })
  it('matches snapshot', () => {
    const component = renderer.create(
      <TestIntlProviderWrapper>
        <FormContext.Provider value={form}>
          <EditParcelDetailsContent parcelInformation={testParcelInfo} deliveryTypes={testDeliveryTypes} parcelFormData={testParcelFormData} />
        </FormContext.Provider>
      </TestIntlProviderWrapper>
    )
    expect(component.toJSON()).toMatchSnapshot()
  })

  it('renders a EditParcelDetailsContent', () => {
    const renderedComponent = shallow(
      <EditParcelDetailsContent parcelInformation={testParcelInfo} deliveryTypes={testDeliveryTypes} parcelFormData={testParcelFormData} />
    )
    expect(renderedComponent).toBeDefined()
  })
})
