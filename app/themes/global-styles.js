import { createGlobalStyle } from 'styled-components'
import { Fonts, Colors } from '@app/themes'
import './global.css'

export default createGlobalStyle`
body {
  font-size: 18px;
  line-height: 1.33;
  color: black;
}

.bold-medium {
  ${Fonts.style.medium}
}

.bold {
  ${Fonts.style.bold}
}
.bold-light {
  ${Fonts.style.light}
}

.color-nvPriRed { color: ${Colors.nvPriRed}; }
.color-nvPriBlue { color: ${Colors.nvPriBlue}; }
.color-nvNeutral900 { color: ${Colors.nvNeutral900}; }
.color-nvNeutral800 { color: ${Colors.nvNeutral800}; }
.color-nvNeutral700 { color: ${Colors.nvNeutral700}; }
.color-nvNeutral600 { color: ${Colors.nvNeutral600}; }
.color-nvNeutral500 { color: ${Colors.nvNeutral500}; }
.color-nvNeutral400 { color: ${Colors.nvNeutral400}; }
.color-nvNeutral300 { color: ${Colors.nvNeutral300}; }
.color-nvNeutral200 { color: ${Colors.nvNeutral200}; }
.color-nvNeutral100 { color: ${Colors.nvNeutral100}; }
.color-nvNeutral050 { color: ${Colors.nvNeutral050}; }
.color-nvBrand1 { color: ${Colors.nvBrand1}; }
.color-nvBrand2 { color: ${Colors.nvBrand2}; }
.color-nvBrand3 { color: ${Colors.nvBrand3}; }
.color-nvBrand4 { color: ${Colors.nvBrand4}; }
.color-nvBrand5 { color: ${Colors.nvBrand5}; }
.color-nvBrand6 { color: ${Colors.nvBrand6}; }
.color-nvBrand7 { color: ${Colors.nvBrand7}; }
.color-nvBrand8 { color: ${Colors.nvBrand8}; }
.color-nvBrandSec1 { color: ${Colors.nvBrandSec1}; }
.color-nvBrandSec2 { color: ${Colors.nvBrandSec2}; }
.color-nvBrandSec3 { color: ${Colors.nvBrandSec3}; }
.color-nvBrandSec4 { color: ${Colors.nvBrandSec4}; }
.color-nvBrandSec5 { color: ${Colors.nvBrandSec5}; }
.color-nvBrandSec6 { color: ${Colors.nvBrandSec6}; }
.color-nvBrandSec7 { color: ${Colors.nvBrandSec7}; }
.color-nvBrandSec8 { color: ${Colors.nvBrandSec8}; }
.color-nvBlue1 { color: ${Colors.nvBlue1}; }
.color-nvBlue2 { color: ${Colors.nvBlue2}; }
.color-nvBlue3 { color: ${Colors.nvBlue3}; }
.color-nvBlue4 { color: ${Colors.nvBlue4}; }
.color-nvBlue5 { color: ${Colors.nvBlue5}; }
.color-nvGreen1 { color: ${Colors.nvGreen1}; }
.color-nvGreen2 { color: ${Colors.nvGreen2}; }
.color-nvGreen3 { color: ${Colors.nvGreen3}; }
.color-nvGreen4 { color: ${Colors.nvGreen4}; }
.color-nvGreen5 { color: ${Colors.nvGreen5}; }
.color-nvRed1 { color: ${Colors.nvRed1}; }
.color-nvRed2 { color: ${Colors.nvRed2}; }
.color-nvRed3 { color: ${Colors.nvRed3}; }
.color-nvRed4 { color: ${Colors.nvRed4}; }
.color-nvRed5 { color: ${Colors.nvRed5}; }
.color-nvYellow1 { color: ${Colors.nvYellow1}; }
.color-nvYellow2 { color: ${Colors.nvYellow2}; }
.color-nvYellow3 { color: ${Colors.nvYellow3}; }
.color-nvYellow4 { color: ${Colors.nvYellow4}; }
.color-nvYellow5 { color: ${Colors.nvYellow5}; }
.color-nvFieldName { color: ${Colors.nvFieldName}; }
.color-nvLinkBlue { color: ${Colors.nvLinkBlue}; }
.color-nvCheckGreen { color: ${Colors.nvCheckGreen}; }
.color-nvCopyGreen { color: ${Colors.nvCopyGreen}; }
.color-nvBackgroundGrey { color: ${Colors.nvBackgroundGrey}; }
`



