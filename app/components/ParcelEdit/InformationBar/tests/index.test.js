import React from 'react'
import { shallow } from 'enzyme'
import InformationBar from '../index'

const testCurrency = 'SGD'
const testTrackingNumber = 'NVSG0000124'
const testCod = 59
const testPriorityLevel = 39
const testStatus = 'PENDING PICKUP'
const isRts = true

describe('InformationBar', () => {
  it('renders a InformationBar component', () => {
    const renderedComponent = shallow(
      <InformationBar
        currency={testCurrency}
        trackingNumber={testTrackingNumber}
        cod={testCod}
        priorityLevel={testPriorityLevel}
        status={testStatus}
        isRts={isRts} />
    )
    expect(renderedComponent).toBeDefined()
  })
})
